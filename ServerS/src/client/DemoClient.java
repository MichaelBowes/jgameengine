package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import server.transferdata.SystemMessage;

public class DemoClient extends Client {
	
	private static Client client;
	
	public static void main(String[] args) {
		client = new DemoClient();
		//Get login data
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter username:");
		String username = getString(br);
		System.out.println("Enter password:");
		String password = getString(br);
		client.connect(username, password, "127.0.0.1", 89, client);	
	}

	@Override
	public void processData(Object object) {
		if (object instanceof SystemMessage) {
			SystemMessage message = (SystemMessage) object;
			System.out.println(message.getMessage());
		}
	}

	@Override
	public void loginErrorResult(SystemMessage message) {
		System.out.println(message.getMessage());	
	}

	@Override
	public void onGameServerLogin() {
		System.out.println("Successfully logged in!");	
		client.disconnect();
	}

	@Override
	public void reportError(SystemMessage message, Throwable cause) {
		System.out.println(message.getMessage());	
	}
	
	private static String getString(BufferedReader br) {
		String result = null;
		try {
			result = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void onInvalidLoginToken(SystemMessage message) {
		System.out.println(message.getMessage());	
	}

	@Override
	public void onLoninServerLogin() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnect() {
		// TODO Auto-generated method stub
		
	}
}
