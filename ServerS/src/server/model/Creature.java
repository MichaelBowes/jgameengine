package server.model;

import java.io.Serializable;

import server.util.Vector2f;

public class Creature implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 197010784888691359L;
	private long id;
	private String name;
	private int maxHP;
	private Vector2f position;
	
	public Creature(long id) {
		this.id = id;
	}
	
	public int getMaxHP() {
		return maxHP;
	}
	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}

	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
	}
}
