package server.model;

import java.io.Serializable;

import server.util.Vector2f;

public class Building implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4504111243692920769L;
	private long id;
	private long buildingID;
	private int durability;
	private Vector2f position;
	
	public Building(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	public long getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(long buildingID) {
		this.buildingID = buildingID;
	}
	public int getDurability() {
		return durability;
	}
	public void setDurability(int durability) {
		this.durability = durability;
	}
	public Vector2f getPosition() {
		return position;
	}
	public void setPosition(Vector2f position) {
		this.position = position;
	}

}
