package server.model;

import java.io.Serializable;
import java.util.List;

import javolution.util.FastTable;

public class Character implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7403635214948171135L;
	private long characterID;
	private String name;
	private int maxHP;
	private int hp;
	private int hunger;
	private int thirst;
	private int stamina;
	private int bodyTemperature;
	private int intelligence;
	private int perception;
	private int strength;
	private int dexterity;
	private int charisma;
	private int psyche;
	private int agility;
	private int constitution;
	private float positionX;
	private float positionY;
	private int direction;
	private List<InventoryItem> inventory = new FastTable<InventoryItem>();
	private List<Effect> effects;
	
	public Character(long id) {
		this.characterID = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMaxHP() {
		return maxHP;
	}
	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		this.hunger = hunger;
	}
	public int getThirst() {
		return thirst;
	}
	public void setThirst(int thirst) {
		this.thirst = thirst;
	}
	public int getStamina() {
		return stamina;
	}
	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	public int getBodyTemperature() {
		return bodyTemperature;
	}
	public void setBodyTemperature(int bodyTemperature) {
		this.bodyTemperature = bodyTemperature;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}
	public int getPerception() {
		return perception;
	}
	public void setPerception(int perception) {
		this.perception = perception;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getDexterity() {
		return dexterity;
	}
	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}
	public int getCharisma() {
		return charisma;
	}
	public void setCharisma(int charisma) {
		this.charisma = charisma;
	}
	public int getPsyche() {
		return psyche;
	}
	public void setPsyche(int psyche) {
		this.psyche = psyche;
	}
	public int getAgility() {
		return agility;
	}
	public void setAgility(int agility) {
		this.agility = agility;
	}
	public int getConstitution() {
		return constitution;
	}
	public void setConstitution(int constitution) {
		this.constitution = constitution;
	}
	public float getPositionX() {
		return positionX;
	}
	public void setPositionX(float positionX) {
		this.positionX = positionX;
	}
	public float getPositionY() {
		return positionY;
	}
	public void setPositionY(float positionY) {
		this.positionY = positionY;
	}
	public List<InventoryItem> getInventory() {
		return inventory;
	}
	public void setInventory(List<InventoryItem> inventory) {
		this.inventory = inventory;
	}
	public long getCharacterID() {
		return characterID;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public List<Effect> getEffects() {
		return effects;
	}

	public void setEffects(List<Effect> effects) {
		this.effects = effects;
	}
	
	public void addEffect(Effect effect) {
		this.effects.add(effect);
	}
	
	public void removeEffect(Effect effect) {
		this.effects.remove(effect);
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

}
