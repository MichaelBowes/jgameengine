package server.model;

import java.io.Serializable;

public abstract class Item implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7618276468755397466L;
	private long itemID;
	private int durability;
	private int state;


	public Item(long id) {
		this.itemID = id;
	}
	
	public long getItemID() {
		return itemID;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
