package server.model;

import java.io.Serializable;
import java.util.List;

import server.util.Vector2f;

public class MapData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7426254289435460596L;
	private List<Structure> structures;
	private List<Building> buildings;
	private Vector2f centerPosition;
	private List<Claim> claims;
	private List<ItemInstance> itemInstances;
	private List<Character> characters;
	
	public MapData(Vector2f position) {
		this.centerPosition = position;
	}
	public List<Structure> getStructures() {
		return structures;
	}
	public void setStructures(List<Structure> structures) {
		this.structures = structures;
	}
	public List<Building> getBuildings() {
		return buildings;
	}
	public void setBuildings(List<Building> buildings) {
		this.buildings = buildings;
	}
	public Vector2f getCenterPosition() {
		return centerPosition;
	}
	public void setCenterPosition(Vector2f centerPosition) {
		this.centerPosition = centerPosition;
	}
	public List<Claim> getClaims() {
		return claims;
	}
	public void setClaims(List<Claim> claims) {
		this.claims = claims;
	}
	public List<Character> getCharacters() {
		return characters;
	}
	public void setCharacters(List<Character> characters) {
		this.characters = characters;
	}
	public List<ItemInstance> getItemInstances() {
		return itemInstances;
	}
	public void setItemInstances(List<ItemInstance> itemInstances) {
		this.itemInstances = itemInstances;
	}	
}
