package server.logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javolution.util.FastTable;
import server.database.commands.Read;
import server.exceptions.DatabaseException;
import server.exceptions.InitialisationException;
import server.model.Building;
import server.model.ItemInstance;
import server.model.MapData;
import server.model.Structure;
import server.util.Vector2f;

public class WorldInformation {

	private Connection connection;
	private Map<Vector2f, Structure> structures = new ConcurrentHashMap<Vector2f, Structure>();
	private Map<Vector2f, ItemInstance> itemInstances = new ConcurrentHashMap<Vector2f, ItemInstance>();
	private Map<Vector2f, Building> buildings = new ConcurrentHashMap<Vector2f, Building>();
	
	public WorldInformation(Connection connection) {
		this.connection = connection;
		loadWorldInformation();
	}
	
	public MapData getMapInformation(Vector2f position, int xRange, int yRange) {
		int startPointX = (int) position.x - xRange / 2;
		int startPointY = (int) position.y - yRange / 2;
		Vector2f currentPosition = new Vector2f(position.x - xRange / 2, position.y - yRange / 2);
		MapData mapData = new MapData(position);
		List<Building> buildings = new FastTable<Building>();
		List<ItemInstance> itemInstances = new FastTable<ItemInstance>();
		List<Structure> structures = new FastTable<Structure>();
		
		//Read all of the map information.
		for(int x = (int)startPointX; x <= position.x + xRange / 2; x++) {
			for(int y = (int)startPointY; y <= position.y + yRange / 2; y++) {
				currentPosition.x = x;
				currentPosition.y = y;
				Building building = this.buildings.get(currentPosition);
				if(building != null) {
					buildings.add(building);
				}
				ItemInstance itemInstance = this.itemInstances.get(currentPosition);
				if(itemInstance != null) {
					itemInstances.add(itemInstance);
				}
				Structure structure = this.structures.get(currentPosition);
				if(structure != null) {
					structures.add(structure);
				}
			}
		}
		
		mapData.setBuildings(buildings);
		mapData.setStructures(structures);
		mapData.setItemInstances(itemInstances);
		return mapData;
	}
	
	private void loadWorldInformation() {
		try {
			if(connection.isClosed()) {
				throw new InitialisationException("Could not load world information because database connection was closed");
			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not access world information", e);
		}
		List<Building> buildings = Read.getAllBuildings(connection);
		List<ItemInstance> itemInstances = Read.getAllItemInstances(connection);
		List<Structure> structures = Read.getAllStructures(connection);
		
		for(Building building : buildings) {
			this.buildings.put(building.getPosition(), building);
		}
		
		for(ItemInstance item : itemInstances) {
			this.itemInstances.put(item.getPosition(), item);
		}
		
		for(Structure structure : structures) {
			this.structures.put(structure.getPosition(), structure);
		}
	}
}
