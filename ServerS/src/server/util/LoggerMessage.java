package server.util;

public class LoggerMessage {

	public static final int SERVERMESSAGE = 0;
	public static final int TRANSACTION = 1;
	public static final int SHUTDOWN = 2;
	
	private String message;
	private int type;
	
	public LoggerMessage(String message, int type) {
		if(type != 2) {
			if(message == null) {
				throw new IllegalArgumentException("Log message can't be null!");
			}
		}
		this.message = message;
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
