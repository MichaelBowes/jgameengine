package server.util;

import java.io.Serializable;

import org.joda.time.DateTime;

public class LoginToken implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2862404985392287375L;
	private DateTime date;
	private String hash;
	private String serverIp;
	private String username;
	private int serverPort;
	
	public LoginToken(String hash, String username, DateTime date, String serverIp, int port) {
		this.hash = hash;
		this.date = date;
		this.serverIp = serverIp;
		this.serverPort = port;
		this.username = username;
	}
	
	public String getHash() {
		return hash;
	}

	public DateTime getDate() {
		return date;
	}
	
	public String getServerIp() {
		return serverIp;
	}

	public int getServerPort() {
		return serverPort;
	}

	public String getUsername() {
		return username;
	}

}
