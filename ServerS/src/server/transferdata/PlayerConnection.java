package server.transferdata;

import java.io.Serializable;

import server.model.Player;

public class PlayerConnection implements Serializable {

	private static final long serialVersionUID = -482846905175968359L;
	public static final int LOGIN = 0;
	public static final int LOGOUT = 1;
	private Player player;
	private int action;
	
	public PlayerConnection(Player player, int action) {
		this.action = action;
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	public int getAction() {
		return action;
	}
}
