package server.transferdata;

import java.io.Serializable;

public class SystemMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 348227188553484216L;
	public static final int INFO = 0;
	public static final int WARNING = 1;
	public static final int KICK = 2;
	public static final int BAN = 3;
	public static final int SUCCESS = 4;
	public static final int FAIL = 5;
	public static final int CONNECTING = 6;	
	public static final int DISCONNECT = 7;	
	
	private String message;
	private int type;
	private String from;
	
	public SystemMessage(String message, int type) {
		this.message = message;
		this.setType(type);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
