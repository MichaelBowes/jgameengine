package server.database;

import java.sql.Connection;

import org.mindrot.jbcrypt.BCrypt;

import server.database.commands.Insert;
import server.database.commands.Read;
import server.model.Player;
import server.util.IDGen;

public class DatabaseController {

	public static void banPlayer(Connection connection, long playerID, int duration, String reason, String from) {
		String actualReason = reason;
		if(reason == null) {
			actualReason = "Unknown";
		}
		String fromPerson = from;
		if(from == null) {
			fromPerson = "System";
		}
		Insert.addBan(connection, fromPerson, playerID, actualReason, duration);
	}
	
	public static void banPlayer(Connection connection, String username, int duration, String reason, String from) {
		String actualReason = reason;
		if(reason == null) {
			actualReason = "Unknown";
		}
		String fromPerson = from;
		if(from == null) {
			fromPerson = "System";
		}
		Player player = Read.getPlayer(connection, username);
		if(player != null) {
			Insert.addBan(connection, fromPerson, player.getGetPlayerID(), actualReason, duration);			
		}
	}
	
	/**
	 * Creates an player account for the given username and password.
	 * @param connection
	 * @param username
	 * @param password
	 * @return
	 */
	public static boolean createAccount(Connection connection, String username, String password) {
		long id = IDGen.getID();
		String salt = BCrypt.gensalt();
		String encryptedPassword = BCrypt.hashpw(password, salt);
		if (Insert.addPlayer(connection, id, username, encryptedPassword, salt, 0)) {
			return true;
		}
		return false;
	}
	
}
