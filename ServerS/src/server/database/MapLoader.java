package server.database;

import java.sql.Connection;

import server.database.commands.Read;
import server.model.Claim;
import server.model.MapData;
import server.util.Vector2f;

public class MapLoader {

	
	public static MapData loadMapData(Connection connection, Vector2f position, int width, int height) {
		MapData map = new MapData(position);
		float x1 = position.x - width/2;
		float x2 = position.x + width/2;
		float y1 = position.y - height/2;
		float y2 = position.y + height/2;
		map.setBuildings(Read.getBuildings(connection, x1, x2, y1, y2));
		map.setStructures(Read.getStructures(connection, x1, x2, y1, y2));
		map.setClaims(Read.getClaims(connection,
				position.x - Claim.CLAIM_MAX_SIZE,
				position.x + Claim.CLAIM_MAX_SIZE,
				position.y - Claim.CLAIM_MAX_SIZE,
				position.y + Claim.CLAIM_MAX_SIZE));
		
		return map;
	}
}
