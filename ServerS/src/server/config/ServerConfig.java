package server.config;

public class ServerConfig {

	private int port;
	private int timeout;
	private int maxPlayers;
	private String gameServerIp;
	private int gameServerPort;
	
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	public String getGameServerIp() {
		return gameServerIp;
	}
	public void setGameServerIp(String gameServerIp) {
		this.gameServerIp = gameServerIp;
	}
	public int getGameServerPort() {
		return gameServerPort;
	}
	public void setGameServerPort(int gameServerPort) {
		this.gameServerPort = gameServerPort;
	}
	
}
