package server.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import server.exceptions.InitialisationException;

public class ConfigReader {

	public static ServerConfig readConfig(String path) {
		FileReader reader;
		try {
			reader = new FileReader(path);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}

		List<String> lines = new LinkedList<>();

		// Read the lines
		try (BufferedReader br = new BufferedReader(reader)) {
			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				if (!currentLine.startsWith("//")) {
					lines.add(currentLine);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ServerConfig config = new ServerConfig();

		for (String line : lines) {
			String[] words = line.split("=");
			if (words.length > 2) {
				throw new InitialisationException("Invalid config format.");
			}
			switch (words[0].toLowerCase()) {
			case "port":
				int port = Integer.parseInt(words[1]);
				config.setPort(port);
				break;
				
			case "timeout":
				int timeout = Integer.parseInt(words[1]);
				config.setTimeout(timeout);
				break;
				
			case "maxplayer":
				int maxplayers = Integer.parseInt(words[1]);
				config.setMaxPlayers(maxplayers);
				break;
				
			case "gameserverip":
				config.setGameServerIp(words[1]);
				break;
					
			case "gameserverport":
				int gameport = Integer.parseInt(words[1]);
				config.setGameServerPort(gameport);
				break;
			
			}		
}

return config;}}
