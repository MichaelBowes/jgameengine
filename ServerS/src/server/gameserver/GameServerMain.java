package server.gameserver;

import java.util.List;
import java.util.Scanner;

import server.config.ConfigReader;
import server.config.ServerConfig;

public class GameServerMain {

	private static GameServer server;

	public static void main(String[] args) {
		ServerConfig config = ConfigReader.readConfig("res/GameServerConfig.cfg");
		server = new GameServer(config);
		// Start server
		server.start();
		Scanner in = new Scanner(System.in);
		boolean exit = false;
		while (!exit) {
			String message = in.nextLine();
			switch (message) {
			case "exit":
				exit = true;
				break;
				
			case "clients":
				listClients(server.getClientList());
				break;
			}
		}
		in.close();
		// Shut down server
		server.shutdown();
	}
	
	static void listClients(List<String> clients) {
		for(String client : clients) {
			System.out.println(client);
		}
	}
}
