package server.loginserver;

import java.util.List;
import java.util.Scanner;

import server.Server;
import server.config.ConfigReader;
import server.config.ServerConfig;

public class LoginServerMain {
	
	private static LoginServer server;
	
	public static void main(String[] args) {
		ServerConfig config = ConfigReader.readConfig("res/LoginServerConfig.cfg");
		server = new LoginServer(config);
		//Start server
		server.start();
		Scanner in = new Scanner(System.in);
		boolean exit = false;
		while(!exit) {
			String message = in.nextLine();
			switch (message) {
			case "exit":
				exit = true;
				break;
				
			case "clients":
				listClients(server.getClientList());
				break;
			}
		}
		in.close();
		//Shut down server
		server.shutdown();
	}
	
	static void listClients(List<String> clients) {
		for(String client : clients) {
			System.out.println(client);
		}
	}
}
