package server.loginserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.mindrot.jbcrypt.BCrypt;

import server.ClientHandler;
import server.Server;
import server.database.commands.Read;
import server.model.Player;
import server.transferdata.SystemMessage;
import server.util.Logger;
import server.util.LoginData;
import server.util.LoginToken;

/**
 * Handles the connection and authentication of a client.
 *
 */
public class LoginHandler extends ClientHandler {
	private Socket socket;
	private Connection connection;
	private boolean connected = false;
	private Logger logger;
	private ObjectOutputStream output;
	private ObjectInputStream inputStream;

	public LoginHandler(Socket socket, Connection connection, Logger logger) {
		this.socket = socket;
		this.connection = connection;
		this.logger = logger;
	}

	@Override
	public void run() {		
		// Receive login
		Object input;
		try {
			//Create input streams
			inputStream = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());
			connected = true;
				//Send/receive information
				input = inputStream.readObject();
				if (input instanceof LoginData) {
					//Verify data and send information if correct
					LoginData data = (LoginData) input;
					login(data.getUsername(), data.getPassword());
				}
		} catch(SocketTimeoutException e) {
			logger.logClientServerMessage(socket.getInetAddress(), "timed out");
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
		}
		// Close the connection
		try {
			disconnect();
		} catch (IOException e) {
			e.printStackTrace();
			logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
		}
	}

	private boolean sendToken(String username) {
		LoginToken token = generateToken(socket.getInetAddress(), new DateTime(), username);
		try {
			sendObject(token);
			output.flush();
			logger.logClientServerMessage(socket.getInetAddress(), " logged in successfully");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Verifies the username and password and sends a token back to the client
	 * if successful.
	 * @param username of the client.
	 * @param password of the client.
	 */
	private void login(String username, String password) {
		Player player = Read.getPlayer(connection, username);
		if (player == null) {
			// Notify user that no account with the username exists
			try {
				sendObject(new SystemMessage("No account with that username was found", SystemMessage.INFO));
			} catch (IOException e) {
				e.printStackTrace();
				logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
			}
			return;
		}
		// Check if password was right
		if (BCrypt.hashpw(password, player.getSalt()).equals(player.getPassword())) {
			// Check if user was banned
			SystemMessage message = Read.getBan(connection, player.getGetPlayerID());
			if (message != null) {// Notify user about ban.
				try {
					output.writeObject(message);
				} catch (IOException e) {
					e.printStackTrace();
					logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
				}
				return;
			}
			sendToken(username);
			logger.logClientTransaction(socket.getInetAddress(), "Token sent");
		} else {
			try {
				sendObject(new SystemMessage("Invalid password", SystemMessage.INFO));
			} catch (IOException e) {
				e.printStackTrace();
				logger.logClientServerMessage(socket.getInetAddress(), e.getMessage());
			}
		}
	}

	@Override
	public void disconnect() throws IOException, SQLException {
		if(socket != null) {
			if(!socket.isClosed()) {
				socket.close();
				connection.close();
			}	
		}
		super.disconnect();
		logger.logClientTransaction(socket.getInetAddress(), "disconnected");
	}

	@Override
	public Socket getSocket() {
		return socket;
	}

	@Override
	public boolean sendObject(Object object) throws IOException {
		if (connected) {
			output.writeObject(object);
			return true;
		}
		return false;
	}
	
	private static LoginToken generateToken(InetAddress ip, DateTime date, String username) {		
		String serverIp = Server.gameServerIp;
		int serverPort = Server.gameServerPort;
		/*
		 * Token consists of:
		 * login date
		 * username
		 * own ip
		 * server ip
		 * 
		 * + SALT:
		 * key
		 */
		String hashString = date.toString()
				+ username
				+ ip.getHostAddress()
				+ serverIp;
		String hash = BCrypt.hashpw(hashString, LoginServer.LOGIN_KEY);
		return new LoginToken(hash, username, date, serverIp, serverPort);
	}
}
