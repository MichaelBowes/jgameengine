package server.exceptions;

public class InitialisationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2455292329245670108L;
	
	public InitialisationException() {
		super();
	}
	
	public InitialisationException(String message) {
		super(message);
	}
	
	public InitialisationException(String message, Throwable cause) {
		super(message, cause);
	}
}
