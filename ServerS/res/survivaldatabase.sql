-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 24. Sep 2018 um 03:10
-- Server-Version: 5.6.34-log
-- PHP-Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `survivaldatabase`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ban`
--

CREATE TABLE `ban` (
  `Reason` varchar(50) COLLATE utf8_bin NOT NULL,
  `Duration` int(11) DEFAULT NULL,
  `PlayerID` int(11) NOT NULL,
  `receivedFrom` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `building`
--

CREATE TABLE `building` (
  `BuildingID` int(11) NOT NULL,
  `BuildingName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buildinginstance`
--

CREATE TABLE `buildinginstance` (
  `ID` int(11) NOT NULL,
  `BuildingID` int(11) NOT NULL,
  `Durability` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `charactercombatstats`
--

CREATE TABLE `charactercombatstats` (
  `CharacterID` int(11) NOT NULL,
  `SkillID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `charactereffect`
--

CREATE TABLE `charactereffect` (
  `CharacterID` int(11) NOT NULL,
  `EffectID` int(11) NOT NULL,
  `EffectLevel` int(11) NOT NULL,
  `Duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `characterskillstat`
--

CREATE TABLE `characterskillstat` (
  `CharacterID` int(11) NOT NULL,
  `SkillID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `claim`
--

CREATE TABLE `claim` (
  `ClaimID` int(11) NOT NULL,
  `Duration` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `combatstats`
--

CREATE TABLE `combatstats` (
  `SkillID` int(11) NOT NULL,
  `SkillName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `community`
--

CREATE TABLE `community` (
  `Type` varchar(50) COLLATE utf8_bin NOT NULL,
  `CommunityID` int(11) NOT NULL,
  `Authority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `creature`
--

CREATE TABLE `creature` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `MaxHp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `creaturedrop`
--

CREATE TABLE `creaturedrop` (
  `CreatureID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `DropChance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `creatureinstance`
--

CREATE TABLE `creatureinstance` (
  `CreatureID` int(11) NOT NULL,
  `CurrentHp` int(11) NOT NULL,
  `StatusEffect` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cummunitymember`
--

CREATE TABLE `cummunitymember` (
  `CommunityID` int(11) NOT NULL,
  `CharacterID` int(11) NOT NULL,
  `Rank` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `effect`
--

CREATE TABLE `effect` (
  `EffectID` int(11) NOT NULL,
  `EffectName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gamecharacter`
--

CREATE TABLE `gamecharacter` (
  `CharacterID` int(11) NOT NULL,
  `CharacterName` varchar(50) COLLATE utf8_bin NOT NULL,
  `MaxHp` int(11) NOT NULL,
  `Hp` int(11) NOT NULL,
  `Hunger` int(11) NOT NULL,
  `Thirst` int(11) NOT NULL,
  `Stamina` int(11) NOT NULL,
  `BodyTemperature` int(11) NOT NULL,
  `Intelligence` int(11) NOT NULL,
  `Perception` int(11) NOT NULL,
  `Strength` int(11) NOT NULL,
  `Dexterity` int(11) NOT NULL,
  `Charisma` int(11) NOT NULL,
  `Psyche` int(11) NOT NULL,
  `Agility` int(11) NOT NULL,
  `Constitution` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL,
  `Direction` int(11) NOT NULL,
  `PlayerID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `inventoryitem`
--

CREATE TABLE `inventoryitem` (
  `SlotID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `Durability` int(11) DEFAULT NULL,
  `CharacterID` int(11) NOT NULL,
  `State` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `item`
--

CREATE TABLE `item` (
  `ItemID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `item`
--

INSERT INTO `item` (`ItemID`) VALUES
(44);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `iteminstance`
--

CREATE TABLE `iteminstance` (
  `ItemID` int(11) NOT NULL,
  `Durability` int(11) NOT NULL,
  `Duration` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL,
  `State` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `login`
--

CREATE TABLE `login` (
  `LoginKey` varchar(100) NOT NULL,
  `Nr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `login`
--

INSERT INTO `login` (`LoginKey`, `Nr`) VALUES
('$2a$10$Lz5BprK91LKq4xd8qrmwze', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `player`
--

CREATE TABLE `player` (
  `PlayerID` int(11) NOT NULL,
  `PlayerName` varchar(50) COLLATE utf8_bin NOT NULL,
  `Type` int(11) NOT NULL,
  `Salt` varchar(50) COLLATE utf8_bin NOT NULL,
  `Password` varchar(80) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `player`
--

INSERT INTO `player` (`PlayerID`, `PlayerName`, `Type`, `Salt`, `Password`) VALUES
(123, 'Peter', 0, '$2a$10$ZmmwCjB.Ok1yQqBfUJsdfO', '$2a$10$ZmmwCjB.Ok1yQqBfUJsdfOSqV.dqqB3EuEw4.bqHIVDvR0qrwZSXO');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `skillstats`
--

CREATE TABLE `skillstats` (
  `SkillID` int(11) NOT NULL,
  `SkillName` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `structure`
--

CREATE TABLE `structure` (
  `StructureID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `structureinstance`
--

CREATE TABLE `structureinstance` (
  `ID` int(11) NOT NULL,
  `StructureID` int(11) NOT NULL,
  `Durability` int(11) NOT NULL,
  `PositionX` float NOT NULL,
  `PositionY` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `ban`
--
ALTER TABLE `ban`
  ADD KEY `FK_247` (`PlayerID`);

--
-- Indizes für die Tabelle `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`BuildingID`);

--
-- Indizes für die Tabelle `buildinginstance`
--
ALTER TABLE `buildinginstance`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `BuildingID` (`BuildingID`);

--
-- Indizes für die Tabelle `charactercombatstats`
--
ALTER TABLE `charactercombatstats`
  ADD KEY `FK_235` (`CharacterID`),
  ADD KEY `FK_239` (`SkillID`);

--
-- Indizes für die Tabelle `charactereffect`
--
ALTER TABLE `charactereffect`
  ADD KEY `FK_150` (`CharacterID`),
  ADD KEY `FK_160` (`EffectID`);

--
-- Indizes für die Tabelle `characterskillstat`
--
ALTER TABLE `characterskillstat`
  ADD KEY `FK_216` (`CharacterID`),
  ADD KEY `FK_220` (`SkillID`);

--
-- Indizes für die Tabelle `claim`
--
ALTER TABLE `claim`
  ADD PRIMARY KEY (`ClaimID`);

--
-- Indizes für die Tabelle `combatstats`
--
ALTER TABLE `combatstats`
  ADD PRIMARY KEY (`SkillID`);

--
-- Indizes für die Tabelle `community`
--
ALTER TABLE `community`
  ADD PRIMARY KEY (`CommunityID`);

--
-- Indizes für die Tabelle `creature`
--
ALTER TABLE `creature`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indizes für die Tabelle `creaturedrop`
--
ALTER TABLE `creaturedrop`
  ADD KEY `CreatureID` (`CreatureID`),
  ADD KEY `ItemID` (`ItemID`);

--
-- Indizes für die Tabelle `creatureinstance`
--
ALTER TABLE `creatureinstance`
  ADD KEY `CreatureID` (`CreatureID`);

--
-- Indizes für die Tabelle `cummunitymember`
--
ALTER TABLE `cummunitymember`
  ADD KEY `FK_265` (`CommunityID`),
  ADD KEY `FK_269` (`CharacterID`);

--
-- Indizes für die Tabelle `effect`
--
ALTER TABLE `effect`
  ADD PRIMARY KEY (`EffectID`);

--
-- Indizes für die Tabelle `gamecharacter`
--
ALTER TABLE `gamecharacter`
  ADD PRIMARY KEY (`CharacterID`),
  ADD KEY `PlayerID` (`PlayerID`);

--
-- Indizes für die Tabelle `inventoryitem`
--
ALTER TABLE `inventoryitem`
  ADD PRIMARY KEY (`ItemID`,`CharacterID`,`SlotID`),
  ADD KEY `FK_224` (`CharacterID`);

--
-- Indizes für die Tabelle `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`ItemID`);

--
-- Indizes für die Tabelle `iteminstance`
--
ALTER TABLE `iteminstance`
  ADD KEY `ItemID` (`ItemID`);

--
-- Indizes für die Tabelle `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `Nr` (`Nr`);

--
-- Indizes für die Tabelle `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`PlayerID`),
  ADD UNIQUE KEY `PlayerName` (`PlayerName`);

--
-- Indizes für die Tabelle `skillstats`
--
ALTER TABLE `skillstats`
  ADD PRIMARY KEY (`SkillID`);

--
-- Indizes für die Tabelle `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`StructureID`),
  ADD UNIQUE KEY `StructureID` (`StructureID`);

--
-- Indizes für die Tabelle `structureinstance`
--
ALTER TABLE `structureinstance`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `StructureID` (`StructureID`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ban`
--
ALTER TABLE `ban`
  ADD CONSTRAINT `FK_247` FOREIGN KEY (`PlayerID`) REFERENCES `player` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `buildinginstance`
--
ALTER TABLE `buildinginstance`
  ADD CONSTRAINT `buildinginstance_ibfk_1` FOREIGN KEY (`BuildingID`) REFERENCES `building` (`BuildingID`);

--
-- Constraints der Tabelle `charactercombatstats`
--
ALTER TABLE `charactercombatstats`
  ADD CONSTRAINT `FK_235` FOREIGN KEY (`CharacterID`) REFERENCES `gamecharacter` (`CharacterID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_239` FOREIGN KEY (`SkillID`) REFERENCES `combatstats` (`SkillID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `charactereffect`
--
ALTER TABLE `charactereffect`
  ADD CONSTRAINT `FK_150` FOREIGN KEY (`CharacterID`) REFERENCES `gamecharacter` (`CharacterID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_160` FOREIGN KEY (`EffectID`) REFERENCES `effect` (`EffectID`) ON UPDATE CASCADE;

--
-- Constraints der Tabelle `characterskillstat`
--
ALTER TABLE `characterskillstat`
  ADD CONSTRAINT `FK_216` FOREIGN KEY (`CharacterID`) REFERENCES `gamecharacter` (`CharacterID`),
  ADD CONSTRAINT `FK_220` FOREIGN KEY (`SkillID`) REFERENCES `skillstats` (`SkillID`);

--
-- Constraints der Tabelle `creaturedrop`
--
ALTER TABLE `creaturedrop`
  ADD CONSTRAINT `creaturedrop_ibfk_1` FOREIGN KEY (`CreatureID`) REFERENCES `creature` (`ID`),
  ADD CONSTRAINT `creaturedrop_ibfk_2` FOREIGN KEY (`ItemID`) REFERENCES `item` (`ItemID`);

--
-- Constraints der Tabelle `creatureinstance`
--
ALTER TABLE `creatureinstance`
  ADD CONSTRAINT `creatureinstance_ibfk_1` FOREIGN KEY (`CreatureID`) REFERENCES `creature` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `cummunitymember`
--
ALTER TABLE `cummunitymember`
  ADD CONSTRAINT `FK_265` FOREIGN KEY (`CommunityID`) REFERENCES `community` (`CommunityID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_269` FOREIGN KEY (`CharacterID`) REFERENCES `gamecharacter` (`CharacterID`) ON UPDATE CASCADE;

--
-- Constraints der Tabelle `gamecharacter`
--
ALTER TABLE `gamecharacter`
  ADD CONSTRAINT `gamecharacter_ibfk_1` FOREIGN KEY (`PlayerID`) REFERENCES `player` (`PlayerID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `inventoryitem`
--
ALTER TABLE `inventoryitem`
  ADD CONSTRAINT `FK_138` FOREIGN KEY (`ItemID`) REFERENCES `item` (`ItemID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_224` FOREIGN KEY (`CharacterID`) REFERENCES `gamecharacter` (`CharacterID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `iteminstance`
--
ALTER TABLE `iteminstance`
  ADD CONSTRAINT `iteminstance_ibfk_1` FOREIGN KEY (`ItemID`) REFERENCES `item` (`ItemID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `structureinstance`
--
ALTER TABLE `structureinstance`
  ADD CONSTRAINT `structureinstance_ibfk_1` FOREIGN KEY (`StructureID`) REFERENCES `structure` (`StructureID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
