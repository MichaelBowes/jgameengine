package fontreader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

public class FontCreator {
	
	public List<Integer> createFont(String path, int charWidth, int charHeight) throws IOException {
		
		List<Integer> effectiveWidths = new LinkedList<>();
		
		BufferedImage image = ImageIO.read(new FileInputStream(path));
		
		int columns = image.getWidth()/charWidth;
		int rows = image.getHeight()/charHeight;
		
		int yPosition = 0;
		for(int r = 0; r < rows; r++) {
			int xPosition = 0;
			for(int c = 0; c < columns; c++) {
				
				effectiveWidths.add(getEffectiveCharacterWidth(image.getSubimage(xPosition, yPosition, charWidth, charHeight)));
				xPosition += charWidth; 
			}
			yPosition += charHeight;
		}
		
		return effectiveWidths;
	}
	
	
	private int getEffectiveCharacterWidth(BufferedImage subImage) {
		int emptyRightLines = 0;
		
		int effectiveWidth = 0;
		for(int x = subImage.getWidth() - 1; x >= 0; x--) {
			boolean foundPixel = false;
			for(int y = 0; y < subImage.getHeight(); y++) {
				//Check if the column contains a pixel other than alpha.
				Color color = new Color(subImage.getRGB(x, y), true);
				if(color.getAlpha() != 0) { //If a pixel is not transparent
					foundPixel = true;
				}
			}
			if(foundPixel) {
				break;
			}else {
				emptyRightLines++;
			}
		}
		//Subtract the empty lines.
		effectiveWidth = subImage.getWidth() - emptyRightLines;	
		return effectiveWidth;
	}
	
	/**
	 * Creates a file containing the name of the font, the character width, height and a list
	 * of the individual character widths. Form:<br><br>
	 * name width height<br>
	 * x,x,x,x,x,x,x,x,x,x,...<br><br>
	 * @param fileName - Name of the font file.
	 * @param effectiveWidths - list of individual widths for the characters.
	 * @param charWidth - universal width per character.
	 * @param charHeight - universal height per character.
	 * @throws IOException
	 */
	public void writeInfoFile(String fileName, List<Integer> effectiveWidths, int charWidth, int charHeight)
			throws IOException {
		
		String path = "output/";
		String resultFileName = fileName + "Info"; 
		FileWriter fw = new FileWriter(path + resultFileName);		
		try(BufferedWriter bw = new BufferedWriter(fw)){
			bw.write(fileName + charWidth + " " + charHeight);
			bw.newLine();
			for(int i : effectiveWidths) {
				String s = i + ",";
				bw.write(s);
			}
		};
		
	}
	
	
}
