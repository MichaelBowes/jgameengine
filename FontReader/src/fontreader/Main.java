package fontreader;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		FontCreator creator = new FontCreator();

		System.out.println("Creating file for '" + args[0] + "'.");
		System.out.println("width '" + args[1] + "'.");
		System.out.println("height '" + args[2] + "'.");
		List<Integer> result;
		if(args.length != 3) {
			throw new IllegalArgumentException("Invalid argument. The argument has to be in the form 'path characterwidth characterheight'.");
		}
		int charWidth = Integer.parseInt(args[1]);
		int charHeight = Integer.parseInt(args[2]);
		if(charWidth == 0 || charHeight == 0) {
			throw new IllegalArgumentException("Invalid argument. The character width or hight have to be numbers greater than 0.");
		}
		try {
			result = creator.createFont(args[0], charWidth, charHeight);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Could not read the given file.");
		}
		if(result != null) {
			File file = new File(args[0]);			
			String fileNameWithOutExt = file.getName().replaceFirst("[.][^.]+$", "");
			try {
				creator.writeInfoFile(fileNameWithOutExt, result, charWidth, charHeight);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Could not write file. Reason: " + e.getStackTrace());
			}
		}
	}
}
