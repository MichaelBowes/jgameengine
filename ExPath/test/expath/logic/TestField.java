package expath.logic;

public class TestField implements PathField{

	private boolean isBlocked;
	
	public void setBlocked(boolean value) {
		isBlocked = value;
	}
	
	@Override
	public boolean isBlocked() {
		return isBlocked;
	}

}
