package expath.logic;

import javolution.util.function.Equality;

public class PathComparator implements Equality<Path> {

	@Override
	public int hashCodeOf(Path object) {
		return object.getPath().hashCode();
	}

	@Override
	public boolean areEqual(Path left, Path right) {
		if(left == right) {
			return true;
		}
		if(left.getClass() != right.getClass()) {
			return false;
		}
		if(left.getPath().equals(right.getPath())) {
			return true;
		}
		return false;
	}

	@Override
	public int compare(Path left, Path right) {
		if(left.getDistance() > right.getDistance()) {
			return 1;
		}else if(left.getDistance() == right.getDistance()) {
			return 0;
		}else {
			return -1;
		}				
	}

}
