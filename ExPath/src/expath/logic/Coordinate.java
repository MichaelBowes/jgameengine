package expath.logic;

public class Coordinate {
	public int x;
	public int y;
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		
		if(o.getClass() != this.getClass())
			return false;
		
		Coordinate coord = (Coordinate) o;
		if(this.x == coord.x && this.y == coord.y)
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		return x + "," + y;
	}
}
