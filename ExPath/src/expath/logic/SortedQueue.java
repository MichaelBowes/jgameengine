package expath.logic;

import java.util.Collection;
import java.util.List;

import javolution.util.FastSortedTable;

/**
 * Queue that is sorted every time a new element is added.
 */
public class SortedQueue {

	private List<Path> pathList = new FastSortedTable<Path>(new PathComparator());
	
	
	public Path poll() {
		if(pathList.isEmpty()) {
			return null;
		}
		return pathList.remove(0);
	}
	
	public Path peek() {
		if(pathList.isEmpty()) {
			return null;
		}
		return pathList.get(0);
	}
	
	public void add(Path path) {
		pathList.add(path);
	}
	
	public void addAll(Collection<Path> paths) {
		pathList.addAll(paths);
	}
}
