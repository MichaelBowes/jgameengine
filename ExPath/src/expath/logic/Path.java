package expath.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *	Represents a list of fields that form a path.
 */
public class Path {

	private List<Coordinate> fields;
	private double distance;
	
	public Path(Coordinate...coordinates) {
		fields = new ArrayList<Coordinate>(Arrays.asList(coordinates));
	}
	
	public Coordinate getEndField() {
		return fields.get(fields.size()-1);
	}

	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public List<Coordinate> getPath(){
		return fields;
	}		
	
	public void addAll(Collection<Coordinate> list) {
		fields.addAll(list);
	}
	
	public void add(Coordinate coordinate) {
		fields.add(coordinate);
	}
	
	@Override
	public String toString() {
		String result = "";
		for(Coordinate c : fields) {
			result += c.toString() + " - ";
		}
		result += distance;
		return result;
	}
}
