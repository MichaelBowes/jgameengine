package expath.logic;

/**
 * Has to be implemented by Map fields to allow the checking of blocked fields 
 * while finding a path.
 */
public interface PathField {
	/**
	 * Checks if the field is passable.
	 * @return True if the path can be generated through this field.<br>
	 * False if the field is not passable.
	 */
	public boolean isBlocked();
}
