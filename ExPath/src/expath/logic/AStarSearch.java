package expath.logic;

import java.util.LinkedList;
import java.util.List;
import javolution.util.FastMap;
import javolution.util.FastTable;

public class AStarSearch {

	private Heuristic heuristic;
	private PathType type;
	private PathField[][] map;
	private int fieldNumber;
	private List<Coordinate> visited;
	
	/**
	 * Storage for the calculated heuristics.
	 */
	private FastMap<PathField, Double> heuristics = new FastMap<PathField, Double>();
	private SortedQueue queue = new SortedQueue();
		
	public AStarSearch(PathField[][] map, Heuristic heuristic, PathType type) {
		if(map == null) {
			throw new IllegalArgumentException("Map can't be null!");
		}
		this.heuristic = heuristic;
		this.type = type;
		this.map = map;
		fieldNumber = map.length * map[0].length;
	}
	
	public void setMap(PathField[][] map) {
		if(map == null) {
			throw new IllegalArgumentException("Map can't be null!");
		}
		this.map = map;
		fieldNumber = map.length * map[0].length;
	}
	
	/**
	 * Generates a path consisting of an {@link Coordinate} array
	 * for the given start and goal.
	 * @param start of the path.
	 * @param goal of the path.
	 * @return Array of {@link Coordinate}s if a path was found.
	 * Otherwise returns null.
	 */
	public Coordinate[] generatePath(Coordinate start, Coordinate goal) {
		visited = new FastTable<Coordinate>();
		heuristics.clear();
		//Add start point to queue.
		Path path = new Path(start);
		queue.add(path);
		visited.add(start);
		
		//Search for path.
		while(true) {
			if(visited.size() == fieldNumber || queue.peek() == null) {
				return null;
			}
			if(queue.peek().getEndField().equals(goal)) { //Found path.
				List<Coordinate> resultList = queue.poll().getPath();
				Coordinate[] result = new Coordinate[resultList.size()];
				result = resultList.toArray(result);
				return result;
			}
			
			Path currentPath = queue.poll();
			List<Coordinate> neighbors = getNeighbors(currentPath.getEndField()); 
			queue.addAll(createPaths(goal, currentPath, neighbors));		
		}		
	}
	
	private List<Path> createPaths(Coordinate goal, Path current ,List<Coordinate> fields){
		List<Path> pathList = new LinkedList<Path>();
		for(Coordinate field : fields) {
			Path path = new Path();
			path.addAll(current.getPath());
			path.add(field);
			path.setDistance(path.getPath().size() + generateHeuristics(field , goal));
			pathList.add(path);
		}
		return pathList;
	}
	
	/**
	 * Returns a list of all neighboring elements that have not yet been visited.
	 * @param coordinate for which the neighbors are to be returned.
	 * @return
	 */
	protected List<Coordinate> getNeighbors(Coordinate coordinate) {
		List<Coordinate> neighbors = new LinkedList<Coordinate>();
		boolean leftBlocked = false;
		boolean rightBlocked = false;
		boolean topBlocked = false;
		boolean bottomBlocked = false;
		
		//Right neighbor
		if(coordinate.x + 1 < map.length) {
			PathField field = map[coordinate.x + 1][coordinate.y];
			Coordinate coord = new Coordinate(coordinate.x + 1, coordinate.y);
			if(!field.isBlocked() && !visited.contains(coord)) {
				neighbors.add(coord);
				visited.add(coord);
			}else {
				rightBlocked = true;
			}
		}
			
		//left neighbor
		if(coordinate.x - 1 >= 0) {
			PathField field = map[coordinate.x - 1][coordinate.y];
			Coordinate coord = new Coordinate(coordinate.x - 1, coordinate.y);
			if(!field.isBlocked() && !visited.contains(coord)) {
				neighbors.add(coord);
				visited.add(coord);
			}else {
				leftBlocked = true;
			}
		}		
		//Bottom neighbor
		if(coordinate.y - 1 >= 0) {
			PathField field = map[coordinate.x][coordinate.y - 1];
			Coordinate coord = new Coordinate(coordinate.x, coordinate.y - 1);
			if(!field.isBlocked() && !visited.contains(coord)) {
				neighbors.add(coord);	
				visited.add(coord);
			}else {
				bottomBlocked = true;
			}
		}
		//Top neighbor
		if(coordinate.y + 1 < map[0].length) {
			PathField field = map[coordinate.x][coordinate.y + 1];
			Coordinate coord = new Coordinate(coordinate.x, coordinate.y + 1);
			if(!field.isBlocked() && !visited.contains(coord)) {
				neighbors.add(coord);	
				visited.add(coord);
			}else {
				topBlocked = true;
			}
		}
		
		if(type == PathType.FourWay) {
			return neighbors;
		}
		
		if(coordinate.y - 1 >= 0) {
			//Bottom-right neighbor
			if(coordinate.x + 1 < map.length) {
				PathField field = map[coordinate.x + 1][coordinate.y - 1];		
				Coordinate coord = new Coordinate(coordinate.x + 1, coordinate.y - 1);
				if(!field.isBlocked() && !visited.contains(coord) && !rightBlocked && !bottomBlocked) {
					neighbors.add(coord);	
					visited.add(coord);
				}
			}
			//Bottom-left neighbor
			if(coordinate.y - 1 >= 0) {
				if(coordinate.x -1 >= 0) {
					PathField field = map[coordinate.x - 1][coordinate.y - 1];	
					Coordinate coord = new Coordinate(coordinate.x - 1, coordinate.y - 1);
					if(!field.isBlocked() && !visited.contains(coord) && !leftBlocked && !bottomBlocked) {
						neighbors.add(coord);	
						visited.add(coord);
					}
				}				
			}
		}
		
		if(coordinate.y + 1 < map[0].length) {
			//Top-right neighbor
			if(coordinate.x + 1 < map.length) {
				PathField field = map[coordinate.x + 1][coordinate.y + 1];	
				Coordinate coord = new Coordinate(coordinate.x + 1, coordinate.y + 1);
				if(!field.isBlocked() && !visited.contains(coord) && !rightBlocked && !topBlocked) {
					neighbors.add(coord);	
					visited.add(coord);
				}
			}
			//Top-left neighbor
			if(coordinate.x - 1 >= 0) {
				PathField field = map[coordinate.x - 1][coordinate.y + 1];		
				Coordinate coord = new Coordinate(coordinate.x - 1, coordinate.y + 1);
				if(!field.isBlocked() && !visited.contains(coord) && !leftBlocked && !topBlocked) {
					neighbors.add(coord);
					visited.add(coord);
				}
			}
		}
		return neighbors;
	}
	
	private double generateHeuristics(Coordinate start, Coordinate goal) {
		if(heuristic == Heuristic.Diagonal) {
			return generateDiagonalDistance(start, goal);
		}
		if(heuristic == Heuristic.Euclidean) {
			return generateEuclideanDistance(start, goal);
		}
		if(heuristic == Heuristic.Manhattan) {
			return generateManhattanDistance(start, goal);
		}
		return 0;
	}
	
	private double generateDiagonalDistance(Coordinate start, Coordinate goal) {
		if(heuristics.containsKey(map[start.x][start.y])) {
			return heuristics.get(map[start.x][start.y]);
		}else {
			double distance = 20 * Math.max(Math.abs(start.x - goal.x), Math.abs(start.y - goal.y));
			heuristics.put(map[start.x][start.y], distance);
			return distance;
		}	
	}
	
	private double generateManhattanDistance(Coordinate start, Coordinate goal) {
		if(heuristics.containsKey(map[start.x][start.y])) {
			return heuristics.get(map[start.x][start.y]);
		}else {
			double distance = Math.abs(start.x - goal.x) + Math.abs(start.y - goal.y);
			heuristics.put(map[start.x][start.y], distance);
			return distance;
		}
	}
	
	private double generateEuclideanDistance(Coordinate start, Coordinate goal) {
		if(heuristics.containsKey(map[start.x][start.y])) {
			return heuristics.get(map[start.x][start.y]);
		}else {
			double distancePow2 = Math.pow(start.x - goal.x, 2) + Math.pow(start.y - goal.y, 2);
			heuristics.put(map[start.x][start.y], distancePow2);
			return distancePow2;
		}
	}
}
