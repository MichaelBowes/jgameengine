package expath.logic;

public enum Heuristic {
	Manhattan,
	Diagonal,
	Euclidean;
}
