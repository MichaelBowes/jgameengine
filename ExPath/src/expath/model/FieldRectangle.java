package expath.model;

import expath.logic.PathField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FieldRectangle extends Rectangle implements PathField{

	private boolean isBlocked;
	
	public FieldRectangle() {
		super();
		setStyle();
	}
	
	public FieldRectangle(float x, float y, float width, float height) {
		super(x, y, width, height);
		setStyle();
	}
	
	public Tuple<Double, Double> getCenter() {
		Tuple<Double, Double> center = new Tuple<Double, Double>(
				this.getX() + (this.getWidth() / 2),
				this.getY() + (this.getHeight() / 2));
		return center;
	}
	
	private void setStyle() {
		this.setFill(Color.GREY);
		this.setStroke(Color.BLACK);
	}
	
	public boolean setBlock() {
		if(isBlocked) {
			isBlocked = false;
			return false;
		}else {
			isBlocked = true;
			return true;
		}
	}

	@Override
	public boolean isBlocked() {
		return isBlocked;
	}
}
