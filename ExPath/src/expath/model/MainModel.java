package expath.model;

import java.util.Observable;

import expath.logic.AStarSearch;
import expath.logic.Coordinate;
import expath.logic.Heuristic;
import expath.logic.PathType;

public class MainModel extends Observable {

	private Coordinate[] path;
		
	public void generatePath(FieldRectangle[][] map) {
		AStarSearch search = new AStarSearch(map, Heuristic.Euclidean, PathType.EightWay);
		path = search.generatePath(new Coordinate(0,0), new Coordinate(49,29));
		setChanged();
		notifyObservers(path);
	}
}
