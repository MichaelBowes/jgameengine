package expath.model;

public class Tuple<T, K> {
	
	public T x;
	public K y;
	
	public Tuple(T x, K y) {
		this.x = x;
		this.y = y;
	}
}
