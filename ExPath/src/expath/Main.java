package expath;

import expath.controller.MainController;
import expath.model.MainModel;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage arg0) throws Exception {
		MainModel model = new MainModel();
		MainController controller = new MainController(model);
		controller.startUp();		
	}

}
