package expath.view;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import expath.controller.MainController;
import expath.logic.Coordinate;
import expath.model.FieldRectangle;
import expath.model.Tuple;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.stage.Stage;

public class MainView extends Stage implements Observer {
	
	private final MainController controller;
	private Group mapField;
	private FieldRectangle[][] fields;
	
	public MainView(MainController controller) {
		this.controller = controller;
		this.setTitle("ExPath");
		Pane rootPane = createRootPane();
		Scene scene = new Scene(rootPane, 991, 650);
		this.setResizable(false);
		this.setScene(scene);
		drawMap(50, 30);
	}
	
	private Pane createRootPane() {
		VBox root = new VBox();
		root.getChildren().add(createMenu());
		root.getChildren().add(createMapPane());
		root.getChildren().add(createControlPane());
		
		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		return root;
	}
	
	private MenuBar createMenu() {
		MenuBar menuBar = new MenuBar();
		Menu dataMenu = new Menu("Data");
		MenuItem item1 = new MenuItem("Debug");
		item1.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				
			}
		});
		menuBar.getMenus().add(dataMenu);
		
		return menuBar;
	}
	
	private void resetMapPath() {
		List<Node> lineNodes = new LinkedList<Node>();
		for(Node node : mapField.getChildren()) {
			if(node.getClass() == Line.class) {
				lineNodes.add(node);
			}
		}
		mapField.getChildren().removeAll(lineNodes);
	}
	
	private Pane createControlPane() {
		GridPane pane = new GridPane();
		Button startButton = new Button("Start");
		startButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				resetMapPath();
				controller.findPath(fields);
			}
			
		});
		pane.getChildren().add(startButton);
		return pane;
	}
	
	private Group createMapPane() {
		Group group = new Group(); 
		group.autosize();
		group.autoSizeChildrenProperty();
		this.mapField = group;
		return group;
	}
	
	private void drawMap(int x, int y) {
		
		FieldRectangle[][] fields = new FieldRectangle[x][y];
		float positionX = 0;
		float positionY = 0;
		float size = 20; //Calculate width/height per field.
		
		for(int i = 0; i < x; i++) {
			for(int j = 0; j < y; j++) {
				FieldRectangle field = new FieldRectangle(positionX, positionY, size, size); 
				field.setOnMouseClicked(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent event) {
						if(controller.setFieldBlock(field)) {
							field.setFill(Color.BLACK);							
						}else {
							field.setFill(Color.GREY);
						}
					}
					
				});
				fields[i][j] = field;
				mapField.getChildren().add(field);
				positionY += size;
			}
			positionY = 0;
			positionX += size;
		}
		this.fields = fields;
	}
	
	public void showAlert() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Pathfinder");
		alert.setHeaderText("No path found!");
		alert.setContentText("No path could be calculated for the current map!");

		alert.showAndWait();
	}
	
	public void drawPath(Coordinate[] path) {
		
		FieldRectangle previousPoint = null;
		
		for(Coordinate point : path) {
			FieldRectangle currentPoint = fields[point.x][point.y];
			if(previousPoint != null) {
				Tuple<Double, Double> prevCenter = previousPoint.getCenter();
				Tuple<Double, Double> currentCenter = currentPoint.getCenter();
				Line line = new Line(prevCenter.x, prevCenter.y, currentCenter.x, currentCenter.y);
				line.setStroke(Color.DARKRED);
				line.setStrokeLineCap(StrokeLineCap.ROUND);
				line.setStrokeLineJoin(StrokeLineJoin.ROUND);
				line.setStrokeWidth(5);
				line.toFront();
				this.mapField.getChildren().add(line);			
			}
			previousPoint = currentPoint;
			
		}		
		
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg == null) {
			showAlert();
			return;
		}
		Coordinate[] coords = (Coordinate[]) arg;
		drawPath(coords);
	}
}
