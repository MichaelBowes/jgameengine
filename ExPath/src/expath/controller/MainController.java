package expath.controller;

import expath.model.FieldRectangle;
import expath.model.MainModel;
import expath.view.MainView;

public class MainController {

	private final MainModel model;
	private MainView view;
	
	public MainController(MainModel model) {
		this.model = model;
	}
	
	public void startUp() {
		view = new MainView(this);
		model.addObserver(view);
		view.showAndWait();
	}

	public boolean setFieldBlock(FieldRectangle field) {
		return field.setBlock();		
	}
	
	public void findPath(FieldRectangle[][] map) {
		model.generatePath(map);
	}
}
