package Exceptions;

public class InvalidFileException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4920529426153557467L;

	public InvalidFileException(String message) {
		super(message);
	}
	
	public InvalidFileException(String message, Throwable e) {
		super(message, e);
	}
}
