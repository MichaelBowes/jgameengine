package engine.textures;

import java.awt.image.BufferedImage;

import org.lwjgl.opengl.GL11;

import engine.textures.manager.TextureManager;
import engine.utils.BufferUtils;

public class Texture{
	
	private int width;
	private int height;
	private int texture;
	private String path;
	private boolean isInMemory;
	
	public Texture(String path, int width, int height) {
		this.path = path;
		isInMemory = false;
		this.height = height;
		this.width = width;
	}
	
	private int createGLTexture(BufferedImage image) {	
		int[] pixels = null;
		width = image.getWidth();
		height = image.getHeight();
		pixels = new int[width * height];
		image.getRGB(0, 0, width, height, pixels, 0, width);

		int[] data = new int[width * height];
		for (int i = 0; i < width * height; i++) {
			int a = (pixels[i] & 0xff000000) >> 24;
			int r = (pixels[i] & 0xff0000) >> 16;
			int g = (pixels[i] & 0xff00) >> 8;
			int b = (pixels[i] & 0xff);
			
			data[i] = a << 24 | b << 16 | g << 8 | r;
		}
		
		int result = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, result);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA,
				GL11.GL_UNSIGNED_BYTE, BufferUtils.createIntBuffer(data));
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		isInMemory = true;
		return result;
	}
	
	/**
	 * Notifies the texture manager that the resource is not in use
	 * and increments the counter of objects using the texture.
	 * If the texture was not loaded in memory it will be after this.
	 */
	public void load() {
		TextureManager.getTextureResource(path).usingIncrement();
	}
	/**
	 * Notifies the texture manager that the resource is not in use
	 * and decrements the counter of objects using the texture.
	 */
	public void unload() {
		TextureManager.getTextureResource(path).usingDecrement();
	}
	
	public boolean isInMemory() {
		return isInMemory;
	}
	
	public void loadInMemory(BufferedImage image) {
		texture = createGLTexture(image);
	}
	
	public void setMemoryState(boolean state) {
		isInMemory = state;
	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getTextureId() {
		if(isInMemory == false) {
			System.err.println("Trying to access unloaded texture!");
		}
		return texture;
	}
	
	public String getPath(){
		return path;
	}
	
	public void delete() {
		GL11.glDeleteTextures(texture);
		texture = 0;
	}
}
