package engine.textures;

public class SpriteGraphic implements Sprite {
	
	private Texture texture;
	
	public SpriteGraphic(Texture texture){
		this.texture = texture;
	}

	@Override
	public Texture getTexture() {
		return texture;
	}
	
	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	@Override
	public void delete() {
		this.texture.delete();	
	}

	@Override
	public void update(double delta) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int getWidth() {
		return getTexture().getWidth();
	}

	@Override
	public int getHeight() {
		return getTexture().getHeight();
	}	
}
