package engine.textures;

public class SpriteAnimation implements Sprite {

	/**
	 * Array containing all sprites of the animation.
	 */
	private Sprite[] sprites;
	/**
	 * Current sprite of the animation.
	 */
	private Sprite currentSprite;
	/**
	 * Index of the current sprite.
	 */
	private int currentIndex;
	private int startIndex = 0;
	private int endIndex = 4;
	/**
	 * The time that has elapsed since changing the sprite.
	 */
	private double elapsedTime;

	/**
	 * The time after which the sprite changes in milliseconds.
	 */
	private int time;
	/**
	 * True when animation should play.
	 */
	private boolean active = true;

	public SpriteAnimation(Sprite[] sprites, int time) {
		if (sprites == null) {
			throw new IllegalArgumentException("Animation array is null!");
		}
		for (int i = 0; i < sprites.length; i++) {
			if (sprites[i] == null) {
				throw new IllegalArgumentException("Element of animation array is null!");
			}
		}
		this.sprites = sprites;
		this.time = time;
		currentSprite = sprites[0];
	}

	public Sprite getCurrentSprite() {
		return this.currentSprite;
	}

	/**
	 * Updates the current sprite to the next sprite.
	 */
	private void updateSprite() {
		currentIndex++;
		if (currentIndex > endIndex-1 || currentIndex > sprites.length - 1) {
			currentIndex = startIndex;
		}
		currentSprite = sprites[currentIndex];
	}

	@Override
	public Texture getTexture() {
		return sprites[currentIndex].getTexture();
	}

	/**
	 * Sets the current index.
	 * @param index
	 */
	public void setIndex(int index) {
		this.currentIndex = index;
	}
	/**
	 * Sets start- and endIndex.
	 * @param index
	 */
	public void setIndizes(int startIndex, int endIndex) {
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.currentIndex = startIndex;
	}

	@Override
	public void delete() {
		for (int i = 0; i < sprites.length; i++) {
			if (sprites[i] != null) {
				sprites[i].delete();
			}
		}
	}

	@Override
	public void update(double delta) {
		if (active) {
			// int delta = Renderer.getDelta();
			// Update the sprite if the elapsed time plus the time since
			// the last frame is bigger or equal the time for sprite change.
			if (elapsedTime + delta >= time) {
				updateSprite();
				double newTime = delta + elapsedTime - time;
				elapsedTime = newTime;
			} else {// If the time is not bigger add it to elapsed time.
				elapsedTime += delta;
			}
		}
	}

	/**
	 * Starts the playback of the animation.
	 */
	public void play() {
		this.active = true;
	}

	/**
	 * Stops the playback of the animation.
	 */
	public void stop() {
		this.active = false;
	}

	@Override
	public int getWidth() {
		return getTexture().getWidth();
	}

	@Override
	public int getHeight() {
		return getTexture().getHeight();
	}

	public int getCurrentIndex() {
		return currentIndex;
	}
	
	public int getStartIndex() {
		return startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	
	
}
