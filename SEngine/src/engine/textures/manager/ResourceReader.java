package engine.textures.manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Exceptions.InvalidFileException;

public class ResourceReader {
	
	public static List<TextureResource> getResources(String path) throws FileNotFoundException, InvalidFileException {
		
		List<TextureResource> resources = new ArrayList<TextureResource>();
		FileReader fr = new FileReader(path);
		try(BufferedReader br = new BufferedReader(fr)){
			String currentLine;
			int lineCount = 0;
			while((currentLine = br.readLine()) != null) {
				lineCount++;
				if(!currentLine.startsWith("#")) {
					String[] pieces = currentLine.split(" ");
					if(pieces.length < 5) {
						throw new InvalidFileException("Resource file has an invalid format in line " + lineCount + ".");
					}
					int width;
					int height;
					int tileWidth;
					int tileHeight;
					try {
						width = Integer.parseInt(pieces[1]);
						height = Integer.parseInt(pieces[2]);
						tileWidth = Integer.parseInt(pieces[3]);
						tileHeight = Integer.parseInt(pieces[4]);
					}catch(NumberFormatException e) {
						throw new InvalidFileException("Resource file has an invalid format in line " + lineCount + "." + e.getMessage());
					}
					TextureResource res = new TextureResource(pieces[0], width, height, tileWidth, tileHeight);
					resources.add(res);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}	
}
