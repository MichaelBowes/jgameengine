package engine.textures.manager;

public class TextureResource {
	
	private String path;
	/**
	 * Width of the image.
	 */
	private int width;
	/**
	 * Height of the image
	 */
	private int height;
	/**
	 * Number of game objects currently using a texture of the
	 * image in a rendered scene.
	 */
	private int inUse;
	/**
	 * Number of tiles in x direction.
	 */
	private int tileRows;
	/**
	 * Number of tiles in y direction.
	 */
	private int tileColumns;
	/**
	 * Width of a single tile.
	 */
	private int tileWidth;
	/**
	 * Height of a single tile.
	 */
	private int tileHeight;	

	/**
	 * 
	 */
	private boolean isLoaded;
	
	public TextureResource(String path, int width, int height, int tileWidth,  int tileHeight) {
		this.path = path;
		this.width = width;
		this.height = height;
		this.tileRows = height / tileHeight;
		this.tileColumns = width/tileWidth;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
	}
	
	public int getTileRows() {
		return tileRows;
	}

	public int getTileColumns() {
		return tileColumns;
	}

	public boolean isLoaded() {
		return isLoaded;
	}

	public String getPath() {
		return path;
	}
	
	public int getTileWidth() {
		return tileWidth;
	}

	public int getTileHeight() {
		return tileHeight;
	}

	public void usingIncrement() {
		inUse++;
		if(!isLoaded) {
			TextureManager.loadImage(path);
			isLoaded = true;
		}
	}
	
	public void usingDecrement() {
		inUse--;
		if(inUse == 0) {
			isLoaded = false;
			TextureManager.unloadImage(path);
		}
	}
	
	public int getUsingCount() {
		return inUse;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
}
