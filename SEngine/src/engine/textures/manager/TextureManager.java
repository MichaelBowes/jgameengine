package engine.textures.manager;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import Exceptions.InvalidFileException;
import engine.textures.Sprite;
import engine.textures.SpriteGraphic;
import engine.textures.Texture;
import engine.utils.BufferUtils;
import engine.utils.FileUtils;
import engine.utils.Tuple;
import javolution.util.FastMap;

/**
 * Manager class for managing {@link Textures}.
 */
public class TextureManager {

	private TextureManager() {};
	
	private static Map<String, Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>>> resources =
			new FastMap<String, Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>>>();
	 
	/**
	 * Reads the given resource file and saves all {@link Texture}s. The {@link Texture}s will only
	 * be loaded if they are contained within a currently rendered {@link Scene}, and automatically unloaded.<br>
	 * Format for the resource file:<br><br>
	 * 
	 * image-path image-width image-height tile-height tile-width<br><br>
	 * 
	 * for every image to be loaded.
	 * @param path to the resource file.
	 * @throws FileNotFoundException
	 * @throws InvalidFileException
	 */
	public static void readResourceFile(String path) throws FileNotFoundException, InvalidFileException {
		List<TextureResource> resourceList = ResourceReader.getResources(path);	 	
		//Put all resource files in the map.
		for(TextureResource res : resourceList) {
			//Create texture map.
			Map<Tuple<Integer,Integer>, Texture> resMap = new FastMap<Tuple<Integer,Integer>, Texture>();	
			for(int c = 0; c < res.getTileColumns(); c++) {
				for(int r = 0; r < res.getTileRows(); r++) {
					resMap.put(new Tuple<Integer, Integer>(r, c), new Texture(res.getPath(), res.getTileWidth(), res.getTileHeight()));
				}
			}
			//Put new resource in map.
			resources.put(res.getPath(), new Tuple<TextureResource, Map<Tuple<Integer,Integer>,Texture>>(res, resMap));
		}		
	}
	
	/**
	 * Returns an {@link Sprite} from a given sprite sheet file path.
	 * If the image file is a single image, use 0, 0 as x and y.
	 * The texture of this sprite will only be loaded into memory, if
	 * the {@link GameObject} it is contained in a {@link Scene} that is currently
	 * loaded in the {@link SceneManager}.
	 * @param path to the sprite sheet file.
	 * @param x position of the sprite in the raster.
	 * @param y position of the sprite in the raster.
	 * @return Single {@link Sprite}.
	 */
	public static Sprite getSprite(String path, int x, int y) {
		Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> tuple = resources.get(path);
		if(tuple == null) { //No image file was defined in the resource file.
			throw new IllegalArgumentException("Could not find texture for the given path: " + path);
		}
		Texture texture = tuple.y.get(new Tuple<Integer, Integer>(x, y));
		if(texture == null) {
			throw new IllegalArgumentException("Could not find sprite with coordinates " 
										+ x + " , " + y + " for the given path: " + path);
		}
		Sprite sprite = new SpriteGraphic(texture);
		return sprite;
	}
	
	/**
	 * @param path
	 * @return
	 */
	public static TextureResource getTextureResource(String path) {
		Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> tuple = resources.get(path);
		return tuple.x;
	}
	
	/**
	 * Adds a image file to the resources.
	 * @param path to the image file.
	 * @param width of a single image tile. Use 0 if it is a single tile.
	 * @param height of a single image tile. Use 0 if it is a single tile.
	 */
	public static void addImageFile(String path, int width, int height, int tileWidth, int tileHeight) {
		TextureResource textureRes = new TextureResource(path, height, height, tileWidth, tileHeight);
		//Add resource to map.
		resources.putIfAbsent(path,
				new Tuple<TextureResource, Map<Tuple<Integer,Integer>,Texture>>(textureRes,
						new HashMap<Tuple<Integer, Integer>,Texture>()));
	}
	
	/**
	 * Loads a single {@link TextureResource} into memory.
	 * @param entry
	 */
	private static void loadResourceInMemory(Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> entry) {
		Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> resTuple = entry;
		TextureResource res = resTuple.x;
		String path = res.getPath();
		int columns = res.getTileColumns();
		int rows = res.getTileRows();
		BufferedImage image = FileUtils.loadImage(path);
		if(rows == 1 && columns == 1) {
			//Don't split the image.
			resTuple.y.get(new Tuple<Integer, Integer>(0, 0)).loadInMemory(image);
		}else{
			//Split the image.
			BufferedImage[][] images = FileUtils.splitImage(image, res.getTileWidth(), res.getTileHeight());
			for(int x = 0; x < images.length; x++) {
				for(int y = 0; y < images[0].length; y++) {
					resTuple.y.get(new Tuple<Integer, Integer>(x, y)).loadInMemory(images[x][y]);
				}
			}
		}
	}
			
	/**
	 * Gets called once an {@link Texture} is not contained in a currently rendering {@link Scene} anymore.
	 * @param path to resource.
	 */
	public static void unloadImage(String path) {
		Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> tuple = resources.get(path);
		for(Map.Entry<Tuple<Integer,Integer>, Texture> entry : tuple.y.entrySet()) {
			entry.getValue().delete();
			entry.getValue().setMemoryState(false);
		}
	}
	
	/**
	 * Gets called once an {@link Texture} is contained at least once in a currently rendering {@link Scene}.
	 * @param path to resource.
	 */
	public static void loadImage(String path) {
		Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>> tuple = resources.get(path);
		loadResourceInMemory(tuple);
	}
	
	/**
	 * Deletes all cached textures.
	 */
	public static void cleanUp() {
		for(Map.Entry<String, Tuple<TextureResource, Map<Tuple<Integer,Integer>, Texture>>> entry : resources.entrySet()) {
			if(entry.getValue().x.isLoaded()) {
				for(Map.Entry<Tuple<Integer,Integer>, Texture> textureEntry : entry.getValue().y.entrySet()) {
					textureEntry.getValue().delete();
				}				
				entry.getValue().y.clear();
			}
		}
	}
}
