package engine.textures;

public interface Sprite {
	public Texture getTexture();
	public int getWidth();
	public int getHeight();
	public void delete();
	public void update(double delta);
}
