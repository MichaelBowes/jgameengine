package engine;

import java.io.IOException;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

import Exceptions.EngineException;
import engine.display.Camera;
import engine.display.Monitor;
import engine.display.Resolution;
import engine.display.Window;
import engine.graphics.Color;
import engine.input.KeyInput;
import engine.input.MouseInput;
import engine.objects.fonts.FontManager;
import engine.objects.scene.SceneManager;
import engine.renderer.Renderer;
import engine.textures.manager.TextureManager;

public class Engine implements Runnable {

	private final Thread engineThread;
	private final GameLogic gameLogic;
	private static Window window;
	private static Monitor monitor;
	private static Renderer renderer;
	private static Camera camera = new Camera();
	private static SceneManager sceneManager = new SceneManager();
	private static boolean isInitialised;
	private static boolean isCreated;
	private boolean rendererSet;
	private int frameSyncRate;
	private int updateRate;

	public Engine(Resolution resolution, GameLogic gameLogic, int updateRate) {
		GLFWErrorCallback.createPrint(System.err).set();
		this.gameLogic = gameLogic;
		window = new Window(resolution);
		engineThread = new Thread(this, "GAME_ENGINE_THREAD");
		isCreated = true;
		setUpdateRate(updateRate);
	}

	public void start() {
		engineThread.start();
	}

	private void initialise() {
		// TODO: Read ini file with settings here

		if (GLFW.glfwInit() == false) {
			throw new RuntimeException("Could not initialise GLFW!");
		}
		// Boolean for fullscreen or not.
		window.createWindow(false);
		monitor = new Monitor();
		GLFW.glfwMakeContextCurrent(window.getWindowHandle());
		GLFW.glfwSetKeyCallback(window.getWindowHandle(), new KeyInput());
		GLFW.glfwSetMouseButtonCallback(window.getWindowHandle(), new MouseInput());
		int width = Window.getResolution().width();
		int height = Window.getResolution().height();
		GLFW.glfwSetWindowSizeLimits(window.getWindowHandle(), width, height, width, height);
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 2);
		GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
		GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GL11.GL_TRUE);
		GLFW.glfwSwapInterval(0);
		GL.createCapabilities();

		// If renderer is not already set, create a new renderer.
		if (!rendererSet) {
			renderer = new Renderer(window, camera, null, sceneManager);
		}
		isInitialised = true;
		try {
			FontManager.readFonts();
		} catch (IOException e) {
			e.printStackTrace();
			throw new EngineException("Could not read font data. Reason: " + e.getMessage());
		}

		gameLogic.init();
	}

	public static Window getWindow() {
		if (isCreated) {
			return window;
		} else {
			throw new EngineException("Trying to access non existend window!");
		}
	}

	public static Camera getCamera() {
		return camera;
	}

	public static void setCamera(Camera newCamera) {
		camera = newCamera;
	}

	/**
	 * Closes the game window.
	 */
	public void close() {
		GLFW.glfwDestroyWindow(window.getWindowHandle());
	}

	/**
	 * Starts the engine. If no {@link Renderer} is set, a new {@link Renderer} will
	 * be created automatically.
	 */
	@Override
	public void run() {
		initialise();

		double timePassed = 0;
		while (!GLFW.glfwWindowShouldClose(window.getWindowHandle())) {
			renderer.render();

			timePassed += Renderer.getDelta();
			if (timePassed >= updateRate) {
				timePassed = 0;
				sceneManager.getGameScene().checkCollisions();
			}

			if (frameSyncRate != 0) {
				Sync.sync(frameSyncRate);
			}
			GLFW.glfwPollEvents();
			GLFW.glfwSwapBuffers(window.getWindowHandle());
			gameLogic.update();
		}

		// Free ressources
		deleteRessources();
		gameLogic.cleanUp();
		GLFW.glfwTerminate();
		GLFW.glfwSetErrorCallback(null).free();
	}

	/**
	 * Returns the version number of the currently used open GL version.
	 * 
	 * @return - Version number of the used open GL version.
	 */
	public String getOpenGLVersion() {
		return GL11.glGetString(GL11.GL_VERSION);
	}

	public void setSetBackgroundColor(Color color) {
		if (isInitialised) {
			renderer.setBackgroundColor(color);
		} else {
			System.err.println("Could not set background color because engine was not yet initialized.");
		}
	}

	/**
	 * Sets the rate at which the <code>update()</code> method of the
	 * {@link GameLogic} will be called.
	 * 
	 * @param rate
	 *            per second.
	 */
	public void setUpdateRate(int rate) {
		this.updateRate = 1000 / rate;
	}

	/**
	 * Limits the fps to the given number. The image can stutter if the fps sink
	 * under the limit.<br>
	 * If it is set to 0, the fps will be unlimited.
	 * 
	 * @param syncRate
	 */
	public void setFrameSyncRate(int syncRate) {
		this.frameSyncRate = syncRate;
	}

	/**
	 * Sets the framerate limitations of the {@link Engine}.<br>
	 * 0 = unlocked <br>
	 * 1 = monitor framerate <br>
	 * 2 = half of monitor framerate<br>
	 * 
	 * @param vSync
	 */
	public void setVSync(int vSync) {
		if (isInitialised) {
			GLFW.glfwSwapInterval(vSync);
		} else {
			System.err.println("Can't set vSync when engine is not initialized.");
		}
	}

	/**
	 * Returns the {@link SceneManager} that contains all rendered
	 * {@link GameObject}s. The {@link GameObject}s in the ui list will be rendered
	 * after the objects in the game object list.<br>
	 * {@link GameObject}s in the ui list will also not be affected by a view or
	 * projection matrix, and use the {@link UIShader}.<br>
	 * If a {@link Scene} is to be rendered, it has to be set in the
	 * {@link SceneManager} as the desired ui- or game{@link Scene}.
	 * 
	 * @return manager for {@link Scene}s.
	 */
	public static SceneManager getSceneManager() {
		return sceneManager;
	}

	public static int getFps() {
		if (isInitialised) {
			return renderer.getFps();
		} else {
			System.err.println("Could not access fps because engine is not initialized.");
			return 0;
		}
	}

	private void deleteRessources() {
		TextureManager.cleanUp();
		FontManager.cleanUp();
		renderer.delete();
	}
}
