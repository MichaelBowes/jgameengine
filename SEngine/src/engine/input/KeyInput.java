package engine.input;

import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;

import javolution.util.FastTable;

public class KeyInput extends GLFWKeyCallback{
	
	private static List<KeyInputListener> listeners = new FastTable<KeyInputListener>();
	
	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {
		long time = System.currentTimeMillis();
		Input.keys[key] = action != GLFW.GLFW_RELEASE;
		for(KeyInputListener listener : listeners) {
			switch(action) {
				case GLFW.GLFW_PRESS:
					listener.onKeyDown(key);
				break;	
				case GLFW.GLFW_RELEASE:
					listener.onKeyReleased(key);
				break;	
			}
		}
		
	}
	
	public static void addListener(KeyInputListener listener) {
		if(listener == null) {
			throw new IllegalArgumentException();
		}
		listeners.add(listener);
	}
}
