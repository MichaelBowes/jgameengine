package engine.input;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_MIDDLE;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;

import java.nio.DoubleBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import engine.Engine;
import engine.display.Camera;
import engine.display.Window;
import engine.math.Vector2f;
import engine.objects.GameObject;
import javolution.util.FastTable;

public class MouseInput extends GLFWMouseButtonCallback {
	public static final int LEFT_MOUSE_BUTTON = GLFW_MOUSE_BUTTON_LEFT;
	public static final int RIGHT_MOUSE_BUTTON = GLFW_MOUSE_BUTTON_RIGHT;
	public static final int MIDDLE_MOUSE_BUTTON = GLFW_MOUSE_BUTTON_MIDDLE;
	private static DoubleBuffer xPos = BufferUtils.createDoubleBuffer(1);
	private static DoubleBuffer yPos = BufferUtils.createDoubleBuffer(1);
	private static List<MouseInputListener> listeners = new FastTable<MouseInputListener>();
	private static boolean[] activeMouseButtons = new boolean[16];
	private static long lastMouseNS = 0;
	private static long mouseDoubleClickPeriodNS = 1000000000 / 5; // 5th of a second for double click.
	private static Vector2f mousePosition = new Vector2f(0, 0);
	private static long windowID;

	@Override
	public void invoke(long window, int button, int action, int mods) {
		windowID = window;
		mousePosition.x = (float) xPos.get(0);
		mousePosition.y = (float) Math.abs(yPos.get(0) - Window.getResolution().height());
		
			for(MouseInputListener listener : listeners) {
				switch(action) {
					case GLFW.GLFW_PRESS:
						listener.onMouseDown(button);
					break;	
					
					case GLFW.GLFW_RELEASE:
						listener.onMouseReleased(button);
					break;	
				}
		}
		Input.keys[button] = action != GLFW.GLFW_RELEASE;			
	}
	
	/**
	 * Returns true if the given mouse button was pressed.
	 * 
	 * @param button
	 *            - index of the mouse button.
	 * @return
	 */
	public static boolean mouseButtonDown(int button) {
		return activeMouseButtons[button];
	}
	
	public static void addListener(MouseInputListener listener) {
		if(listener == null) {
			throw new IllegalArgumentException();
		}
		listeners.add(listener);
	}
	
	/**
	 * Returns mouse position in world coordinates relative to a game object.
	 * @param gameobject
	 * @return
	 */
	public static Vector2f getWorldCoords(GameObject gameobject) {
		Vector2f mousePosition2 = new Vector2f(0,0);
		float z = Engine.getCamera().getPosition().z;
		
		int resW = Window.getResolution().width();
		int resH = Window.getResolution().height();
		mousePosition2.x = gameobject.getPosition().x+(-resW/2+getMousePosition().x)*z;
		mousePosition2.y = gameobject.getPosition().y+(-resH/2+getMousePosition().y)*z;
		return mousePosition2;
	}

	public static Vector2f getMousePosition() {
		glfwGetCursorPos(windowID, xPos, yPos);
		mousePosition.x = (float) xPos.get(0);
		mousePosition.y = (float) Math.abs(yPos.get(0) - Window.getResolution().height());
		return mousePosition;
	}

}