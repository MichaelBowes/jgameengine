package engine.input;

public interface KeyInputListener {

	void onKeyDown(int key);
	void onKeyReleased(int key);
}
