package engine.input;

public interface MouseInputListener {

	void onMouseDown(int button);
	void onMouseDoubleClick(int button);
	void onMouseReleased(int button);
}
