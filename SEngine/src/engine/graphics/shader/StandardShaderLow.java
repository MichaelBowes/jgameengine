package engine.graphics.shader;

import engine.math.Vector3f;
import engine.objects.Light;

public class StandardShaderLow extends StandardShader {

	private static final String FRAGMENT_FILE = "res/shader/StandardFS_LOW.frag";
	private static final String VERTEX_FILE = "res/shader/StandardVS_LOW.vert";
	protected static final String LIGHT_POSITION_NAME = "lightPositions";
	protected static final String LIGHT_COLORS_NAME = "lightColors";
	protected static final String LIGHT_ATTENUATIONS_NAME = "lightAttenuations";
	protected static final String LIGHT_BRIGHTNESS_NAME = "lightBrightness";
	protected static final String LIGHT_RADIUS_NAME = "lightRadius";
	
	public StandardShaderLow() {
		super();
	}
	
	public void loadLights(Light[] lights) {
		if(lights.length > 10) {
			throw new IllegalArgumentException("Too many lights");
		}
		for(int i = 0; i < lights.length; i++) {
			super.setUniform2f(LIGHT_POSITION_NAME+"["+i+"]",
					lights[i].getPosition().x,
					lights[i].getPosition().y);
			super.setUniform3f(LIGHT_COLORS_NAME+"["+i+"]", lights[i].getColor());
			super.setUniform3f(LIGHT_ATTENUATIONS_NAME+"["+i+"]", lights[i].getAttenuation());
			super.setUniform1f(LIGHT_BRIGHTNESS_NAME+"["+i+"]", lights[i].getBrightness());
			super.setUniform1f(LIGHT_RADIUS_NAME+"["+i+"]", lights[i].getRadius());
		}
		//Filling empty spaces
		if(lights.length < 10) {
			for(int i = lights.length; i < 10; i++) {
				super.setUniform2f(LIGHT_POSITION_NAME+"["+i+"]", 0, 0);	
				super.setUniform3f(LIGHT_COLORS_NAME+"["+i+"]", new Vector3f(0, 0, 0));
				super.setUniform3f(LIGHT_ATTENUATIONS_NAME+"["+i+"]", new Vector3f(0, 0, 0));
				super.setUniform1f(LIGHT_BRIGHTNESS_NAME+"["+i+"]", 0);
				super.setUniform1f(LIGHT_RADIUS_NAME+"["+i+"]", 0);
			}
		}		
	}
}
