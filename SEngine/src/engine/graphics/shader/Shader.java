package engine.graphics.shader;

import engine.graphics.Model;
import engine.math.Matrix4f;
import engine.math.Vector4f;

public abstract class Shader extends ShaderProgram {

	protected static final String TRAMSFORMATION_MATRIX_NAME = "transformationMatrix";
	protected static final String PROJECTION_MATRIX_NAME = "projectionMatrix";
	protected static final String VIEW_MATRIX_NAME = "viewMatrix";
	protected static final String SIZE_VECTOR_NAME = "sizeVector";
	protected static final String COLOR_VECTOR_NAME = "colorVector";
	protected static final String SCALE_VECTOR_NAME = "scaleVector";

	public Shader(String vertexShaderPath, String fragmentShaderPath) {
		super(Shader.load(vertexShaderPath, fragmentShaderPath));
	}

	public Shader(int programID) {
		super(programID);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(Model.VERTEX_ATTRIBUTE_INDEX, "position");
	}

	public void loadTransformationMatrix(Matrix4f matrix) {
		super.setUniformMatrix4f(TRAMSFORMATION_MATRIX_NAME, matrix);
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		super.setUniformMatrix4f(PROJECTION_MATRIX_NAME, projection);
	}

	public void loadViewMatrix(Matrix4f viewMatrix) {
		super.setUniformMatrix4f(VIEW_MATRIX_NAME, viewMatrix);
	}

	public void loadSizeVector(float x, float y) {
		super.setUniform2f(SIZE_VECTOR_NAME, x, y);
	}

	protected void loadColorVector(Vector4f color, Vector4f generalColor) {
		Vector4f colorVector = color.add(generalColor);
		super.setUniform4f(COLOR_VECTOR_NAME, colorVector);
	}

	public void loadScaleVector(float x, float y) {
		super.setUniform2f(SCALE_VECTOR_NAME, x, y);
	}
}
