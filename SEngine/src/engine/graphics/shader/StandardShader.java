package engine.graphics.shader;

import engine.math.Vector4f;

public class StandardShader extends Shader{
	
	private static final String FRAGMENT_FILE = "res/shader/StandardFS.frag";
	private static final String VERTEX_FILE = "res/shader/StandardVS.vert";
		
	private static Vector4f generalColorAddition = new Vector4f(0, 0, 0 ,0);
	
	public StandardShader(){
		super(StandardShader.load(VERTEX_FILE, FRAGMENT_FILE));
	}
	

	@Override
	protected void bindAttributes() {	
		super.bindAttributes();
	}
	
	/**
	 * Sets a color that is added to every {@link GameObject} using an {@link StandardShader}.
	 * @param color to be added.
	 */
	public static void setGeneralColorAddition(Vector4f color) {
		generalColorAddition = color;
	}
	
	public static Vector4f getGeneralColorAddition() {
		return generalColorAddition;
	}
	
	/**
	 * Sets a color that is added to every {@link GameObject} using an {@link StandardShader}.
	 * @param color to be added.
	 */
	public static void setGeneralColorAddition(float r, float g, float b, float a) {
		generalColorAddition.x = r;
		generalColorAddition.x = g;
		generalColorAddition.x = b;
		generalColorAddition.x = a;
	}
	
	/**
	 * Loads a color Vector into the shader. 
	 * @param color
	 */
	public void loadColorVector(Vector4f color) {
		super.loadColorVector(color, generalColorAddition);
	}
}
