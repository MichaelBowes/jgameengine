package engine.graphics;

public enum RenderMode {
	MODE_2D,
	MODE_3D;
}
