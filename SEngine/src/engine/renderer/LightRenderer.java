package engine.renderer;

import java.nio.ByteBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL43;


import engine.objects.Light;

public class LightRenderer {

	private int ssbo;

	public LightRenderer() {
	}
	
	public void loadLights(List<Light> lights) {
		
		this.ssbo = GL20.glGenBuffers();
		GL15.glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER, ssbo);
		ByteBuffer ssboData = BufferUtils.createByteBuffer(lights.size() * 100);
		
		for(Light light : lights) {
			ssboData.putFloat(light.getPosition().x).putFloat(light.getPosition().y).putFloat(light.getPosition().z).putFloat(0.0f);
			ssboData.putFloat(light.getColor().x).putFloat(light.getColor().y).putFloat(light.getColor().z).putFloat(0.0f);
			ssboData.putFloat(light.getAttenuation().x).putFloat(light.getAttenuation().y).putFloat(light.getAttenuation().z).putFloat(0.0f);
			ssboData.putFloat(light.getBrightness()).putFloat(light.getRadius()).putFloat(0.0f).putFloat(0.0f);
		}
		ssboData.flip();
		GL20.glBufferData(GL43.GL_SHADER_STORAGE_BUFFER, ssboData, GL15.GL_DYNAMIC_DRAW);
		GL30.glBindBufferBase(GL43.GL_SHADER_STORAGE_BUFFER, 3, ssbo);
		GL15.glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER, 0);
	}
	

}
