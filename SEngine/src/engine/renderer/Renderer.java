package engine.renderer;

import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import engine.display.Camera;
import engine.display.Window;
import engine.graphics.Color;
import engine.graphics.Model;
import engine.graphics.shader.StandardShader;
import engine.graphics.shader.UIShader;
import engine.math.Matrix4f;
import engine.objects.GameObject;
import engine.objects.Light;
import engine.objects.fonts.TextObject;
import engine.objects.scene.SceneManager;
import engine.objects.util.DepthComparator;
import javolution.util.FastSortedTable;

/**
 * TODO: Change renderable to Abstract class and add observer to
 *         notify the renderer is it was destroyed
 * 
 *         TODO: Add fps functionality.
 */
public class Renderer {

	private final Model quad;
	private UIShader uiShader = new UIShader();
	private StandardShader gameObjectShader = new StandardShader();
	private LightRenderer lightRenderer;
	private Window window;
	private Camera camera;
	private SceneManager sceneManager;
	private float fieldOfView = 70;// Default = 70
	private float nearPlane = 0.01f;// Default = 0.1f
	private float farPlane = 1000;// Default = 1000

	private static double lastFrame;
	/**
	 * Time of the last FPS update in seconds.
	 */
	private double lastFpsUpdate;
	/**
	 * Currently calculating FPS.
	 */
	private int fps;
	/**
	 * FPS after every second.
	 */
	private int currentFps;
	private static double delta;

	private List<GameObject> orderedList = new FastSortedTable<GameObject>(new DepthComparator());

	/**
	 * @param display
	 *            - The {@link Window} on which the {@link Renderer} is
	 *            rendering.
	 * @param color
	 *            - {@link Color} of the Background.
	 * @param shader
	 *            - Using the standard shader if it set to {@link Null}.
	 */
	public Renderer(Window display, Camera camera, UIShader shader, SceneManager sceneManager) {
		if (shader != null) {
			this.uiShader = shader;
		}
		initialise(display, camera);
		quad = Model.createSimpleFlatModel();
		this.sceneManager = sceneManager;
	}

	public void setBackgroundColor(Color color) {
		GL11.glClearColor(color.red, color.green, color.blue, color.alpha);
	}

	private void initialise(Window window, Camera camera) {
		this.window = window;
		this.camera = camera;
		/*
		 * Don't display back of the Sprites. GL11.glEnable(GL11.GL_CULL_FACE);
		 * GL11.glCullFace(GL11.GL_BACK);
		 */
		// Enable Transparency
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		calculateDelta(); // initialize last frame
		lastFpsUpdate = GLFW.glfwGetTime();
		lightRenderer = new LightRenderer();
	}

	/**
	 * Calculates the seconds that are elapsed since the last frame was
	 * rendered.
	 * 
	 * @return Seconds that elapsed since the last frame was rendered.
	 */
	private void calculateDelta() {
		double currentTime = GLFW.glfwGetTime() * 1000;
		double newDelta = currentTime - lastFrame;
		lastFrame = currentTime;
		delta = newDelta;
	}

	private void updateFps() {
		if ((GLFW.glfwGetTime() - lastFpsUpdate) > 1.0) {
			currentFps = fps;
			fps = 0;
			lastFpsUpdate += 1;
		}
		fps++;
	}

	private Matrix4f create3DProjectionMatrix() {
		float aspect = (float) window.getWindowWidth() / (float) window.getWindowheight();
		Matrix4f matrix = Matrix4f.perspective(fieldOfView, aspect, nearPlane, farPlane);
		return matrix;
	}

	private Matrix4f createUIProjectionMatrix() {
		Matrix4f matrix = Matrix4f.orthographic(0, Window.getResolution().width(), 0, Window.getResolution().height(),
				nearPlane, farPlane);

		return matrix;
	}

	private Matrix4f createGOProjectionMatrix() {
		Matrix4f matrix = Matrix4f.orthographic(-(Window.getResolution().width() / 2f) * camera.getPosition().z,
				(Window.getResolution().width() / 2f) * camera.getPosition().z,
				-(Window.getResolution().height() / 2f) * camera.getPosition().z,
				(Window.getResolution().height() / 2f) * camera.getPosition().z, nearPlane, farPlane);
		return matrix;
	}

	/**
	 * Start rendering all objects within the current {@link SceneManager}.
	 */
	public void render() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		// Binding the model.
		GL30.glBindVertexArray(quad.getVertexArrayObjectId());
		GL20.glEnableVertexAttribArray(Model.VERTEX_ATTRIBUTE_INDEX);

		renderGameObjects();

		renderUIObjects();// Actual rendering.
		// Unbinding the model.
		GL20.glDisableVertexAttribArray(Model.VERTEX_ATTRIBUTE_INDEX);
		GL30.glBindVertexArray(0);

		updateFps();
		calculateDelta();
		sceneManager.update(getDelta());
	}

	private void renderUIObjects() {
		// Rendering all ui objects.
		// Putting all ui objects into the map.
		for (GameObject object : sceneManager.getUIScene().getObjects()) {
			if (object instanceof TextObject) {
				processOrderedObject(object);
			} else {
				orderedList.add(object);
			}
		}
		uiShader.enable();
		uiShader.loadScaleVector(Window.getResolution().getScaleFactorX(), Window.getResolution().getScaleFactorY());
		uiShader.loadProjectionMatrix(createUIProjectionMatrix());
		for (GameObject object : orderedList) {
			renderUIObject(object);
		}
		orderedList.clear();
		uiShader.disable();
	}

	private void renderGameObjects() {
		// Rendering all gameObjects
		for (GameObject object : sceneManager.getGameScene().getObjects()) {
			if (object instanceof TextObject) {
				processOrderedObject(object);
			} else {
				orderedList.add(object);
			}
		}
		gameObjectShader.enable();
		gameObjectShader.loadScaleVector(Window.getResolution().getScaleFactorX(),
				Window.getResolution().getScaleFactorY());
		gameObjectShader.loadViewMatrix(Matrix4f.createViewMatrix(camera)); // View
																			// matrix
																			// camera
																			// movement
		gameObjectShader.loadProjectionMatrix(createGOProjectionMatrix()); // projection
																			// matrix
		lightRenderer.loadLights(sceneManager.getLights());
		for (GameObject object : orderedList) {
			renderGameObject(object);
		}
		orderedList.clear();
		gameObjectShader.disable();
	}

	private void renderGameObject(GameObject object) {
		if (!object.isHidden()) {
			if (object.getTexture() != null) {
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, object.getTexture().getTextureId());
				gameObjectShader.loadSizeVector(
						object.getTexture().getWidth() * Window.getResolution().getScaleFactorX(),
						object.getTexture().getHeight() * Window.getResolution().getScaleFactorY());
				// gameObjectShader.loadSizeVector(object.getTexture().getWidth(),
				// object.getTexture().getHeight());
				gameObjectShader.loadColorVector(object.getColorVector());
				gameObjectShader.loadTransformationMatrix(object.getTransformationMatrix()); // matrix
																								// for
																								// transformation
				GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.getCount()); // render
																				// quad
			}
		}
	}

	/**
	 * Renders a list of {@link GameObject}s with a given texture to the screen.
	 * 
	 * @param texture
	 *            to be used.
	 * @param objects
	 *            to be rendered.
	 */
	private void renderUIObject(GameObject object) {
		if (!object.isHidden()) {
			if (object.getTexture() != null) {
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, object.getTexture().getTextureId());
				uiShader.loadSizeVector(object.getTexture().getWidth() * Window.getResolution().getScaleFactorX(),
						object.getTexture().getHeight() * Window.getResolution().getScaleFactorY());
				uiShader.loadColorVector(object.getColorVector());
				uiShader.loadTransformationMatrix(object.getTransformationMatrix()); // matrix
																						// for
																						// transformation
				GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.getCount()); // render
																				// quad
			}
		}
	}

	private void processOrderedObject(GameObject object) {
		orderedList.add(object);
		for (GameObject child : object.getChildren()) {
			processOrderedObject(child);
		}
	}

	/**
	 * Returns the currently rendered frames per second.
	 * 
	 * @return Current frames per second.
	 */
	public int getFps() {
		return currentFps;
	}

	/**
	 * Sets the closest rendered plane in the projection matrix.
	 * 
	 * @param nearPlane
	 *            - Near plane
	 */
	public void setNearPlane(float nearPlane) {
		this.nearPlane = nearPlane;
	}

	/**
	 * Sets the farthest rendered plane in the projection matrix.
	 * 
	 * @param farPlane
	 *            - Far plane
	 */
	public void setFarPlane(float farPlane) {
		this.farPlane = farPlane;
	}

	/**
	 * Sets the field of view for the projection matrix.
	 * 
	 * @param fov
	 *            - field of view.
	 */
	public void setFieldOfView(float fov) {
		this.fieldOfView = fov;
	}

	/**
	 * Returns the time between the currently rendered and the last rendered
	 * frame in milliseconds.
	 * 
	 * @return value for Delta.
	 */
	public static double getDelta() {
		return delta;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public void delete() {
		uiShader.delete();
		gameObjectShader.delete();
	}

	private void loadLights(List<Light> lights) {
		Light[] lightArray = new Light[lights.size()];
		lights.toArray(lightArray);
		// gameObjectShader.loadLights(lightArray);
	}

}
