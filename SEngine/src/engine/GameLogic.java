package engine;

public interface GameLogic {

	/**
	 * Gets called every frame.
	 */
	void update();
	/**
	 * Gets called at the start of the {@link Engine}.
	 */
	void init();
	/**
	 * Gets called at the end of the engines runtime.
	 */
	void cleanUp();
}
