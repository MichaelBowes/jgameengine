package engine.utils;

public class Tuple<X,Y> {

	public X x;
	public Y y;
	
	public Tuple(X x, Y y) {
		this.x = x;
		this.y = y;
	}
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
	        if (!x.equals(tuple.x)) return false;
	        return y.equals(tuple.y);
	    }

	    @Override
	    public int hashCode() {
	        int result = x.hashCode();
	        result = 31 * result + y.hashCode();
	        return result;
	    }
}
