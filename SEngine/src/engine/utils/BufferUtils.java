package engine.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class BufferUtils {
	
	private BufferUtils () {};
	
	public static ByteBuffer byteBuffer;
	public static FloatBuffer floatBuffer;
	public static IntBuffer intBuffer;
	
	public static ByteBuffer createByteBuffer(byte[] array) {
		byteBuffer = ByteBuffer.allocateDirect(array.length).order(ByteOrder.nativeOrder());
		byteBuffer.put(array).flip();
		return byteBuffer;
	}
	
	public static FloatBuffer createFloatBuffer(float[] array) {
		floatBuffer = ByteBuffer.allocateDirect(array.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		floatBuffer.put(array).flip();
		return floatBuffer;
	}
	
	public static IntBuffer createIntBuffer(int[] array) {
		intBuffer = ByteBuffer.allocateDirect(array.length * 4).order(ByteOrder.nativeOrder()).asIntBuffer();
		intBuffer.put(array).flip();
		return intBuffer;
	}
	
	public static void clearFloatBuffer() {
		floatBuffer.clear();
	}
	
}
