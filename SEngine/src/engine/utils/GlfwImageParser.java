package engine.utils;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.stb.STBImage.*;

import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.system.MemoryStack;

/**
 * Parses an image into GLFWImage format
 *
 */
public class GlfwImageParser {

	private ByteBuffer image;
	private int width, height;

	public ByteBuffer get_image() {
		return image;
	}

	public int get_width() {
		return width;
	}

	public int get_height() {
		return height;
	}

	public GlfwImageParser(int width, int height, ByteBuffer image) {
		this.image = image;
		this.height = height;
		this.width = width;
	}

	public static GlfwImageParser loadImage(String path) {
		ByteBuffer image;
		int width, height;
		try (MemoryStack stack = MemoryStack.stackPush()) {
			IntBuffer comp = stack.mallocInt(1);
			IntBuffer w = stack.mallocInt(1);
			IntBuffer h = stack.mallocInt(1);

			image = stbi_load(path, w, h, comp, 4);
			if (image == null) {
				// throw new resource_error("Could not load image resources.");
			}
			width = w.get();
			height = h.get();
		}
		return new GlfwImageParser(width, height, image);
	}

}