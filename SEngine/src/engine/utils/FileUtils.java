package engine.utils;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;

import engine.display.Window;
import engine.graphics.Model;
import engine.objects.util.VertexArray;
import engine.textures.Sprite;
import engine.textures.SpriteGraphic;
import engine.textures.Texture;

public class FileUtils {
	
	private FileUtils() {
	}
	
	/**
	 * Reads the file of a given path and returns a string with its content.
	 * @param file - Path of the file to be loaded.
	 * @return String of the file where every line is separated by a line separator.
	 */
	public static String loadAsString(String file) {
		StringBuilder result = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String buffer = "";
			while((buffer = reader.readLine()) != null) {
				result.append(buffer + '\n');
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return result.toString();
	}
	
	/**
	 * Creates a {@link BufferedImage} from a given file url.
	 * @param path of the image.
	 * @return {@link BufferedImage} of the given image.
	 */
	public static BufferedImage loadImage(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new FileInputStream(path));
		} catch (IOException e) {
			System.err.println("Could not load file '"+ path +"'!");
			System.err.println("Reason: " + e.getMessage());
		}
		return image;
	}
	
	/**
	 * Splits a given {@link BufferedImage} in pieces of given size.
	 * @param image - {@link BufferedImage} to be split.
	 * @param width - Width of each piece.
	 * @param height - Height of each piece.
	 * @return Multidimensional array containing the pieces in rows and columns.
	 */
	public static BufferedImage[][] splitImage(BufferedImage image, int width, int height) {	
		if(image == null) {
			throw new IllegalArgumentException("The given image can't be null.");
		}
		
		int rows = image.getHeight() / height;
		int columns = image.getWidth() / width;
		BufferedImage[][] imageList = new BufferedImage[rows][columns];
		int positionX = 0;
		int positionY = 0;
		for(int r = 0; r < rows; r++) {
			for(int c = 0; c < columns; c++) {
				imageList[r][c] = image.getSubimage(positionX, positionY, width, height);
				positionX += width;
			}
			positionX = 0;
			positionY += height;
		}
		return imageList;
	}
}
