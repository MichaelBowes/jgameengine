package engine.objects.fonts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Manager for managing all {@link Font}s.
 */
public class FontManager {
	
	private static final String FONT_FOLDER_PATH = "res/fonts";
	
	private static Map<String, Font> fonts = new HashMap<String, Font>();
	
	/**
	 * Read all font info files in the res/fonts folder and save them in the manager.
	 * @throws IOException
	 */
	public static void readFonts() throws IOException {
		//Checks the fonts folder for info files.
		try (Stream<Path> paths = Files.walk(Paths.get(FONT_FOLDER_PATH))) {
		    paths.filter(f -> f.getFileName().toString().endsWith("Info"))
		    .forEach(f -> readFont(f.toString()));
		} 
	}
	
	/**
	 * Returns the font with the given name.
	 * @param name of the font.
	 * @return font for the given name.
	 */
	public static Font getFont(String name) {
		Font font =  fonts.get(name);
		if(font == null) {
			throw new IllegalArgumentException("No font with name '" + name + "' found.");
		}
		return font;
	}
	
	/**
	 * Reads a font info file and creates the in the file specified {@link Font}.
	 * @param infoPath for the font.
	 */
	private static void readFont(String infoPath) {
		List<Integer> effectiveCharacterWidths = new LinkedList<>();
		
		FileReader fr;
		try {
			fr = new FileReader(infoPath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
		
		List<String> lines = new LinkedList<>();
		
		//Read the lines
		try(BufferedReader br = new BufferedReader(fr)) {
			String currentLine;
			while((currentLine = br.readLine()) != null) {
				lines.add(currentLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(lines.size() < 2) {
			throw new RuntimeException("The file has an invalid format.");
		}
		//remove first line
		String[] firstLine = lines.remove(0).split(" ");
		String targetFile = firstLine[0];
		int characterWidth = Integer.parseInt(firstLine[1]);
		int characterHeight = Integer.parseInt(firstLine[2]);
		
		for(String line : lines) {
			String[] values = line.split(",");
			for(String value : values) {
				effectiveCharacterWidths.add(Integer.parseInt(value));
			}
		}
		String fileName = new File(infoPath).getName();
		String folder = infoPath.substring(0, infoPath.length() - fileName.length());
		//Creates the font.
		Font font = new Font(folder + targetFile + ".png", effectiveCharacterWidths, characterWidth, characterHeight);	
		fonts.put(targetFile, font);
	}
	
	public static void cleanUp() {
		for(Map.Entry<String, Font> entry : fonts.entrySet()) {
			entry.getValue().delete();
		}
	}
}
