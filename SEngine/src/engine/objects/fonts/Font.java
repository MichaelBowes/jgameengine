package engine.objects.fonts;

import java.awt.image.BufferedImage;
import java.util.List;

import engine.utils.FileUtils;

public class Font {
	
	private ReferenceCharacter[] characters;
	private int characterWidth;
	private int characterHeight;
	
	public Font(String path, List<Integer> effectiveWidths, int characterWidth, int characterHeight) {	
		BufferedImage[][] characterImages = loadFontFile(path, characterWidth, characterHeight);
		createCharacters(effectiveWidths, characterImages);
		this.characterHeight = characterHeight;
		this.characterWidth = characterWidth;
	}
	
	public int getCharacterWidth() {
		return characterWidth;
	}

	public int getCharacterHeight() {
		return characterHeight;
	}

	private BufferedImage[][] loadFontFile(String path, int characterWidth, int characterHeight) {
		BufferedImage image = FileUtils.loadImage(path);
		if(image == null) {
			throw new IllegalArgumentException("Could not read font file.");
		}
		BufferedImage[][] characterImages = FileUtils.splitImage(image, characterWidth, characterHeight);	
		return characterImages;
	}
	
	private void createCharacters(List<Integer> effectiveWidths, BufferedImage[][] characterImages) {
		characters = new ReferenceCharacter[161];
		int columns = characterImages[0].length;
		
		//Create all characters by iterating over the multidimentsional array.
		int rowCounter = 0;
		int columnCounter = 0;
		for(int i = 0; i < effectiveWidths.size(); i++) {
			int effectiveWidth = effectiveWidths.get(i);
			if(i < 161) {
				BufferedImage image = characterImages[rowCounter][columnCounter];
				characters[i] = new ReferenceCharacter(image, image.getWidth(), image.getHeight(),
						effectiveWidth, Character.CHARACTERS[i]);
				columnCounter++;
				if(columnCounter == columns) {
					rowCounter++;
					columnCounter = 0;
				}
			}		
		}
	}
	
	/**
	 * Returns the amount of characters.
	 * @return amount of characters.
	 */
	public int getCharacterAmount() {
		return characters.length;
	}
	
	/**
	 * Returns the {@link ReferenceCharacter} for a given char.
	 * @param character for which the {@link ReferenceCharacter} is to be returned.
	 * @return {@link ReferenceCharacter} representing the char in the {@link Font}.
	 */
	public ReferenceCharacter getCharacter(char character) {
		int index = 0;
		for(char c : Character.CHARACTERS) {
			if(character == c) {
				break;
			}
			index++;
		}
		return characters[index];
	}
	
	/**
	 * Returns the {@link ReferenceCharacter} for a given index.
	 * @param index of the {@link ReferenceCharacter} to be returned.
	 * @return {@link ReferenceCharacter} at the index in the {@link Font}.
	 */
	public ReferenceCharacter getCharacter(int index) {
		if(index < 0 || index >= characters.length) {
			throw new IllegalArgumentException("Index is not in range");
		}
		return characters[index];
	}
	
	public void delete() {
		for(ReferenceCharacter character : characters) {
			character.delete();
		}
		characters = null;
	}
}
