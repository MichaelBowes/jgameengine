package engine.objects.fonts;

import java.util.LinkedList;
import java.util.List;

import engine.math.Vector3f;
import engine.objects.GameObject;

/**
 * Represents displayed text or characters.
 */
public class TextObject extends GameObject {
	
	private Font font;
	private String text;
	/**
	 * Space between characters.
	 */
	private int space = 2;
	private int width;
	private int height;

	/**
	 * List of characters set as children.
	 */
	private List<Character> characters = new LinkedList<>();
	private FontAlignment alignment = FontAlignment.Centered;
	
	public TextObject(String text, Font font) {
		super(new Vector3f(0, 0, 0));	
		this.font = font;
		setText(text);
	}
	
	public TextObject(String text, Font font, Vector3f position) {
		super(position);
		this.font = font;
		setText(text);
	}

	public String getText() {
		return text;
	}

	/** 
	 * Sets the font for the {@link TextObject} 
	 * and calculates the new text and its character positions.
	 * @param font
	 */
	public void setFont(Font font) {
		this.font = font;
		createText();
	}
	
	public int getSpace() {
		return space;
	}

	/**
	 * Sets space between each character.
	 * @param space
	 */
	public void setSpace(int space) {
		this.space = space;
	}
	
	/**
	 * Returns the {@link Character} object on the given position.
	 * @param index - position of the character.
	 * @return
	 */
	public Character getCharacter(int index) {
		return characters.get(index);
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public Font getFont() {
		return this.font;
	}
	
	/**
	 * Changes the rendered text to the given message.
	 * @param message to be displayed.
	 */
	public void setText(String message) {
		if(text == null || !text.equals(message)) {
			this.text = message;
			createText();
		}
	}
	
	/**
	 * Sets the alignment for the {@link TextObject}.<br>
	 * Default is left.
	 * @param alignment for the text.
	 */
	public void setAlignment(FontAlignment alignment) {
		this.alignment = alignment;
		createText();
	}
	
	/**
	 * Creates the {@link Character} objects and sets their position.
	 */
	private void createText() {
		if(this.font == null) {
			throw new RuntimeException("Can't create text without font.");
		}
		int newWidth = 0;
		clearChildren();
		characters.clear();
		int counter = 0;
		for(char c : this.text.toCharArray()) {
			//Create the characters
			ReferenceCharacter refCharacter = font.getCharacter(c); //ref to font character texture.
			Character character = new Character(refCharacter); //Unique character
			//Set text color
			character.setAddedColor(
					this.getColorVector().x,
					this.getColorVector().y,
					this.getColorVector().z,
					this.getColorVector().w);
			if(characters.size() > 0) { //If there is already a child set for the text object.
				Character previousCharacter = characters.get(counter - 1);
				previousCharacter.addChild(character);
				
				float xPosition = -(previousCharacter.getTexture().getWidth() / 2)
						+ previousCharacter.getEffectiveWidth() + (character.getTexture().getWidth() / 2);
				character.setPosition(xPosition, 0);
			}else {
				this.addChild(character);
			}
			characters.add(character);
			//Add characters size to width;
			newWidth += refCharacter.getEffectiveWidth() + space;
			counter++;
		}
		width = newWidth - space;
		if(characters.size() > 0) {
			Character firstCharacter = characters.get(0);
			height = firstCharacter.getTexture().getHeight();
			float halfCharacterSize = firstCharacter.getTexture().getWidth() / 2;
			
			//Set alignment
			switch(alignment){
				case Centered:
					firstCharacter.setPosition(halfCharacterSize
							-(this.width/2),
							0);
					break;
			
				case Left:
					firstCharacter.setPosition(halfCharacterSize - this.width + (space * characters.size() - 1), 0);
					break;
					
				case Right:
					firstCharacter.setPosition(halfCharacterSize, 0);
					break;
			}
		}else {
			height = 0;
		}		
	}

	@Override
	public void update(double delta) {
		
	}

	@Override
	public void delete() {
		super.delete();
		
		notifyObservers(this);
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
	
}
