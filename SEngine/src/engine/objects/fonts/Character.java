package engine.objects.fonts;

import engine.objects.GameObject;
import engine.textures.SpriteGraphic;

public class Character extends GameObject {
	
	private int effectiveWidth;
	
	public Character(ReferenceCharacter character) {
		super(new SpriteGraphic(character));
		this.effectiveWidth = character.getEffectiveWidth();
	}
	
	public int getEffectiveWidth() {
		return effectiveWidth;
	}
	
	public static final char[] CHARACTERS = new char[] {
			' ', '!', '"', '#', '$', '%', '&',
			'\'', '(', ')', '*', '+', ',', '-',
			'.', '/', '0', '1', '2', '3', '4',
			'5', '6', '7', '8', '9', ':', ';',
			'<', '=', '>', '?', '@', 'A', 'B',
			'C', 'D', 'E', 'F', 'G', 'H', 'I', 
			'J','K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', '[', ' ', ']', '^',
			'_', '`', 'a', 'b', 'c', 'd', 'e',
			'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z',
			'{', '|', '}', '~', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
	};

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(double delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
}
