package engine.objects.fonts;

public enum FontAlignment {
	Centered,
	Right,
	Left;
}
