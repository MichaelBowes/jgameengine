package engine.objects.fonts;

import java.awt.image.BufferedImage;

import engine.textures.Texture;


public class ReferenceCharacter extends Texture {
	
	private final char CHARACTER;
	private int effectiveWidth;

	public ReferenceCharacter(BufferedImage image, int width, int height, int effectiveWidth, char character) {
		super("", width, height);
		this.loadInMemory(image);
		this.CHARACTER = character;
		this.effectiveWidth = effectiveWidth;
	}

	public char getCharacter() {
		return CHARACTER;
	}

	public int getEffectiveWidth() {
		return effectiveWidth;
	}
	
}
