package engine.objects.util;

import engine.objects.GameObject;
import javolution.util.function.Equality;

public class DepthComparator implements Equality<GameObject> {

	@Override
	public int hashCodeOf(GameObject object) {
		return this.hashCode();
	}

	@Override
	public boolean areEqual(GameObject left, GameObject right) {
		if(left.getPosition().z == right.getPosition().z) {
			return true;
		}
		return false;
	}

	@Override
	public int compare(GameObject left, GameObject right) {
		if( left.getPosition().z == right.getPosition().z) {
			if(left.getGroundHeight() < right.getGroundHeight()) {
				return 1;
			}else {
				return -1;
			}
		}
		if(left.getPosition().z > right.getPosition().z) {
			return 1;
		}
		return -1;
	}

}
