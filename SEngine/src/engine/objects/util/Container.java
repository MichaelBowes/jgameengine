package engine.objects.util;

import java.util.List;
import java.util.Observable;

import engine.utils.Tuple;
import javolution.util.FastTable;

public abstract class Container<T extends Container<T>> extends Observable{

	protected Container<T> parent;
	protected List<T> children = new FastTable<>();
	
	/**
	 * Add a {@link T} as child and set this {@link T}
	 * as its parent.
	 * @param object to be added as child.
	 */
	public void addChild(T object) {
		this.setChanged();
		if(object == null) {
			throw new IllegalArgumentException("Child object mustn't be null");
		}
		children.add(object);
		object.setParent(this);
		notifyObservers(new Tuple<Object, Object>(this, object));
	}
	
	/**
	 * Remove a {@link T} from the children and 
	 * set its parent {@link T} to null.
	 * @param object to be removed.
	 */
	public void removeChild(T object) {
		this.setChanged();
		if(object == null) {
			throw new IllegalArgumentException("Child object mustn't be null");
		}
		children.remove(object);
		object.setParent(null);
		notifyObservers(object);
	}
	
	/**
	 * Removes all children elements.
	 */
	public void clearChildren() {
		children.clear();
	}
	
	/**
	 * Returns true if the {@link T} is contained as child.
	 * @param object to check.
	 * @return true if the {@link T} is contained in the children list.
	 * false otherwise.
	 */
	public boolean hasChild(T object) {
		if(object == null) {
			throw new IllegalArgumentException("Child object mustn't be null");
		}
		if(children.contains(object)) {
			return true;
		}
		return false;
	}
	
	public void setParent(Container<T> object) {
		this.parent = object;
	}
	
	public Container<T> getParent() {
		return this.parent;
	}
	
	/**
	 * Returns a list of all children objects.
	 * @return list of children objects.
	 */
	public List<T> getChildren(){
		return children;
	}
	
	public abstract void delete();
}
