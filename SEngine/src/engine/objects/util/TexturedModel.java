package engine.objects.util;

import engine.graphics.Model;
import engine.textures.Texture;

/**
 * Interface for {@link Sprite} and {@link SpriteAnimation}, providing methods
 * for getting and setting {@link Model} and {@link Texture}s.
 */
public interface TexturedModel {

	public void update();
	public Model getModel();
	public Texture getTexture();
	public void setModel(Model model);
	public void setTexture(Texture texture);
	/**
	 * Deletes the model and the textures of the {@link TexturedModel}.<br>
	 * If another {@link TexturedModel} uses the resources it will also
	 * lose them.
	 */
	public void delete();
}
