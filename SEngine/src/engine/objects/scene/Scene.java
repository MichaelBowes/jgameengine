package engine.objects.scene;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import engine.objects.GameObject;
import engine.objects.Hitbox;
import engine.utils.Tuple;
import javolution.util.FastMap;
import javolution.util.FastTable;

public class Scene implements Observer {

	/**
	 * Contains all {@link GameObject}s of the {@link Scene}.
	 */
	private List<GameObject> gameObjects = new FastTable<GameObject>();
	private int maxHitboxSize = 100;
	/**
	 * Contains all {@link Hitbox}es of the {@link Scene}.
	 */
	private List<Hitbox> hitboxes = new FastTable<Hitbox>();
	/**
	 * Used to sort the {@link Hitbox}es for their spatial properties.
	 */
	private Map<Integer, List<GameObject>> spatialMap = new FastMap<Integer, List<GameObject>>();

	public void add(GameObject object) {
		if (object == null) {
			throw new IllegalArgumentException();
		}
		if (gameObjects.contains(object)) {
			System.err.println("Duplicate GameObject in Scene!");
		} else {
			gameObjects.add(object);
			object.addObserver(this);
			if (object.hasHitbox()) {
				hitboxes.add(object.getHitbox());
				if (object.getHitbox().getWidth() * 2 > maxHitboxSize) {
					maxHitboxSize = object.getHitbox().getWidth() * 2;
				}
				if (object.getHitbox().getHeight() * 2 > maxHitboxSize) {
					maxHitboxSize = object.getHitbox().getHeight() * 2;
				}
			}
		}
	}

	/**
	 * Checks all {@link GameObject}s {@link Hitbox}es for collisions. If a collion
	 * is detected, the <code>onCollision({@link GameObject} collider)</code> method
	 * within the colliding {@link GameObject}s will be called.#
	 */
	public void checkCollisions() {
		if (hitboxes.size() > 0) {
			for (Hitbox hb : hitboxes) {
				// Add corner values to map;
				int hash = hb.getTopLeftHash(maxHitboxSize);
				insertIntoHashMap(hb, hash);
				hash = hb.getTopRightHash(maxHitboxSize);
				insertIntoHashMap(hb, hash);
				hash = hb.getBottomLeftHash(maxHitboxSize);
				insertIntoHashMap(hb, hash);
				hash = hb.getBottomRightHash(maxHitboxSize);
				insertIntoHashMap(hb, hash);
			}
			for (Hitbox hb : hitboxes) {
				// Get all possible colliding elements
				int hash = hb.getTopLeftHash(maxHitboxSize);
				spatialMap.computeIfPresent(hash, (key, value) -> checkSpaceForCollision(hb, value));
				hash = hb.getTopRightHash(maxHitboxSize);
				spatialMap.computeIfPresent(hash, (key, value) -> checkSpaceForCollision(hb, value));
				hash = hb.getBottomLeftHash(maxHitboxSize);
				spatialMap.computeIfPresent(hash, (key, value) -> checkSpaceForCollision(hb, value));
				hash = hb.getBottomRightHash(maxHitboxSize);
				spatialMap.computeIfPresent(hash, (key, value) -> checkSpaceForCollision(hb, value));
			}
			spatialMap.clear();
		}
	}
	
	public boolean contains(GameObject object) {
		return gameObjects.contains(object);
	}

	/**
	 * Checks the given {@link Hitbox} for collisions with a list of
	 * {@link GameObject}s.
	 * 
	 * @param hb
	 *            - Hitbox to be checked.
	 * @param objects
	 *            - List of {@link GameObject}s with {@link Hitbox}es.
	 * @return the starting list without the {@link GameObject} whose hitbox has
	 *         just been checked.
	 */
	private List<GameObject> checkSpaceForCollision(Hitbox hb, List<GameObject> objects) {
		for (GameObject go : objects) {
			if (go.getHitbox() != hb) {
				if (checkObjectCollision(hb, go.getHitbox())) {
					hb.getParent().onCollision(go);
					go.onCollision(hb.getParent());
				} else {
					hb.getParent().onCollisionExit(go);
					go.onCollisionExit(hb.getParent());
				}
			}
		}
		objects.remove(hb.getParent());
		return objects;
	}
	
	/**
	 * Checks if two {@link Hitbox}es are colliding.
	 * 
	 * @param hb1
	 * @param hb2
	 * @return
	 */
	private boolean checkObjectCollision(Hitbox hb1, Hitbox hb2) {
		if (hb1.level == hb2.level) {
			//System.out.println("check1: "+Math.abs(hb1.getPosition().x - hb2.getPosition().x)+" , "+hb1.getHalfWidth() + hb2.getHalfWidth());
			//System.out.println("check2: "+Math.abs(hb1.getPosition().y - hb2.getPosition().y)+" , "+hb1.getHalfHeight() + hb2.getHalfHeight());
			if (Math.abs(hb1.getPosition().x - hb2.getPosition().x) < hb1.getHalfWidth() + hb2.getHalfWidth()) {
				if (Math.abs(hb1.getPosition().y - hb2.getPosition().y) < hb1.getHalfHeight() + hb2.getHalfHeight()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Inserts the {@link Hitbox} into the map.
	 * 
	 * @param hitbox
	 * @param hash
	 */
	private void insertIntoHashMap(Hitbox hitbox, int hash) {
		List<GameObject> contained = spatialMap.get(hash);
		if (contained == null) {
			List<GameObject> list = new FastTable<GameObject>();
			list.add(hitbox.getParent());
			spatialMap.put(hash, list);
		} else {
			contained.add(hitbox.getParent());
		}
	}

	public void remove(GameObject object) {
		if (object == null) {
			throw new IllegalArgumentException();
		}
		gameObjects.remove(object);
		if (object.hasHitbox()) {
			hitboxes.remove(object.getHitbox());
		}
	}

	public List<GameObject> getObjects() {
		return gameObjects;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		GameObject sender = (GameObject) arg0;
		// Notify if hitbox was removed or added.
		if (arg1 instanceof Hitbox) {
			Hitbox hb = (Hitbox) arg1;
			if (sender.hasHitbox()) {
				hitboxes.remove(hb);
			} else {
				hitboxes.add(hb);
			}
		} else {
			// Hitbox was changed for a new hitbox.
			if (arg1 instanceof Tuple<?, ?>) {
				Tuple<?, ?> tuple = (Tuple<?, ?>) arg1;
				if (tuple.x instanceof Hitbox) {
					Hitbox oldHb = (Hitbox) tuple.x;
					Hitbox newHb = (Hitbox) tuple.y;
					hitboxes.add(newHb);
					hitboxes.remove(oldHb);
				}
			}
		}
	}

}
