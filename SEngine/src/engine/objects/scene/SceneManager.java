package engine.objects.scene;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import engine.objects.GameObject;
import engine.objects.Light;
import engine.objects.fonts.TextObject;
import engine.textures.Texture;
import engine.utils.Tuple;
import javolution.util.FastTable;

public class SceneManager implements Observer {
	
	public static final int MAX_LIGHTS = 10;
	private Scene uiScene = new Scene();
	private Scene gameScene = new Scene();
	
	private List<Light> lights = new FastTable<Light>();
	
	public void addLight(Light light) {
		if(light == null) {
			throw new IllegalArgumentException("Can't add light that is null.");
		}
		lights.add(light);
	}
	
	public List<Light> getLights(){
		return lights;
	}
	
	/**
	 * Adds a {@link GameObject} to the game scene.
	 * Objects in the UI scene will not be affected by the camera.
	 * @param object
	 */
	public void addUIObject(GameObject object) {		
		if(object == null) {
			throw new IllegalArgumentException("GameObject is null!");
		}
		if(uiScene.contains(object)) {
			System.err.println("Object is already contained in the scene.");
		}else {
			if(!(object instanceof TextObject)) {
				registerObject(object);		
			}
			uiScene.add(object);
			//Only add children of game objects that are no text objects.
			if(!(object instanceof TextObject)) {
				for(GameObject child : object.getChildren()) {
					addUIObject(child);
				}
			}
		}
	}
	
	/**
	 * Adds a {@link GameObject} to the game scene.
	 * Objects in the game scene will be affected by the camera.
	 * @param object
	 */
	public void addGameObject(GameObject object) {
		if(object == null) {
			throw new IllegalArgumentException("GameObject is null!");
		}
		if(gameScene.contains(object)) {
			System.err.println("Object is already contained in the scene.");
		}else {
			if(!(object instanceof TextObject)) {
				registerObject(object);		
			}		
			gameScene.add(object);
			//Only add children of game objects that are no text objects.
			if(!(object instanceof TextObject)) {
				for(GameObject child : object.getChildren()) {
					addGameObject(child);
				}
			}			
		}		
	}
	
	public void removeUIObject(GameObject object) {
		if(object == null) {
			throw new IllegalArgumentException("GameObject is null!");
		}
		if(uiScene.contains(object)) {
			uiScene.remove(object);
			if(!(object instanceof TextObject)) {
				unregisterObject(object);	
			}
			if(!(object instanceof TextObject)) {
				for(GameObject child : object.getChildren()) {
					removeUIObject(child);
				}
			}		
		}	
	}
	
	public void removeGameObject(GameObject object) {
		if(object == null) {
			throw new IllegalArgumentException("GameObject is null!");
		}
		if(gameScene.contains(object)) {
			gameScene.remove(object);
			if(!(object instanceof TextObject)) {
				unregisterObject(object);	
			}
			if(!(object instanceof TextObject)) {
				for(GameObject child : object.getChildren()) {
					removeGameObject(child);
				}
			}	
		}		
	}

	/**
	 * Adds this class as observer to the object.
	 * If the object is no {@link TextObject}, its {@link Texture}s will be activated.
	 * @param object
	 */
	private void registerObject(GameObject object) {
		object.addObserver(this);
		if(!(object instanceof TextObject)) {
			activateTexture(object);			
		}
	}
	
	private void unregisterObject(GameObject object) {
		object.deleteObserver(this);
		if(!(object instanceof TextObject)) {
			deactivateTexture(object);	
		}		
	}
	
	private void activateTexture(GameObject object) {
		Texture texture = object.getTexture();
		if(texture != null) {
			texture.load();
		}
	}
	
	private void deactivateTexture(GameObject object) {
		Texture texture = object.getTexture();
		if(texture != null) {
			texture.unload();
		}
	}
	
	public Scene getUIScene() {
		return uiScene;
	}
	
	public Scene getGameScene() {
		return gameScene;
	}
	
	public void update(double delta) {
		for(GameObject object : gameScene.getObjects()) {
			object.update(delta);
		}
		for(GameObject object : uiScene.getObjects()) {
			object.update(delta);
		}
	}
	
	public void setUIScene(Scene scene) {		
		for(GameObject newObject : scene.getObjects()) {
			registerObject(newObject);
		}
		for(GameObject oldObject : uiScene.getObjects()) {
			unregisterObject(oldObject);
		}
		this.uiScene = scene;
	}
	
	public void setGameScene(Scene scene) {	
		for(GameObject newObject : scene.getObjects()) {
			registerObject(newObject);
		}
		for(GameObject oldObject : gameScene.getObjects()) {
			unregisterObject(oldObject);
		}
		this.gameScene = scene;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		//If a child or object was deleted from a scene.
		if(arg1 instanceof GameObject) {
			GameObject object = (GameObject) arg1;
			if(gameScene.contains(object)) {
				removeGameObject(object);
			}else if(uiScene.contains(object)) {
				removeUIObject(object);
			}
		}
		if(arg1 instanceof Tuple) {
			Tuple<?,?> object = (Tuple<?,?>) arg1;
			//If the texture was changed.
			if(object.x instanceof Texture) {
				Texture oldTexture = (Texture) object.x;
				Texture newTexture = (Texture) object.y;
				oldTexture.unload();
				newTexture.load();
			}
			//if a child was added to a gameobject within the scene.
			if(object.x instanceof GameObject) {
				GameObject parent = (GameObject) object.x;
				GameObject child = (GameObject) object.y;
				//Check in what list the parent is contained.
				if(gameScene.contains(parent)) {
					//Check if the child is already in the scene.
					addGameObject(child);						
				}else if(uiScene.contains(parent)) {
					//Check if the child is already in the scene.
					addUIObject(child);						
				}
			}
		}
	}
	
}
