package engine.objects;

import engine.math.Vector2f;

public class Hitbox {

	/**
	 * The level determines if a collision can occur between two objects.
	 */
	public int level;
	private GameObject parent;
	private Vector2f center = new Vector2f(0, 0);
	private int width;
	private int height;
	private int halfWidth;
	private int halfHeight;
	private Vector2f topLeftPosition = new Vector2f();

	private Vector2f bottomLeftCornerPosition = new Vector2f();
	private Vector2f topRightCornerPosition = new Vector2f();
	private Vector2f BottomRightCornerPosition = new Vector2f();

	public Hitbox(GameObject parent, int width, int height) {
		this.parent = parent;
		this.width = width;
		this.height = height;
		update();
	}

	private void calculateCornerPositions() {
		int leftSide = (int) (center.x - (width / 2));
		int rightSide = (int) (center.x + (width / 2));
		int bottomSide = (int) (center.y - (height / 2));
		int topSide = (int) (center.y + (height / 2));

		halfWidth = width / 2;
		halfHeight = height / 2;

		topLeftPosition.x = leftSide;
		topLeftPosition.y = topSide;

		bottomLeftCornerPosition.x = leftSide;
		bottomLeftCornerPosition.y = bottomSide;

		topRightCornerPosition.x = rightSide;
		topRightCornerPosition.y = topSide;

		BottomRightCornerPosition.x = rightSide;
		BottomRightCornerPosition.y = bottomSide;
	}

	public int getLevel() {
		return level;
	}
	
	public void update() {
		if(parent.getTexture() != null) {
			float width = (float)parent.getTexture().getWidth() * parent.getScale().x;
			float height = (float)parent.getTexture().getHeight() * parent.getScale().y;
			this.width = (int)width;
			this.height = (int)height;
			calculateCornerPositions();
		}
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getHalfWidth() {
		return halfWidth;
	}

	public int getHalfHeight() {
		return halfHeight;
	}

	public Vector2f getPosition() {
		return new Vector2f(parent.getAbsolutePosition().x + center.x, parent.getAbsolutePosition().y + center.y);
	}

	public void setCenter(float x, float y) {
		this.center.x = x;
		this.center.y = y;
	}

	/**
	 * Checks if a given {@link Vector2f} is inside the {@link Hitbox}.
	 * 
	 * @param position
	 *            to be checked.
	 * @return true if the position is inside.
	 */
	public boolean isInside(Vector2f position) {
		if (Math.abs(this.getPosition().x - position.x) < this.halfWidth) {
			if (Math.abs(this.getPosition().y - position.y) < this.halfHeight) {
				return true;
			}
		}
		return false;
	}

	private static int floor(int number, int size) {
		if (number < 0) {
			return (int) (Math.floor((number - size) / size) * size);
		} else {
			return (int) (Math.floor(number / size) * size);
		}
	}

	public int getTopLeftHash(int size) {
		return floor((int) (topLeftPosition.x + parent.getPosition().x), size) * 31
				+ floor((int) (topLeftPosition.y + parent.getPosition().y), size);
	}

	public int getBottomLeftHash(int size) {
		return floor((int) (bottomLeftCornerPosition.x + parent.getPosition().x), size) * 31
				+ floor((int) (bottomLeftCornerPosition.y + parent.getPosition().y), size);
	}

	public int getTopRightHash(int size) {
		return floor((int) (topRightCornerPosition.x + parent.getPosition().x), size) * 31
				+ floor((int) (topRightCornerPosition.y + parent.getPosition().y), size);
	}

	public int getBottomRightHash(int size) {
		return floor((int) (BottomRightCornerPosition.x + parent.getPosition().x), size) * 31
				+ floor((int) (BottomRightCornerPosition.y + parent.getPosition().y), size);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
		calculateCornerPositions();
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
		calculateCornerPositions();
	}

	public GameObject getParent() {
		return parent;
	}

	public void setParent(GameObject parent) {
		this.parent = parent;
	}

	public Vector2f getTopLeftCornerPosition() {
		return topLeftPosition;
	}

	public Vector2f getBottomLeftCornerPosition() {
		return bottomLeftCornerPosition;
	}

	public Vector2f getTopRightCornerPosition() {
		return topRightCornerPosition;
	}

	public Vector2f getBottomRightCornerPosition() {
		return BottomRightCornerPosition;
	}

}
