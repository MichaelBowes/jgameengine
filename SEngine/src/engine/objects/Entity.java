package engine.objects;

import engine.Engine;
import engine.display.Camera;
import engine.display.Window;
import engine.math.Matrix4f;
import engine.math.Vector3f;
import engine.math.Vector4f;
import engine.objects.util.Container;
import engine.renderer.Renderer;

public abstract class Entity<T extends Container<T>> extends Container<T>{

	protected Vector3f position = new Vector3f(0f, 0f, 0f);
	protected Vector3f rotation = new Vector3f(0f, 0f, 0f);
	protected Vector3f scale = new Vector3f(1, 1, 1);
	/**
	 * Color that gets added to the texture.
	 */
	private Vector4f addedColor = new Vector4f(0, 0, 0, 0);
	private Matrix4f finalTransformationMatrix;
	private Matrix4f ownTransformationMatrix;
	private boolean hidden;
	private boolean parentMatrixChanged = true;
	protected boolean transformationChanged;
	
	public Entity(Vector3f position) {
		super();	
		this.setPosition(position);		
	}		
	
	public void parentMatrixChanged() {
		parentMatrixChanged = true;
	}
	
	private void notifyChildrenMatrixChanged() {
		for(T child : children) {
			((Entity<T>) child).parentMatrixChanged();
		}
	}
	
	/**
	 * Returns the transformation matrix of the {@link Entity}.
	 * The matrix will only be calculated, if the scale, position or rotation
	 * of the {@link Entity} or its parent have changed.<br>
	 * Else a cached matrix will be returned.
	 * @return transformation matrix.
	 */
	public Matrix4f getTransformationMatrix() {	
		if(transformationChanged) { //Check if own matrix has to be calculated.
			ownTransformationMatrix = Matrix4f.createTransformationMatrix(
					position,
					rotation,
					scale);	
			notifyChildrenMatrixChanged();
		}
		//Check if parent exists.
		if(this.parent != null) {
			if(parentMatrixChanged || transformationChanged) { //Matrix of parent or own has changed.
				Entity<T> parentObj = (Entity<T>) this.parent;
				finalTransformationMatrix = parentObj.getTransformationMatrix().multiply(ownTransformationMatrix);
				if(!transformationChanged) {
					notifyChildrenMatrixChanged();					
				}				
			}			
		}else {
			finalTransformationMatrix = ownTransformationMatrix;
		}
		transformationChanged = false;
		parentMatrixChanged = false;
		return finalTransformationMatrix;
	}
	
	/**
	 * Adds the values to the rotation of the {@link Entity}.
	 * @param x - Rotation on the x axis.
	 * @param y - Rotation on the y axis.
	 * @param z - Rotation on the z axis.
	 */
	public void changeRotation(float x, float y, float z) {
		this.rotation.x += x * Renderer.getDelta();
		this.rotation.y += y * Renderer.getDelta();
		this.rotation.z += z * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Adds the values to the rotation of the {@link Entity}.
	 * @param z - Rotation on the z axis.
	 */
	public void changeRotation(float z) {
		this.rotation.z += z * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Adds the {@link Vector3f} to the rotation of the Object.
	 * @param vector to be added.
	 */
	public void changeRotation(Vector3f vector) {
		this.rotation.x += vector.x * Renderer.getDelta();
		this.rotation.y += vector.y * Renderer.getDelta();
		this.rotation.z += vector.z * Renderer.getDelta();
		transformationChanged = true;
	}	
	
	public abstract void update(double delta);
	public abstract void delete();

	/**
	 * Returns the position of the {@link Entity}.
	 * If the {@link Entity} has a parent it will add its position
	 * vector.
	 * @return Position + parent position.
	 */
	public Vector3f getPosition() {		
		return position;
	}
	
	/**
	 * Returns the absolute position of the {@link GameObject}.
	 * @return Absolute position.
	 */
	public Vector3f getAbsolutePositionWithZ() {
		if(parent == null) {
			return this.position;
		}
		GameObject parentObject = (GameObject) parent;
		return parentObject.getAbsolutePosition().add(this.position);
	}
	
	/**
	 * Returns the absolute position of the {@link GameObject} while ignoring the Z coordinate.
	 * @return Absolute position.
	 */
	public Vector3f getAbsolutePosition() {
		if(parent == null) {
			return this.position;
		}
		GameObject parentObject = (GameObject) parent;
		return parentObject.getAbsolutePosition().add(new Vector3f(this.position.x,this.position.y,parentObject.getPosition().z));
	}
	
	/**
	 * Sets the absolute position of the {@link GameObject}.
	 */
	public void setAbsolutePosition(float x, float y) {
		if(parent == null) {
			this.position = new Vector3f(x, y, 0.02f);
		} else {
			GameObject parentObject = (GameObject) parent;
			this.position = new Vector3f(x, y, this.getPosition().z).subtract(parentObject.getAbsolutePosition());
		}
		transformationChanged = true;
	}
	
	/**
	 * Adds the values to the position of the {@link Entity}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 * @param z - Position on the z axis.
	 */
	protected void changePosition(float x, float y, float z) {
		this.position.x += x * Renderer.getDelta();
		this.position.y += y * Renderer.getDelta();
		this.position.z += z * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Adds the values to the position of the {@link Entity}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 */
	protected void changePosition(float x, float y) {
		this.position.x += x * Renderer.getDelta();
		this.position.y += y * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Adds the values to the position of the {@link Entity},
	 * without adding delta time, for precise tile placements.
	 */
	protected void changePositionIgnoreDelta(float x, float y){
		this.position.x += x;
		this.position.y += y;
		transformationChanged = true;
	}
	
	/**
	 * Adds the {@link Vector3f} to the position of the Object.
	 * @param vector to be added.
	 */
	protected void changePosition(Vector3f vector) {
		this.position.x += vector.x * Renderer.getDelta();
		this.position.y += vector.y * Renderer.getDelta();
		this.position.z += vector.z * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Sets the position of the {@link Entity} to the given
	 * position.
	 * @param position - new coordinates.
	 */
	public void setPosition(float x, float y, float z) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		transformationChanged = true;
	}
	
	/**
	 * Sets the position of the {@link Entity} to the given
	 * position.
	 * @param position - new coordinates.
	 */
	public void setPosition(Vector3f position) {
		this.position.x = position.x;
		this.position.y = position.y;
		this.position.z = position.z;
		transformationChanged = true;
	}
	
	/**
	 * Sets the position of the {@link Entity} to the given
	 * position.
	 * @param x - new x coordinate.
	 * @param y - new y coordinate.
	 */
	protected void setPosition(float x, float y) {
		this.position.x = x;
		this.position.y = y;
		transformationChanged = true;
	}

	
	public void setRotation(Vector3f rotation) {
		this.rotation.x = rotation.x;
		this.rotation.y = rotation.y;
		this.rotation.z = rotation.z;
		transformationChanged = true;
	}
	
	/**
	 * Returns the rotation of the {@link Entity}.
	 * If the {@link Entity} has a parent it will add its rotation
	 * vector.
	 * @return Rotation + parent rotation.
	 */
	public Vector3f getRotation() {
		return rotation;
	}

	/**
	 * Returns the scale of the {@link Entity}.
	 * If the {@link Entity} has a parent it will add its scale
	 * vector.
	 * @return Scale + parent scale.
	 */
	public Vector3f getScale() {	
		return scale;
	}

	public void setScale(float scaleX, float scaleY) {
		this.scale = new Vector3f(scaleX, scaleY, 1);
		transformationChanged = true;
	}
	
	/**
	 * Sets the scale of the {@link Entity} to the given values.
	 * @param scale of the {@link Entity}
	 */
	public void setScale(Vector3f scale) {
		this.scale.x = scale.x;
		this.scale.y = scale.y;
		this.scale.z = scale.z;
		transformationChanged = true;
	}
	
	/**
	 * Adds the given {@link Vector3f} to the scale of the 
	 * {@link Entity}.
	 * @param scale to be added.
	 */
	public void addScale(Vector3f scale) {
		this.scale.x += scale.x * Renderer.getDelta();
		this.scale.y += scale.y * Renderer.getDelta();
		this.scale.z += scale.z * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Adds the given x and y values to the scale 
	 * {@link Vector3f} of the {@link Entity}.
	 * @param x scale factor.
	 * @param y scale factor.
	 */
	public void addScale(float x, float y) {
		this.scale.x += x * Renderer.getDelta();
		this.scale.y += y * Renderer.getDelta();
		transformationChanged = true;
	}
	
	/**
	 * Sets the color that is added to the texture and its children.
	 * @param r - red
	 * @param g - green
	 * @param b - blue
	 * @param a - alpha
	 */
	public void setAddedColor(float r, float g, float b, float a) {
		this.addedColor = new Vector4f(r, g, b, a);
		for(T child : children) {
			((Entity<T>) child).setAddedColor(r, g, b, a);
		}
	}
	
	/**
	 * Sets the color that is added to the texture and NOT its children.
	 * @param r - red
	 * @param g - green
	 * @param b - blue
	 * @param a - alpha
	 */
	public void setAddedColorNoChildren(float r, float g, float b, float a) {
		this.addedColor = new Vector4f(r, g, b, a);
	}
	
	/**
	 * Adds a color to the color that is added to the texture and its children.
	 * @param r - red
	 * @param g - green
	 * @param b - blue
	 * @param a - alpha
	 */
	public void addColor(float r, float g, float b, float a) {
		this.addedColor.add(new Vector4f(r, g, b, a));
		for(T child : children) {
			((Entity<T>) child).addColor(r, g, b, a);
		}
	}
	
	/**
	 * Adds a color to the color that is added to the texture and NOT its children.
	 * @param r - red
	 * @param g - green
	 * @param b - blue
	 * @param a - alpha
	 */
	public void addColorNoChildren(float r, float g, float b, float a) {
		this.addedColor.add(new Vector4f(r, g, b, a));
	}
	
	/**
	 * Removes the added color from the texture and its children.
	 */
	public void deleteAddedColor() {
		this.addedColor.x = 0;
		this.addedColor.y = 0;
		this.addedColor.z = 0;
		for(T child : children) {
			((Entity<T>) child).deleteAddedColor();
		}
	}
	
	/**
	 * Removes the added color from the texture and NOT its children.
	 */
	public void deleteAddedColorNoChildren() {
		this.addedColor.x = 0;
		this.addedColor.y = 0;
		this.addedColor.z = 0;
		for(T child : children) {
			((Entity<T>) child).deleteAddedColor();
		}
	}
	
	public Vector4f getColorVector() {
		return this.addedColor;
	}	
	
	/**
	 * Hides the object and will not render it.
	 */
	public void hide() {
		hidden = true;
	}
	
	/**
	 * Shows the object and will render it.
	 */
	public void show() {
		hidden = false;
	}
	
	/**
	 * @return true if the object is hidden.
	 */
	public boolean isHidden() {
		return hidden;
	}
	
	@Override
	public void setParent(Container<T> object) {
		super.setParent(object);
		parentMatrixChanged = true;
	}
	
	/**
	 * Gets the X position of the Entity within the camera coordinates.
	 * Ranges from 0 to Resolution width.
	 * @return 
	 */
	public double getPosX(Camera camera) {
		double x = this.getPosition().x; //Difference to origin in world coordinates
		double borderDistance = Window.getResolution().width()/2;
		double objPos = 0;
		
		//Correction factor for zooming camera in
		float z = Engine.getCamera().getPosition().z;
		
		double cameraR = camera.getPosition().x+borderDistance;
		double cameraL = camera.getPosition().x-borderDistance;
		if(x > cameraL && x < cameraR) {
				objPos = (x/z - cameraL);
		}
		return objPos;
	}
	
	/**
	 * Gets the Y position of the Entity within the camera coordinates.
	 * Ranges from 0 to Resolution height.
	 * @return 
	 */
	public double getPosY(Camera camera) {
		double y = this.getPosition().y; //Difference to origin in world coordinates
		double borderDistance = Window.getResolution().height()/2;
		double objPos = 0;
		
		//Correction factor for zooming camera in
		float z = Engine.getCamera().getPosition().z;
		
		double cameraTop = camera.getPosition().y+borderDistance;
		double cameraBot = camera.getPosition().y-borderDistance;
		if(y > cameraBot && y < cameraTop) {
			objPos = (y/z - cameraBot);
		}
		
		return objPos;
	}
}
