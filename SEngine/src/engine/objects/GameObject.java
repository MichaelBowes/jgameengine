package engine.objects;

import Exceptions.EngineException;
import engine.Engine;
import engine.graphics.shader.StandardShader;
import engine.math.Vector2f;
import engine.math.Vector3f;
import engine.textures.Sprite;
import engine.textures.Texture;
import engine.utils.Tuple;

/**
 * The {@link GameObject} class extends an {@link Entity} to
 * provide more functionality for an object represented in the game.
 *
 * TODO: Notify the scene if this object was deleted.
 */
public abstract class GameObject extends Entity<GameObject> {
	
	public static final float Z_FACTOR = Float.MIN_VALUE*100000;
	private StandardShader shader = new StandardShader();
	private Hitbox hitbox;
	private Light light;
	private float groundOffset = 0;
	private boolean yScaling = false;
	
	/**
	 * Sprite of which the texture will be rendered.
	 */
	private Sprite sprite;
	
	public GameObject() {
		super(new Vector3f(0, 0, 0));
	}
		
	public GameObject(Vector3f position) {
		super(position);
	}
	
	public GameObject(Sprite sprite) {
		super(new Vector3f(0, 0, 0));
		this.sprite = sprite;
	}
	
	public GameObject(Sprite sprite, Vector3f position) {
		super(position);
		this.sprite = sprite;
	}
	
	public void changeGroundOffset(float height) {
		this.groundOffset = height;
		scaleY();
	}
	
	public float getGroundHeight() {
		if(sprite != null) {
			return getPosition().y - sprite.getTexture().getHeight()/2 + groundOffset;
		}
		return 0;
	}
	
	public void setLight(Light light) {
		this.light = light;
	}
	
	public Light getLight() {
		return this.light;
	}
	
	public Texture getTexture() {
		if(sprite != null) {
			return sprite.getTexture();			
		}else {
			return null;
		}
	}
	
	public void setSprite(Sprite sprite) {
		this.setChanged();
		if(sprite == null) {
			throw new IllegalArgumentException("Sprite musntn't be null!");
		}
		this.sprite = sprite;
		//Notifying the scene manager about the old and new texture for memory management.
		notifyObservers(new Tuple<Texture, Texture>(this.sprite.getTexture(), sprite.getTexture()));			
	}
	
	public Sprite getSprite(){
		return sprite;
	}
	
	@Override
	public void update(double delta) {
		if(sprite != null) {
			sprite.update(delta);
		}
	}
		
	@Override
	public void delete() {
		shader.delete();
	}
	
	public void remove() {
		for(GameObject child : children) {
			child.remove();
		}
	}
	
	private void updateHitbox() {
		if(this.hitbox != null) {
			hitbox.update();
		}
	}
	
	@Override
	public void setScale(Vector3f scale) {
		super.setScale(scale);
		updateHitbox();
	}
	
	@Override
	public void setScale(float x, float y) {
		super.setScale(x, y);
		updateHitbox();
	}
	
	@Override
	public void addScale(Vector3f scale) {
		super.addScale(scale);
		updateHitbox();
	}
	
	@Override
	public void addScale(float x, float y) {
		super.addScale(x, y);
		updateHitbox();
	}
	
	public void changePosition(float x, float y) {
		super.changePosition(x, y);
		scaleY();
	}	
	
	public void changePositionIgnoreDelta(float x, float y) {
		super.changePositionIgnoreDelta(x, y);
		scaleY();
	}	
	
	public float getGroundOffset(){
		return groundOffset;
	}
	
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		scaleY();
	}
	
	public void setPosition(Vector3f position) {
		super.setPosition(position);
		scaleY();
	}
	
	public void changePosition(float x, float y, float z) {
		super.changePosition(x, y, z);
		scaleY();
	}
	
	public void changePosition(Vector3f position) {
		super.changePosition(position);
		scaleY();
	}
	
	/**
	 * Calculates the Z position from the y position of the
	 * {@link GameObject}.
	 * @param state
	 */
	public void setYScaling(boolean state) {
		this.yScaling = state;
		scaleY();
	}
	
	private void scaleY() {
		if(yScaling) {
			this.position.z = -(getGroundHeight() * Z_FACTOR);
			this.transformationChanged = true;
		}					
	}
	
	public abstract void onCollision(GameObject collider);
	
	public abstract void onCollisionExit(GameObject collider);

	public StandardShader getShader() {		
		return this.shader;
	}
	
	public void setShader(StandardShader shader) {
		this.shader = shader;
	}

	public Hitbox getHitbox() {
		return hitbox;
	}

	public void addHitbox(int width, int height) {
		setHitbox(new Hitbox(this, width, height));
	}
	
	public void addHitbox() {
		if(this.sprite == null) {
			throw new EngineException("Hitbox can't be added if no sprite is set or a size is given.");
		}
		setHitbox(new Hitbox(this, this.getTexture().getWidth(), this.getTexture().getHeight()));
	}	
	
	public boolean hasHitbox() {
		if(this.hitbox == null) {
			return false;
		}else {
			return true;
		}
	}
	
	private void setHitbox(Hitbox hitbox) {
		this.setChanged();
		if(this.hitbox == null) {
			notifyObservers(hitbox);
		}else {
			notifyObservers(new Tuple<Hitbox, Hitbox>(this.hitbox, hitbox));
		}
		this.hitbox = hitbox;
		hitbox.setParent(this);
	}
	
	public void removeHitbox() {
		this.setChanged();
		notifyObservers(hitbox);
		this.hitbox = null;
	}
	
	/**
	 * Checks if a given {@link Vector2f} is inside the {@link Hitbox}.
	 * 
	 * @param position
	 *            to be checked.
	 * @return true if the position is inside.
	 */
	public boolean mouseInsideHitbox(Vector2f position) {
		
		System.out.println(position.x+" "+position.y);
		System.out.println(this.getPosX(Engine.getCamera())+" "+this.getPosY(Engine.getCamera()));
		System.out.println(Math.abs(this.getPosX(Engine.getCamera()))+" "+Math.abs(this.getPosY(Engine.getCamera())));
		
		if (Math.abs(this.getPosX(Engine.getCamera()) - position.x) < this.getHitbox().getHalfWidth()) {
			if (Math.abs(this.getPosY(Engine.getCamera()) - position.y) < this.getHitbox().getHalfHeight()) {
				return true;
			}
		}
		return false;
	}

}
