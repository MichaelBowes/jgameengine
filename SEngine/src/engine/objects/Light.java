package engine.objects;

import engine.math.Vector3f;
import engine.renderer.Renderer;

public class Light {

	private GameObject parent;
	private Vector3f position = new Vector3f(0, 0, 0);
	private Vector3f color = new Vector3f(1, 1, 1);
	private Vector3f attenuation = new Vector3f(0.6f, 0.01f, 0.004f);
	private float brightness = 0.5f;
	private float radius = 10f;
	private boolean yScaling = false;
	
		
	public Vector3f getColor() {
		return color;
	}
	
	public void setColor(float r, float g, float b) {
		this.color.x = r;
		this.color.y = g;
		this.color.z = b;
	}
	
	public Vector3f getPosition() {
		if(parent == null) {
			return position;			
		}else {
			return position.add(parent.getPosition());
		}
	}
	
	public void setPosition(float x, float y) {
		this.position.x = x;
		this.position.y = y;
		scaleY();
	}
	
	public Vector3f getAttenuation() {
		return attenuation;
	}
	
	/**
	 * Calculates the Z position from the y position of the
	 * {@link GameObject}.
	 * @param state
	 */
	public void setYScaling(boolean state) {
		this.yScaling = state;
		scaleY();
	}
	
	private void scaleY() {
		if(yScaling) {
			this.position.z = -(getPosition().y * GameObject.Z_FACTOR);
		}					
	}
	
	public void setAttenuation(float value1, float value2, float value3) {
		if(value1 < 0) {
			this.attenuation.x = 0;
		}else {
			this.attenuation.x = value1;			
		}
		if(value2 < 0) {
			this.attenuation.y = 0;
		}else {
			this.attenuation.y = value2;			
		}if(value3 < 0) {
			this.attenuation.z = 0;			
		}else {
			this.attenuation.z = value3;
		}
	}
	
	public float getBrightness() {
		return brightness;
	}
	
	public void setBrightness(float brightness) {
		if(brightness < 0) {
			this.brightness = 0;
		}else {
			this.brightness = brightness;			
		}
	}
	
	public void changePosition(float x, float y) {
		this.position.x += x * Renderer.getDelta();
		this.position.y += y * Renderer.getDelta();
		scaleY();
	}
	
	public float getRadius() {
		return radius;
	}
	
	public void setRadius(float radius) {
		if(radius < 0) {
			this.radius = 0;
		}else {
			this.radius = radius;			
		}
	}
	
	
}
