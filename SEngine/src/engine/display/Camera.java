package engine.display;

import engine.math.Vector3f;
import engine.renderer.Renderer;

/**
 * Camera that defines what is displayed on the window.
 */
public class Camera {
	
	private Vector3f position = new Vector3f(0, 0, 1f);
	private float pitch;
	private float yaw;
	private float roll;
	
	public Camera() {
		
	}
	
	/**
	 * Adds the values to the position of the {@link Camera}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 * @param z - Position on the z axis.
	 */
	public void changePosition(float x, float y, float z) {
		this.position.x += x * Renderer.getDelta();
		this.position.y += y * Renderer.getDelta();
		this.position.z += z * Renderer.getDelta();
	}
	
	/**
	 * Adds the values to the position of the {@link Camera}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 */
	public void changePosition(float x, float y) {
		this.position.x += x * Renderer.getDelta();
		this.position.y += y * Renderer.getDelta();
	}
	
	/**
	 * Adds the values to the position of the {@link Camera}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 */
	public void changePositionIgnoreDelta(float x, float y) {
		this.position.x += x;
		this.position.y += y;
	}
	
	/**
	 * Adds the {@link Vector3f} to the position of the {@link Camera}.
	 * @param vector to be added.
	 */
	public void changePosition(Vector3f vector) {
		this.position.x += vector.x * Renderer.getDelta();
		this.position.y += vector.y * Renderer.getDelta();
		this.position.z += vector.z * Renderer.getDelta();
	}
	
	/**
	 * Changes the position values of the {@link Camera}.
	 * @param x - Position on the x axis.
	 * @param y - Position on the y axis.
	 * @param z - Position on the z axis.
	 */
	public void setPosition(float x, float y, float z) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
	}

	public void rotate(Vector3f vector) {
		this.pitch += vector.x * Renderer.getDelta();
		this.yaw += vector.y * Renderer.getDelta();
		this.roll += vector.z * Renderer.getDelta();
	}
	
	public void rotate(float x, float y, float z) {
		this.pitch += x * Renderer.getDelta();
		this.yaw += y * Renderer.getDelta();
		this.roll += z * Renderer.getDelta();
	}
	
	/**
	 * Rotates the camera around its x axis.
	 * @param angle of the rotation.
	 */
	public void tilt(float angle) {
		this.pitch += angle * Renderer.getDelta();
	}
	
	/**
	 * Rotates the camera around its y axis.
	 * @param angle of the rotation.
	 */
	public void pan(float angle) {
		this.yaw += angle * Renderer.getDelta();
	}
	
	/**
	 * Rotates the camera around its z axis.
	 * @param angle of the rotation.
	 */
	public void roll(float angle) {
		this.roll += angle * Renderer.getDelta();
	}
	
	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}
}
