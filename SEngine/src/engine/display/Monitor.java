package engine.display;

import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMonitorCallback;
import org.lwjgl.glfw.GLFWVidMode;

import engine.Engine;

public class Monitor extends GLFWMonitorCallback {
	
	/**
	 * The primary monitor.
	 */
	public static long MonitorHandle;
	/**
	 * The current video mode.
	 */
	public static GLFWVidMode videoMode;
	/**
	 * All currently connected monitors.
	 */
	public static PointerBuffer monitors;
	/**
	 * All video modes supported by the monitor.
	 */
	public static GLFWVidMode.Buffer videoModes;
	
	/**
	 * Physical screen width in millimeters.
	 */
	private static IntBuffer widthMM = BufferUtils.createIntBuffer(1);
	/**
	 * Physical screen height in millimeters.
	 */
	private static IntBuffer heightMM = BufferUtils.createIntBuffer(1);
	/**
	 * The dots per inch.
	 */
	private double DPI =  widthMM.get(0)/25.4;
	
	/**
	 * Virtual position of Monitor on the screen in screen coordinates.
	 */
	private static IntBuffer xPos = BufferUtils.createIntBuffer(1);
	private static IntBuffer yPos = BufferUtils.createIntBuffer(1);
	public static String name;
	
	public Monitor() {
		MonitorHandle = GLFW.glfwGetPrimaryMonitor();
		monitors = GLFW.glfwGetMonitors();
	}
	
	@Override
	public void invoke(long monitor, int event) {
		MonitorHandle = monitor;
		videoMode = GLFW.glfwGetVideoMode(monitor);
		videoModes = GLFW.glfwGetVideoModes(monitor);
		GLFW.glfwGetMonitorPhysicalSize(monitor, widthMM, heightMM);
		GLFW.glfwGetMonitorPos(monitor, xPos, yPos);
		name = GLFW.glfwGetMonitorName(monitor);
		
		if (event == GLFW.GLFW_CONNECTED)
	    {
	        // The monitor was connected
	    }
	    else if (event == GLFW.GLFW_DISCONNECTED)
	    {
	        // The monitor was disconnected
	    }
	}
	
	/**
	 * Changes the monitor to the selected one,
	 * adjusts the resolution and goes fullscreen mode.
	 */
	public static void changeMonitor(long Monitor, Resolution res) {
		GLFW.glfwSetWindowMonitor(Engine.getWindow().getWindowHandle(), Monitor, 0, 0, res.width(), res.height(), 1); 
	}

}
