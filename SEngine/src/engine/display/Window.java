package engine.display;

import java.nio.ByteBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;

import engine.Engine;
import engine.utils.GlfwImageParser;
import static org.lwjgl.glfw.GLFW.*;


public class Window {
	
	private long windowHandle;
	private boolean windowCreated;
	/**
	 * Resolution within the window.
	 */
	private static Resolution currentResolution = Resolution.SVGA;
	private String title = "";
	private static float aspectRatio;
	
	public Window(Resolution resolution) {
		currentResolution = resolution;
		aspectRatio = (float)currentResolution.aspect1() / (float)currentResolution.aspect2();
	}
	
	public void createWindow(boolean fullscreen) {		
		if(fullscreen) {
			windowHandle = GLFW.glfwCreateWindow(currentResolution.width(),
					currentResolution.height(), title, GLFW.glfwGetPrimaryMonitor(), 0); //Creating the window			
		}else {
			windowHandle = GLFW.glfwCreateWindow(currentResolution.width(),
					currentResolution.height(), title, 0, 0); //Creating the window			
		}			
		windowCreated = true;
	}
	
	public int getWindowWidth() {
		return currentResolution.width();
	}
	
	public int getWindowheight() {
		return currentResolution.height();
	}
	
	/**
	 * Sets the title of the game window.
	 * @param title of the window.
	 */
	public void setTitle(String newTitle) {
		if(title == null) {
			throw new IllegalArgumentException("Title mustn't be null");
		}
		title = newTitle;
	}
	
	public boolean windowCreated() {
		return windowCreated;
	}
	
	public long getWindowHandle() {
		return windowHandle;
	}
	
	public static Resolution getResolution() {
		return currentResolution;
	}
	
	public static float getAspectRatio() {
		return aspectRatio;
	}
	
	public void setResolution(Resolution resolution) {
		if(resolution == null) {
			throw new IllegalArgumentException("Resolution mustn't be null!");
		}
		currentResolution = resolution;
	}
	
	/**
	 * Changes window to fullscreen mode without changing its virtual size.
	 * For actual fullscreen, see {@link Engine}'s changeMonitor() or this classes 
	 * createWindow().
	 */
	public void goFullscreen() {
		GLFW.glfwMaximizeWindow(windowHandle);
	}
	/**
	 * Changes window to window mode without changing its virtual size.
	 */
	public void restoreWindow(Resolution res) {
		GLFW.glfwRestoreWindow(windowHandle);
	}
	
	/**
	 * Sets a custom cursor with the given image for whenever the cursor moves within the window boundaries.
	 * @param path where the picture is stored.
	 */
	public void setCursor(String path) {
		GLFWImage image = GLFWImage.malloc();
		try{
			GlfwImageParser parser = GlfwImageParser.loadImage(path);
	        image.set(parser.get_width(), parser.get_height(), parser.get_image());
			GLFW.glfwSetCursor(Engine.getWindow().getWindowHandle(), GLFW.glfwCreateCursor(image, 0, 0));
		} finally {
			GLFW.glfwDestroyCursor(GLFW.glfwCreateCursor(image, 0, 0));
			image.free();
		}
	}
	
	/**
	 * Sets an icon for the window. Preferably a size of 16x16 or 32x32.
	 * @param path where the icon is stored.
	 */
	public void setIcon(String path) {
		GLFWImage image = GLFWImage.malloc(); 
		try {
			GlfwImageParser parser = GlfwImageParser.loadImage(path);
			GLFWImage.Buffer imagebf = GLFWImage.malloc(1);
	        image.set(parser.get_width(), parser.get_height(), parser.get_image());
	        imagebf.put(0, image);
	        glfwSetWindowIcon(this.getWindowHandle(), imagebf);
		} finally {
			image.free();
		}
	}
	
}
