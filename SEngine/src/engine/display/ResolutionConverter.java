package engine.display;

import javafx.util.StringConverter;

public class ResolutionConverter extends StringConverter<Resolution> {

	@Override
	public Resolution fromString(String string) {
		String[] args = string.split(" x ");
		int width = Integer.parseInt(args[0]);
		Resolution result = null;
		switch(width) {
		case 800:
			result = Resolution.SVGA;
			break;		
		case 1024:
			result = Resolution.XGA;
			break;
		case 1280:
			result = Resolution.HD;
			break;			
		case 1360:
			result = Resolution.HDPLUS;
			break;
		case 1440:
			result = Resolution.WXGAPLUS;
			break;			
		case 1920:
			result = Resolution.FHD;
			break;
		case 2560:
			result = Resolution.WQHD;
			break;
		case 3840:
			result = Resolution.UHD;
			break;
		}
		return result;
	}

	@Override
	public String toString(Resolution object) {
		String result = object.width() + " x " + object.height()
			+ " (" + object.aspect1() + ":" + object.aspect2() + ")";
		return result;
	}

}
