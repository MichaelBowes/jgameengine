package network.server.database;

import java.util.List;

import server.model.Character;

/**
 * User class representing a user's account.
 *
 */

public class User {

	public User(String id, String userId, String userPw, String lastLogin, String iP, String accountCreated, int cashCurrency) {
		super();
		this.id = id;
		this.userId = userId;
		this.userPw = userPw;
		this.lastLogin = lastLogin;
		IP = iP;
		this.accountCreated = accountCreated;
		this.characters = characters;
		this.cashCurrency = cashCurrency;
	}
	
	/**
	 * Unique identifier for the user
	 */
	private String id;
	/**
	 * User-chosen nickname
	 */
	private String userId;
	/**
	 * User-chosen password
	 */
	private String userPw;
	/**
	 * Last time the user logged in
	 */
	private String lastLogin;
	/**
	 * IP address the user last logged in from
	 */
	private String IP;
	/**
	 * Date the account was created on
	 */
	private String accountCreated;
	/**
	 * List of the user's Player characters
	 */
	private List<Character> characters; 
	/**
	 * The user's owned cash currency
	 */
	private int cashCurrency;
	
}
