package network.server.database;

import network.server.database.WorldMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import network.server.database.User;

public class Server {

	private String directory = System.getProperty("user.dir");
	private WorldMap world;
	private Set<User> registeredUsers = new LinkedHashSet<>();
	private Set<User> onlineUsers = new LinkedHashSet<>();

	public Server() {
		world = loadWorld();
		registeredUsers = loadRegisteredUsers(directory + "\\\\registeredUsers.txt");
		onlineUsers = loadOnlineUsers(directory + "\\\\onlineUsers.txt");
	}

	private WorldMap loadWorld() {
		WorldMap map = new WorldMap();
		
		return map;
	}

	private Set<User> loadRegisteredUsers(String file) {
		Set<User> users = new LinkedHashSet<>();
		Scanner read;
		try {
			read = new Scanner(new File(file));
			read.useDelimiter(",");
			String id = "", userId = "", userPw = "", lastLogin = "", Ip = "", accountCreated = "", characters = "",
					cashCurrency = "";

			while (read.hasNext()) {
				id = read.next();
				userId = read.next();
				userPw = read.next();
				lastLogin = read.next();
				Ip = read.next();
				accountCreated = read.next();
				cashCurrency = read.next();
				System.out.println(id + " " + userId + " " + userPw 
						+ " " + lastLogin + " " + Ip + " " +accountCreated+" "+ cashCurrency);
				users.add(new User(id, userId, userPw, lastLogin, Ip, accountCreated, Integer.parseInt(cashCurrency)));
			}
			read.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return users;
	}

	private Set<User> loadOnlineUsers(String file) {
		Set<User> online = new LinkedHashSet<>();
		Scanner read;
		try {
			read = new Scanner(new File(file));
			read.useDelimiter(",");
			String id = "", userId = "", userPw = "", lastLogin = "", Ip = "", accountCreated = "", characters = "",
					cashCurrency = "";

			while (read.hasNext()) {
				id = read.next();
				userId = read.next();
				userPw = read.next();
				lastLogin = read.next();
				Ip = read.next();
				accountCreated = read.next();
				cashCurrency = read.next();
				online.add(new User(id, userId, userPw, lastLogin, Ip, accountCreated, Integer.parseInt(cashCurrency)));
			}
			read.close();
			System.out.println("online: "+ online.size());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return online;
	}

	public WorldMap getWorld() {
		return world;
	}

	public void setWorld(WorldMap world) {
		this.world = world;
	}

	public Set<User> getRegisteredUsers() {
		return registeredUsers;
	}

	public void setRegisteredUsers(Set<User> registeredUsers) {
		this.registeredUsers = registeredUsers;
	}

	public Set<User> getOnlineUsers() {
		return onlineUsers;
	}

	public void setOnlineUsers(Set<User> onlineUsers) {
		this.onlineUsers = onlineUsers;
	}

}
