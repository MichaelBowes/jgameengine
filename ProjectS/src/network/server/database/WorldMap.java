package network.server.database;

import java.util.HashMap;
import java.util.LinkedHashMap;

import game.map.Map;
import game.map.MonsterContainer;
import game.map.Tile;
import game.map.pvpMode;
import game.map.spawnMode;

public class WorldMap {
	private HashMap<String, Map> world = new LinkedHashMap<>();
	
	public WorldMap(){
		createMap("Town1", spawnMode.DISABLE_SPAWN, pvpMode.ENABLED);
	}
	
	private void createMap(String name, spawnMode mode1, pvpMode mode2) {
		Tile[][] map = new Tile[50][50];
		MonsterContainer[] spawns = new MonsterContainer[5];
		Map m = new Map(name, map, spawns, mode1, mode2);
		world.put(name, m);
	}
	
}
