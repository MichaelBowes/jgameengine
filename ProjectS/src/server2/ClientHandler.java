package server2;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Observable;

public abstract class ClientHandler extends Observable implements Runnable {
	
	/**
	 * Disconnects the client from the server.
	 */
	public void disconnect() throws IOException, SQLException {
		this.setChanged();
		this.notifyObservers(this);
	}
	
	public abstract Socket getSocket();
	
	/**
	 * Sends an {@link Object} via the clients output stream.
	 * @param object
	 * @return true if the object was sent successfully.
	 */
	public abstract boolean sendObject(Object object) throws IOException;
	
}
