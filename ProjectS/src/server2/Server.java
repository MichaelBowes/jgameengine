package server2;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javolution.util.FastTable;
import server.config.ServerConfig;
import server.database.Database;
import server.exceptions.InitialisationException;
import server.util.Logger;

/**
 * Abstract server that connects to a database and accepts connections of 
 * the type {@link ClientHandler} or Parent classes of this class.
 *
 * @param <T> Type of client used.
 */
public abstract class Server<T extends ClientHandler> implements Runnable, Observer {

	private final Thread serverThread;
	protected ServerSocket serverSocket;
	protected Database database;
	private Connection connection;
	private List<T> clients = new FastTable<T>();
	private ExecutorService executors;
	private boolean isRunning;
	protected ServerConfig config;
	private boolean initialized;
	public static String gameServerIp;
	public static int gameServerPort;
	protected Logger logger;
	
	public Server(ServerConfig config, String threadName) {
		serverThread = new Thread(this, threadName);
		this.config = config;
		gameServerIp = config.getGameServerIp();
		gameServerPort = config.getGameServerPort();
	}
	
	/**
	 * Adds a client to the list of clients and observes it.
	 * The client is then executed on a separate thread.
	 * @param client
	 */
	protected void addClient(T client) {
		clients.add(client);
		client.addObserver(this);
		executors.execute(client);
	}
	
	public Logger getLogger() {
		if(initialized) {
			return logger;			
		}else {
			throw new InitialisationException("Can't access logger because server was not initialized");
		}
	}
	
	public List<String> getClientList(){
		List<String> clients = new FastTable<String>();
		for(ClientHandler client : getClients()) {
			clients.add(client.getSocket().getInetAddress().getHostAddress());
		}
		return clients;
	}
	
	protected List<T> getClients(){
		return clients;
	}
	
	/**
	 * Starts the server thread.
	 * @param transactionLoggerFilePath - path to the logger file for transactions.
	 * @param serverLoggerFilePath - path to the logger file for system messages.
	 */
	public void start(String transactionLoggerFilePath, String serverLoggerFilePath) {
		this.logger = new Logger(transactionLoggerFilePath, serverLoggerFilePath);
		database = new Database();
		
		try {
			serverSocket = createServerSocket(config.getPort(), InetAddress.getLocalHost());	
		} catch (IOException e) {
			System.err.println("Could not create server socket!");
			shutdown();
			throw new RuntimeException(e);
		}
		
		executors = Executors.newFixedThreadPool(config.getMaxPlayers());
		database.startUp();	
		try {
			connection = database.getConnection();
		} catch (SQLException e) {
			throw new InitialisationException("Server could not create database connection!");
		}
		isRunning = true;
		serverThread.start();
		initialized = true;
	}
	
	protected ServerSocket createServerSocket(int port, InetAddress ip) throws IOException {
		ServerSocket socket = null;
		InetSocketAddress serverAddress = new InetSocketAddress(ip, port);
		socket = new ServerSocket();
		socket.bind(serverAddress);
		socket.setSoTimeout(config.getTimeout());
		return socket;
	}
	
	protected Connection getConnection() {
		if(initialized) {
			return connection;			
		}else {
			throw new InitialisationException("Can't access connection to database before server was started!");
		}
	}
	
	public void shutdown() {
		isRunning = false;
		if(executors != null) {
			executors.shutdown();
		}		
		try {
			if(serverSocket != null) {
				serverSocket.close();				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(database != null) {
			database.close();			
		}
		if(logger != null) {
			logger.shutdown();			
		}
	}

	@Override
	public void run() {		
		while(isRunning){		
			T client = null;
			try {
				try{
					client = connect(serverSocket.accept());
					if(client!=null){
						addClient(client);
					}
				}catch(SocketTimeoutException e){
					//connection timed out
				}catch(SocketException e) {
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			for(ClientHandler client : clients){ //do we have to disconnect every client?
				disconnect(client);
			}
			if(!serverSocket.isClosed()) {
				serverSocket.close();				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}	
	
	public abstract T connect(Socket socket);
	
	public void disconnect(ClientHandler clientHandler) {
		if(clients.contains(clientHandler)){
			try {
				clientHandler.disconnect();
			} catch (IOException | SQLException e) {
				e.printStackTrace();
			}
			clients.remove(clientHandler);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof ClientHandler) {
			ClientHandler handler = (ClientHandler) arg;
			clients.remove(handler);
		}
		
	}
	
}
