package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Observable;

import javax.net.ssl.SSLSocketFactory;

import server.transferdata.SystemMessage;
import server.util.LoginData;
import server.util.LoginToken;

/**
 * @author BpZ
 * 
 *         Client that handels the connection to the login and game server.
 * 
 */
public abstract class Client extends Observable implements Runnable {

	private Thread connectionThread;
	private String loginServerIp;
	private int loginServerPort;
	private static final int TIMEOUT = 10000;
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private boolean running = true;
	private boolean isConnected = false;
	private LoginData userData;

	@Override
	public void run() {
		startConnection();
		if(isConnected) {
			onGameServerLogin();
			Object inputObject = null;
			try {
				while (running) {
					inputObject = input.readObject();
					processData(inputObject);
				}
			} catch (SocketException e) {
				 //Disconnected
			} catch (IOException e) {
				e.printStackTrace();
				reportError(new SystemMessage(
						"Connection to the server has been lost",
						SystemMessage.FAIL), e);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				reportError(new SystemMessage(
						"An error occured while communicating with the server",
						SystemMessage.FAIL), e);
				throw new RuntimeException("An error occured while reading data from the server");
			}
		}
		onDisconnect();
	}

	/**
	 * Sends an {@link Object} to the game server if a connection was established.
	 * 
	 * @param object
	 *            to be sent.
	 * @return true if the {@link Object} was sent successfully.
	 */
	public boolean sendObject(Object object) {
		if (isConnected) {
			try {
				output.writeObject(object);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				reportError(new SystemMessage(
						"An error occured while communicating with the server",
						SystemMessage.FAIL), e);
				return false;
			}
		}
		return false;
	}

	/**
	 * Gets called when an error occured.
	 * 
	 * @param message
	 * @param cause
	 *            of the error. Can be null!
	 */
	public abstract void reportError(SystemMessage message, Throwable cause);

	/**
	 * Gets called after the login server responded.
	 * 
	 * @param message
	 *            of the login server.
	 */
	public abstract void loginErrorResult(SystemMessage message);

	public abstract void onInvalidLoginToken(SystemMessage message);
	
	public abstract void onLoninServerLogin();

	/**
	 * Gets called after successful login into the game server.
	 */
	public abstract void onGameServerLogin();

	
	public abstract void onDisconnect();
	/**
	 * Gets called every time an object was received from the game server.
	 * 
	 * @param object
	 */
	public abstract void processData(Object object);

	/**
	 * Connects to the login server and if the connection and authentication was
	 * successful, connects to the game server.
	 * 
	 * @param username
	 *            for login authentication.
	 * @param password
	 *            for login authentication.
	 * @param loginServerIp
	 *            - ip address of the login server.
	 * @param loginServerPort
	 *            - port of the login server.
	 * @param client
	 *            - Client runnable on which the connection will be established.
	 */
	public void connect(String username, String password, String loginServerIp, int loginServerPort, Client client) {
		//Set connection data
		this.loginServerIp = loginServerIp;
		this.loginServerPort = loginServerPort;
		LoginData login = new LoginData();
		login.setUsername(username);
		login.setPassword(password);
		if(client != this) {
			throw new IllegalArgumentException();
		}
		userData = login;	
		//Start connection thread
		connectionThread = new Thread(client, "CLIENT_CONNECTION_THREAD");
		connectionThread.start();
	}
	
	private void startConnection() {
		Object inputObject = null;
		try {
			if (connectToLoginServer()) {
				onLoninServerLogin();
				// Send login data to login server.
				output.writeObject(userData);
				// Receive response from the login server.
				inputObject = input.readObject();
				processLoginServerRespose(inputObject);
			} else {
				reportError(new SystemMessage(
						"Could not connect to login server",
						SystemMessage.FAIL), null);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			reportError(new SystemMessage(
					"Invalid server output", SystemMessage.FAIL), e);
			disconnect();
		} catch (IOException e) {
			e.printStackTrace();
			reportError(new SystemMessage("Could not connect to login server",
					SystemMessage.FAIL), e);
			disconnect();
		}
	}

	public void disconnect() {
		running = false;
		if (socket != null) {
			if (!socket.isClosed()) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
					reportError(new SystemMessage(
							"IO error while closing the connection",
							SystemMessage.FAIL), e);
				}
			}
		}
		isConnected = false;
	}

	private void processLoginServerRespose(Object object) {
		// Received token of login server.
		if (object instanceof LoginToken) {
			LoginToken token = (LoginToken) object;
			try {
				// Try to connect to the game server.
				if (connectToGameServer(token.getServerIp(), token.getServerPort())) {
					// send the token and wait for response.
					output.writeObject(token);
					processGameServerResponse(input.readObject());
				} else {
					loginErrorResult(new SystemMessage("Could not connect to game server",
							SystemMessage.FAIL));
				}
			} catch (IOException e) {
				e.printStackTrace();
				reportError(new SystemMessage("Could not connect to game server",
						SystemMessage.FAIL), e);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				reportError(new SystemMessage("Version conflict",
						SystemMessage.FAIL), e);
			}
		} else if (object instanceof SystemMessage) {
			SystemMessage message = (SystemMessage) object;
			loginErrorResult(message);
		}
	}

	private void processGameServerResponse(Object object) {
		if (object instanceof SystemMessage) {
			SystemMessage message = (SystemMessage) object;
			if (message.getType() == SystemMessage.SUCCESS) {
				isConnected = true;				
			} else if (message.getType() == SystemMessage.FAIL) {
				onInvalidLoginToken(message);
				isConnected = false;
				disconnect();
			}
		}
	}

	protected boolean connectToGameServer(String ip, int port) throws IOException {
		closeSocket();
		socket = new Socket();
		socket.connect(new InetSocketAddress(ip, port), TIMEOUT);
		if (socket.isConnected()) {
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
		} else {
			return false;
		}
		return true;
	}

	protected boolean connectToLoginServer() throws IOException {
		System.setProperty("javax.net.ssl.trustStore", "res/keystore/serverkeystore");
		SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
		socket = ssf.createSocket(loginServerIp, loginServerPort);
		socket.setSoTimeout(TIMEOUT);
		if (socket.isConnected()) {		
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
		} else {
			return false;
		}
		return true;
	}

	private void closeSocket() throws IOException {
		if (socket != null) {
			socket.close();
		}
	}
}
