package projectS.gamedemo;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

public class Character extends GameObject {

	private String name = "";
	
	public Character(Sprite sprite) {
		super();
		setSprite(sprite);
	}
	
	public Character(Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void onCollision(GameObject collider) {
		Character other = (Character) collider;
		System.out.println("Collision between: " + name + " and " + other.getName());

	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
