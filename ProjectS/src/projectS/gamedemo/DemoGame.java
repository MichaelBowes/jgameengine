package projectS.gamedemo;

import java.io.FileNotFoundException;

import Exceptions.InvalidFileException;
import engine.Engine;
import engine.GameLogic;
import engine.display.Window;
import engine.graphics.shader.StandardShader;
import engine.display.Resolution;
import engine.input.Input;
import engine.input.MouseInput;
import engine.input.MouseInputListener;
import engine.math.Vector3f;
import engine.math.Vector4f;
import engine.textures.Sprite;
import engine.textures.SpriteAnimation;
import engine.textures.manager.TextureManager;
import engine.objects.GameObject;
import engine.objects.Light;
import engine.objects.fonts.Font;
import engine.objects.fonts.FontManager;
import engine.objects.fonts.TextObject;
import engine.objects.scene.SceneManager;

public class DemoGame implements GameLogic, MouseInputListener {
	
	private Engine engine;
	private SceneManager manager;
	private Character character1;
	private Character character2;
	private Character rabbit1;
	private Character rabbit2;
	private Character transparent;
	private Character tree;
	private Character crosshair;
	private TextObject text;
	private Font font;
	private Light light1;
	private Light light2;
	
	public void start() {
		engine = new Engine(Resolution.HD, this, 30);
		engine.setFrameSyncRate(60);
		engine.start();
	}
	
	@Override
	public void update() {
		movement();
		
		updateFps();
		if(dragging) {
			draggedObject.setPosition(
					MouseInput.getMousePosition().x,
					MouseInput.getMousePosition().y);
		}
	}	
	
	@Override
	public void init() {	
		//Loading texture files.
		try {
			TextureManager.readResourceFile("res/resources");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Load font
		text = new TextObject("Test", FontManager.getFont("GothicFont"));
		text.setPosition(Window.getResolution().width()/2,
				Window.getResolution().height() - text.getHeight());
		
		text.setAddedColor(-1f, -1f, -1f, 0);		
		
		
		createObjects();
		MouseInput.addListener(this);
		//createLights();
	}
	
	private void createObjects() {
		Sprite transparentSprite = TextureManager.getSprite("res/pictures/Black_Transparent.png", 0, 0);
		Sprite[] animationSprites = new Sprite[4];
		animationSprites[0] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		animationSprites[1] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 0);
		animationSprites[2] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 0);
		animationSprites[3] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		SpriteAnimation animation = new SpriteAnimation(animationSprites, 1000);
		
		
		Sprite transparentSprite2 = TextureManager.getSprite("res/pictures/Black_Transparent.png", 0, 0);
		Sprite[] animationSprites2 = new Sprite[4];
		animationSprites2[0] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		animationSprites2[1] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 0);
		animationSprites2[2] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 0);
		animationSprites2[3] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		SpriteAnimation animation2 = new SpriteAnimation(animationSprites, 1000);
				
		rabbit1 = new Character(animation, new Vector3f(100, 100, 0));
		rabbit1.setYScaling(true);
		rabbit2 = new Character(animation2, new Vector3f(150, 300, 0));
		rabbit2.setYScaling(true);
		rabbit2.setPosition(1, 1);
		
		//UI Element
		crosshair = new Character(TextureManager.getSprite("res/pictures/Crosshair.png", 0, 0));
		crosshair.addHitbox();
		
		Vector3f centerScreen = new Vector3f(Window.getResolution().width()/2, Window.getResolution().height()/2, 0);
		crosshair.setPosition(centerScreen);		
		
		character1 = new Character(transparentSprite, new Vector3f(100f, 0f, 0f));	
		character1.setYScaling(true);
		character1.addHitbox();
		character1.setName("Hans");
		
		character2 = new Character(transparentSprite, new Vector3f(-20f, 0f, 0f));
		character2.setYScaling(true);
		character2.setName("Werner");
		character2.addHitbox();	
		
		
		transparent = new Character(transparentSprite, new Vector3f(0,0,0));	
		GameObject transparent2 = new Character(animation, centerScreen);
		
		Sprite treeSprite = TextureManager.getSprite("res/pictures/Fir.png", 0, 0);
		tree = new Character(treeSprite, new Vector3f(-200, 40, 0.1f));
		tree.changeGroundOffset(20);
		tree.setYScaling(true);
		
		manager = Engine.getSceneManager();
		
		light1 = new Light();
		light1.setColor(1,1,1);
		light1.setBrightness(1);
		light1.setPosition(2.0f, 3.0f);
		light1.setYScaling(true);
		manager.addLight(light1);
		
		light2 = new Light();
		light2.setBrightness(1);
		light2.setYScaling(true);
		light2.setPosition(100,100);
		manager.addLight(light2);
		
		manager.addGameObject(tree);
		manager.addGameObject(rabbit1);
		//manager.addGameObject(rabbit2);
		manager.addGameObject(character1);		
		manager.addGameObject(character2);
		manager.addUIObject(crosshair);
		manager.addUIObject(text);
		renderMap(manager);		
	}
	
	private void createLights() {
		int step = 200;
		int firstRow = -100;
		int firstColumn = -400;
		for(int row = 0; row < 10; row++) {
			for(int column = 0; column < 10; column++) {
				Light l = new Light();
				l.setPosition(firstColumn, firstRow);
				manager.addLight(l);
				firstColumn += step;
			}
			firstColumn = -400;
			firstRow += 200;
		}
	}
	
	//render sprites
	private void renderMap(SceneManager manager) {
		float depth = -20;
		int displayWidth = Window.getResolution().width();
		int displayHeight = Window.getResolution().height();
		Sprite gras = TextureManager.getSprite("res/pictures/Gras_tile_1.png", 1, 1);
		int textureWidth = gras.getWidth();
		int textureHeight = gras.getHeight();
		int rows = displayHeight / textureHeight;
		int columns = displayWidth / textureWidth;
	
		Vector3f firstPosition = new Vector3f(
				 (textureWidth/2)-(displayWidth / 2),
				(textureHeight/2)-(displayHeight / 2),
				depth);

		Vector3f position = new Vector3f(
				(textureWidth/2)-(displayWidth / 2),
				(textureHeight/2)-(displayHeight / 2),
				depth);

		for(int r = 0; r < 100; r++) {
			for(int c = 0; c < 100; c++) {		
				Character newTile = new Character(gras);
				newTile.setPosition(new Vector3f(position.x, position.y, depth));
				manager.addGameObject(newTile);
				position.x += textureWidth;
			}
			position.x = firstPosition.x;
			position.y += textureHeight;
		}
	}
	
	//Dragging test
	private boolean dragging = false;
	private GameObject draggedObject;
	
	//Update fps text.
	private void updateFps() {
		text.setText(String.valueOf(Engine.getFps()));
	}
	
	//print position of object
	public void printPosition(GameObject obj) {
		System.out.println(obj.getPosition().x + " / " + obj.getPosition().y + " / " + obj.getPosition().z);
	}
	
	private void movement() {
		/*
		 * Change the position of the object with arrow keys.
		 * Prints the position of the object.
		 */
		if(Input.keys[Input.KEY_LEFT] && !Input.keys[Input.KEY_LEFT_SHIFT]) {
			rabbit1.changePosition(-0.1f, 0f);	
		}
		if(Input.keys[Input.KEY_RIGHT] && !Input.keys[Input.KEY_LEFT_SHIFT]) {
			rabbit1.changePosition(0.1f, 0f);
		}
		if(Input.keys[Input.KEY_UP] && !Input.keys[Input.KEY_LEFT_SHIFT]) {
			rabbit1.changePosition(0f, 0.1f);
		}
		if(Input.keys[Input.KEY_DOWN] && !Input.keys[Input.KEY_LEFT_SHIFT]) {
			rabbit1.changePosition(0f, -0.1f);	
		}
		if(Input.keys[Input.KEY_X]) {
			Engine.getSceneManager().addUIObject(rabbit2);
		}	
		if(Input.keys[Input.KEY_C]) {
			rabbit1.changePosition(new Vector3f(0, 0, 0.01f));
		}	
		
		if(Input.keys[Input.KEY_LEFT] && Input.keys[Input.KEY_LEFT_SHIFT]) {
			light1.changePosition(-0.5f, 0f);	
		}
		if(Input.keys[Input.KEY_RIGHT] && Input.keys[Input.KEY_LEFT_SHIFT]) {
			light1.changePosition(0.5f, 0f);
		}
		if(Input.keys[Input.KEY_UP] && Input.keys[Input.KEY_LEFT_SHIFT]) {
			light1.changePosition(0f, 0.5f);
		}
		if(Input.keys[Input.KEY_DOWN] && Input.keys[Input.KEY_LEFT_SHIFT]) {
			light1.changePosition(0f, -0.5f);	
		}
		
		if(Input.keys[Input.KEY_DELETE]) {
			System.out.println("Light "+light1.getPosition().z);
			System.out.println("Tree "+tree.getPosition().z);
			System.out.println(light1.getPosition().z > tree.getPosition().z);
		}
		
		if(Input.keys[Input.KEY_O]) {
			character2.changeRotation(0f, 0f, 0.1f);
		}
		if(Input.keys[Input.KEY_P]) {
			character2.changeRotation(0f, 0f, -0.1f);
		}
		if(Input.keys[Input.KEY_T]) {
			character2.changeRotation(0f, 0f, -0.1f);
		}
		/*
		 * Change the camera with WASD
		 */
		if(Input.keys[Input.KEY_W]) {
			Engine.getCamera().changePosition(0.0f, 0.1f, 0f);
		}
		if(Input.keys[Input.KEY_S]) {
			Engine.getCamera().changePosition(0.0f, -0.1f, 0f);
		}
		if(Input.keys[Input.KEY_A]) {
			Engine.getCamera().changePosition(-0.1f, 0.0f, 0f);
		}
		if(Input.keys[Input.KEY_D]) {
			Engine.getCamera().changePosition(0.1f, 0.0f, 0f);
		}	
		if(Input.keys[Input.KEY_KP_ADD]) {
			Engine.getCamera().changePosition(0.0f, 0.0f, -0.01f);
		}	
		if(Input.keys[Input.KEY_KP_SUBTRACT]) {
			Engine.getCamera().changePosition(0.0f, 0.0f, 0.01f);
		}	
		if(Input.keys[Input.KEY_L]) {
			Engine.getCamera().roll(0.9f);
		}	
		if(Input.keys[Input.KEY_K]) {
			Engine.getCamera().roll(-0.9f);
		}	
		if(Input.keys[Input.KEY_SPACE]) {
			Engine.getCamera().tilt(0.9f);
		}	
		if(Input.keys[Input.KEY_INSERT]) {
			StandardShader.setGeneralColorAddition(new Vector4f(-0.9f, -0.9f, -0.9f, 0f));
		}
	}
	
	@Override
	public void cleanUp() {
	}

	@Override
	public void onMouseDown(int button) {
		if(button == MouseInput.LEFT_MOUSE_BUTTON) {
			//If it is inside a hitbox.
			for(GameObject go : manager.getUIScene().getObjects()) {
				if(go.hasHitbox()) {
					if(go.getHitbox().isInside(MouseInput.getMousePosition())) {
						dragging = true;
						draggedObject = go;
					}
				}
			}
		}
	}

	@Override
	public void onMouseDoubleClick(int button) {
		
	}

	@Override
	public void onMouseReleased(int button) {		
		if(button == MouseInput.LEFT_MOUSE_BUTTON) {
			//If it is inside a hitbox.
			if(dragging) {
				dragging = false;
				draggedObject = null;
			}
		}
	}
}
