package projectS.model.Map.Algorithms;

import game.map.random.*;

//Tests the semi-random generation.
public class MainTest {
	public static void main(String... args){
		
		System.out.println("north:");
		int[][] north = new int[9][9];
		for (int i=0;i<north.length;i++) {
			for (int j=0;j<north.length;j++) {
				if(i==0) {
					north[i][j] = 4;
				}
				else if(i==8) {
					north[i][j] = 6;
					if(j==4) {north[i][j]=2;}
				} else {
				north[i][j] = 5;
				}
				System.out.print(north[i][j]);
			}
			System.out.println("");
		}
		System.out.println("");
		System.out.println("west:");
		int[][] west = new int[9][9];
		for (int i=0;i<west.length;i++) {
			for (int j=0;j<west.length;j++) {
				if(j==0) {
					west[i][j] = 1;
				}
				else if(j==8) {
					west[i][j] = 3;
					if(i==4) {west[i][j]=2;}
				} else {
				west[i][j] = 5;
				}
				System.out.print(west[i][j]);
			}
			System.out.println("");
		}
		
		DiamondSquareAlgorithm3 algo = new DiamondSquareAlgorithm3(north, west);
		printMap(algo.getMap());
	  }
	
	//prints the map to console
	private static void printMap(int[][] map) {
		System.out.println("PRINTED MAP:");
		for (int i=0;i<map.length;i++) {
			for (int j=0;j<map.length;j++) {
				System.out.print(map[i][j]+",");
			}
			System.out.println("");
		}
	}
}
