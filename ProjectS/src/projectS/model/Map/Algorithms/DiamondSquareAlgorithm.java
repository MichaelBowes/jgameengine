package projectS.model.Map.Algorithms;


/**
 * 
 * My first interpretation of the DiamondSquareAlgorithm.
 *
 */
public class DiamondSquareAlgorithm {
	
	int width;
	int height;
	int w;
	int h;
	int featuresize;
	double[] values;
	
	public DiamondSquareAlgorithm(){
		this.values = new double[width * height];	
	}
	
	public double sample(int x, int y)
	{
	    return values[(x + (width - 1)) + (y + (height - 1)) * w];
	}
	 
	public void setSample(int x, int y, double value)
	{
	    values[(x + (width - 1)) + (y + (height - 1)) * width] = value;
	}
	 
	public void dosmth(){
		for( int y = 0; y < height; y += featuresize)
		    for (int x = 0; x < width; x += featuresize)
		    {
		        setSample(x, y, Math.random());  //IMPORTANT: frand() is a random function that returns a value between -1 and 1.
		    }
	}
	
	public void sampleSquare(int x, int y, int size, double value)
	{
	    int hs = size / 2;
	 
	    // a     b 
	    //
	    //    x
	    //
	    // c     d
	 
	    double a = sample(x - hs, y - hs);
	    double b = sample(x + hs, y - hs);
	    double c = sample(x - hs, y + hs);
	    double d = sample(x + hs, y + hs);
	 
	    setSample(x, y, ((a + b + c + d) / 4.0) + value);
	 
	}
	 
	public void sampleDiamond(int x, int y, int size, double value)
	{
	    int hs = size / 2;
	 
	    //   c
	    //
	    //a  x  b
	    //
	    //   d
	 
	    double a = sample(x - hs, y);
	    double b = sample(x + hs, y);
	    double c = sample(x, y - hs);
	    double d = sample(x, y + hs);
	 
	    setSample(x, y, ((a + b + c + d) / 4.0) + value);
	}
	
	void DiamondSquare(int stepsize, double scale)
	{
	 
	    int halfstep = stepsize / 2;
	 
	    for (int y = halfstep; y < h + halfstep; y += stepsize)
	    {
	        for (int x = halfstep; x < w + halfstep; x += stepsize)
	        {
	            sampleSquare(x, y, stepsize, Math.random() * scale);
	        }
	    }
	 
	    for (int y = 0; y < h; y += stepsize)
	    {
	        for (int x = 0; x < w; x += stepsize)
	        {
	            sampleDiamond(x + halfstep, y, stepsize, Math.random() * scale);
	            sampleDiamond(x, y + halfstep, stepsize, Math.random() * scale);
	        }
	    }
	 
	}
	
	public void createMap(){
		int samplesize = featuresize;
		 
		double scale = 1.0;
		 
		while (samplesize > 1)
		{
		 
		    DiamondSquare(samplesize, scale);
		 
		    samplesize /= 2;
		    scale /= 2.0;
		}
	}
	
}