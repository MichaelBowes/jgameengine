package server.gameserver;

import java.net.Socket;
import java.sql.SQLException;
import java.util.Timer;

import server2.Server;
import server.config.ServerConfig;
import server.database.commands.Read;
import server.exceptions.InitialisationException;
import server.logic.WorldServer;

public class GameServer extends Server<PlayerHandler> {

	/**
	 * Login key used to verify the token information.
	 */
	public static String LOGIN_KEY;
	/**
	 * Rate at which the world server updates.
	 */
	private static final int TICKRATE = 100;
	private WorldServer worldServer;
	private Timer worldTimer;
	
	public GameServer(ServerConfig config) {
		super(config, "GAME_SERVER_THREAD");
	}
	
	public void start() {
		super.start("res/log/GSTransactions.txt", "res/log/GSMessages.txt");
		LOGIN_KEY = readLoginKey();
		worldServer = new WorldServer();
		try {
			worldServer.initialize(database.getConnection());
		} catch (SQLException e) {
			throw new InitialisationException("Could not create database connection for world server.");
		}
		worldTimer = new Timer();
		worldTimer.schedule(worldServer, 0, TICKRATE);
	}
	
	private String readLoginKey() {
		String key = Read.getLoginKey(getConnection(), 1);
		if(key == null) {
			logger.logServerMessage("Could not read login key");
			throw new InitialisationException("Could not read login key");
		}
		return key;
	}
	
	@Override
	public PlayerHandler connect(Socket socket) {
		PlayerHandler client = null;
	    try {
			client = new PlayerHandler(socket, database.getClientConnection(), logger);
			logger.logServerMessage(socket.getInetAddress().getHostAddress() + " connected");
		} catch (SQLException e) {
			logger.logServerMessage("Could not create connection for socket " + socket.getInetAddress());
			e.printStackTrace();
			return null;
		}
		return client;
	}
	
	@Override
	public void shutdown() {
		super.shutdown();
		worldTimer.cancel();
	}

}
