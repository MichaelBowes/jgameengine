package server.gameserver;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.mindrot.jbcrypt.BCrypt;

import server2.ClientHandler;
import server2.Server;
import server.database.commands.Insert;
import server.database.commands.Read;
import server.logic.WorldServer;
import server.model.Player;
import server.transferdata.ChatMessage;
import server.transferdata.PlayerRestriction;
import server.transferdata.SystemMessage;
import server.util.Logger;
import server.util.LoginToken;
import server.util.Vector2f;
import server.model.Building;
import server.model.Character;

/**
 *         Handles the requests for a single client.
 *
 */
public class PlayerHandler extends ClientHandler {

	private static final int TOKEN_TIME_LIMIT = 1;
	private Socket socket;
	private Connection connection;
	private boolean connected = true;
	private Player player;
	private ObjectOutputStream output;
	private ObjectInputStream inputStream;
	private Logger logger;
	private boolean isIngame;

	public PlayerHandler(Socket socket, Connection connection, Logger logger) {
		this.socket = socket;
		this.connection = connection;
		this.logger = logger;
	}

	public Socket getSocket() {
		return socket;
	}

	@Override
	public void run() {
		// Receive login
		Object input;
		try {
			inputStream = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());
			while (socket.isConnected() && connected) {
				// receive information
				try {
					input = inputStream.readObject();
					processData(input);

				} catch (SocketException | EOFException e) {
					// Connection cut
					connected = false;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Close the connection
		try {
			disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processData(Object object) {
		// If logged in
		if (player != null) {
			if (isIngame) {
				if (object instanceof Vector2f) {
					// Update position
				} else if (object instanceof Integer) {
					// Update direction
				} else if (object instanceof Building) {
					// Placed building
				} else if ((object instanceof String) && player.getType() == Player.PLAYER) {
					String content = (String) object;
					ChatMessage message = new ChatMessage(player, content, DateTime.now());
					WorldServer.sendChatMessage(message);
				}
			}
			// Admin commands
			if (player.getType() != Player.PLAYER) {
				if (player.getType() == Player.GAMEMASTER || player.getType() == Player.ADMIN) {
					processAdminData(object);
				}
			}
		} else {// Confirm login
			if (object instanceof LoginToken) {
				LoginToken token = (LoginToken) object;
				// Verify token
				if (verifyToken(token, socket.getInetAddress())) {
					// On successfully verifying the token load the account
					if (WorldServer.isConnected(token.getUsername())) {
						WorldServer.disconnectPlayer(token.getUsername(), "Duplicate login");
					}
					this.player = loadAccount(token.getUsername());
					// And send success message to client
					sendObject(new SystemMessage(null, SystemMessage.SUCCESS));
					WorldServer.addPlayer(this);
					sendInitialInformation();
					logger.logClientServerMessage(socket.getInetAddress(), "logged in");
				} else {
					sendObject(new SystemMessage("Token could not be verified.", SystemMessage.FAIL));
					logger.logClientServerMessage(socket.getInetAddress(), "could not verify token");
				}
			}
		}
	}

	/**
	 * Process data only for gamemasters or admins.
	 * @param object
	 */
	private void processAdminData(Object object) {
		//Normal chat message
		if ((object instanceof String)) {
			String content = (String) object;
			ChatMessage message = new ChatMessage(player, content, DateTime.now());
			WorldServer.sendChatMessage(message);

		} else if (object instanceof PlayerRestriction) {//Kick or ban
			PlayerRestriction restriciton = (PlayerRestriction) object;
			if (WorldServer.disconnectPlayer(restriciton.getPlayer(), restriciton.getReason())) {
				sendObject(new SystemMessage("Player '" + restriciton.getPlayer() + "' was kicked from the server",
						SystemMessage.INFO));
			} else if (restriciton.getType() == PlayerRestriction.KICK) {
				sendObject(new SystemMessage(
						"Player '" + restriciton.getPlayer() + "' could not be kicked from the server",
						SystemMessage.INFO));
			}
			// Create ban if necessary
			if (restriciton.getType() == PlayerRestriction.BAN) {
				Player banned = Read.getPlayer(connection, restriciton.getPlayer());
				if (banned != null) {
					Insert.addBan(connection, player.getUsername(), banned.getGetPlayerID(), restriciton.getReason(),
							restriciton.getDuration());
				} else {
					sendObject(
							new SystemMessage("Could not find player with the name '" + restriciton.getPlayer() + "'",
									SystemMessage.INFO));
				}
			}
		}
	}

	/**
	 * Returns true if the client is logged in.
	 * 
	 * @return
	 */
	public boolean isLoggenIn() {
		if (player == null) {
			return false;
		}
		return true;
	}

	private Player loadAccount(String username) {
		Player player = Read.getPlayer(connection, username);
		if (player == null) {
			String message = "Could not load player with username: '" + username + "' after successful login.";
			logger.logServerMessage(message);
		}
		return player;
	}

	@Override
	public boolean sendObject(Object object) {
		if (connected) {
			try {
				output.writeObject(object);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		return false;
	}

	@Override
	public void disconnect() throws IOException, SQLException {
		if (socket != null) {
			if (!socket.isClosed()) {
				socket.close();
				connection.close();
			}
		}
		super.disconnect();
		WorldServer.removePlayer(this);
		logger.logClientTransaction(socket.getInetAddress(), "disconnected");
	}

	/**
	 * Loads the inventory of the selected character.
	 * 
	 * @param character
	 * @return
	 */
	private Character selectCharacter(Character character) {
		if (player != null) {
			// Set character
			player.setCharacter(character);
			// Retrieve character items
			character.setInventory(Read.getInventoryItems(connection, character.getCharacterID()));
		}
		isIngame = true;
		return player.getCharacter();
	}

	public List<Character> loadCharacters() {
		if (player == null) {
			System.err.println("Cannot access characters of a client that is not logged in.");
			return null;
		}
		// Load characters
		List<Character> characters = Read.getGameCharacter(connection, player.getGetPlayerID());
		for (Character character : characters) {
			character.setEffects(Read.getCharacterEffect(connection, character.getCharacterID()));
		}
		return characters;
	}

	/**
	 * Verifies a given token.
	 * 
	 * @param token
	 *            received by the client.
	 * @param ip
	 *            of the client trying to connect.
	 * @return true if the token is valid and not expired.
	 */
	private static boolean verifyToken(LoginToken token, InetAddress ip) {
		// Check if elapsed time is smaller than 1 minute
		DateTime tokenTime = token.getDate();
		Interval elapsed = new Interval(tokenTime, new DateTime());
		if (elapsed.toDuration().getStandardMinutes() < TOKEN_TIME_LIMIT) {
			// Create own hash string for comparison.
			String hashString = token.getDate().toString() + token.getUsername() + ip.getHostAddress()
					+ Server.gameServerIp;
			String hash = BCrypt.hashpw(hashString, GameServer.LOGIN_KEY);
			if (token.getHash().equals(hash)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public void sendInitialInformation() {
		if (isIngame) {
			// Send mapdata and
		} else {
			if (player.getType() > Player.MODERATOR) {
				sendObject(WorldServer.getPlayers());
			}
		}
	}

	public void sendInformation() {
		if (isIngame) {
			// Send mapdata and
		}
		if (player.getType() > Player.MODERATOR) {

		}
	}

	public Player getPlayer() {
		return player;
	}

	/**
	 * Closes the connection for the player.
	 */
	public void kickPlayer(String reason) {
		sendObject(new SystemMessage(reason, SystemMessage.KICK));
		this.connected = false;
		logger.logClientTransaction(socket.getInetAddress(), "kicked. Reason: " + reason);
	}

}
