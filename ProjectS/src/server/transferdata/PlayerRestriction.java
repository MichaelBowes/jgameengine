package server.transferdata;

import java.io.Serializable;

public class PlayerRestriction implements Serializable {

	private static final long serialVersionUID = 6778954114769885644L;
	public static final int BAN = 0;
	public static final int KICK = 1;
	
	private int type;
	private int duration;
	private String username;
	private String reason;
	
	public PlayerRestriction(String username, String reason, int type) {
		this.username = username;
		this.reason = reason;
		this.type = type;
	}
	
	public String getPlayer() {
		return username;
	}
	public void setPlayer(String username) {
		this.username = username;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
