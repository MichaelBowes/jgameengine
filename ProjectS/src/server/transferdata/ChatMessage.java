package server.transferdata;

import java.io.Serializable;

import org.joda.time.DateTime;

import server.model.Player;

public class ChatMessage implements Serializable {

	private static final long serialVersionUID = -324956214354561902L;
	private boolean isPm;
	private String content;
	private Player sender;
	private DateTime date;
	
	public ChatMessage(Player sender, String content, DateTime date) {
		this.sender = sender;
		this.content = content;
		this.date = date;
	}
	
	public String getContent() {
		return content;
	}
	
	public Player getSender() {
		return sender;
	}
	
	public DateTime getDate() {
		return date;
	}

	public boolean isPm() {
		return isPm;
	}

	public void setPm(boolean isPm) {
		this.isPm = isPm;
	}
	
}
