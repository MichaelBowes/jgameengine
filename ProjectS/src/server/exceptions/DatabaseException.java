package server.exceptions;

public class DatabaseException extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3926611718903116666L;

	public DatabaseException() {
		super();
	}
	
	public DatabaseException(String message) {
		super(message);
	}
	
	public DatabaseException(Throwable cause) {
		super(cause);
	}
	
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}
