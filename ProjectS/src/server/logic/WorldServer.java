package server.logic;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import javolution.util.FastCollection;
import javolution.util.FastMap;
import javolution.util.FastTable;
import server.exceptions.InitialisationException;
import server.gameserver.PlayerHandler;
import server.model.Player;
import server.transferdata.ChatMessage;
import server.transferdata.PlayerConnection;

/**
 * Part of the Map-Centric Game Server pattern.
 *
 * The World server tracks player character location 
 * and orchestrates high level cross-region activity.
 * The World server assigns region maps to area servers.
 * It coordinates movement of player characters between region maps
 * and their area servers.
 *
 */
public class WorldServer extends TimerTask {

	private static FastMap<String,PlayerHandler> playerList = new FastMap<String,PlayerHandler>().parallel();
	private static FastCollection<PlayerHandler> admins = new FastTable<PlayerHandler>().parallel();
	private static WorldInformation worldInfo;
	private boolean initialized = false;
	
	public static void addPlayer(PlayerHandler handler) {
		playerList.put(handler.getPlayer().getUsername(), handler);
		for(PlayerHandler admin : admins) {
			admin.sendObject(new PlayerConnection(handler.getPlayer(), PlayerConnection.LOGIN));			
		}
		if(handler.getPlayer().getType() > Player.MODERATOR) {
			admins.add(handler);
		}
	}
	
	/**
	 * Removes a player from the worldserver without disconnecting.
	 * @param handler
	 */
	public static void removePlayer(PlayerHandler handler) {
		playerList.remove(handler.getPlayer().getUsername());
		admins.remove(handler);
		for(PlayerHandler admin : admins) {
			admin.sendObject(new PlayerConnection(handler.getPlayer(), PlayerConnection.LOGOUT));			
		}
	}
	
	/**
	 * Returns a list of all players that are currently logged in.
	 * @return
	 */
	public static List<Player> getPlayers(){
		List<Player> players = new FastTable<Player>();
		for(Map.Entry<String, PlayerHandler> entry : playerList.entrySet()) {
			players.add(entry.getValue().getPlayer());
		}		
		return players;
	}
	
	/**
	 * Checks if a player with the given username is currently connected.
	 * @param username
	 * @return
	 */
	public static boolean isConnected(String username) {
		return playerList.containsKey(username);
	}
	
	public void initialize(Connection connection) {
		worldInfo = new WorldInformation(connection);
		initialized = true;
	}
	
	public static void sendChatMessage(ChatMessage message) {
		for(PlayerHandler handler : playerList.values()) {
			handler.sendObject(message);
		}
	}
	
	/**
	 * Disconnects the given player from the server.
	 * @param player
	 * @param reason for the disconnect.
	 */
	public static boolean disconnectPlayer(String username, String reason) {		
		PlayerHandler playerHandler = playerList.get(username);
		if(playerHandler != null) {
			playerHandler.kickPlayer(reason);
			return true;
		}
		return false;
	}	
	
	@Override
	public void run() {
		if(!initialized) {
			throw new InitialisationException("Can't start world server before initializing it.");
		}
		sendPlayerInformation();		
	}
	
	private void sendPlayerInformation() {
		for(Map.Entry<String, PlayerHandler> entry : playerList.entrySet()) {
			entry.getValue().sendInformation();
		}		
	}

}
