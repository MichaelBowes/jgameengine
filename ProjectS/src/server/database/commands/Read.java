package server.database.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javolution.util.FastTable;
import server.model.Building;
import server.model.Character;
import server.model.Claim;
import server.model.Effect;
import server.model.InventoryItem;
import server.model.ItemInstance;
import server.model.Player;
import server.model.Structure;
import server.transferdata.SystemMessage;
import server.util.Vector2f;

/**
 *         Provides methods for selecting data from the database.
 */
public class Read {

	/**
	 * Reads the login key to verify user tokens from the database.
	 * The login key is written every time the login server starts.
	 * @param connection
	 * @param nr - Index of the login key to be used.
	 * @return
	 */
	public static String getLoginKey(Connection connection, int nr) {
		String sql = "SELECT LoginKey FROM login WHERE Nr = ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;

		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setInt(1, nr);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String key = null;
		try {
			if (result.next()) {
				key = result.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return key;
	}
	
	/**
	 * Returns the player with the given username.
	 * @param connection
	 * @param username
	 * @return
	 */
	public static Player getPlayer(Connection connection, String username) {
		String sql = "SELECT PlayerID,Password,Type,Salt FROM player WHERE PlayerName = ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;

		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Player player = null;
		try {

			if (result.next()) {
				long id = result.getLong(1);
				String password = result.getString(2);
				int type = result.getInt(3);
				String salt = result.getString(4);
				player = new Player(id, username, password);
				player.setType(type);
				player.setSalt(salt);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return player;
	}
	
	/**
	 * Returns a {@link SystemMessage} if a ban exists for the given player id.
	 * Null is returned otherwise.
	 * @param connection
	 * @param playerID
	 * @return
	 */
	public static SystemMessage getBan(Connection connection, long playerID) {
		String sql = "SELECT Reason, Duration, receivedFrom FROM ban WHERE PlayerID = ? ";
		PreparedStatement statement = null;

		ResultSet result = null;

		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setLong(1, playerID);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SystemMessage systemMessage = null;
		try {
			if (result.next()) {		
				String reason = result.getString(1);
				int duration = result.getInt(2);
				String from = result.getString(3);
				systemMessage = new SystemMessage("Your account was banned by '"+ from + "' for " + duration
						+ " hours for the following reason: " + reason , SystemMessage.BAN);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return systemMessage;
	}

	/**
	 * Returns all {@link Character}s from the database for a given playerID.
	 * @param connection
	 * @param playerID
	 * @return
	 */
	public static List<Character> getGameCharacter(Connection connection, long playerID) {
		String sql = "SELECT * FROM gamecharacter WHERE PlayerID = ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;

		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setLong(1, playerID);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Character> characters = new FastTable<Character>();
		try {
			while (result.next()) {
				long id = result.getLong(1);
				String name = result.getString(2);
				int maxHP = result.getInt(3);
				int hp = result.getInt(4);
				int hunger = result.getInt(5);
				int thirst = result.getInt(6);
				int stamina = result.getInt(7);
				int bodyTemperature = result.getInt(8);
				int intelligence = result.getInt(9);
				int perception = result.getInt(10);
				int strength = result.getInt(11);
				int dexterity = result.getInt(12);
				int charisma = result.getInt(13);
				int psyche = result.getInt(14);
				int agility = result.getInt(15);
				int constitution = result.getInt(16);
				float positionX = result.getFloat(17);
				float positionY = result.getFloat(18);
				Character character = new Character(id);
				character.setName(name);
				character.setMaxHP(maxHP);
				character.setHp(hp);
				character.setHunger(hunger);
				character.setThirst(thirst);
				character.setStamina(stamina);
				character.setBodyTemperature(bodyTemperature);
				character.setIntelligence(intelligence);
				character.setPerception(perception);
				character.setStrength(strength);
				character.setDexterity(dexterity);
				character.setCharisma(charisma);
				character.setPsyche(psyche);
				character.setAgility(agility);
				character.setConstitution(constitution);
				character.setPositionX(positionX);
				character.setPositionY(positionY);
				characters.add(character);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return characters;
	}

	/**
	 * Returns the {@link Effect} of a given character. Or null of none was found.
	 * @param connection
	 * @param characterID
	 * @return
	 */
	public static List<Effect> getCharacterEffect(Connection connection, long characterID) {
		String sql = "SELECT EffectID, EffectLevel, Duration FROM charactereffect WHERE CharacterID = ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;

		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setLong(1, characterID);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Effect> effects = new FastTable<Effect>();
		try {
			if (result.next()) {
				while (result.next()) {
					Effect effect = new Effect(result.getLong(1));
					effect.setLevel(result.getInt(2));
					effect.setDuration(result.getInt(3));
					effects.add(effect);
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return effects;
	}
	
	/**
	 * Returns all {@link Character}s within the given coordinates.
	 * @param connection
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public static List<Character> getCharacters(Connection connection, float x1, float x2, float y1, float y2){
		String sql = "SELECT * FROM gamecharacter WHERE PositionX BETWEEN ? AND ? AND PositionY BETWEEN ? AND ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setFloat(1, x1);
			statement.setFloat(2, x2);
			statement.setFloat(3, y1);
			statement.setFloat(4, y2);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Character> characters = new FastTable<Character>();
		try {
			while (result.next()) {
				long id = result.getLong(1);
				String name = result.getString(2);
				int maxHP = result.getInt(3);
				int hp = result.getInt(4);
				int hunger = result.getInt(5);
				int thirst = result.getInt(6);
				int stamina = result.getInt(7);
				int bodyTemperature = result.getInt(8);
				int intelligence = result.getInt(9);
				int perception = result.getInt(10);
				int strength = result.getInt(11);
				int dexterity = result.getInt(12);
				int charisma = result.getInt(13);
				int psyche = result.getInt(14);
				int agility = result.getInt(15);
				int constitution = result.getInt(16);
				float positionX = result.getFloat(17);
				float positionY = result.getFloat(18);
				Character character = new Character(id);
				character.setName(name);
				character.setMaxHP(maxHP);
				character.setHp(hp);
				character.setHunger(hunger);
				character.setThirst(thirst);
				character.setStamina(stamina);
				character.setBodyTemperature(bodyTemperature);
				character.setIntelligence(intelligence);
				character.setPerception(perception);
				character.setStrength(strength);
				character.setDexterity(dexterity);
				character.setCharisma(charisma);
				character.setPsyche(psyche);
				character.setAgility(agility);
				character.setConstitution(constitution);
				character.setPositionX(positionX);
				character.setPositionY(positionY);
				characters.add(character);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return characters;
	}
	
	/**
	 * Returns all {@link Building}s.
	 * @param connection
	 * @return
	 */
	public static List<Building> getAllBuildings(Connection connection){
		String sql = "SELECT * FROM buildinginstance;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Building> buildings = new FastTable<Building>();
		try {
			while (result.next()) {
				long id = result.getLong(1);
				long buildingID = result.getLong(2);
				int durability = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				Building building = new Building(id);
				building.setBuildingID(buildingID);
				building.setDurability(durability);
				building.setPosition(new Vector2f(positionX, positionY));
				buildings.add(building);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buildings;
	}

	/**
	 * Returns all buildings between the given coordinates from the database.
	 * 
	 * @param connection
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public static List<Building> getBuildings(Connection connection, float x1, float x2, float y1, float y2) {
		String sql = "SELECT * FROM buildinginstance WHERE PositionX BETWEEN ? AND ? AND PositionY BETWEEN ? AND ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setFloat(1, x1);
			statement.setFloat(2, x2);
			statement.setFloat(3, y1);
			statement.setFloat(4, y2);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Building> buildings = new FastTable<Building>();
		try {
			while (result.next()) {
				long id = result.getLong(1);
				long buildingID = result.getLong(2);
				int durability = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				Building building = new Building(id);
				building.setBuildingID(buildingID);
				building.setDurability(durability);
				building.setPosition(new Vector2f(positionX, positionY));
				buildings.add(building);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buildings;
	}
	
	/**
	 * Returns all {@link Structure}s.
	 * @param connection
	 * @return
	 */
	public static List<Structure> getAllStructures(Connection connection) {
		String sql = "SELECT * FROM structureinstance;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Structure> structures = new FastTable<Structure>();

		try {
			while (result.next()) {
				long id = result.getLong(1);
				long structureID = result.getLong(2);
				int durability = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				Structure structure = new Structure(id);
				structure.setDurability(durability);
				structure.setPosition(new Vector2f(positionX, positionY));
				structure.setStructureID(structureID);
				structures.add(structure);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return structures;
	}

	/**
	 * Returns all {@link Structure}s within the given coordinates.
	 * @param connection
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public static List<Structure> getStructures(Connection connection, float x1, float x2, float y1, float y2) {
		String sql = "SELECT * FROM structureinstance WHERE PositionX BETWEEN ? AND ? AND PositionY BETWEEN ? AND ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setFloat(1, x1);
			statement.setFloat(2, x2);
			statement.setFloat(3, y1);
			statement.setFloat(4, y2);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Structure> structures = new FastTable<Structure>();

		try {
			while (result.next()) {
				long id = result.getLong(1);
				long structureID = result.getLong(2);
				int durability = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				Structure structure = new Structure(id);
				structure.setDurability(durability);
				structure.setPosition(new Vector2f(positionX, positionY));
				structure.setStructureID(structureID);
				structures.add(structure);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return structures;
	}
	
	/**
	 * Returns all {@link Claim}s.
	 * @param connection
	 * @return
	 */
	public static List<Claim> getAllClaims(Connection connection) {
		String sql = "SELECT * FROM claim;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Claim> claims = new FastTable<Claim>();

		try {
			while (result.next()) {
				//long id = result.getLong(1);
				int duration = result.getInt(2);
				float positionX = result.getFloat(3);
				float positionY = result.getFloat(4);
				Claim claim = new Claim();
				claim.setDuration(duration);
				claim.setPosition(new Vector2f(positionX, positionY));
				claims.add(claim);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return claims;
	}

	/**
	 * Returns all {@link Claim}s.
	 * @param connection
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public static List<Claim> getClaims(Connection connection, float x1, float x2, float y1, float y2) {
		String sql = "SELECT * FROM claim WHERE PositionX BETWEEN ? AND ? AND PositionY BETWEEN ? AND ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setFloat(1, x1);
			statement.setFloat(2, x2);
			statement.setFloat(3, y1);
			statement.setFloat(4, y2);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Claim> claims = new FastTable<Claim>();

		try {
			while (result.next()) {
				//long id = result.getLong(1);
				int duration = result.getInt(2);
				float positionX = result.getFloat(3);
				float positionY = result.getFloat(4);
				Claim claim = new Claim();
				claim.setDuration(duration);
				claim.setPosition(new Vector2f(positionX, positionY));
				claims.add(claim);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return claims;
	}
	
	/**
	 * Returns all {@link ItemInstance}s.
	 * @param connection
	 * @return
	 */
	public static List<ItemInstance> getAllItemInstances(Connection connection){
		String sql = "SELECT * FROM iteminstance;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ItemInstance> items = new FastTable<ItemInstance>();

		try {
			while (result.next()) {
				long itemID = result.getLong(1);
				int durability = result.getInt(2);
				int duration = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				int state = result.getInt(6);
				ItemInstance item = new ItemInstance(itemID);
				item.setPosition(new Vector2f(positionX, positionY));
				item.setDurability(durability);
				item.setDuration(duration);
				item.setState(state);
				items.add(item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return items;
	}
	
	/**
	 * Returns all {@link ItemInstance}s within the given coordinates.
	 * @param connection
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public static List<ItemInstance> getItemInstances(Connection connection, float x1, float x2, float y1, float y2){
		String sql = "SELECT * FROM iteminstance WHERE PositionX BETWEEN ? AND ? AND PositionY BETWEEN ? AND ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setFloat(1, x1);
			statement.setFloat(2, x2);
			statement.setFloat(3, y1);
			statement.setFloat(4, y2);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ItemInstance> items = new FastTable<ItemInstance>();

		try {
			while (result.next()) {
				long itemID = result.getLong(1);
				int durability = result.getInt(2);
				int duration = result.getInt(3);
				float positionX = result.getFloat(4);
				float positionY = result.getFloat(5);
				int state = result.getInt(6);
				ItemInstance item = new ItemInstance(itemID);
				item.setPosition(new Vector2f(positionX, positionY));
				item.setDurability(durability);
				item.setDuration(duration);
				item.setState(state);
				items.add(item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return items;
	}

	/**
	 * Returns all {@link InventoryItem}s for the given character id.
	 * @param connection
	 * @param characterID
	 * @return
	 */
	public static List<InventoryItem> getInventoryItems(Connection connection, long characterID) {
		String sql = "SELECT SlotID, ItemID, Durability, State FROM inventoryitem WHERE CharacterID = ? ;";
		PreparedStatement statement = null;

		ResultSet result = null;
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			statement.setLong(1, characterID);
			result = statement.executeQuery();
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<InventoryItem> items = new FastTable<InventoryItem>();

		try {
			while (result.next()) {
				long slotID = result.getLong(1);
				long itemID = result.getLong(2);
				int durability = result.getInt(3);
				int state = result.getInt(4);
				InventoryItem item = new InventoryItem(itemID);
				item.setSlotID(slotID);
				item.setDurability(durability);
				item.setState(state);
				items.add(item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return items;
	}
}
