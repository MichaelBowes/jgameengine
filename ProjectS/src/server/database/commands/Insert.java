package server.database.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import server.model.Character;
import server.util.Vector2f;

public class Insert {
	
	public static void addLoginKey(Connection connection, String key, int nr) {
		String sql = "REPLACE INTO login (LoginKey, Nr) VALUES( ?, ?);";		
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setString(1, key);
			insert.setInt(2, nr);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void addInventoryItem(Connection connection, long characterID,
			int slot, long itemID, int durability, int state) {
		String sql = "INSERT INTO inventoryitem (SlotID, ItemID, Durability, CharacterID, State) VALUES( ?, ?, ?, ?, ?);";		
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setInt(1, slot);
			insert.setLong(2, itemID);
			insert.setInt(3, durability);
			insert.setLong(4, characterID);
			insert.setInt(5, state);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void addItemInstance(Connection connection, long itemID, int durability,
			int duration, float positionX, float positionY, int state) {
		String sql = "INSERT INTO iteminstance (ItemID, Durability, Duration, PositionX, PositionY, State) VALUES( ?, ?, ?, ?, ?, ?);";		
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, itemID);
			insert.setInt(2, durability);
			insert.setInt(3, duration);
			insert.setFloat(4, positionX);
			insert.setFloat(5, positionY);
			insert.setInt(6, state);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void addGameCharacter(Connection connection, Character character, long playerID) {
		String sql = "INSERT INTO gamecharacter (CharacterID, CharacterName, MaxHp,"
				+" Hp, Hunger, Thirst, Stamina, BodyTemperature, Intelligence, Perception,"
				+" Strength, Dexterity, Charisma, Psyche, Agility, Constitution, PositionX, PositionY, Direction, PlayerID)"
				+" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, character.getCharacterID());
			insert.setString(2, character.getName());
			insert.setInt(3, character.getMaxHP());
			insert.setInt(4, character.getHp());
			insert.setInt(5, character.getHunger());
			insert.setInt(6, character.getThirst());
			insert.setInt(7, character.getStamina());
			insert.setInt(8, character.getBodyTemperature());
			insert.setInt(9, character.getIntelligence());
			insert.setInt(10, character.getPerception());
			insert.setInt(11, character.getStrength());
			insert.setInt(12, character.getDexterity());
			insert.setInt(13, character.getCharisma());
			insert.setInt(14, character.getPsyche());
			insert.setInt(15, character.getAgility());
			insert.setInt(16, character.getConstitution());
			insert.setFloat(17, character.getPositionX());
			insert.setFloat(18, character.getPositionY());
			insert.setInt(19, character.getDirection());
			insert.setLong(20, playerID);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static boolean addPlayer(Connection connection, long playerID, String username, String password, String salt, int type) {
		String sql = "INSERT INTO player (PlayerID, Password, PlayerName, Type) VALUES(?, ?, ?, ?);";
		PreparedStatement insert = null;
		boolean success = false;
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, playerID);
			insert.setString(2, username);
			insert.setString(3, password);
			insert.setInt(4, type);
			insert.setString(5, salt);
			insert.executeUpdate();
			connection.commit();
			success = true;
		} catch (SQLException e) {
			success = false;
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
		return success;
	}
	
	public static boolean addCreature(Connection connection, long creatureID, String creatureName, int maxHp) {
		//TODO Add hp and other animal stats
		String sql = "INSERT INTO creature (CreatureID, CreatureName, MaxHp) VALUES(?, ?, ?);";
		PreparedStatement insert = null;
		boolean success = false;
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, creatureID);
			insert.setString(2, creatureName);
			insert.setInt(3, maxHp);
			insert.executeUpdate();
			connection.commit();
			success = true;
		} catch (SQLException e) {
			success = false;
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
		return success;
	}
	
	public static void addCreatureInstance(Connection connection, long creatureID,
			int currentHp, int statusEffect, Vector2f position) {
			String sql = "INSERT INTO creatureinstance (CreatureID, CurrentHp, StatusEffect, PositionX, PositionY) VALUES(?, ?, ?, ?, ?);";
			PreparedStatement insert = null;
			
			try {
				connection.setAutoCommit(false);
				insert = connection.prepareStatement(sql);
				insert.setLong(1, creatureID);
				insert.setInt(2, currentHp);
				insert.setInt(3, statusEffect);
				insert.setFloat(4, position.x);
				insert.setFloat(5, position.y);
				insert.executeUpdate();
				connection.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				if (connection != null) {
					try {
						System.err.print("Transaction is being rolled back");
			            connection.rollback();
					} catch(SQLException excep) {
						excep.printStackTrace();
			        }
			    }
			}
	}
	
	public static void addCreature(Connection connection, long creatureID, long itemID, int dropChance) {
		//TODO Add hp and other animal stats
		String sql = "INSERT INTO creatureDrop (CreatureID, ItemID, DropChance) VALUES(?, ?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, creatureID);
			insert.setLong(2, itemID);
			insert.setInt(3, dropChance);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	/**
	 * Inserts an {@link Item} in the item table.
	 * @param connection
	 * @param item
	 */
	public static void addItem(Connection connection, long itemID) {
		String sql = "INSERT INTO item (ItemID) VALUES(?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, itemID);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addBuildingInstance(Connection connection, long id, long buildingID, int durability, Vector2f position) {
		String sql = "INSERT INTO buildinginstance (ID, BuildingID, Durability, PositionX, PositionY) VALUES(?, ?, ?, ?, ?);";
		PreparedStatement insert = null;
			
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, id);
			insert.setLong(2, buildingID);
			insert.setInt(3, durability);
			insert.setFloat(4, position.x);
			insert.setFloat(5, position.y);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addBuilding(Connection connection, long buildingID, String buildingName) {
		String sql = "INSERT INTO building (BuildingID, BuildingName) VALUES(?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, buildingID);
			insert.setString(2, buildingName);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addBan(Connection connection, String from, long userID, String reason, int duration) {
		String sql = "INSERT INTO ban (Reason, Duration, PlayerID, receivedFrom) VALUES(?, ?, ?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setString(1, reason);
			insert.setInt(2, duration);
			insert.setLong(3, userID);
			insert.setString(4, from);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addClaim(Connection connection, long claimID, int duration) {
		//TODO who can create a claim? player or community?
		String sql = "INSERT INTO claim (ClaimID, Duration) VALUES(?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, claimID);
			insert.setInt(2, duration);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addCharacterEffect(Connection connection, long characterID, long effectID, int duration, int level) {
		String sql = "INSERT INTO charactereffect (PlayerID, EffectID, Duration, EffectLevel) VALUES(?, ?, ?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, characterID);
			insert.setLong(2, effectID);
			insert.setInt(3, duration);
			insert.setInt(4, level);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	public static void addEffect(Connection connection, long effectID, String effectName) {
		String sql = "INSERT INTO effect (EffectID, EffectName, EffectLevel) VALUES(?, ?, ?);";
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setLong(1, effectID);
			insert.setString(2, effectName);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
		            connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
		        }
		    }
		}
	}
	
	/*
	 * TODO:
	 * ADD
	 * Combatstats
	 * Communitymember
	 * Community
	 * 
	 */
	
}
