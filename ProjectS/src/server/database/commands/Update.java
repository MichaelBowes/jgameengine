package server.database.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Update {
	
	public static void updateLoginKey(Connection connection, String key, int nr) {
		String sql = "UPDATE login SET LoginKey = ? WHERE Nr = ?;";		
		PreparedStatement insert = null;
		
		try {
			connection.setAutoCommit(false);
			insert = connection.prepareStatement(sql);
			insert.setString(1, key);
			insert.setInt(2, nr);
			insert.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
