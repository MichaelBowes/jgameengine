package server.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class Database {
	
	private HikariDataSource dataSource;
	
	public void startUp() {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://localhost:3306/survivaldatabase?useLegacyDatetimeCode=false&serverTimezone=UTC");
		config.setUsername("root");
		config.setPassword("databasePassword");
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

		dataSource = new HikariDataSource(config);
	}
	
	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	public Connection getClientConnection() throws SQLException {
		//return dataSource.getConnection("client","clientconnection");
		return dataSource.getConnection();
	}
	
	public void close() {
		if(dataSource != null) {
			dataSource.close();			
		}
	}
}
