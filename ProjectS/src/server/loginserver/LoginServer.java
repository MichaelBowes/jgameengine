package server.loginserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

import javax.net.ssl.SSLServerSocketFactory;

import org.mindrot.jbcrypt.BCrypt;

import server2.Server;
import server.config.ServerConfig;
import server.database.commands.Insert;

public class LoginServer extends Server<LoginHandler> {

	public static String LOGIN_KEY;

	public LoginServer(ServerConfig config) {
		super(config, "LOGIN_SERVER_THREAD");
	}

	public void start() {
		super.start("res/log/LSTransactions.txt", "res/log/LSMessages.txt");
		LOGIN_KEY = updateLoginKey();
	}

	private String updateLoginKey() {
		String key = BCrypt.gensalt();
		Insert.addLoginKey(getConnection(), key, 1);
		return key;
	}

	@Override
	protected ServerSocket createServerSocket(int port, InetAddress ip) throws IOException {
		System.setProperty("javax.net.ssl.keyStore", "res/keystore/serverkeystore");
		System.setProperty("javax.net.ssl.keyStorePassword", "RWPSksU5");

		ServerSocket socket = null;
		SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		socket = ssf.createServerSocket();
		InetSocketAddress serverAddress = new InetSocketAddress(ip, port);
		socket.bind(serverAddress);
		socket.setSoTimeout(config.getTimeout());

		return socket;
	}

	@Override
	public LoginHandler connect(Socket socket) {
		LoginHandler client = null;
		try {
			client = new LoginHandler(socket, database.getClientConnection(), logger);
			logger.logServerMessage(socket.getInetAddress().getHostAddress() + " connected");
		} catch (SQLException e) {
			logger.logServerMessage("Could not create connection for socket " + socket.getInetAddress());
			e.printStackTrace();
			return null;
		}
		return client;
	}
}
