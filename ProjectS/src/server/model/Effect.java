package server.model;

import java.io.Serializable;

public class Effect implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2463257748866518560L;
	private int level;
	private long id;
	private int duration;
	
	public Effect(long id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public long getId() {
		return id;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
}
