package server.model;

import java.io.Serializable;

import server.util.Vector2f;

public class Structure implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3828974857037827595L;
	private int durability;
	private long id;
	private long structureID;
	private Vector2f position;
	
	public Structure(long id) {
		this.id = id;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}

	public long getId() {
		return id;
	}

	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
	}

	public long getStructureID() {
		return structureID;
	}

	public void setStructureID(long structureID) {
		this.structureID = structureID;
	}
}
