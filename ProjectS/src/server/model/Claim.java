package server.model;

import java.io.Serializable;

import server.util.Vector2f;

public class Claim implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -749732296081524285L;
	public static final int CLAIM_MAX_SIZE = 200;
	private Vector2f position;
	private int duration;
	
	public Vector2f getPosition() {
		return position;
	}
	public void setPosition(Vector2f position) {
		this.position = position;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
