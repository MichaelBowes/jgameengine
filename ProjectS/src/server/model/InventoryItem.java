package server.model;

public class InventoryItem extends Item{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7306088531027272022L;

	public InventoryItem(long id) {
		super(id);
	}

	private long slotID;
	
	public long getSlotID() {
		return slotID;
	}

	public void setSlotID(long slotID) {
		this.slotID = slotID;
	}
}
