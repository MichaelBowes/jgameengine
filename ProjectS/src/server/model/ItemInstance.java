package server.model;

import server.util.Vector2f;

public class ItemInstance extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8118321246794220556L;
	private Vector2f position;
	private int duration;
	
	public ItemInstance(long id) {
		super(id);
	}
	
	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
