package server.model;

import java.io.Serializable;

public class Player implements Serializable {
		
	private static final long serialVersionUID = 5511439871255798549L;
	public static final int PLAYER = 0;
	public static final int MODERATOR = 1;
	public static final int GAMEMASTER = 2;
	public static final int ADMIN = 3;
	
	private String username;
	private Character currentCharacter;
	private long playerID;
	private String password;
	private String salt;
	private int type;
	
	public Player(long playerID, String username, String password) {
		this.username = username;
		this.playerID = playerID;
		this.password = password;
	}
	
	public long getCharacterID() {
		if(currentCharacter != null) {
			return currentCharacter.getCharacterID();			
		}
		return 0;
	}
	public void setCharacter(Character character) {
		this.currentCharacter = character;
	}
	
	public Character getCharacter() {
		return this.currentCharacter;
	}
	
	public String getUsername() {
		return username;
	}

	public long getGetPlayerID() {
		return playerID;
	}

	public String getPassword() {
		return password;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
}
