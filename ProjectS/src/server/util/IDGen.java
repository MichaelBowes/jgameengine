package server.util;

import server.exceptions.InitialisationException;

/**
 * Id generator
 */
public class IDGen {

	
	private static long idCounter;
	private static boolean isInitialized = false;
	
	
	public static void initialize(long id) {
		idCounter = id;
	}
	/**
	 * Generates a unique id.
	 * @return
	 */
	public synchronized static long getID() {	
		if(isInitialized) {
			long counter = idCounter;
			idCounter++;
			return counter;
		}else {
			throw new InitialisationException("IDGen can't be accessed before it is initialized!");
		}
	}
}
