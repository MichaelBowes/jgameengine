package server.util;

import java.net.InetAddress;

import org.joda.time.DateTime;

public class Logger {

	
	private Thread serverLoggerThread;
	private LogWriter serverLogWriter;

	public Logger(String transactionFilePath, String serverLogFilePath) {
		serverLogWriter = new LogWriter(transactionFilePath, serverLogFilePath);
		serverLoggerThread = new Thread(serverLogWriter,"SERVER_LOGGER_TREAD");
		serverLoggerThread.start();
	}

	/**
	 * Loggs all server messages.
	 * 
	 * @param message
	 */
	public void logServerMessage(String message) {
		String finalMessage = DateTime.now().toString() + " - " + message;
		System.out.println(finalMessage);
		LoggerMessage logMessage = new LoggerMessage(finalMessage, LoggerMessage.SERVERMESSAGE);
		serverLogWriter.addLine(logMessage);
	}

	public void logClientServerMessage(InetAddress ip, String message) {
		String finalMessage = DateTime.now().toString() + " - Client:"+ ip.getHostAddress() + " - " + message;
		System.out.println(finalMessage);
		LoggerMessage logMessage = new LoggerMessage(finalMessage, LoggerMessage.SERVERMESSAGE);
		serverLogWriter.addLine(logMessage);
	}

	/**
	 * Logs all transactions from clients
	 * 
	 * @param message
	 */
	public void logTransaction(String message) {
		String finalMessage = DateTime.now().toString() + " - " + message;
		System.out.println(finalMessage);
		LoggerMessage logMessage = new LoggerMessage(finalMessage,
				LoggerMessage.TRANSACTION);
		serverLogWriter.addLine(logMessage);
	}

	public void logClientTransaction(InetAddress ip, String message) {
		String finalMessage = DateTime.now().toString() + " - Client:"+ ip.getHostAddress() + " - " + message;
		System.out.println(finalMessage);
		LoggerMessage logMessage = new LoggerMessage(finalMessage,
				LoggerMessage.TRANSACTION);
		serverLogWriter.addLine(logMessage);
	}
	
	public void shutdown() {
		LoggerMessage logMessage = new LoggerMessage(null, LoggerMessage.SHUTDOWN);
		serverLogWriter.addLine(logMessage);
	}
}
