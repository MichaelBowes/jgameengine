package server.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogWriter implements Runnable {

	private boolean running = true;
	private BlockingQueue<LoggerMessage> queue;
	private String transactionLogFilePath;
	private String serverLogFilePath;
	private BufferedWriter transactionLogWriter;
	private BufferedWriter serverLogWriter;

	public LogWriter(String transactionLogFilePath, String serverLogFilePath) {
		queue = new LinkedBlockingQueue<LoggerMessage>();
		this.serverLogFilePath = serverLogFilePath;
		this.transactionLogFilePath = transactionLogFilePath;
	}

	@Override
	public void run() {
		Path transactionPath = Paths.get(transactionLogFilePath);
		Path serverLogPath = Paths.get(serverLogFilePath);
		try {
			transactionLogWriter = Files.newBufferedWriter(transactionPath, Charset.forName("UTF-8"));
			serverLogWriter = Files.newBufferedWriter(serverLogPath, Charset.forName("UTF-8"));
			while (running) {
				LoggerMessage message = queue.take();
				int type = message.getType();
				switch(type) {
					case LoggerMessage.SERVERMESSAGE :
						serverLogWriter.write(message.getMessage());
						serverLogWriter.newLine();
						break;
					
					case LoggerMessage.TRANSACTION :
						transactionLogWriter.write(message.getMessage());
						transactionLogWriter.newLine();
						break;
						
					case LoggerMessage.SHUTDOWN :
						serverLogWriter.close();
						transactionLogWriter.close();
						running = false;
						break;
				}
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Get lines to be written in the log file
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Adds a line to the queue to be written into the log file. If the line is null
	 * the writer will close.
	 * 
	 * @param line
	 * @return
	 */
	public boolean addLine(LoggerMessage message) {
		if (message != null) {
			try {
				queue.put(message);
				return true;
			} catch (InterruptedException e) {
				return false;
			}
		}
		return false;
	}
}
