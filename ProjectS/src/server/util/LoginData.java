package server.util;

import java.io.Serializable;

public class LoginData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 305279208308829412L;
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
