package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.logic.UIManager;

public class EndCombatButton extends Button {
	
	public EndCombatButton(String name, Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
		this.name = name;
	}

	@Override
	public void onClick(){
		UIManager.closeCombatResults();
	}
	
	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
