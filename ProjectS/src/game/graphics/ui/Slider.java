package game.graphics.ui;

import engine.Engine;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * A UI element that lets you move a slider within the geographic constraints of a sliderbar.
 *
 */
public class Slider extends GameObject {

	String name;
	/**
	 * Tells the {@link Game} which slider is currently being dragged in case more than 1 slider is present.
	 */
	public boolean dragMe = false;
	/**
	 * The sliderbar within which the slider can be dragged around.
	 */
	private GameObject sliderbar;
	/**
	 * Value representing the slider position on the sliderbar.
	 * Ranges from 0 to 100. Should be binded or observed for changes.
	 */
	private int value;
	
	public Slider(String name, Sprite sliderSprite, Sprite sliderbarSprite, Vector3f position) {
		super(position);
		this.name = name;
		setSprite(sliderSprite);
		//Sliderbar behind the Slider
		sliderbar = new DragBar(sliderbarSprite, this.getPosition().add(new Vector3f(0,0,-0.001f)));
		sliderbar.addHitbox();
		Engine.getSceneManager().addUIObject(sliderbar);
	}

	public void dragOn() {
		dragMe = true;
	}

	public void dragOff() {
		if(dragMe) {
			dragMe = false;
		}
	}
	
	public GameObject getSliderBar() {
		return sliderbar;
	}
	
	public int getvalue() {
		return value;
	}

	@Override
	public void onCollision(GameObject collider) {
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
}
