package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

public class MapEditorButton extends Button {

	private int id;
	
	public MapEditorButton(int id, String name, Sprite sprite, Vector3f position) {
		super(name, sprite, position);
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	@Override
	public void onClick(){
		//save tileID
		//highlight tile
	}

	@Override
	public void onCollisionExit(GameObject collider) {
	}

}
