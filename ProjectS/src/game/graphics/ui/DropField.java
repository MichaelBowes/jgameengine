package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.model.items.Item;

public class DropField extends GameObject {
	
	public DropField(Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
	}
	
	public void addItem(Item item) {
		this.addChild(item);
	}
	
	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
