package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * Abstract class for Buttons.
 * Buttons need to override the onClick() method for individual functionality.
 *
 */
public abstract class Button extends GameObject {
	protected String name;

	public Button(Vector3f position){
		super(position);
	}
	
	public Button(String name, Sprite sprite, Vector3f position) {
		super(position);
		this.name = name;
		setSprite(sprite);
	}
	
	public String getName(){
		return name;
	}

	public void onClick() {
		System.out.println(name);
	}

	@Override
	public void onCollision(GameObject collider) {
	}

}
