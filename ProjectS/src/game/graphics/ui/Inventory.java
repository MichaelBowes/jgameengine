package game.graphics.ui;

import java.util.List;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.manager.TextureManager;
import game.model.items.Item;
import javolution.util.FastTable;

public class Inventory extends GameObject {

	String name;
	InventoryBorder cornerTL;
	InventoryBorder cornerTR;
	InventoryBorder cornerBL;
	InventoryBorder cornerBR;
	List<InventoryBorder> rowLeft = new FastTable<InventoryBorder>();
	List<InventoryBorder> rowRight = new FastTable<InventoryBorder>();
	List<InventoryBorder> columnTop = new FastTable<InventoryBorder>();
	List<InventoryBorder> columnBot = new FastTable<InventoryBorder>();

	public Inventory(String name, int rows, int columns, Vector3f position) {
		super(position);
		this.name = name;
		createBase();
		for(int i=1;i<rows;i++) {
			addRow();
		}
		for(int i=1;i<columns;i++) {
			addColumn();
		}
		//CloseButton btn = new CloseButton();
		//cornerTR.addChild(btn);
	}
	/**
	 * Creates the inventory with a base size of 1x1 drop fields.
	 * @param rows
	 * @param columns
	 */
	private void createBase() {
		cornerTL = new InventoryBorder(TextureManager.getSprite("res/pictures/TL2.png", 0, 0), new Vector3f(-32, 32, 0.01f));
		cornerTR = new InventoryBorder(TextureManager.getSprite("res/pictures/TR2.png", 0, 0), new Vector3f(32, 32, 0.01f));
		cornerBL = new InventoryBorder(TextureManager.getSprite("res/pictures/BL2.png", 0, 0), new Vector3f(-32, -32, 0.01f));
		cornerBR = new InventoryBorder(TextureManager.getSprite("res/pictures/BR2.png", 0, 0), new Vector3f(32, -32, 0.01f));
		cornerTL.addHitbox();
		cornerTR.addHitbox();
		cornerBL.addHitbox();
		cornerBR.addHitbox();
		this.addChild(cornerTL);
		this.addChild(cornerTR);
		this.addChild(cornerBL);
		this.addChild(cornerBR);
		//add sides
		InventoryBorder leftBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/L2.png", 0, 0),
				new Vector3f(-32, 0, 0.01f));
		InventoryBorder rightBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/R2.png", 0, 0),
				new Vector3f(32, 0, 0.01f));
		leftBorder.addHitbox();
		rightBorder.addHitbox();
		rowLeft.add(leftBorder);
		rowRight.add(rightBorder);
		this.addChild(leftBorder);
		this.addChild(rightBorder);
		InventoryBorder topBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/T2.png", 0, 0),
				new Vector3f(0, 32, 0.01f));
		InventoryBorder botBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/B2.png", 0, 0),
				new Vector3f(0, -32, 0.01f));
		topBorder.addHitbox();
		botBorder.addHitbox();
		columnTop.add(topBorder);
		columnBot.add(botBorder);
		this.addChild(topBorder);
		this.addChild(botBorder);
		//first drop field
		DropField mid = new DropField(TextureManager.getSprite("res/pictures/M.png", 0, 0),
				new Vector3f(0, 0, 0.01f));
		mid.addHitbox();
		this.addChild(mid);
	}
	/**
	 * Expands the inventory by 1 row.
	 */
	public void addRow() {
		//create drop fields
		for (int i = 0; i < columnBot.size(); i++) {
			DropField mid = new DropField(TextureManager.getSprite("res/pictures/M.png", 0, 0),
					new Vector3f(0+32*i, -32*rowRight.size(), 0.01f));
			mid.addHitbox();
			this.addChild(mid);
		}
		
		// add borders of new row to the side
		InventoryBorder leftBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/L2.png", 0, 0),
				new Vector3f(-32, -32* rowRight.size(), 0.01f));
		InventoryBorder rightBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/R2.png", 0, 0),
				new Vector3f(32*columnBot.size(), -32* rowRight.size(), 0.01f));
		leftBorder.addHitbox();
		rightBorder.addHitbox();
		rowLeft.add(leftBorder);
		rowRight.add(rightBorder);
		this.addChild(leftBorder);
		this.addChild(rightBorder);
		
		// adjust already existing sides' positions
		for(int i=0;i<columnBot.size();i++) {
			columnBot.get(i).setPosition(32*i, -32*rowRight.size(), -0.01f);
		}
		
		// change corner positions
		cornerBL.setPosition(-32, -32*rowRight.size(), -0.01f);
		cornerBR.setPosition(32*columnBot.size(), -32*rowRight.size(), -0.01f);
	}
	
	/**
	 * Expands the inventory by 1 column.
	 */
	public void addColumn() {
		//add drop fields
		for (int i = 0; i < rowRight.size(); i++) {
			DropField mid = new DropField(TextureManager.getSprite("res/pictures/M.png", 0, 0),
					new Vector3f(32*columnBot.size(), 0-32*i, 0.01f));
			mid.addHitbox();
			this.addChild(mid);
		}
		//add border sides
		InventoryBorder topBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/T2.png", 0, 0),
				new Vector3f(32*columnBot.size(), 32, 0.01f));
		InventoryBorder botBorder = new InventoryBorder(TextureManager.getSprite("res/pictures/B2.png", 0, 0),
				new Vector3f(32*columnBot.size(), -32*rowRight.size(), 0.01f));
		topBorder.addHitbox();
		botBorder.addHitbox();
		columnTop.add(topBorder);
		columnBot.add(botBorder);
		this.addChild(topBorder);
		this.addChild(botBorder);
		
		// adjust already existing sides' positions
		for(int i=0;i<rowRight.size();i++) {
			rowRight.get(i).setPosition(32*columnBot.size(), -32*i, -0.01f);
		}
		//adjust corner position
		cornerTR.setPosition(32*columnBot.size(), 32, -0.01f);
		cornerBR.setPosition(32*columnBot.size(), -32*rowRight.size(), -0.01f);
	}
	
	/**
	 * Returns a list of all {@link DropField}s in this inventory.
	 * @return a List of DropFields
	 */
	public List<DropField> getFields(){
		List<DropField> dropFields = new FastTable<DropField>();
		for(GameObject go : this.getChildren()) {
			if(go instanceof DropField) {
				dropFields.add(((DropField)go));
			}
		}
		return dropFields;
	}
	
	public void addItem(Item item) {
		//TODO: check item size
		//TODO: check for free DropFields
		//TODO: add item if conditions are met, else exchange dragging item and item in field.
	}
	
	/**
	 * Signals that the inventory should be dragged.
	 */
	public void dragInventory() {
		
	}
	
	/**
	 * Opens the inventory
	 */
	public void openInventory() {
		
	}
	
	/**
	 * Closes the inventory
	 */
	public void closeInventory() {
		
	}

	@Override
	public void onCollision(GameObject collider) {
	}

	@Override
	public void onCollisionExit(GameObject collider) {
	}

}