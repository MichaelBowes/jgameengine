package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * Button with no functionality on click.
 *
 */
public class StandardButton extends Button {

	public StandardButton(String name, Sprite sprite, Vector3f position) {
		super(name, sprite, position);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onClick() {
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
}
