package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.logic.UIManager;

public class InventoryButton extends Button{

	public InventoryButton(String name, Sprite sprite, Vector3f position) {
		super(name, sprite, position);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onClick(){
		if(UIManager.invOpen){
			UIManager.closeInventory();
		} else {
			UIManager.openInventory();
		}
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
