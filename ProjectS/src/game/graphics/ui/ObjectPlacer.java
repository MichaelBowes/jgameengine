package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.logic.MapManager;

public class ObjectPlacer extends Button {
	
	int id;

	public ObjectPlacer(String name, Sprite sprite, Vector3f position, int id) {
		super(name, sprite, position);
		this.id = id;
	}

	@Override
	public void onClick() {
		System.out.println(id);
		MapManager.placingTileID=id;
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub

	}

}
