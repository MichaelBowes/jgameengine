package game.graphics.ui;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.logic.UIManager;

/**
 * Button for sending request to server to validate login information,
 * and if valid, load game scene in {@link UIManager}.
 *
 */
public class LoginButton extends Button {

	public LoginButton(String name, Sprite sprite, Vector3f position) {
		super(name, sprite, position);
	}

	@Override
	public void onClick() {
		UIManager.loadGameScene();
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
	
}
