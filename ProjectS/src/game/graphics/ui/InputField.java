package game.graphics.ui;

import engine.Engine;
import engine.input.Input;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.objects.fonts.Font;
import engine.objects.fonts.TextObject;
import engine.textures.Sprite;

public class InputField extends GameObject {

	public String name;
	/**
	 * Limit of characters the InputField will allow.
	 */
	private int maxCharacters = 10;
	/**
	 * Tells the {@link Game} that this InputField is listening, in case more than 1 InputField exists.
	 */
	public boolean listenForInput = false;
	/**
	 * The text to be displayed, as a {@link TextObject}.
	 */
	public TextObject textDisplay;
	Thread t;
	/**
	 * Time delay the program should wait, between each key input.
	 */
	private final long delay = 120L;
	private long lastTime = System.currentTimeMillis() - delay;

	public InputField(String name, Sprite sprite, Vector3f position, Font font) {
		super(position);
		this.name = name;
		setSprite(sprite);
		textDisplay = new TextObject("", font,
				new Vector3f(this.getPosition().x, this.getPosition().y, this.getPosition().z + 0.01f));
		textDisplay.setAddedColor(-0.2f, -0.2f, -0.1f, 0);
		Engine.getSceneManager().addUIObject(textDisplay);
	}

	public void onClick() {
		listenForInput = true;
	}

	public void inputFieldListen() {

		long time = System.currentTimeMillis();

		if ((time - lastTime) >= delay) {
			if (textDisplay.getText().length() + 1 <= maxCharacters) {
				// Small letters
				if (Input.keys[Input.KEY_A] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;

					textDisplay.setText(textDisplay.getText() + 'a');
				}
				if (Input.keys[Input.KEY_B] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'b');
				}
				if (Input.keys[Input.KEY_C] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'c');
				}
				if (Input.keys[Input.KEY_D] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'd');
				}
				if (Input.keys[Input.KEY_E] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'e');
				}
				if (Input.keys[Input.KEY_F] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'f');
				}
				if (Input.keys[Input.KEY_G] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'g');
				}
				if (Input.keys[Input.KEY_H] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'h');
				}
				if (Input.keys[Input.KEY_I] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'i');
				}
				if (Input.keys[Input.KEY_J] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'j');
				}
				if (Input.keys[Input.KEY_K] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'k');
				}
				if (Input.keys[Input.KEY_L] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'l');
				}
				if (Input.keys[Input.KEY_M] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'm');
				}
				if (Input.keys[Input.KEY_N] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'n');
				}
				if (Input.keys[Input.KEY_O] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'o');
				}
				if (Input.keys[Input.KEY_P] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'p');
				}
				if (Input.keys[Input.KEY_Q] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'q');
				}
				if (Input.keys[Input.KEY_R] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'r');
				}
				if (Input.keys[Input.KEY_S] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 's');
				}
				if (Input.keys[Input.KEY_T] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 't');
				}
				if (Input.keys[Input.KEY_U] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'u');
				}
				if (Input.keys[Input.KEY_V] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'v');
				}
				if (Input.keys[Input.KEY_W] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'w');
				}
				if (Input.keys[Input.KEY_X] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'x');
				}
				if (Input.keys[Input.KEY_Y] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'y');
				}
				if (Input.keys[Input.KEY_Z] && !(Input.keys[Input.KEY_LEFT_SHIFT] || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'z');
				}

				// Big letters
				if (Input.keys[Input.KEY_A] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'A');
				}
				if (Input.keys[Input.KEY_B] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'B');
				}
				if (Input.keys[Input.KEY_C] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'C');
				}
				if (Input.keys[Input.KEY_D] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'D');
				}
				if (Input.keys[Input.KEY_E] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'E');
				}
				if (Input.keys[Input.KEY_F] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'F');
				}
				if (Input.keys[Input.KEY_G] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'G');
				}
				if (Input.keys[Input.KEY_H] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'H');
				}
				if (Input.keys[Input.KEY_I] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'I');
				}
				if (Input.keys[Input.KEY_J] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'J');
				}
				if (Input.keys[Input.KEY_K] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'K');
				}
				if (Input.keys[Input.KEY_L] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'L');
				}
				if (Input.keys[Input.KEY_M] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'M');
				}
				if (Input.keys[Input.KEY_N] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'N');
				}
				if (Input.keys[Input.KEY_O] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'O');
				}
				if (Input.keys[Input.KEY_P] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'P');
				}
				if (Input.keys[Input.KEY_Q] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'Q');
				}
				if (Input.keys[Input.KEY_R] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'R');
				}
				if (Input.keys[Input.KEY_S] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'S');
				}
				if (Input.keys[Input.KEY_T] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'T');
				}
				if (Input.keys[Input.KEY_U] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'U');
				}
				if (Input.keys[Input.KEY_V] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'V');
				}
				if (Input.keys[Input.KEY_W] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'W');
				}
				if (Input.keys[Input.KEY_X] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'X');
				}
				if (Input.keys[Input.KEY_Y] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'Y');
				}
				if (Input.keys[Input.KEY_Z] && ((Input.keys[Input.KEY_LEFT_SHIFT]) || Input.MOD_CAPS_LOCK)) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText() + 'Z');
				}
			}
			// Numbers
			if (Input.keys[Input.KEY_0] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '0');
			}
			if (Input.keys[Input.KEY_1] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '1');
			}
			if (Input.keys[Input.KEY_2] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '2');
			}
			if (Input.keys[Input.KEY_3] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '3');
			}
			if (Input.keys[Input.KEY_4] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '4');
			}
			if (Input.keys[Input.KEY_5] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '5');
			}
			if (Input.keys[Input.KEY_6] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '6');
			}
			if (Input.keys[Input.KEY_7] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '7');
			}
			if (Input.keys[Input.KEY_8] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '8');
			}
			if (Input.keys[Input.KEY_9] && (!Input.keys[Input.KEY_LEFT_SHIFT] && !Input.MOD_CAPS_LOCK)) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + '9');
			}

			if (Input.keys[Input.KEY_BACKSPACE]) {
				if (textDisplay.getText().length() > 0) {
					lastTime = time;
					textDisplay.setText(textDisplay.getText().substring(0, textDisplay.getText().length() - 1));
				}
			}
			if (Input.keys[Input.KEY_SPACE]) {
				lastTime = time;
				textDisplay.setText(textDisplay.getText() + ' ');
			}
		}
	}

	public void clearField() {
		textDisplay.setText("");
	}

	public String getText() {
		return textDisplay.getText();
	}

	public void setMaxCharacters(int limit) {
		maxCharacters = limit;
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
