package game.main;

import java.io.IOException;
import java.util.List;
import engine.Engine;
import engine.GameLogic;
import engine.display.Resolution;
import engine.input.Input;
import engine.input.KeyInput;
import engine.input.KeyInputListener;
import engine.input.MouseInput;
import engine.input.MouseInputListener;
import engine.math.Vector2f;
import engine.math.Vector3f;
import engine.objects.GameObject;
import game.client.GameClient;
import game.combat.Projectile;
import game.graphics.ui.DropField;
import game.graphics.ui.InputField;
import game.graphics.ui.Inventory;
import game.graphics.ui.InventoryBorder;
import game.graphics.ui.ObjectPlacer;
import game.graphics.ui.Slider;
import game.logic.GOManager;
import game.logic.KeyMovementChecker;
import game.logic.MapManager;
import game.logic.MouseMovementChecker;
import game.logic.UIManager;
import game.graphics.ui.Button;
import game.map.MonsterContainer;
import game.map.ObjectFactory;
import game.map.Tile;
import game.map.WorldObject;
import game.map.spawnMode;
import game.model.items.Item;

public class Game implements GameLogic, MouseInputListener, KeyInputListener {

	String fps;
	int LastInput;

	private Engine engine;
	/**
	 * Contains all the UI and non-UI GameObjects.
	 */
	private UIManager userInterface;
	/**
	 * 
	 */
	private GOManager goManager;

	private GameClient client;
	/**
	 * The user of this account.
	 */
	// private User user;

	/**
	 * Current map the player character is on. TODO: Send request to server to
	 * receive currentMap from characters DB table.
	 */
	private Tile[][] currentMap;

	/**
	 * A representation of the enemy(s) in the combat
	 */
	private MonsterContainer enemies;

	private Vector2f mouseWorldCoord;
	ObjectFactory of = new ObjectFactory();

	public Game(Resolution resolution, int frameRate, GameClient client) {
		engine = new Engine(resolution, this, frameRate);
		this.client = client;
	}

	public void start() {
		engine.start();
	}

	public Game() {
		engine = new Engine(Resolution.HD, this, 60);
		engine.start();
	}

	public static void main(String[] args) {
		new Game();
	}

	@Override
	public void update() {
		// keyChecker.moveMonster();
		if (UIManager.mouseChecker != null) {
			UIManager.mouseChecker.movePlayer();
			if (MapManager.getCurrentMap().getMode1() == spawnMode.NORMAL) {
				checkSpawns();
			}
		}
		if (UIManager.keyChecker != null) {
			if (Input.keys[263]) {
				UIManager.keyChecker.setNextDirection(2);
			} else if (Input.keys[262]) {
				UIManager.keyChecker.setNextDirection(3);
			} else if (Input.keys[264]) {
				UIManager.keyChecker.setNextDirection(1);
			} else if (Input.keys[265]) {
				UIManager.keyChecker.setNextDirection(0);
			}
			UIManager.keyChecker.moveMonster();
		}
		if (UIManager.combatFocused) {
			tickCombat();
		}
		checkForDragging();
	}

	@Override
	public void init() {
		Engine.getCamera().setPosition(0, 0, 0.6f);
		MouseInput.addListener(this);
		KeyInput.addListener(this);
		userInterface = new UIManager();
		goManager = new GOManager();
		engine.setFrameSyncRate(60);
		GOManager.fillItemTable();
		GOManager.loadPlayer();
		GOManager.loadGameScene();
		GOManager.renderGameElements();
		UIManager.loadGameScene();
		UIManager.loadHUD();
		MapManager.loadMap(0);
	}

	@Override
	public void cleanUp() {
	}

	@Override
	public void onMouseDown(int button) {
		if (button == MouseInput.RIGHT_MOUSE_BUTTON) {

			if (MapManager.objectPlacerOn) {
				for (WorldObject go : MapManager.mapObjects) {
					if (go.mouseInsideHitbox(MouseInput.getMousePosition())) {
						if (go instanceof WorldObject) {
							MapManager.mapObjects.remove(go);
							Engine.getSceneManager().removeGameObject(go);
						}
					}
				}
			}

			if (UIManager.gameSceneFocused) {
				System.out.println("RMB");
				for (Item i : GOManager.droppedItems) {
					if (((GameObject) i).mouseInsideHitbox(MouseInput.getMousePosition())) {
						mouseWorldCoord = MouseInput.getWorldCoords(GOManager.player);
						UIManager.mouseChecker = new MouseMovementChecker(mouseWorldCoord);
						GOManager.player.setPickItem(true);
					}
				}
			}

		}

		if (button == MouseInput.LEFT_MOUSE_BUTTON) {
			if (MapManager.objectPlacerOn) {
				Vector3f pos = new Vector3f(MouseInput.getWorldCoords(GOManager.player).x,
						MouseInput.getWorldCoords(GOManager.player).y, 0);
				GameObject f = of.createItem(MapManager.placingTileID, pos);
				MapManager.mapObjects.add((WorldObject) f);
				Engine.getSceneManager().addGameObject(f);
			}

			if (UIManager.combatFocused) {
			} else if (UIManager.resultFocused) {
				for (GameObject b : UIManager.getResultBG().getChildren()) {
					if (b.hasHitbox()) {
						if (b.getHitbox().isInside(MouseInput.getMousePosition())) {
							if (((Button) b).getName() == "resultButton") {
								((Button) b).onClick();
							}
						}
					}
				}
			} else {
				UIManager.gameSceneFocused = true;

				// if UIObject was clicked on.

				for (GameObject go : Engine.getSceneManager().getUIScene().getObjects()) {

					if (go.hasHitbox()) {
						if (go.getHitbox().isInside(MouseInput.getMousePosition())) {
							if (go instanceof ObjectPlacer) {
								((ObjectPlacer) go).onClick();
								UIManager.gameSceneFocused = false;
							} else if (go instanceof Button) {
								UIManager.gameSceneFocused = false;
								System.out.println("Button hit");
								((Button) go).onClick();
							} else if (go instanceof Slider) {
								UIManager.gameSceneFocused = false;
								((Slider) go).dragOn();
								UIManager.dragSliders = true;
							} else if (go instanceof Item) {
								((Item) go).dragMe = true;
								UIManager.dragItem = true;
								UIManager.draggedItem = ((Item) go);
								System.out.println("item hit");
							} else if (go instanceof InputField) {
								UIManager.gameSceneFocused = false;
								((InputField) go).onClick();
								UIManager.listenForInput = true;
							} else if (go instanceof DropField) {
								UIManager.gameSceneFocused = false;
								System.out.println("dropfield hit");
							} else if (go instanceof Inventory) {
								UIManager.gameSceneFocused = false;
								System.out.println("inventory hit");
							} else if (go instanceof InventoryBorder) {
								System.out.println("inventory border hit");
								UIManager.gameSceneFocused = false;
							}
						} else {
							if (go instanceof InputField) {
								if (((InputField) go).listenForInput) {
									((InputField) go).listenForInput = false;
									UIManager.setListenForInput(false);
									System.out.println("Stopped listening");
									System.out.println("Text: " + ((InputField) go).getText());
								}
							}

						}
					}
				}

				// Checks in gameScene if player should be moved to mouse cursor
				// location.
				if (UIManager.gameSceneFocused && !MapManager.objectPlacerOn) {
					if (GOManager.player != null) {
						mouseWorldCoord = MouseInput.getWorldCoords(GOManager.player);
						UIManager.mouseChecker = new MouseMovementChecker(mouseWorldCoord);
					}
				}

			}
		}

	}

	@Override
	public void onMouseReleased(int button) {
		if (button == MouseInput.LEFT_MOUSE_BUTTON) {

			for (GameObject go : Engine.getSceneManager().getUIScene().getObjects()) {
				if (go instanceof Slider) {
					((Slider) go).dragOff();
					UIManager.dragSliders = false;
				}

			}

			if (UIManager.dragItem) {
				Inventory inv = UIManager.playerInventory;
				List<DropField> drops = inv.getFields();
				int count = drops.size();
				for (DropField drop : drops) {
					if (drop.getHitbox().isInside(MouseInput.getMousePosition())) {
						UIManager.dragItem = false;
						UIManager.draggedItem.dragMe = false;
						UIManager.draggedItem.setAbsolutePosition(drop.getAbsolutePosition().x,
								drop.getAbsolutePosition().y);
					} else {
						count -= 1;
						UIManager.dragItem = false;
					}
					if (count == 0) {
						GOManager.dropDraggedItem();
					}
				}
			}

		}
	}

	@Override
	public void onMouseDoubleClick(int button) {

	}

	@Override
	public void onKeyDown(int key) {
		if (Input.KEY_7 == key) {
			MapManager.loadMapObjects("res/MapObjects/map");
		}
		if (Input.KEY_8 == key) {
			if (!MapManager.objectPlacerOn) {
				MapManager.activateObjectPlacer();
			} else {
				MapManager.deactivateObjectPlacer();
			}
		}

		// input for combat ONLY
		if (UIManager.combatFocused) {
			// skill hotkeys
			if (Input.KEY_1 == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.use1();
				}
			}
			if (Input.KEY_2 == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.use2();
				}
			}
			if (Input.KEY_3 == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.use3();
				}
			}
			if (Input.KEY_4 == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.use4();
				}
			}
			// Change direction monster is facing
			if (Input.KEY_W == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(0);
					GOManager.monsterAnimation.setIndizes(0, 3);
				}
			}
			if (Input.KEY_A == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(2);
					GOManager.monsterAnimation.setIndizes(4, 7);
				}
			}
			if (Input.KEY_S == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(1);
					GOManager.monsterAnimation.setIndizes(12, 15);
				}
			}
			if (Input.KEY_D == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(3);
					GOManager.monsterAnimation.setIndizes(8, 11);
				}
			}

			if (Input.KEY_UP == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(0);
					int onY = GOManager.currentMonster.getOnTileY();
					int onX = GOManager.currentMonster.getOnTileX();
					if (GOManager.combatTiles[onX][onY].getNeighbours()[0] == 100) {
						// do nothing. Neighbour tile is outside of combat
						// field.
						System.out.println("not moving");
					} else {
						GOManager.currentMonster.setOnTileY(onY + 1);
						UIManager.keyChecker = new KeyMovementChecker(0);
					}
				}
			}
			if (Input.KEY_DOWN == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(1);
					int onY = GOManager.currentMonster.getOnTileY();
					int onX = GOManager.currentMonster.getOnTileX();
					if (GOManager.combatTiles[onX][onY].getNeighbours()[1] == 100) {
						// do nothing. Neighbour tile is outside of combat
						// field.
						System.out.println("not moving");
					} else {
						GOManager.currentMonster.setOnTileY(onY - 1);
						UIManager.keyChecker = new KeyMovementChecker(1);
					}
				}
			}
			if (Input.KEY_LEFT == key) {
				if (UIManager.keyChecker == null) {
					GOManager.currentMonster.setFacing(2);
					int onY = GOManager.currentMonster.getOnTileY();
					int onX = GOManager.currentMonster.getOnTileX();
					if (GOManager.combatTiles[onX][onY].getNeighbours()[2] == 100) {
						// do nothing. Neighbour tile is outside of combat
						// field.
						System.out.println("not moving");
					} else {
						GOManager.currentMonster.setOnTileX(onX + 1);
						UIManager.keyChecker = new KeyMovementChecker(2);
					}
				}
			}
			if (Input.KEY_RIGHT == key) {
				if (UIManager.keyChecker == null) {
					// System.out.println("Start " +
					// GOManager.currentMonster.getPosition().x + ","
					// + GOManager.currentMonster.getPosition().y);
					GOManager.currentMonster.setFacing(3);
					int onY = GOManager.currentMonster.getOnTileY();
					int onX = GOManager.currentMonster.getOnTileX();
					if (GOManager.combatTiles[onX][onY].getNeighbours()[3] == 100) {
						// do nothing. Neighbour tile is outside of combat
						// field.
						System.out.println("not moving");
					} else {
						GOManager.currentMonster.setOnTileX(onX - 1);
						UIManager.keyChecker = new KeyMovementChecker(3);
					}
				}
			}

		}

		if (Input.KEY_0 == key)

		{
			try {
				MapManager.loadMapForPrint(System.getProperty("user.dir") + "/res/Maps", "map1.csv", 50);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onKeyReleased(int key) {
		if (UIManager.keyChecker != null) {
			UIManager.keyChecker.setNextDirection(4);
		}

		/*
		 * if (UIManager.gameSceneFocused) { if (key == Input.KEY_UP) {
		 * UIManager.playerAnimation.setIndizes(12, 15);
		 * UIManager.playerAnimation.stop(); } else if (key == Input.KEY_LEFT) {
		 * UIManager.playerAnimation.setIndizes(4, 7);
		 * UIManager.playerAnimation.stop(); } else if (key == Input.KEY_DOWN) {
		 * UIManager.playerAnimation.setIndizes(0, 3);
		 * UIManager.playerAnimation.stop(); } else if (key == Input.KEY_RIGHT)
		 * { UIManager.playerAnimation.setIndizes(8, 11);
		 * UIManager.playerAnimation.stop(); } }
		 */
	}

	/**
	 * Checks for dragging interactions (currently for sliders only)
	 */
	private void checkForDragging() {
		if (UIManager.dragItem) {
			Inventory inv = UIManager.playerInventory;
			for (GameObject df : inv.getChildren()) {
				List<GameObject> list = df.getChildren();
				for (GameObject child : list) {
					if (((Item) child).dragMe) {
						Vector3f cc = child.getPosition();
						((Item) child).setAbsolutePosition(MouseInput.getMousePosition().x,
								MouseInput.getMousePosition().y);
					}
				}
			}
		}
		if (UIManager.dragSliders) {
			for (Slider s : userInterface.getSliders()) {
				if (s.dragMe) {
					float barWidth = s.getSliderBar().getHitbox().getWidth() / 2;
					float sliderWidth = s.getHitbox().getWidth() / 2;
					// Basic check if mouse is outside the boundaries of the
					// slider
					if (MouseInput.getMousePosition().x > s.getPosition().x
							&& s.getPosition().x + sliderWidth < s.getSliderBar().getPosition().x + barWidth) {
						// Checks if SliderObject would be outside from
						// instantenous mouse movements
						// If yes, sets position to the min/max but no further.
						if (MouseInput.getMousePosition().x > s.getSliderBar().getPosition().x + barWidth
								- sliderWidth) {
							s.setPosition(s.getSliderBar().getPosition().x + barWidth - sliderWidth, s.getPosition().y);
						} else {
							s.setPosition(MouseInput.getMousePosition().x, s.getPosition().y);
						}
					} else if (MouseInput.getMousePosition().x < s.getPosition().x
							&& s.getPosition().x - s.getHitbox().getWidth() / 2 > s.getSliderBar().getPosition().x
									- s.getSliderBar().getHitbox().getWidth() / 2) {
						// Checks if SliderObject would be outside from
						// instantenous mouse movements
						// If yes, sets position to the min/max but no further.
						if (MouseInput.getMousePosition().x < s.getSliderBar().getPosition().x - barWidth
								+ sliderWidth) {
							s.setPosition(s.getSliderBar().getPosition().x - barWidth + sliderWidth, s.getPosition().y);
						} else {
							s.setPosition(MouseInput.getMousePosition().x, s.getPosition().y);
						}
					}
				}
			}
		}
	}

	/**
	 * Ticks down life for projectiles during combat and checks if enemy HP
	 * reached 0 to end combat.
	 */
	private void tickCombat() {
		for (Projectile p : GOManager.projectiles) {
			p.countDown();
			p.move();
		}
		if (GOManager.enemyMonster.getMonster().getCurrentHP() <= 0) {
			UIManager.loadCombatResults();
		}
	}

	private void checkSpawns() {
		MapManager.lastTime += Math.random() * 10;

		if (MapManager.lastTime >= MapManager.delay) {
			MapManager.lastTime = 0;
			if ((enemies = MapManager.spawnMonsters()) != null && UIManager.combatFocused == false
					&& UIManager.resultFocused == false) {
				GOManager.startPvECombat(enemies);
			}
			;
		}
	}

	/**
	 * Checks for interactions with input fields
	 */
	private void checkForInputFields() {
		if (UIManager.listenForInput) {
			for (InputField f : userInterface.getInputFields()) {
				if (f.listenForInput) {
					f.inputFieldListen();
				}
			}
		}
	}

}
