package game.model.items;

public enum ItemType {
	/**
	 * Parts used for crafting Monsters, including Monster Hearts.
	 */
	Parts,
	/**
	 * Items used for upgrading a Monster.
	 * e.g. Monster cores, Evolution Stones, Upgrade Orbs, 
	 * Rarity Stones
	 */
	Steroids,
	/**
	 * e.g. Potions, Buffs, Capsules, Removers 
	 */
	Consumable,
	/**
	 * Wearable by character or monsters
	 */
	Cosmetics,
	/**
	 * Items that serves as key to check<br>
	 * character's progress in the game
	 */
	KeyItem,
	/**
	 * e.g. Bike, Fishing Rod
	 */
	Tool,
	/**
	 * Items that can be equipped by Monsters
	 */
	Gear,
	/**
	 * Runes that can be set into Rune Slots.
	 */
	Rune
}
