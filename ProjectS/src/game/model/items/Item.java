package game.model.items;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

public abstract class Item extends GameObject {

	public String id;
	public boolean dragMe = false;
	
	public Item(Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
	}
	
	public String getId() {
		return id;
	}
}
