package game.model.monster;

import java.util.HashMap;
import java.util.List;

import engine.utils.Tuple;
import game.model.items.Item;

/**
 * A Monster.
 *
 */

public abstract class Monster {

	/**
	 * unique identificator.
	 */
	protected float id;
	/**
	 * Name of the monster
	 */
	protected String name;
	/**
	 * Background story of the monster
	 */
	protected String description;
	/**
	 * Element of the monster.
	 * Gives weakness&resistances against other elements.
	 */
	protected Element element;
	/**
	 * Elemental affinity ranging from 1 to 4.<br>
	 * Each affinity point adds 25% to its weaknesses/resistances.
	 */
	protected int affinity;
	/**
	 * The species the monster belongs to.
	 */
	protected MonsterSpecies species;
	/**
	 * The group the monster belongs to.
	 * Affects how the monster can grow.
	 */
	protected MonsterGroup group;
	/**
	 * Natural rarity of the monster.
	 * Ranges from 1 to 5 stars.
	 * Each stage adds a multiplicator of +50% to stats.
	 */
	protected Rarity baseRarity;
	/**
	 * Experience monster gives on defeat.
	 */
	protected int experience;
	/**
	 * Drop table of the monster.
	 */
	protected List<Item> loot;
	
	
	/**
	 * The color type of the monster.
	 * Affects the monster's sprite.
	 */
	protected DeviantType deviant;
	/**
	 * Character that originally captured the monster.
	 */
	protected String OT;
	
	//Progress Attributes
	/**
	 * Level of the monster. Increases the monster's stats
	 * by level/100*BaseStats.
	 */
	protected int level;
	/**
	 * Amount of fusions with duplicates of this monster.
	 */
	protected int fusionLevel;
	/**
	 * 
	 */
	protected int powerLevel;
	/**
	 * Training points obtainable by training with bracelets.
	 * Ranging from 0 to 120.
	 * Adds a flat bonus of TP/4.
	 */
	protected int TP;
	/**
	 * The current evolution stage of the monster.
	 * Ranges from 1 to 5.
	 */
	protected int evoStage;
	/**
	 * The current rarity of the monster.
	 * Ranges from 1 to 5 stars.
	 */
	protected Rarity currentRarity;
	
	//Combat Attributes
	protected int currentHP;
	//Base stats that are translated into actual numbers with level but stay FIX
	protected int Health;
	protected int Str;
	protected int Int;
	protected int Dex;
	protected int Def;
	protected int Mdef;
	protected float crit;
	protected float critDmg;
	protected float Spd;
	protected int Weight;
	/**
	 * Contains the Elemental Resistances of the monster.
	 * keys are named after the 4 Attack Types.
	 * The modifier value ranges from 0 to 100 resistance.
	 */
	protected HashMap<String, Integer> atkRes = new HashMap<String, Integer>();
	/**
	 * Contains the Elemental Resistances of the monster.
	 * keys are named after the 8 elements.
	 * The modifier value ranges from 0 to 200 percent of the typical 100 % damage.
	 */
	protected HashMap<String, Integer> elementRes = new HashMap<String, Integer>();
	
	/**
	 * Maps integers to a monster size.
	 * 1 = 1x1, 2 = 2x2, 3 = 3x3
	 */
	protected int size;
	protected Passive activePassive1;
	protected Passive activePassive2;
	protected Reactive activeReactive;
	protected Skillset activeMain;
	protected Skillset activeSub;
	/**
	 * List of main skillsets known by the Monster.
	 */
	protected List<Skillset> skillsets;
	/**
	 * List of sub skillsets known by the Monster.
	 */
	protected List<Skillset> subSkillsets;
	/**
	 * List of Passives known by the Monster.
	 */
	protected List<Passive> knownPassives;
	/**
	 * List of Known reactives.
	 */
	protected List<Reactive> knownReactives;
	/**
	 * Equipped gear: Weapon, Armor, Accessory
	 */
	protected List<Item> gear;
	/**
	 * Equipped runes.
	 */
	protected List<Item> Runes;
	
	//Job Attributes
	/**
	 * Increases speed at which buildings are built.
	 */
	protected int building;
	/**
	 * Increases speed at which buildings are repaired.
	 */
	protected int repair;
	/**
	 * Increases the yield from production facilities.
	 */
	protected int harvest;
	public float getId() {
		return id;
	}
	public void setId(float id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Element getElement() {
		return element;
	}
	public void setElement(Element element) {
		this.element = element;
	}
	public int getAffinity() {
		return affinity;
	}
	public void setAffinity(int affinity) {
		this.affinity = affinity;
	}
	public MonsterSpecies getSpecies() {
		return species;
	}
	public void setSpecies(MonsterSpecies species) {
		this.species = species;
	}
	public MonsterGroup getGroup() {
		return group;
	}
	public void setGroup(MonsterGroup group) {
		this.group = group;
	}
	public Rarity getBaseRarity() {
		return baseRarity;
	}
	public void setBaseRarity(Rarity baseRarity) {
		this.baseRarity = baseRarity;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public List<Item> getLoot() {
		return loot;
	}
	public void setLoot(List<Item> loot) {
		this.loot = loot;
	}
	public DeviantType getDeviant() {
		return deviant;
	}
	public void setDeviant(DeviantType deviant) {
		this.deviant = deviant;
	}
	public String getOT() {
		return OT;
	}
	public void setOT(String oT) {
		OT = oT;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getFusionLevel() {
		return fusionLevel;
	}
	public void setFusionLevel(int fusionLevel) {
		this.fusionLevel = fusionLevel;
	}
	public int getPowerLevel() {
		return powerLevel;
	}
	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}
	public int getTP() {
		return TP;
	}
	public void setTP(int tP) {
		TP = tP;
	}
	public int getEvoStage() {
		return evoStage;
	}
	public void setEvoStage(int evoStage) {
		this.evoStage = evoStage;
	}
	public Rarity getCurrentRarity() {
		return currentRarity;
	}
	public void setCurrentRarity(Rarity currentRarity) {
		this.currentRarity = currentRarity;
	}
	public int getCurrentHP() {
		return currentHP;
	}
	public void setCurrentHP(int currentHP) {
		this.currentHP = currentHP;
	}
	public int getHealth() {
		return Health;
	}
	public void setHealth(int health) {
		Health = health;
	}
	public int getStr() {
		return Str;
	}
	public void setStr(int str) {
		Str = str;
	}
	public int getInt() {
		return Int;
	}
	public void setInt(int i) {
		Int = i;
	}
	public int getDex() {
		return Dex;
	}
	public void setDex(int dex) {
		Dex = dex;
	}
	public int getDef() {
		return Def;
	}
	public void setDef(int def) {
		Def = def;
	}
	public int getMdef() {
		return Mdef;
	}
	public void setMdef(int mdef) {
		Mdef = mdef;
	}
	public float getCrit() {
		return crit;
	}
	public void setCrit(float crit) {
		this.crit = crit;
	}
	public float getCritDmg() {
		return critDmg;
	}
	public void setCritDmg(float critDmg) {
		this.critDmg = critDmg;
	}
	public float getSpd() {
		return Spd;
	}
	public void setSpd(float spd) {
		Spd = spd;
	}
	public int getWeight() {
		return Weight;
	}
	public void setWeight(int weight) {
		Weight = weight;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Passive getActivePassive1() {
		return activePassive1;
	}
	public void setActivePassive1(Passive activePassive1) {
		this.activePassive1 = activePassive1;
	}
	public Passive getActivePassive2() {
		return activePassive2;
	}
	public void setActivePassive2(Passive activePassive2) {
		this.activePassive2 = activePassive2;
	}
	public Reactive getActiveReactive() {
		return activeReactive;
	}
	public void setActiveReactive(Reactive activeReactive) {
		this.activeReactive = activeReactive;
	}
	public Skillset getActiveMain() {
		return activeMain;
	}
	public void setActiveMain(Skillset activeMain) {
		this.activeMain = activeMain;
	}
	public Skillset getActiveSub() {
		return activeSub;
	}
	public void setActiveSub(Skillset activeSub) {
		this.activeSub = activeSub;
	}
	public List<Skillset> getSkillsets() {
		return skillsets;
	}
	public void setSkillsets(List<Skillset> skillsets) {
		this.skillsets = skillsets;
	}
	public List<Skillset> getSubSkillsets() {
		return subSkillsets;
	}
	public void setSubSkillsets(List<Skillset> subSkillsets) {
		this.subSkillsets = subSkillsets;
	}
	public List<Passive> getKnownPassives() {
		return knownPassives;
	}
	public void setKnownPassives(List<Passive> knownPassives) {
		this.knownPassives = knownPassives;
	}
	public List<Reactive> getKnownReactives() {
		return knownReactives;
	}
	public void setKnownReactives(List<Reactive> knownReactives) {
		this.knownReactives = knownReactives;
	}
	public List<Item> getGear() {
		return gear;
	}
	public void setGear(List<Item> gear) {
		this.gear = gear;
	}
	public List<Item> getRunes() {
		return Runes;
	}
	public void setRunes(List<Item> runes) {
		Runes = runes;
	}
	public int getBuilding() {
		return building;
	}
	public void setBuilding(int building) {
		this.building = building;
	}
	public int getRepair() {
		return repair;
	}
	public void setRepair(int repair) {
		this.repair = repair;
	}
	public int getHarvest() {
		return harvest;
	}
	public void setHarvest(int harvest) {
		this.harvest = harvest;
	}
	public HashMap<String, Integer> getAtkRes() {
		return atkRes;
	}
	public HashMap<String, Integer> getElementRes() {
		return elementRes;
	}
	
}
