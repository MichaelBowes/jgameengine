package game.model.monster;

import game.model.monsterlist.Momon;
import game.model.monsterlist.Mudster;
import game.model.monsterlist.Pom;
import game.model.monsterlist.Zwitscher;

public class MonsterFactory {

	public MonsterFactory() {
	}

	public Monster createMonster(int id, int level) {
		Monster wo = null;
		switch (id) {
		case 0:
			wo = new Momon(level);
			return wo;
		case 1:
			wo = new Pom();
			return wo;
		case 2:
			wo = new Zwitscher();
			return wo;
		case 3:
			wo = new Mudster();
			return wo;
		}

		return wo;
	}
	
}
