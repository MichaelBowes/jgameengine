package game.model.monster;

/**
 * Rarity of the monster or Item:
 * Common,Uncommon,Rare,VeryRare,UltraRare
 */

public enum Rarity {

	/**
	 * 1* Common
	 */
	C,
	/**
	 * 2* Uncommon
	 */
	UC,
	/**
	 * 3* Rare
	 */
	R,
	/**
	 * 4* Very Rare
	 */
	VR,
	/**
	 * 5* Ultra Rare
	 */
	UR
}
