package game.model.monster;

/**
 * Monsters may deviate from their normal appearance in color:
 * Normal, Shiny, Dark, Rainbow
 */
public enum DeviantType {
	Normal,
	Shiny,
	Dark,
	Rainbow
}
