package game.model.monster;

/**
 * Group of a monster:
 * Altros, Virus, Parasite, Decomposer.
 * Cancelled Groups: Scavenger, Hunter
 */
public enum MonsterGroup {
	Altros,
	Virus,
	Parasite,
	Decomposer,
}
