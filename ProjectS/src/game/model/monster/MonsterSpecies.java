package game.model.monster;

/**
 * 
 * Monster species that can be met, including:<br>
 * Pom, Humanoid, Undead, Madou, Feral, Insectoid, Mythical, Ancient.<br>
 * Cancelled species: Amphibian/Aquatic, Plant, Spirit
 * 
 */

public enum MonsterSpecies {

	/**
	 * Pom monsters
	 */
	Slime,
	/**
	 * Human-like creatures
	 */
	Humanoid,
	/**
	 * Undead creatures
	 */
	Undead,
	/**
	 * Magic-fueled creatures e.g. Machines, Golems
	 */
	Madou,
	/**
	 * Animal-like creatures
	 */
	Feral,
	/**
	 * Insect-like creatures
	 */
	Insectoid,
	/**
	 * Mythical creatures e.g. Minotaurs, Medusa, Satyr
	 */
	Mythical,
	/**
	 * Ancient creatures e.g. Dinosaurs, Dragons
	 */
	Ancient,
}
