package game.model.monster;

/**
 * Elemental type of a monster:
 * Water > Fire > Earth > Wind >
 * 
 * Order > Chaos >
 * 
 * Hope > Despair >
 * 
 */

public enum Element {
	Fire,
	Water,
	Earth,
	Wind,
	Order,
	Chaos,
	Hope,
	Despair
}
