package game.model.character;

import java.util.List;

import engine.Engine;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import engine.textures.manager.TextureManager;
import game.logic.GOManager;
import game.logic.UIManager;
import game.map.Tile;
import game.model.items.Item;
import game.model.monster.Monster;
import game.map.WorldObject;
import javolution.util.FastTable;

public class PlayerCharacter extends GameObject {

	/**
	 * Unique identifier
	 */
	private String id;
	/**
	 * Character name chosen by the user
	 */
	private String name;

	private Vector3f prevPos = new Vector3f(0, 0, 0);
	private boolean xFacing;
	private boolean yFacing;
	/**
	 * tells whether player is currently trying to interact with a dropped item.
	 */
	private boolean pickItem;
	/**
	 * in-game currency owned by the character
	 */
	private String gold;
	/**
	 * total hours the character was played with
	 */
	private String hoursPlayed;
	/**
	 * the chosen {@link CharClass} of the character
	 */
	private CharClass charClass;
	/**
	 * List of monsters in the user's current party
	 */
	private List<Monster> party;
	/**
	 * List of monsters in the user's storage box
	 */
	private List<Monster> storageBox;
	/**
	 * List of items carried by the user
	 */
	private List<Item> inventory = new FastTable<Item>();
	/**
	 * List of items equipped by the user
	 */
	private List<Item> equip = new FastTable<Item>();
	/**
	 * Map the character is currently on.
	 */
	private String currentMap;

	public PlayerCharacter(String name, Sprite sprite, Vector3f position, List<Monster> party) {
		super(position);
		this.name = name;
		setSprite(sprite);
		this.party = party;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGold() {
		return gold;
	}

	public void setGold(String gold) {
		this.gold = gold;
	}

	public String getHoursPlayed() {
		return hoursPlayed;
	}

	public void setHoursPlayed(String hoursPlayed) {
		this.hoursPlayed = hoursPlayed;
	}

	public CharClass getCharClass() {
		return charClass;
	}

	public void setCharClass(CharClass charClass) {
		this.charClass = charClass;
	}

	public List<Monster> getParty() {
		return party;
	}

	public void setParty(List<Monster> party) {
		this.party = party;
	}

	public List<Monster> getStorageBox() {
		return storageBox;
	}

	public void setStorageBox(List<Monster> storageBox) {
		this.storageBox = storageBox;
	}

	public List<Item> getInventory() {
		return inventory;
	}

	public List<Item> getEquip() {
		return equip;
	}

	public void setInventory(List<Item> inventory) {
		this.inventory = inventory;
	}

	public String getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(String currentMap) {
		this.currentMap = currentMap;
	}

	@Override
	public void onCollision(GameObject collider) {

		System.out.println(collider.getClass());

		if (collider instanceof WorldObject) {
			if (((WorldObject) collider).getTexture().getPath().contains("block")) {
				if (yFacing) {
					this.changePosition(0, 0.05f);
				} else {
					this.changePosition(0, -0.05f);
				}
				if (xFacing) {
					this.changePosition(0.05f, 0);
				} else {
					this.changePosition(-0.05f, 0);
				}
				Engine.getCamera().setPosition(GOManager.player.getPosition().x, GOManager.player.getPosition().y,
						Engine.getCamera().getPosition().z);
				UIManager.mouseChecker = null;
				GOManager.playerAnimation.stop();
			}
		}
		if (collider instanceof Item) {
			if (this.pickItem) {
				UIManager.addInventoryItem();
				GOManager.removeDroppedItem(collider);
				pickItem = false;
			}
		}
	}

	@Override
	public void onCollisionExit(GameObject collider) {
	}

	public boolean isPickItem() {
		return pickItem;
	}

	public void setPickItem(boolean pickItem) {
		this.pickItem = pickItem;
	}

	public boolean isxFacing() {
		return xFacing;
	}

	public void setxFacing(boolean xFacing) {
		this.xFacing = xFacing;
	}

	public boolean isyFacing() {
		return yFacing;
	}

	public void setyFacing(boolean yFacing) {
		this.yFacing = yFacing;
	}

	public Vector3f getPrevPos() {
		return prevPos;
	}

	public void setPrevPos(Vector3f prevPos) {
		this.prevPos = prevPos;
	}
}
