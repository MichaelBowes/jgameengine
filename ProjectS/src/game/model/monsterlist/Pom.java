package game.model.monsterlist;

import game.model.monster.Element;
import game.model.monster.Monster;
import game.model.monster.MonsterSpecies;
import game.model.monster.Rarity;

public class Pom extends Monster {

	
	public Pom(){
		this.id = id;
		this.name = "Pom";
		this.description = "";
		this.element = Element.Wind;
		this.affinity = 1;
		this.species = MonsterSpecies.Slime;
		this.baseRarity = Rarity.C;
		
		this.Health = 620;
		this.Str = 300;
		this.Int = 400;
		this.Dex = 100;
		this.Def = 250;
		this.Mdef = 250;
		this.crit = 2.5f;
		this.critDmg = 1.5f;
		this.Spd = 2f;
		this.Weight = 5;
		this.size = 1;
		
		//this.group = MonsterGroup.Decomposer;
	}
}
