package game.model.monsterlist;

import game.model.monster.Element;
import game.model.monster.Monster;
import game.model.monster.MonsterSpecies;
import game.model.monster.Rarity;

public class Momon extends Monster {
	
	public Momon(int level) {
		this.id = id;
		this.name = "Momon";
		this.description = "";
		this.element = Element.Hope;
		this.affinity = 1;
		this.species = MonsterSpecies.Feral;
		this.baseRarity = Rarity.C;
		this.level = level;
		
		this.Health = Math.round(510*(level/100f));
		this.currentHP = Health;
		this.Str = Math.round(400*(level/100f));
		this.Int = Math.round(80*(level/100f));
		this.Dex = Math.round(400*(level/100f));
		this.Def = Math.round(150*(level/100f));
		this.Mdef = Math.round(150*(level/100f));
		this.crit = 2.5f;
		this.critDmg = 1.5f;
		this.Spd = 2f;
		this.Weight = 5;
		this.size = 1;
		
		this.atkRes.put("Slash", 0);
		this.atkRes.put("Blunt", 0);
		this.atkRes.put("Projectile", 0);
		this.atkRes.put("Pierce", 0);
		
		this.elementRes.put("Fire", 0);
		this.elementRes.put("Water", 0);
		this.elementRes.put("Wind", 0);
		this.elementRes.put("Earth", 0);
		this.elementRes.put("Order", 0);
		this.elementRes.put("Chaos", 0);
		this.elementRes.put("Hope", 0);
		this.elementRes.put("Despair", 0);
		// this.group = MonsterGroup.Decomposer;
	}
}
