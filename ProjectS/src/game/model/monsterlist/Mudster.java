package game.model.monsterlist;

import game.model.monster.Element;
import game.model.monster.Monster;
import game.model.monster.MonsterSpecies;
import game.model.monster.Rarity;

public class Mudster extends Monster {
	
	public Mudster() {
		this.id = id;
		this.name = "Mudster";
		this.description = "";
		this.element = Element.Earth;
		this.affinity = 1;
		this.species = MonsterSpecies.Madou;
		this.baseRarity = Rarity.C;

		this.Health = 810;
		this.Str = 300;
		this.Int = 200;
		this.Dex = 100;
		this.Def = 250;
		this.Mdef = 50;
		this.crit = 2.5f;
		this.critDmg = 1.5f;
		this.Spd = 2f;
		this.Weight = 10;
		this.size = 1;
	}
}
