package game.model.monsterlist;

import game.model.monster.Element;
import game.model.monster.Monster;
import game.model.monster.MonsterSpecies;
import game.model.monster.Rarity;
public class Zwitscher extends Monster {
	
	public Zwitscher() {
		this.id = id;
		this.name = "Zwitscher";
		this.description = "";
		this.element = Element.Wind;
		this.affinity = 1;
		this.species = MonsterSpecies.Feral;
		this.baseRarity = Rarity.UC;

		this.Health = 610;
		this.Str = 200;
		this.Int = 80;
		this.Dex = 400;
		this.Def = 150;
		this.Mdef = 200;
		this.crit = 2.5f;
		this.critDmg = 1.5f;
		this.Spd = 2f;
		this.Weight = 10;
		this.size = 1;
	}
}
