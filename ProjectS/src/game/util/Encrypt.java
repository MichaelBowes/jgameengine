package game.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author BpZ
 * 
 * Contains methods for encrypting and decrypting data
 * and writing or reading it from and to files.
 *
 */
public class Encrypt {

	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";

	/**
	 * Reads a given file and writes it with encryption in the given output file.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param inputFile - unencrypted file to be encrypted.
	 * @param outputFile - file to be written to.
	 * @throws FileNotFoundException if the input or output file could not be found.
	 * @throws IOException 
	 * @throws InvalidKeyException if the given key was invalid.
	 */
	public static void encrypt(String key, File inputFile, File outputFile) 
			throws FileNotFoundException, IOException, InvalidKeyException {
		
		byte[] input = null;
		try(FileInputStream inputStream = new FileInputStream(inputFile)){
			input = new byte[(int) inputFile.length()];
			inputStream.read(input);	
		}
		if(input != null) {
			encryptWrite(key, input, outputFile);
		}	
	}

	/**
	 * Reads an encrypted file and writes is decripted in the given output file.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param inputFile - encrypted file to be read.
	 * @param outputFile - file to be written to.
	 * @throws InvalidKeyException if the given key was invalid.
	 * @throws FileNotFoundException if the input or output file could not be found.
	 * @throws IOException
	 */
	public static void decrypt(String key, File inputFile, File outputFile) 
			throws InvalidKeyException, FileNotFoundException, IOException {
		
		byte[] decrypted = decryptRead(key, inputFile);
		if(decrypted != null) {
			try(FileOutputStream outputStream = new FileOutputStream(outputFile)){		
				outputStream.write(decrypted);
			}
		}		
	}
	
	/**
	 * Encrypts a given byte array and writes it to the given file.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param input - data to be encrypted.
	 * @param outputFile - file to be written to.
	 * @throws FileNotFoundException if the output file could not be found.
	 * @throws IOException
	 * @throws InvalidKeyException if the given key was invalid.
	 */
	public static void encryptWrite(String key, byte[] input, File outputFile)
			throws FileNotFoundException, IOException, InvalidKeyException {
		
		try(FileOutputStream outputStream = new FileOutputStream(outputFile)){
			byte[] encryptedInput;
			encryptedInput = doEncryption(key, input);	
			outputStream.write(encryptedInput);
		}
	}
	
	/**
	 * Decrypts a given file and returns its content as byte array.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param inputFile - file to be decrypted.
	 * @return the decrypted content of the file as byte array.
	 * @throws FileNotFoundException if the given file could not be found.
	 * @throws IOException
	 * @throws InvalidKeyException if the given key was invalid.
	 */
	public static byte[] decryptRead(String key, File inputFile) 
			throws FileNotFoundException, IOException, InvalidKeyException {
		
		byte[] input = null;
		byte[] decrypted = null;
		try(FileInputStream inputStream = new FileInputStream(inputFile)){
			input = new byte[(int) inputFile.length()];
			inputStream.read(input);	
			decrypted = doDecryption(key, input);
		}
		return decrypted;
	}

	/**
	 * Encrypts the given byte array and returns an encrypted copy.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param input - data to be encrypted.
	 * @return the encrypted byte array.
	 * @throws InvalidKeyException if the given key was invalid.
	 */
	public static byte[] doEncryption(String key, byte[] input) throws InvalidKeyException {
		if(input == null) {
			throw new IllegalArgumentException("The data to be ecrypted mustn't be null");
		}
		byte[] usedKey;
		if(key == null) {
			usedKey = getKey();
		}else {
			usedKey = key.getBytes();
		}
		Key secretKey = new SecretKeySpec(usedKey, ALGORITHM);
		Cipher cipher = null;
		byte[] output = null;
		try {
			cipher = Cipher.getInstance(TRANSFORMATION);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);

			output = cipher.doFinal(input);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException |
				IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	/**
	 * Decrypts a given byte array and returns a dercypted copy.
	 * @param key - has to be of length 8, 16 or be null for use of a static key.
	 * @param input - data to be decrypted.
	 * @return the decrypted byte array.
	 * @throws InvalidKeyException if the given key was invalid.
	 */
	public static byte[] doDecryption(String key, byte[] input) throws InvalidKeyException {
		if(input == null) {
			throw new IllegalArgumentException("The data to be decrypted mustn't be null");
		}
		byte[] usedKey;
		if(key == null) {
			usedKey = getKey();
		}else {
			usedKey = key.getBytes();
		}
		Key secretKey = new SecretKeySpec(usedKey, ALGORITHM);
		Cipher cipher = null;
		byte[] output = null;
		try {
			cipher = Cipher.getInstance(TRANSFORMATION);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);

			output = cipher.doFinal(input);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException |
				IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}
	
	private static byte[] getKey() {
		byte[] key = new byte[] { 
				12, 22, -2, 24, 41, -42, -2 
				, -1, 10, -2, 91, -63 
				, 82, -36, -12, 3};
		return key;
	}
}
