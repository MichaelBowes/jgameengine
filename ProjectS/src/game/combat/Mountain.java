package game.combat;

import engine.math.Vector3f;
import engine.textures.Sprite;
import game.model.monster.Element;

/**
 * Stationary object.
 * Blocks Monster movement.
 * Destructible
 *
 */
public class Mountain extends Projectile {

	public Mountain(Sprite sprite, Vector3f position, int facing, Element element,
			short atkType){
		super(sprite, position, facing, element, atkType);
		this.speed = 0;
		this.impactDamage=5;
		this.hp = 5;
		this.armor = 1;
	}
	
}
