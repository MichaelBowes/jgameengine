package game.combat;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.logic.GOManager;
import game.model.monster.Element;

/**
 * A class covering all non-tile, non-Monster objects in the PvE combat scene.
 * Includes objects such as flying or stationary projectiles and blocking wall objects.
 */
public abstract class Projectile extends GameObject{

	/**
	 * Random id to identify the projectile on the combat field.
	 */
	protected double id;
	protected int casterId;
	/**
	 * The speed at which the projectile flies.
	 * At speed 0, the projectile is stationary.
	 */
	float speed;
	/**
	 * Direction the projectile is facing.
	 */
	int facing;
	/**
	 * The health of the projectile. On reaching 0, the Projectile is
	 * destroyed.
	 */
	int hp;
	/**
	 * The hp that serves as a countdown for the projectiles lifecycle.
	 */
	int softHP;
	/**
	 * Is inflicted on other Projectiles on collision.
	 */
	int impactDamage;
	/**
	 * Is inflicted on Monsters on collision.
	 */
	int Damage;
	/**
	 * False if physical, true if magical attack.
	 */
	boolean magical;
	/**
	 * Element of the attack.
	 */
	Element element;
	/**
	 * Type of the attack.
	 * 0 = slash, 1 = blunt, 2 = projectile, 3 = pierce.
	 */
	short atkType;
	/**
	 * Absorption value of the projectile. 
	 * Reduces incoming damage from collision with other projectiles. 
	 * Default is 0.
	 * 
	 */
	int armor=0;
	
	/**
	 * Is called either if the projectile moves onto a tile with another projectile/monster
	 * or another projectile/monster moves onto a space where the projectile currently is.
	 */
	
	public Projectile(Sprite sprite, Vector3f position, int facing, Element element, short atkType){
		super(position);
		setSprite(sprite);
		this.facing = facing;
		id=Math.random()*100+Math.random()*100;
		this.element = element;
		this.atkType = atkType;
	}
	
	/**
	 * reduces the projectiles softHP by 1 per tick
	 * and destroys the projectile if softHP reaches 0.
	 */
	public void countDown(){
		this.softHP-=1;
		if(softHP == 0 || hp <= 0){
			GOManager.destroyProjectile(id);
		}
	}
	
	/**
	 * Called every frame. Must be implemented in the child classes.
	 */
	public void move(){};
	
	@Override
	public void onCollision(GameObject collider) {
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
	
	public void setId(double id){
		this.id = id;
	}

	public double getId(){
		return id;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getImpactDamage() {
		return impactDamage;
	}

	public void setImpactDamage(int impactDamage) {
		this.impactDamage = impactDamage;
	}

	public int getDamage() {
		return Damage;
	}

	public void setDamage(int damage) {
		Damage = damage;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getCasterId() {
		return casterId;
	}

	public void setCasterId(int casterId) {
		this.casterId = casterId;
	}
	
}
