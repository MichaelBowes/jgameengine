package game.combat;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import game.model.monster.Element;

/**
 * Slow projectile that is easily destroyed without destroying other
 * projectiles.
 *
 */
public class GooShot extends Projectile {

	public GooShot(int dmg, int casterId, Sprite sprite, Vector3f position, int facing, Element element, short atkType) {
		super(sprite, position, facing, element, atkType);
		this.speed = 2f;
		this.impactDamage = 1;
		this.hp = 1;
		this.softHP = 20;
		this.armor = 0;
		setCasterId(casterId);
		this.Damage = dmg;
	}

	@Override
	public void onCollision(GameObject collider) {
		if (collider instanceof Projectile) {
			if (((Projectile) collider).getCasterId() == this.getCasterId()) {
			} else {
				// subtract the maximum between either 0 or the
				// impactDamage-armor.
				hp = hp - Math.max(0, (((Projectile) collider).getImpactDamage() - armor));
			}
		} else if (collider instanceof CombatMonster && hp > 0) {
			if (casterId == ((CombatMonster)collider).getId()) {
			} else {
				hp = 0;
				int atkRes = 0;
				switch (atkType) {
				case 0:
					atkRes = ((CombatMonster) collider).getMonster().getAtkRes().get("Slash");
					break;
				case 1:
					atkRes = ((CombatMonster) collider).getMonster().getAtkRes().get("Blunt");
					break;
				case 2:
					atkRes = ((CombatMonster) collider).getMonster().getAtkRes().get("Projectile");
					break;
				case 3:
					atkRes = ((CombatMonster) collider).getMonster().getAtkRes().get("Pierce");
					break;
				}
				int eleMod = 0;
				switch (element) {
				case Fire:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Fire");
					break;
				case Water:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Water");
					break;
				case Wind:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Wind");
					break;
				case Earth:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Earth");
					break;
				case Hope:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Hope");
					break;
				case Despair:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Despair");
					break;
				case Chaos:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Chaos");
					break;
				case Order:
					eleMod = ((CombatMonster) collider).getMonster().getElementRes().get("Order");
					break;
				}

				if (magical == true) {
					((CombatMonster) collider).getMonster()
							.setCurrentHP(Math.max(0, ((CombatMonster) collider).getMonster().getCurrentHP()
									- Math.round(Math.max(0, (Damage - ((CombatMonster) collider).getMonster().getMdef()
											* (1f - (atkRes / 100f) * (1f * (eleMod / 100f))))))));
				} else {
					((CombatMonster) collider).getMonster()
							.setCurrentHP(Math.max(0, ((CombatMonster) collider).getMonster().getCurrentHP()
									- Math.round(Math.max(0, (Damage - ((CombatMonster) collider).getMonster().getDef()
											* (1f - (atkRes / 100f) * (1f * (eleMod / 100f))))))));
				}

				System.out.println("HP: " + ((CombatMonster) collider).getMonster().getCurrentHP());
			}

		}

	}

	@Override
	public void move() {
		switch (facing) {
		case 0:
			this.changePositionIgnoreDelta(0, speed);
			break;
		case 1:
			this.changePositionIgnoreDelta(0, -speed);
			break;
		case 2:
			this.changePositionIgnoreDelta(-speed, 0);
			break;
		case 3:
			this.changePositionIgnoreDelta(speed, 0);
			break;
		}
	}

}
