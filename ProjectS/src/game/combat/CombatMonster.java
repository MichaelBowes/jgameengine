package game.combat;

import engine.Engine;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import engine.textures.SpriteAnimation;
import engine.textures.manager.TextureManager;
import game.logic.GOManager;
import game.model.monster.Element;
import game.model.monster.Monster;

/**
 * Monster in the real-time PvE combat.
 */
public class CombatMonster extends GameObject {

	/**
	 * The identification number of this monster on the combat field.
	 */
	private int id;
	/**
	 * Tells which direction the monster is currently facing. Important info for
	 * Projectiles to know where they should spawn. 0 = up, 1 = down, 2 = left,
	 * 3 = right
	 */
	private int facing;
	/**
	 * Tells which direction the Monster is trying to come from. Important info
	 * for tiles.
	 */
	boolean L, R, U, D;
	/**
	 * The monster this object is supposed to represent.
	 */
	Monster monster;
	/**
	 * ID of the tile the monster is currently on.
	 */
	private static int onTileX;
	private static int onTileY;

	public CombatMonster(int id, Sprite sprite, Vector3f position, Monster monster) {
		super(position);
		setSprite(sprite);
		this.id = id;
		this.monster = monster;
	}

	public void use1() {
		// TODO: get move from Monster reference instead
		GooShot p = (GooShot) createProjectile(1, 0, "res/pictures/atk_1.png");
		if (p != null) {
			p.addHitbox(23, 21);
			GOManager.projectiles.add(p);
			Engine.getSceneManager().addGameObject(p);
		}
	}

	public void use2() {
		GooShot p = (GooShot) createProjectile(3, -1, "res/pictures/atk_2.png");
		if (p != null) {
			GOManager.projectiles.add(p);
			Engine.getSceneManager().addGameObject(p);
		}
	}

	public void use3() {

	}

	public void use4() {

	}

	/**
	 * Creates a full projectile object and spawns it at the given location.
	 * 
	 * @param x
	 *            -
	 * @param y
	 *            -
	 * @param direction
	 * @param relativePath
	 * @return
	 */
	private Projectile createProjectile(int x, int y, String relativePath) {
		Projectile g;
		SpriteAnimation s0 = createMoveAnimation(relativePath);
		Vector3f spawn = spawnDiagonally(x, y);
		if (spawn != null) {
			int dmg = 2;
			g = new GooShot(10, id, s0, spawnDiagonally(x, y), facing, Element.Earth, (short) 2);
		} else {
			g = null;
		}
		return g;
	}

	/**
	 * Calculates the spawnpoint of the projectile to spawn based on 2 factors.
	 * 
	 * @param f1
	 *            - factor1, affects projectile position horizontally. + = left,
	 *            - = right
	 * @param f2
	 *            - factor2, affects projectile position vertically. + = up, - =
	 *            down
	 * 
	 * @return The vector3f of the tile to be spawned on. Returns null if tile
	 *         does not exist.
	 */
	private Vector3f spawnDiagonally(int f1, int f2) {
		Vector3f pos = null;
		switch (facing) {
		case 0:
			pos = GOManager.currentMonster.getPosition().add(new Vector3f(1 * f2, 1 * f1, 0.01f));
			break;
		case 1:
			pos = GOManager.currentMonster.getPosition().add(new Vector3f(-1 * f2, -1 * f1, 0.01f));
			break;
		case 2:
			pos = GOManager.currentMonster.getPosition().add(new Vector3f(1 * f1, -1 * f2, 0.01f));
			break;
		case 3:
			pos = GOManager.currentMonster.getPosition().add(new Vector3f(-1 * f1, 1 * f2, 0.01f));
			break;
		}
		return pos;
	}

	/*
	 * Deprecated.
	 * Spawned Projectiles only on tiles. private static Vector3f
	 * spawnDiagonally(int f1, int f2) { Vector3f pos = null; switch (facing) {
	 * case 0: if(onTileX+1*f2 > 12 || onTileY+1*f1>12 || onTileX+1*f2 < 0||
	 * onTileY+1*f1<0){ } else { pos =
	 * GOManager.combatTiles[onTileX+1*f2][onTileY+1*f1].getPosition().add(new
	 * Vector3f(0, 0, 0.01f)); } break; case 1: if(onTileX-1*f2 > 12 ||
	 * onTileY-1*f1>12 || onTileX-1*f2 < 0|| onTileY-1*f1<0){ } else { pos =
	 * GOManager.combatTiles[onTileX-1*f2][onTileY-1*f1].getPosition().add(new
	 * Vector3f(0, 0, 0.01f)); } break; case 2: if(onTileX+1*f1 > 12 ||
	 * onTileY-1*f2>12 || onTileX+1*f1 < 0|| onTileY-1*f2<0){ } else { pos =
	 * GOManager.combatTiles[onTileX+1*f1][onTileY-1*f2].getPosition().add(new
	 * Vector3f(0, 0, 0.01f)); } break; case 3: if(onTileX-1*f1 > 12 ||
	 * onTileY+1*f2>12 || onTileX-1*f1 < 0|| onTileY+1*f2<0){ } else { pos =
	 * GOManager.combatTiles[onTileX-1*f1][onTileY+1*f2].getPosition().add(new
	 * Vector3f(0, 0, 0.01f)); } break; } return pos; }
	 */

	/**
	 * Creates the SpriteAnimation of a Projectile based on the direction the
	 * monster is currently facing.
	 * 
	 * @param facing
	 * @param spriteLocation
	 * @return
	 */
	private SpriteAnimation createMoveAnimation(String spriteLocation) {
		Sprite[] animationSprites = new Sprite[4];

		switch (facing) {
		case 0:
			animationSprites[0] = TextureManager.getSprite(spriteLocation, 0, 3);
			animationSprites[1] = TextureManager.getSprite(spriteLocation, 1, 3);
			animationSprites[2] = TextureManager.getSprite(spriteLocation, 2, 3);
			animationSprites[3] = TextureManager.getSprite(spriteLocation, 0, 3);
			break;
		case 1:
			animationSprites[0] = TextureManager.getSprite(spriteLocation, 0, 0);
			animationSprites[1] = TextureManager.getSprite(spriteLocation, 1, 0);
			animationSprites[2] = TextureManager.getSprite(spriteLocation, 2, 0);
			animationSprites[3] = TextureManager.getSprite(spriteLocation, 0, 0);
			break;
		case 2:
			animationSprites[0] = TextureManager.getSprite(spriteLocation, 0, 1);
			animationSprites[1] = TextureManager.getSprite(spriteLocation, 1, 1);
			animationSprites[2] = TextureManager.getSprite(spriteLocation, 2, 1);
			animationSprites[3] = TextureManager.getSprite(spriteLocation, 0, 1);
			break;
		case 3:
			animationSprites[0] = TextureManager.getSprite(spriteLocation, 0, 2);
			animationSprites[1] = TextureManager.getSprite(spriteLocation, 1, 2);
			animationSprites[2] = TextureManager.getSprite(spriteLocation, 2, 2);
			animationSprites[3] = TextureManager.getSprite(spriteLocation, 0, 2);
			break;
		}

		return new SpriteAnimation(animationSprites, 150);
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub

	}

	public int getFacing() {
		return facing;
	}

	public void setFacing(int facing) {
		this.facing = facing;
	}

	public int getOnTileX() {
		return onTileX;
	}

	public int getOnTileY() {
		return onTileY;
	}
	
	public Monster getMonster(){
		return monster;
	}

	public void setOnTileX(int onTileX) {
		this.onTileX = onTileX;
	}

	public void setOnTileY(int onTileY) {
		this.onTileY = onTileY;
	}

	public int getId() {
		return id;
	}

}
