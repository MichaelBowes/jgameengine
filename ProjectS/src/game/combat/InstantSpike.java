package game.combat;

import engine.math.Vector3f;
import engine.textures.Sprite;
import game.model.monster.Element;

/**
 * Instantly damages objects at the target location.
 *
 */
public class InstantSpike extends Projectile {

	public InstantSpike(Sprite sprite, Vector3f position, int facing, Element element,
			short atkType){
		super(sprite, position, facing, element, atkType);
		this.speed = 0;
		this.impactDamage=1;
		this.hp = 1;
		this.softHP=20;
		this.armor = 0;
	}
	
}
