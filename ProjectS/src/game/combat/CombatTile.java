package game.combat;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

public class CombatTile extends GameObject {

	private int x;
	private int y;
	/**
	 * Determines if a Monster can walk onto the tile from the given direction
	 */
	private boolean L, R, U, D;
	/**
	 * Object currently on the tile.
	 */
	private GameObject onTile;

	public CombatTile(int x, int y, Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
		this.x=x;
		this.y=y;
	}

	/**
	 * Returns an int array representing the neighbours of this tile to check if
	 * neighbours are outside the borders.<br>
	 * index 0: above<br>
	 * index 1: below<br>
	 * index 2: left<br>
	 * index 3: right<br>
	 * the array contains integer 100 if the neighbour is outside the border.
	 * @return
	 */
	public int[] getNeighbours() {
		int[] neighbours = new int[4];
		
		// above
		if (y+1 > 12) {
			neighbours[0] = 100; // indicator for border
		} else {
			neighbours[0] = y + 1;
		}
		// below
		if (y-1 < 0) {
			neighbours[1] = 100; // indicator for border
		} else {
			neighbours[1] = y - 1;
		}
		//Left
		if (x+1 > 12) {
			neighbours[2] = 100; // indicator for border
		} else {
			neighbours[2] = x + 1;
		}
		//Right
		if (x-1 < 0) {
			neighbours[3] = 100; // indicator for border
		} else {
			neighbours[3] = x - 1;
		}

		return neighbours;
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub

	}

	public boolean isL() {
		return L;
	}

	public void setL(boolean l) {
		L = l;
	}

	public boolean isR() {
		return R;
	}

	public void setR(boolean r) {
		R = r;
	}

	public boolean isU() {
		return U;
	}

	public void setU(boolean u) {
		U = u;
	}

	public boolean isD() {
		return D;
	}

	public void setD(boolean d) {
		D = d;
	}

	public GameObject getOnTile() {
		return onTile;
	}

	public void setOnTile(GameObject onTile) {
		this.onTile = onTile;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
