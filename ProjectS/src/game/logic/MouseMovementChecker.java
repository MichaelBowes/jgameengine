package game.logic;

import engine.Engine;
import engine.math.Vector2f;

/**
 * A new MouseMovementChecker is created on every LMB click. It moves camera &
 * player to the position of the mouse.
 *
 */
public class MouseMovementChecker {

	private boolean movePlayer = true;
	private boolean moveL = true;
	private boolean moveR = true;
	private boolean moveU = true;
	private boolean moveD = true;
	private Vector2f destinationCoords;
	private float xDiff, yDiff;

	public MouseMovementChecker(Vector2f mouseWorldCoord) {
		this.destinationCoords = mouseWorldCoord;
	}

	private void moveLeft() {
		GOManager.playerAnimation.play();
		if (GOManager.playerAnimation.getStartIndex() != 4 && (xDiff > yDiff) && (-xDiff < yDiff)) {
			GOManager.playerAnimation.setIndizes(4, 7);
		}
			GOManager.player.changePosition(-0.045f, 0f);
			Engine.getCamera().changePosition(-0.045f, 0f, 0f);
	}

	private void moveRight() {
		GOManager.playerAnimation.play();
		if (GOManager.playerAnimation.getStartIndex() != 8 && (xDiff < yDiff) && (-xDiff > yDiff)) {
			GOManager.playerAnimation.setIndizes(8, 11);
		}
			GOManager.player.changePosition(0.045f, 0f);
			Engine.getCamera().changePosition(0.045f, 0f, 0f);
	}

	private void moveUp() {
		GOManager.playerAnimation.play();
		if (GOManager.playerAnimation.getStartIndex() != 12 && (xDiff < -yDiff) && (-xDiff > yDiff)
				&& (-xDiff < -yDiff)) {
			GOManager.playerAnimation.setIndizes(12, 15);
		}
			GOManager.player.changePosition(0f, 0.045f);
			Engine.getCamera().changePosition(0f, 0.045f, 0f);
	}

	private void moveDown() {
		GOManager.playerAnimation.play();
		if (GOManager.playerAnimation.getStartIndex() != 0 && (xDiff < yDiff) && (-xDiff < yDiff)) {
			GOManager.playerAnimation.setIndizes(0, 3);
		}
			GOManager.player.changePosition(0f, -0.045f);
			Engine.getCamera().changePosition(0f, -0.045f, 0f);
	}
	
	/**
	 * Listens for movement input if game scene is focused.
	 */
	public void movePlayer() {
		if (UIManager.gameSceneFocused) {
			if (movePlayer == true) {
				xDiff = GOManager.player.getPosition().x - destinationCoords.x;
				yDiff = GOManager.player.getPosition().y - destinationCoords.y;

				boolean moveLeft = GOManager.player.getPosition().x >= destinationCoords.x;
				boolean moveRight = GOManager.player.getPosition().x < destinationCoords.x;
				boolean moveDown = GOManager.player.getPosition().y >= destinationCoords.y;
				boolean moveUp = GOManager.player.getPosition().y < destinationCoords.y;
				
				if (moveLeft && moveL) {
					GOManager.player.setxFacing(true);
					moveL = true;
					moveLeft();
					moveR = false;
				}
				if (moveRight && moveR) {
					GOManager.player.setxFacing(false);
					moveR = true;
					moveRight();
					moveL = false;
				}
				if (moveUp && moveU) {
					GOManager.player.setyFacing(false);
					moveU = true;
					moveUp();
					moveD = false;
				}
				if (moveDown && moveD) {
					GOManager.player.setyFacing(true);
					moveD = true;
					moveDown();
					moveU = false;
				}

				if (((moveL && !moveLeft) || (moveR && !moveRight)) && ((moveU && !moveUp) || (moveD && !moveDown))) {
					if (GOManager.playerAnimation.getCurrentIndex() < 4) {
						GOManager.playerAnimation.setIndizes(0, 3);
					} else if (GOManager.playerAnimation.getCurrentIndex() < 8) {
						GOManager.playerAnimation.setIndizes(4, 7);
					} else if (GOManager.playerAnimation.getCurrentIndex() < 12) {
						GOManager.playerAnimation.setIndizes(8, 11);
					} else if (GOManager.playerAnimation.getCurrentIndex() < 16) {
						GOManager.playerAnimation.setIndizes(12, 15);
					}
					GOManager.playerAnimation.stop();
					movePlayer = false;
					moveL = true;
					moveR = true;
					moveD = true;
					moveU = true;
					UIManager.destroyMouseChecker();
				}
			}
		}
	}
}
