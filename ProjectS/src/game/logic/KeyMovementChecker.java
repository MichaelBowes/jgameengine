package game.logic;

import engine.Engine;
import engine.input.Input;

public class KeyMovementChecker {

	/**
	 * Direction currently moving the Monster to.
	 */
	private int currentDirection;
	/**
	 * X and Y coordinates of the position from where the movement started.
	 */
	private float startX;
	private float startY;
	/**
	 * Direction to go to if another step was queued. Default value of 4, which
	 * means no direction was set.
	 */
	private int nextDirection = 4;

	public KeyMovementChecker(int Direction) {
		this.currentDirection = Direction;
		startX = GOManager.currentMonster.getPosition().x;
		startY = GOManager.currentMonster.getPosition().y;
	}

	/**
	 * Listens for movement keys if game scene is focused.
	 */
	public void moveMonster() {
		if (UIManager.combatFocused) {
			// left
			if (currentDirection == 2) {
				// move till destination is reached.
				if (startX - 32 == GOManager.currentMonster.getPosition().x) {
					// button was held down at the time of destination reached.
					if (Input.keys[87] == true) { // W
						GOManager.currentMonster.setFacing(0);
						GOManager.monsterAnimation.setIndizes(0, 3);
					} else if (Input.keys[83] == true) { // S
						GOManager.currentMonster.setFacing(1);
						GOManager.monsterAnimation.setIndizes(12, 15);
					} else if (Input.keys[65] == true) { // A
						GOManager.currentMonster.setFacing(2);
						GOManager.monsterAnimation.setIndizes(4, 7);
					} else if (Input.keys[68] == true) { // D
						GOManager.currentMonster.setFacing(3);
						GOManager.monsterAnimation.setIndizes(8, 11);
					}

					if (nextDirection != 4) {
						int onX = GOManager.currentMonster.getOnTileX();
						int onY = GOManager.currentMonster.getOnTileY();
						if (GOManager.combatTiles[onX][onY].getNeighbours()[nextDirection] == 100) {
							UIManager.keyChecker = null;
							System.out.println("not moving");
						} else {
							switch (nextDirection) {
							case 0:
								GOManager.currentMonster.setOnTileY(onY + 1);
								break;
							case 1:
								GOManager.currentMonster.setOnTileY(onY - 1);
								break;
							case 2:
								GOManager.currentMonster.setOnTileX(onX + 1);
								break;
							case 3:
								GOManager.currentMonster.setOnTileX(onX - 1);
								break;
							}
							UIManager.keyChecker = new KeyMovementChecker(nextDirection);
						}
					} else {
						UIManager.keyChecker = null;
					}
				} else {
					GOManager.monsterAnimation.play();
					if (GOManager.monsterAnimation.getStartIndex() != 4 && !Input.keys[Input.KEY_UP]
							&& !Input.keys[Input.KEY_DOWN]) {
						GOManager.monsterAnimation.setIndizes(4, 7);
					}
					GOManager.currentMonster.changePositionIgnoreDelta(-2f, 0f);
					Engine.getCamera().changePositionIgnoreDelta(-2f, 0f);
				}
			}
			// right
			if (currentDirection == 3) {
				if (startX + 32 == GOManager.currentMonster.getPosition().x) {
					if (Input.keys[87] == true) { // W
						GOManager.currentMonster.setFacing(0);
						GOManager.monsterAnimation.setIndizes(0, 3);
					} else if (Input.keys[83] == true) { // S
						GOManager.currentMonster.setFacing(1);
						GOManager.monsterAnimation.setIndizes(12, 15);
					} else if (Input.keys[65] == true) { // A
						GOManager.currentMonster.setFacing(2);
						GOManager.monsterAnimation.setIndizes(4, 7);
					} else if (Input.keys[68] == true) { // D
						GOManager.currentMonster.setFacing(3);
						GOManager.monsterAnimation.setIndizes(8, 11);
					}
					if (nextDirection != 4) {
						int onX = GOManager.currentMonster.getOnTileX();
						int onY = GOManager.currentMonster.getOnTileY();
						if (GOManager.combatTiles[onX][onY].getNeighbours()[nextDirection] == 100) {
							UIManager.keyChecker = null;
							System.out.println("not moving");
						} else {
							switch (nextDirection) {
							case 0:
								GOManager.currentMonster.setOnTileY(onY + 1);
								break;
							case 1:
								GOManager.currentMonster.setOnTileY(onY - 1);
								break;
							case 2:
								GOManager.currentMonster.setOnTileX(onX + 1);
								break;
							case 3:
								GOManager.currentMonster.setOnTileX(onX - 1);
								break;
							}
							UIManager.keyChecker = new KeyMovementChecker(nextDirection);
						}
					} else {
						UIManager.keyChecker = null;
					}
				} else {
					GOManager.monsterAnimation.play();
					if (GOManager.monsterAnimation.getStartIndex() != 8 && !Input.keys[Input.KEY_UP]
							&& !Input.keys[Input.KEY_DOWN]) {
						GOManager.monsterAnimation.setIndizes(8, 11);
					}
					GOManager.currentMonster.changePositionIgnoreDelta(2f, 0f);
					Engine.getCamera().changePositionIgnoreDelta(2f, 0f);
				}
			}
			// up
			if (currentDirection == 0) {
				if (startY + 24 == GOManager.currentMonster.getPosition().y) {
					if (Input.keys[87] == true) { // W
						GOManager.currentMonster.setFacing(0);
						GOManager.monsterAnimation.setIndizes(0, 3);
					} else if (Input.keys[83] == true) { // S
						GOManager.currentMonster.setFacing(1);
						GOManager.monsterAnimation.setIndizes(12, 15);
					} else if (Input.keys[65] == true) { // A
						GOManager.currentMonster.setFacing(2);
						GOManager.monsterAnimation.setIndizes(4, 7);
					} else if (Input.keys[68] == true) { // D
						GOManager.currentMonster.setFacing(3);
						GOManager.monsterAnimation.setIndizes(8, 11);
					}
					if (nextDirection != 4) {
						int onX = GOManager.currentMonster.getOnTileX();
						int onY = GOManager.currentMonster.getOnTileY();
						if (GOManager.combatTiles[onX][onY].getNeighbours()[nextDirection] == 100) {
							UIManager.keyChecker = null;
							System.out.println("not moving");
						} else {
							switch (nextDirection) {
							case 0:
								GOManager.currentMonster.setOnTileY(onY + 1);
								break;
							case 1:
								GOManager.currentMonster.setOnTileY(onY - 1);
								break;
							case 2:
								GOManager.currentMonster.setOnTileX(onX + 1);
								break;
							case 3:
								GOManager.currentMonster.setOnTileX(onX - 1);
								break;
							}
							UIManager.keyChecker = new KeyMovementChecker(nextDirection);
						}
					} else {
						UIManager.keyChecker = null;
					}
				} else {
					GOManager.monsterAnimation.play();
					if (GOManager.monsterAnimation.getStartIndex() != 12) {
						GOManager.monsterAnimation.setIndizes(12, 15);
					}
					GOManager.currentMonster.changePositionIgnoreDelta(0f, 2f);
					Engine.getCamera().changePositionIgnoreDelta(0f, 2f);
				}
			}
			// down
			if (currentDirection == 1) {
				if (startY - 24 == GOManager.currentMonster.getPosition().y) {
					if (Input.keys[87] == true) { // W
						GOManager.currentMonster.setFacing(0);
						GOManager.monsterAnimation.setIndizes(0, 3);
					} else if (Input.keys[83] == true) { // S
						GOManager.currentMonster.setFacing(1);
						GOManager.monsterAnimation.setIndizes(12, 15);
					} else if (Input.keys[65] == true) { // A
						GOManager.currentMonster.setFacing(2);
						GOManager.monsterAnimation.setIndizes(4, 7);
					} else if (Input.keys[68] == true) { // D
						GOManager.currentMonster.setFacing(3);
						GOManager.monsterAnimation.setIndizes(8, 11);
					}
					if (nextDirection != 4) {
						int onX = GOManager.currentMonster.getOnTileX();
						int onY = GOManager.currentMonster.getOnTileY();
						if (GOManager.combatTiles[onX][onY].getNeighbours()[nextDirection] == 100) {
							UIManager.keyChecker = null;
							System.out.println("not moving");
						} else {
							switch (nextDirection) {
							case 0:
								GOManager.currentMonster.setOnTileY(onY + 1);
								break;
							case 1:
								GOManager.currentMonster.setOnTileY(onY - 1);
								break;
							case 2:
								GOManager.currentMonster.setOnTileX(onX + 1);
								break;
							case 3:
								GOManager.currentMonster.setOnTileX(onX - 1);
								break;
							}
							UIManager.keyChecker = new KeyMovementChecker(nextDirection);
						}
					} else {
						UIManager.keyChecker = null;
					}
				} else {
					GOManager.monsterAnimation.play();
					if (GOManager.monsterAnimation.getStartIndex() != 0) {
						GOManager.monsterAnimation.setIndizes(0, 3);
					}
					GOManager.currentMonster.changePositionIgnoreDelta(0f, -2f);
					Engine.getCamera().changePositionIgnoreDelta(0f, -2f);
				}
			}
		}
	}

	public int getNextDirection() {
		return nextDirection;
	}

	public void setNextDirection(int nextDirection) {
		this.nextDirection = nextDirection;
	}
}
