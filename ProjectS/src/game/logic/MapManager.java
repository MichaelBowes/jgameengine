package game.logic;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import engine.Engine;
import engine.math.Vector3f;
import engine.textures.Sprite;
import engine.textures.manager.TextureManager;
import game.graphics.ui.ObjectPlacer;
import game.map.Map;
import game.map.MonsterContainer;
import game.map.Tile;
import game.map.WorldObject;
import game.map.floatToPath;
import game.map.spawnMode;
import javolution.util.FastTable;

public class MapManager {

	/**
	 * Map the player is currently on.
	 */
	private static Map currentMap;
	/**
	 * Representation of all tiles.
	 */
	public static Tile[][] groundRepresentation = new Tile[50][50];
	/**
	 * All WorldObjects with functionality on the current map.
	 */
	public static List<WorldObject> mapObjects = new FastTable<WorldObject>();
	public static int[][] layer1 = new int[50][50];
	public static int[][] layer2 = new int[50][50];
	public static int[][] layer3 = new int[50][50];
	public static int[][] layer4 = new int[50][50];
	public static int[][] layer5 = new int[50][50];
	/**
	 * ID of the WorldObject to spawn, gained from the objectPlacer.
	 */
	public static int placingTileID;
	/**
	 * True if objectPlacer menu is Opened inside the game.
	 */
	public static boolean objectPlacerOn;
	/**
	 * ObjectPlacers that serve as visual representation of the WorldObjects to
	 * choose from to spawn on the map.
	 */
	public static ObjectPlacer[][] objectPlacer = new ObjectPlacer[10][5];
	/**
	 * Maps each WorldObject to a float value for byte-conversion.
	 */
	private static floatToPath imgPaths = new floatToPath();

	/**
	 * Delay time between which the Engine spawns a monster.
	 */
	public static final short delay = 1000;
	/**
	 * lastTime is incremented during movement. 
	 * If lastTime reaches the value of delay, a monster spawns.
	 */
	public static short lastTime = 0;
	
	/**
	 * Spawns monsters with their respective probability, taken from the current
	 * Map's spawnTable.
	 */
	public static MonsterContainer spawnMonsters() {

		double random = Math.random();
		float cumulativeProbability = 0f;
		MonsterContainer spawnedMonster = null;

		for (int i = 0; i < currentMap.getSpawnTable().length; i++) {
			
			cumulativeProbability += (currentMap.getSpawnTable()[i].getSpawnChance()/100f);
			
			if (random <= cumulativeProbability) {
				// return the MonsterContainer
				return currentMap.getSpawnTable()[i];
			}
		}
		return spawnedMonster;
	}

	/**
	 * Loads the BG image and adds the GameObjects that are higher than 1 tile
	 * and/or have functionality.
	 * 
	 * @param name
	 * @param map
	 */
	public static void drawMap(String name, int[][] map) {
	}

	/**
	 * loads the map for the player's scene.
	 * 
	 * @param mapNr
	 */
	public static void loadMap(int mapNr) {
		switch (mapNr) {
		case 0:
			Sprite s0 = TextureManager.getSprite("res/Maps/map1.png", 0, 0);
			MonsterContainer[] table = new MonsterContainer[5];
			table[0] = new MonsterContainer(0, 5, 25);
			table[1] = new MonsterContainer(0, 3, 50);
			table[2] = new MonsterContainer(0, 4, 10);
			table[3] = new MonsterContainer(0, 2, 10);
			table[4] = new MonsterContainer(0, 4, 2);
			currentMap = new Map(s0, new Vector3f(0, 0, -20f), table, spawnMode.NORMAL);
			Engine.getSceneManager().addGameObject(currentMap);
			break;
		case 1:
			break;
		}
		createBoundaries();
		loadMapObjects("res/MapObjects/map");
	}

	/**
	 * Spawns ObjectPlacer fields for current Map.
	 */
	public static void activateObjectPlacer() {
		Engine.getCamera().setPosition(Engine.getCamera().getPosition().x, Engine.getCamera().getPosition().y, 1f);
		objectPlacerOn = true;
		for (int i = 0; i < 50; i++) {
			for (int j = 0; j < 50; j++) {
				groundRepresentation[i][j] = new Tile(i, j);
			}
		}

		Sprite s0 = TextureManager.getSprite("res/pictures/deepWater.jpg", 0, 0);
		Vector3f mapPos = new Vector3f(Engine.getWindow().getResolution().width() / 20,
				Engine.getWindow().getResolution().height() - 16f, 0);
		int count = 0;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 5; j++) {
				ObjectPlacer placer = new ObjectPlacer("", s0, mapPos.add(new Vector3f(0 + 33f * i, 0 - 33f * j, 0)),
						count);
				placer.addHitbox();
				objectPlacer[i][j] = placer;
				Engine.getSceneManager().addUIObject(placer);
				count++;
			}

		}
	}

	/**
	 * Removes ObjectPlacer from screen.
	 */
	public static void deactivateObjectPlacer() {
		Engine.getCamera().setPosition(Engine.getCamera().getPosition().x, Engine.getCamera().getPosition().y, 0.6f);
		objectPlacerOn = false;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 5; j++) {
				Engine.getSceneManager().removeUIObject(objectPlacer[i][j]);
			}
		}
		saveMapObjects("res/MapObjects/map");
	}

	/**
	 * 
	 * Saves the MapObjects in a file as bytes.
	 */
	private static void saveMapObjects(String mapPath) {
		File file = new File(mapPath);
		try (FileOutputStream fop = new FileOutputStream(file)) {

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			for (WorldObject wo : mapObjects) {
				byte[] o = float2ByteArray(wo.getGroundOffset());
				byte[] x = float2ByteArray(wo.getPosition().x);
				byte[] y = float2ByteArray(wo.getPosition().y);

				// Convert String to the mapped float value in FloatToPath.
				String s = wo.getTexture().getPath().substring(13);
				float f = imgPaths.getFloat(s);
				System.out.println("f: " + f);
				byte[] b = float2ByteArray(f);

				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				outputStream.write(o);
				outputStream.write(x);
				outputStream.write(y);
				outputStream.write(b);

				byte allBytes[] = outputStream.toByteArray();

				System.out.println("allBytes: " + allBytes);
				fop.write(allBytes);
				fop.write(System.getProperty("line.separator").getBytes());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Utility function. Converts a float into a byte array.
	 * 
	 * @param value
	 * @return
	 */
	public static byte[] float2ByteArray(float value) {
		return ByteBuffer.allocate(4).putFloat(value).array();
	}

	/**
	 * Loads MapObjects from a file into the current mapObject List.
	 */
	public static void loadMapObjects(String loadPath) {
		try (RandomAccessFile f = new RandomAccessFile(loadPath, "r")) {
			// objects line by line
			int lineCount = countLines(loadPath);
			System.out.println("lines: " + lineCount);
			for (int i = 0; i < lineCount * 16; i += 18) {
				float o = f.readFloat();
				f.seek(4 + i);
				float x = f.readFloat();
				f.seek(8 + i);
				float y = f.readFloat();
				f.seek(12 + i);
				float sFloat = f.readFloat();
				String string = imgPaths.getString(sFloat);
				System.out.println(o + "," + x + "," + y + "," + string);
				WorldObject wo = null;
				Sprite sprite = TextureManager.getSprite("res/pictures/" + string, 0, 0);
				wo = new WorldObject(sprite, new Vector3f(x, y, 0));
				wo.addHitbox();
				wo.setYScaling(true);
				wo.changeGroundOffset(o);
				mapObjects.add(wo);
				Engine.getSceneManager().addGameObject(wo);
			}

		} catch (IOException e) {

		}

	}

	/**
	 * Utility function that counts the lines of a file.
	 */
	public static int countLines(String filename) throws IOException {
		try (InputStream is = new BufferedInputStream(new FileInputStream(filename))) {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		}
	}

	/**
	 * Sets blocks at the borders of the map.
	 */
	private static void createBoundaries() {
		Sprite s0 = TextureManager.getSprite("res/pictures/block.png", 0, 0);
		for (int i = 0; i < 27; i++) {
			Vector3f mapPos = currentMap.getPosition().add(new Vector3f(-currentMap.getTexture().getWidth() / 2 - 16,
					currentMap.getTexture().getHeight() / 2 + 16, 0));
			WorldObject block = new WorldObject(s0, mapPos.add(new Vector3f(0 + 32f * i, 0f, 0)));
			block.addHitbox();
			Engine.getSceneManager().addGameObject(block);
			WorldObject block2 = new WorldObject(s0, mapPos.add(new Vector3f(0, 0 - 32 * i, 0)));
			block2.addHitbox();
			Engine.getSceneManager().addGameObject(block2);
			WorldObject block3 = new WorldObject(s0, mapPos.add(new Vector3f(0 + 32f * i, -832f, 0)));
			block3.addHitbox();
			Engine.getSceneManager().addGameObject(block3);
			WorldObject block4 = new WorldObject(s0, mapPos.add(new Vector3f(832f, 0 - 32 * i, 0)));
			block4.addHitbox();
			Engine.getSceneManager().addGameObject(block4);
		}
	}

	/**
	 * Saves a map with 5 layers. Each layer is saved as a square block,
	 * separated by semicolon. Used in {@link MapEditor}
	 * 
	 * @param name
	 *            Name to give to the file to save
	 * @map Map to save
	 */
	public static void saveGroundMap(String name, Tile[][] map) {
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("res/Maps/" + name + ".csv"),
				Charset.forName("UTF-8"))) {
			// read 5 layers of a map file, each layer separated by a semicolon.
			// Change 5 to higher number for more layers.
			for (int k = 0; k < 5; k++) {
				for (int i = 0; i < map.length; i++) {
					for (int j = 0; j < map.length; j++) {
						if (j == map.length - 1 && i == map.length - 1) {
							if (map[i][j] != null && (map[i][j].getTileID(k) != 0)) {
								writer.write(map[i][j].getTileID(k) + ";" + System.getProperty("line.separator"));
							} else {
								writer.write(0 + ";" + System.getProperty("line.separator"));
							}
						} else if (j == map.length - 1) {
							if (map[i][j] != null && (map[i][j].getTileID(k) != 0)) {
								writer.write(map[i][j].getTileID(k) + "," + System.getProperty("line.separator"));
							} else {
								writer.write(0 + "," + System.getProperty("line.separator"));
							}
						} else {
							if (map[i][j] != null && (map[i][j].getTileID(k) != 0)) {
								System.out.println((map[i][j].getTileID(k)) + " mep");
								writer.write(map[i][j].getTileID(k) + ",");
							} else {
								writer.write(0 + ",");
							}

						}
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Could not save map file");
		}
	}

	/**
	 * Converts a map file into separate int arrays, one for each layer of the
	 * map.
	 * 
	 * @param loadPath
	 *            the path to the file, excluding filename
	 * @param filename
	 *            the name with datatype ending.
	 * @param mapSize
	 *            the length & width of the square array.
	 */
	public static void loadMapForPrint(String loadPath, String filename, int mapSize) throws IOException {
		Path folder = Paths.get(loadPath);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {
			for (Path path : stream) {
				if (path.getFileName().toString().endsWith(filename)) {
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

					List<String> layerblock1 = new FastTable<String>();
					List<String> layerblock2 = new FastTable<String>();
					List<String> layerblock3 = new FastTable<String>();
					List<String> layerblock4 = new FastTable<String>();
					List<String> layerblock5 = new FastTable<String>();

					for (int a = 0; a < 50; a++) {
						layerblock1.add(lines.get(a));
					}
					for (int b = 50; b < 100; b++) {
						layerblock2.add(lines.get(b));
					}
					for (int c = 100; c < 150; c++) {
						layerblock3.add(lines.get(c));
					}
					for (int d = 150; d < 200; d++) {
						layerblock4.add(lines.get(d));
					}
					for (int e = 200; e < 250; e++) {
						layerblock5.add(lines.get(e));
					}

					int i = 0;
					for (String l : layerblock1) {
						String[] splitLine = l.split("[,|;]");
						int j = 0;
						for (String letter : splitLine) {
							int id = Integer.parseInt(letter);
							layer1[i][j] = id;
							j++;
						}
						i++;
					}
					int k = 0;
					for (String l : layerblock2) {
						if (k < 50) {
							String[] splitLine = l.split("[,|;]");
							int j = 0;
							for (String letter : splitLine) {
								int id = Integer.parseInt(letter);
								layer2[k][j] = id;
								j++;
							}
							k++;
						}
					}
					int x = 0;
					for (String l : layerblock3) {
						if (x < 50) {
							String[] splitLine = l.split("[,|;]");
							int j = 0;
							for (String letter : splitLine) {
								int id = Integer.parseInt(letter);
								layer3[x][j] = id;
								j++;
							}
							x++;
						}
					}
					int y = 0;
					for (String l : layerblock4) {
						if (y < 50) {
							String[] splitLine = l.split("[,|;]");
							int j = 0;
							for (String letter : splitLine) {
								int id = Integer.parseInt(letter);
								layer4[y][j] = id;
								j++;
							}
							y++;
						}
					}
					int o = 0;
					for (String l : layerblock5) {
						if (o < 50) {
							String[] splitLine = l.split("[,|;]");
							int j = 0;
							for (String letter : splitLine) {
								int id = Integer.parseInt(letter);
								layer5[o][j] = id;
								j++;
							}
							o++;
						}
					}
				}
			}
		} catch (

		IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * loads a map with 3 layers from a map file in the project into a 2D Tile
	 * array.<br>
	 * Does not draw the map.
	 * 
	 * @param loadPath
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static Tile[][] loadMap(String loadPath, String filename) throws IOException {
		Tile[][] map = new Tile[50][50];
		Path folder = Paths.get(loadPath);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {
			for (Path path : stream) {
				if (path.getFileName().toString().endsWith(filename)) {
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

					int x = 0;
					int count = 0;
					// loads 3 layers from a file. Increase 4 to higher number
					// for more layers.
					for (int n = 1; n < 4; n++) {
						List<String> layer1 = new FastTable<String>();

						for (int m = 0 * x; m < 50 * n; m++) {
							layer1.add(lines.get(m));
						}

						int i = 0;
						for (String l : layer1) {
							if (i < 50) {
								String[] splitLine = l.split("[,|;]");
								int j = 0;
								for (String letter : splitLine) {
									if (map[i][j] == null) {
										map[i][j] = new Tile();
									}
									int id = Integer.parseInt(letter);
									map[i][j].setTileID(id, n - 1);
									j++;
								}
								i++;
							}
						}
						x += 50;
					}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return map;
	}

	public static void printMap(int[][] mapArray, int arraySize) {
		for (int i = 0; i < arraySize; i++) {
			for (int j = 0; j < arraySize; j++) {
				System.out.print(mapArray[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	public static Map getCurrentMap() {
		return currentMap;
	}
}
