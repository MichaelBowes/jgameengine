package game.logic;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import engine.Engine;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import engine.textures.SpriteAnimation;
import engine.textures.manager.TextureManager;
import game.combat.CombatMonster;
import game.combat.CombatTile;
import game.combat.Projectile;
import game.map.MonsterContainer;
import game.map.Tile;
import game.model.character.PlayerCharacter;
import game.model.items.Item;
import game.model.items.ItemFactory;
import game.model.monster.Monster;
import game.model.monster.MonsterFactory;
import javolution.util.FastTable;

/**
 * Manages all GameObjects belonging to the Scene's GameScene, including Tiles,
 * Creatures, Structures, droppedItems, the player (and foragables?).
 *
 */
public class GOManager {

	/**
	 * The non-UI elements to render. Index 0 is always the player.
	 */
	protected static Map<String, Class<?>> itemTable = new HashMap<String, Class<?>>();
	protected static List<Tile> tiles = new FastTable<Tile>();
	public static List<Item> droppedItems = new FastTable<Item>();
	public static CombatTile[][] combatTiles = new CombatTile[13][13];
	public static PlayerCharacter player;
	/**
	 * Player-controlled Monster
	 */
	public static CombatMonster currentMonster;
	public static CombatMonster enemyMonster;
	public static List<CombatMonster> enemyMonsters;
	public static SpriteAnimation monsterAnimation;
	public static SpriteAnimation playerAnimation;
	public static List<Projectile> projectiles = new FastTable<Projectile>();

	public GOManager() {
	}

	/**
	 * Fills the item table with all items of the game and their associated id.
	 */
	public static void fillItemTable() {
		//itemTable.put("0", BlueBerry.class);
	}

	public static Class<?> getItemPerId(String id) {
		return itemTable.get(id);
	}

	public static void loadPlayer() {
		Sprite[] animationSprites = new Sprite[16];
		animationSprites[0] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		animationSprites[1] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 0);
		animationSprites[2] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 0);
		animationSprites[3] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 0);
		animationSprites[4] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 1);
		animationSprites[5] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 1);
		animationSprites[6] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 1);
		animationSprites[7] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 1);
		animationSprites[8] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 2);
		animationSprites[9] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 2);
		animationSprites[10] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 2);
		animationSprites[11] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 2);
		animationSprites[12] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 3);
		animationSprites[13] = TextureManager.getSprite("res/pictures/rabbit.png", 1, 3);
		animationSprites[14] = TextureManager.getSprite("res/pictures/rabbit.png", 2, 3);
		animationSprites[15] = TextureManager.getSprite("res/pictures/rabbit.png", 0, 3);
		playerAnimation = new SpriteAnimation(animationSprites, 300);
		List<Monster> monsters = new LinkedList<Monster>();
		monsters.add(new MonsterFactory().createMonster(0, 5));
		player = new PlayerCharacter(
				"name", 
				playerAnimation,
				new Vector3f(0f, 0f, 0f),
				monsters);
		player.addHitbox();
		player.setYScaling(true);
	}

	public static void loadGameScene() {

		
	}

	public static void renderGameElements() {
		if (player != null) {
			Engine.getSceneManager().addGameObject(player);
		}
		for (GameObject obj : tiles) {
			Engine.getSceneManager().addGameObject(obj);
		}
		for (GameObject obj : droppedItems) {
			Engine.getSceneManager().addGameObject(obj);
		}
	}

	/**
	 * Spawns a monster to fight, with the given data from the MonsterContainer.
	 */
	public static void startPvECombat(MonsterContainer monster) {
		UIManager.mouseChecker=null;
		UIManager.combatFocused=true;
		
		//create field
		Sprite tile = TextureManager.getSprite("res/pictures/tile1.png", 0, 0);
		float relativePosX = player.getPosition().x-Engine.getWindow().getResolution().width()/8f;
		float relativePosY = player.getPosition().y-Engine.getWindow().getResolution().height()/8f;
		for (int i = 0; i < combatTiles.length; i++) {
			for (int j = 0; j < combatTiles.length; j++) {
				CombatTile ct = new CombatTile(i, j, tile, 
						new Vector3f(relativePosX + 32 * -i, 
								relativePosY + 24 * j, 0));
				combatTiles[i][j] = ct;
				Engine.getSceneManager().addGameObject(ct);
			}
		}
		
		//create player's monster
		//GOManager.player.getParty.get(0);
		monsterAnimation = createMonsterAnimation("res/pictures/monDummy.png");
		//Sprite debugSprite = TextureManager.getSprite("res/pictures/t.png", 0, 0);
		currentMonster = new CombatMonster(0, monsterAnimation, 
				new Vector3f(
						combatTiles[6][0].getPosition().x,
						combatTiles[6][0].getPosition().y,
				0.001f), 
				GOManager.player.getParty().get(0));
		combatTiles[6][0].setOnTile(currentMonster);
		currentMonster.setOnTileX(6);
		currentMonster.setOnTileY(0);
		currentMonster.addHitbox();
		Engine.getSceneManager().addGameObject(currentMonster);
		Engine.getCamera().setPosition(combatTiles[6][5].getPosition().x, 
				combatTiles[6][5].getPosition().y, Engine.getCamera().getPosition().z);
		
		//Create the enemy monster
		SpriteAnimation monSprite = createMonsterAnimation("res/pictures/monDummy.png");
		enemyMonster = new CombatMonster(1, monSprite, new Vector3f(
				combatTiles[6][6].getPosition().x, 
				combatTiles[6][6].getPosition().y,
				0.001f),
				new MonsterFactory().createMonster(monster.getMonsterId(), monster.getLevel()));
		combatTiles[6][6].setOnTile(enemyMonster);
		enemyMonster.addHitbox();
		Engine.getSceneManager().addGameObject(enemyMonster);
	}
	
	/**
	 * 
	 */
	public static void endPvECombat(){
		// Remove field
		for(int i=0;i<combatTiles.length;i++){
			for(int j=0;j<combatTiles.length;j++){
				Engine.getSceneManager().removeGameObject(combatTiles[i][j]);
				combatTiles[i][j] = null;
			}
		}
		
		Engine.getSceneManager().removeGameObject(currentMonster);
		Engine.getSceneManager().removeGameObject(enemyMonster);
		for(Projectile p : projectiles){
			Engine.getSceneManager().removeGameObject(p);
			projectiles.remove(p);
		}
		currentMonster = null;
		enemyMonster = null;
		
		UIManager.combatFocused=false;
		Engine.getCamera().setPosition(player.getPosition().x, player.getPosition().y, 0.6f);
	}

	/**
	 * Takes the relative path to the sprite picture and returns the spriteAnimation that
	 * can be added to a GameObject like a Sprite.
	 * @param spriteLocation
	 * @return
	 */
	private static SpriteAnimation createMonsterAnimation(String spriteLocation) {
		Sprite[] animationSprites = new Sprite[16];
		int count=0;
		for (int j = 0; j < 4; j++) {
			for (int i = 0; i < 4; i++) {
				if(i==3){
				animationSprites[count] = TextureManager.getSprite(spriteLocation, 0, j);
				} else {
				animationSprites[count] = TextureManager.getSprite(spriteLocation, i, j);
				}
				count++;
			}
		}
		
		return new SpriteAnimation(animationSprites, 300);
	}

	/**
	 * Drops the dragged item at the current player position. Is called when the
	 * item was not dragged onto a DropField.
	 */
	public static void dropDraggedItem() {
		if (UIManager.draggedItem != null) {
			String id = UIManager.draggedItem.getId();
			//player.getInventory().get(Integer.parseInt(id));
			Item drop = new ItemFactory().createItem("0", player.getPosition().subtract(new Vector3f(0f, 0f, 0.01f)));
			drop.setScale(0.75f, 0.75f);
			drop.addHitbox();
			droppedItems.add((Item) drop);
			Engine.getSceneManager().addGameObject(drop);
			Engine.getSceneManager().removeUIObject(UIManager.draggedItem);
			UIManager.draggedItem = null;
		}
	}

	public static void removeDroppedItem(GameObject dragObject) {
		Engine.getSceneManager().removeGameObject(dragObject);
		droppedItems.remove(0);
	}

	/**
	 * Prints the GameObject's position in 2D world coordinates.
	 * 
	 * @param obj
	 */
	private void printPosition(GameObject obj) {
		System.out.println(obj.getPosition().x + " : " + obj.getPosition().y);
	}

	/**
	 * Debugging method, prints the map to console.
	 */
	public static void printMap(Tile[][] map) {
		for (int k = 0; k < 9; k++) {
			for (int i = 0; i < map.length - 1; i++) {
				for (int j = 0; j < map.length - 1; j++) {
					if (k == 0) {
						System.out.print(map[i][j].getLayer(k).getID() + ",");
					}
				}
				System.out.println("");
			}
		}
	}
	
	/**
	 * Destroys the projectile with the given id.
	 * @param id
	 */
	public static void destroyProjectile(double id){
		ListIterator<Projectile> iter = projectiles.listIterator();
		while(iter.hasNext()){
			 Projectile element = iter.next();
			    if(element.getId()==id){
			        iter.remove();
			        Engine.getSceneManager().removeGameObject(element);
			    }
		}
	}

	public List<Tile> getTiles() {
		return tiles;
	}

	public void setTiles(List<Tile> tiles) {
		this.tiles = tiles;
	}
}
