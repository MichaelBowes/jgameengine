package game.logic;

import java.io.FileNotFoundException;
import java.util.List;

import Exceptions.InvalidFileException;
import engine.Engine;
import engine.display.Window;
import engine.math.Vector2f;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.objects.fonts.Font;
import engine.objects.fonts.FontManager;
import engine.objects.fonts.TextObject;
import engine.objects.scene.Scene;
import engine.textures.Sprite;
import engine.textures.manager.TextureManager;
import game.graphics.ui.Button;
import game.graphics.ui.CompendiumButton;
import game.graphics.ui.EndCombatButton;
import game.graphics.ui.InputField;
import game.graphics.ui.Inventory;
import game.graphics.ui.InventoryButton;
import game.graphics.ui.LoginButton;
import game.graphics.ui.MapEditorButton;
import game.graphics.ui.QuestButton;
import game.graphics.ui.Slider;
import game.graphics.ui.StandardButton;
import game.graphics.ui.WorldMapButton;
import game.model.items.Item;
import game.model.items.ItemFactory;
import javolution.util.FastTable;

/**
 * Manages all GameObjects belonging to the Scene's UIScene, including Buttons,
 * TextOBjects, Sliders, InputFields and Inventories.
 *
 */
public class UIManager {

	/**
	 * The UI elements to render.
	 */
	protected static List<TextObject> Texts = new FastTable<TextObject>();
	protected static List<Button> Buttons = new FastTable<Button>();
	protected static List<Slider> Sliders = new FastTable<Slider>();
	protected static List<InputField> InputFields = new FastTable<InputField>();
	public static Inventory playerInventory;
	protected static List<Button> HUDElements = new FastTable<Button>();
	protected static Button resultBG;
	public static boolean UIvisible = true;
	public static MouseMovementChecker mouseChecker;
	public static KeyMovementChecker keyChecker;
	public static boolean invOpen, wmOpen, qstOpen, compOpen;
	/**
	 * ItemFactory to add items to e.g. the Inventory.
	 */
	private static ItemFactory factory = new ItemFactory();
	public static Item draggedItem;
	/**
	 * Tells the {@link Game} that at least 1 Slider is being dragged.
	 */
	public static boolean dragSliders = false;
	/**
	 * Tells the {@link Game} that at least 1 Item is being dragged.
	 */
	public static boolean dragItem = false;
	/**
	 * Tells the {@link Game} that at least 1 InputField is listening.
	 */
	public static boolean listenForInput = false;
	/**
	 * Tells the {@link Game} if the GameScene is currently in focus. False if
	 * open UI objects are currently in focus instead or if a different Scene is
	 * currently loaded. True only if the GameScene is loaded and no UI element
	 * is in focus.
	 */
	public static boolean gameSceneFocused = false;
	public static boolean combatFocused = false;
	public static boolean resultFocused = false;

	public UIManager() {
		try {
			TextureManager.readResourceFile("res/resources");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (InvalidFileException e) {
			e.printStackTrace();
		}
	}

	public static void loadMapEditor(String tilesetName) {
		loadTileset(tilesetName);
		renderUIElements();
	}

	public static void loadTileset(String name) {
		int h = 704 / 16;
		int w = 176 / 16;
		int count = 0;
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				MapEditorButton mapBtn = new MapEditorButton(count, "mapBtn",
						TextureManager.getSprite("res/pictures/" + name, i, j),
						new Vector3f(16 + 16 * i, 16 + 16 * j, 0));
				mapBtn.addHitbox();
				Buttons.add(mapBtn);
				count += 1;
			}
		}
	}

	/**
	 * Opens a window with infos on all resources obtained, including gold, exp
	 * etc.
	 */
	public static void loadCombatResults() {
		combatFocused = false;
		resultFocused = true;
		resultBG = addStandardButton("resultBG", TextureManager.getSprite("res/pictures/BG.png", 0, 0),
				Engine.getWindow().getResolution().width() * 0.5f, Engine.getWindow().getResolution().height() * 0.5f,
				0);
		resultBG.addHitbox();

		EndCombatButton btn = new EndCombatButton("resultButton",
				TextureManager.getSprite("res/pictures/green.jpg", 0, 0), new Vector3f(0, 0, 0.001f));
		btn.addHitbox();
		resultBG.addChild(btn);

		Button bt = addStandardButton("resultTitle", TextureManager.getSprite("res/pictures/results75.png", 0, 0), 0,
				180, 0);
		resultBG.addChild(bt);
		Engine.getSceneManager().addUIObject(resultBG);
	}

	/**
	 * Closes the combat window and ends the combat.
	 */
	public static void closeCombatResults() {
		Engine.getSceneManager().removeUIObject(resultBG);
		resultBG = null;
		GOManager.endPvECombat();
		resultFocused = false;
	}

	/**
	 * Initializes the Launcher and its elements.
	 */
	public static void loadLauncher() {
	}

	public static void hideUI() {
		for (Button b : Buttons) {
			b.hide();
		}
		UIvisible = false;
	}

	public static void showUI() {
		for (Button b : Buttons) {
			b.show();
		}
		UIvisible = true;
	}

	/**
	 * Initializes Login Screen and its elements.
	 */
	public static void loadLogin() {
		clearScene();
		clearUIManager();

		// Add InputFields
		addInputField("userField", TextureManager.getSprite("res/pictures/TxtField1.png", 0, 0),
				Window.getResolution().width() * 0.5f, Window.getResolution().height() * 0.555f, 0,
				FontManager.getFont("GothicFont"));
		addInputField("pwField", TextureManager.getSprite("res/pictures/TxtField1.png", 0, 0),
				Window.getResolution().width() * 0.5f, Window.getResolution().height() * 0.45f, 0,
				FontManager.getFont("GothicFont"));

		// Add Buttons
		addLoginButton("loginField", TextureManager.getSprite("res/pictures/lgnBtn.png", 0, 0),
				Window.getResolution().width() * 0.56f, Window.getResolution().height() * 0.338f, 0);
		addLoginButton("checkBox", TextureManager.getSprite("res/pictures/checkBox.png", 0, 0), 540, 240, 0);

		// Add Texts
		addTextObject("Title", FontManager.getFont("GothicFont"), Window.getResolution().width() / 2,
				Window.getResolution().height() * 0.95f, 1f, 1f);
		addTextObject("Username", FontManager.getFont("GothicFont"), Window.getResolution().width() * 0.5f,
				Window.getResolution().height() * 0.61f, 1f, 1f);
		addTextObject("Password", FontManager.getFont("GothicFont"), 640, 360, 1f, 1f);
		addTextObject("Remember ID", FontManager.getFont("GothicFont"), 620, 240, 0.6f, 0.6f);

		// Add Sliders
		addSlider("testSlider", TextureManager.getSprite("res/pictures/checkBox.png", 0, 0),
				TextureManager.getSprite("res/pictures/TxtField1.png", 0, 0), 200, 240, 0);

		renderUIElements();
	}

	/**
	 * Loads the Game and its elements.
	 */
	public static void loadGameScene() {
		gameSceneFocused = true;
	}

	public static void renderUIElements() {
		for (GameObject obj : Texts) {
			Engine.getSceneManager().addUIObject(obj);
		}
		for (Button obj : Buttons) {
			Engine.getSceneManager().addUIObject(obj);
		}
		for (Slider obj : Sliders) {
			Engine.getSceneManager().addUIObject(obj);
		}
		for (InputField obj : InputFields) {
			Engine.getSceneManager().addUIObject(obj);
		}
	}

	private static void clearUIManager() {
		Texts.clear();
		Buttons.clear();
		Sliders.clear();
		InputFields.clear();
	}

	private static void clearScene() {
		Engine.getSceneManager().setUIScene(new Scene());
	}

	public static void destroyMouseChecker() {
		mouseChecker = null;
	}

	/**
	 * Creates a TextObject with the given parameters.
	 */
	public static void addTextObject(String id, Font font, float posX, float posY, float scaleX, float scaleY) {
		TextObject txt = new TextObject(id, font);
		txt.setPosition(posX, posY);
		txt.setScale(scaleX, scaleY);
		txt.setAddedColor(-0.2f, -0.2f, -0.1f, 0);
		Texts.add(txt);
	}

	public static void addInputField(String id, Sprite sprite, float posX, float posY, float posZ, Font font) {
		InputField userInput = new InputField(id, sprite, new Vector3f(posX, posY, posZ), font);
		userInput.addHitbox();
		InputFields.add(userInput);
	}

	public static void addSlider(String id, Sprite sliderObj, Sprite sliderBar, float posX, float posY, float posZ) {
		Slider slider = new Slider(id, sliderObj, sliderBar, new Vector3f(posX, posY, posZ));
		Sliders.add(slider);
		slider.addHitbox();
	}

	public static Button addStandardButton(String id, Sprite sprite, float posX, float posY, float posZ) {
		Button btn = new StandardButton(id, sprite, new Vector3f(posX, posY, posZ));
		Buttons.add(btn);
		btn.addHitbox();
		return btn;
	}

	public static void addLoginButton(String id, Sprite sprite, float posX, float posY, float posZ) {
		Button loginBtn = new LoginButton(id, sprite, new Vector3f(posX, posY, posZ));
		loginBtn.addHitbox();
		Buttons.add(loginBtn);
	}

	public static void addInventoryItem() {
		Item mo = factory.createItem("0", new Vector3f(0f, 0f, 0.02f));
		mo.addHitbox();
		// playerInventory.getFields().get(0).addItem(mo);
	}

	public static void loadHUD() {
		Sprite s0 = TextureManager.getSprite("res/pictures/cmd.png", 0, 0);
		Sprite s1 = TextureManager.getSprite("res/pictures/compBtn.png", 0, 0);
		Sprite s2 = TextureManager.getSprite("res/pictures/glBtn.png", 0, 0);
		Sprite s3 = TextureManager.getSprite("res/pictures/invBtn.png", 0, 0);
		Sprite s4 = TextureManager.getSprite("res/pictures/qstBtn.png", 0, 0);
		CompendiumButton cBtn = new CompendiumButton("", s1, new Vector3f(75, 100, 0.001f));
		InventoryButton iBtn = new InventoryButton("", s3, new Vector3f(75, 300, 0.001f));
		WorldMapButton wmBtn = new WorldMapButton("", s2, new Vector3f(75, 200, 0.001f));
		QuestButton qstBtn = new QuestButton("", s4, new Vector3f(75, 400, 0.001f));
		StandardButton blHud = new StandardButton("", s0, new Vector3f(75, 200, 0f));
		qstBtn.addHitbox();
		cBtn.addHitbox();
		iBtn.addHitbox();
		wmBtn.addHitbox();
		blHud.addHitbox();
		Engine.getSceneManager().addUIObject(blHud);
		Engine.getSceneManager().addUIObject(cBtn);
		Engine.getSceneManager().addUIObject(iBtn);
		Engine.getSceneManager().addUIObject(wmBtn);
		Engine.getSceneManager().addUIObject(qstBtn);
	}

	public static void closeHUD() {

	}

	public void removeUIObject() {
	}

	public static boolean getListenForInput() {
		return listenForInput;
	}

	public static boolean getDragging() {
		return dragSliders;
	}

	public static void setListenForInput(boolean b) {
		listenForInput = b;
	}

	public static void setDragging(boolean b) {
		dragSliders = b;
	}

	public static void openInventory() {
		playerInventory = new Inventory("inv", 4, 4,
				new Vector3f(Window.getResolution().width() * 0.3f, Window.getResolution().height() * 0.4f, 0));

		Engine.getSceneManager().addUIObject(playerInventory);

		/*
		 * Item item = new
		 * BlueBerry(TextureManager.getSprite("res/pictures/BlueBerries.png", 0,
		 * 0), new Vector3f(0, 0, 0.02f)); item.addHitbox();
		 * playerInventory.getFields().get(0).addItem(item);
		 */
		invOpen = true;
	}

	public static void closeInventory() {
		Engine.getSceneManager().removeUIObject(playerInventory);
		invOpen = false;
	}

	public List<Slider> getSliders() {
		return Sliders;
	}

	public List<InputField> getInputFields() {
		return InputFields;
	}

	public static List<Button> getButtons() {
		return Buttons;
	}

	public static Button getResultBG() {
		return resultBG;
	}

}
