package game.launcher;

/**
 * Contains the saved informations and settings 
 * of the game that need to be saved.
 */
public class LaunchConfig {
	private PlayerData playerData;
	private Settings settings;
	
	/**
	 * Reads the config and player data files and returns the {@link LaunchConfig} 
	 * containing their data.
	 * @return
	 */
	public static LaunchConfig readConfig() {
		PlayerData playerData = PlayerData.readPlayerData();
		if(playerData == null) {
			playerData = new PlayerData();
		}
		
		Settings settings = Settings.readSettings();
		
		if(settings == null) {
			return new LaunchConfig(Settings.getDefaultSettings(), playerData);
		}else {
			return new LaunchConfig(settings, playerData);
		}

	}
	
	public void saveSettings() {
		settings.writeSettings();
	}
	
	public void savePlayerData() {
		playerData.writePlayerData();
	}
	
	private LaunchConfig(Settings settings, PlayerData playerData) {
		this.settings = settings;
		this.playerData = playerData;
	}

	public PlayerData getPlayerData() {
		return playerData;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}
}
