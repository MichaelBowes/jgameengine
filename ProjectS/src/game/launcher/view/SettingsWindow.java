package game.launcher.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXTextField;

import engine.display.Resolution;
import engine.display.ResolutionConverter;
import game.launcher.Settings;
import game.launcher.controller.LauncherMainController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class SettingsWindow extends Stage {

	private LauncherMainController controller;
	private JFXDecorator decorator;
	private SimpleObjectProperty<Resolution> selectedResolution = new SimpleObjectProperty<Resolution>();
	private StringProperty frameRate = new SimpleStringProperty();
	private Settings settings;
	private Pane rootPane;
	
	public SettingsWindow(LauncherMainController controller, Settings settings) {
		this.controller = controller;
		this.settings = settings;
		selectedResolution.setValue(settings.getResolution());
		this.frameRate.setValue(String.valueOf(settings.getFrameRate()));
		this.setTitle("Settings");
		rootPane = createRootPane();
		rootPane.setId("settings-root");
		decorator = new JFXDecorator(this, rootPane);
		decorator.setCustomMaximize(true);
		Scene scene = new Scene(decorator, 300, 400);
		scene.getStylesheets().add(SettingsWindow.class.getResource("MainWindow.css").toExternalForm());		
		this.setScene(scene);
		this.setMinWidth(300);
		this.setMinHeight(400);
		this.setResizable(false);
	}
	
	private Pane createRootPane() {
		StackPane root = new StackPane();
		BorderPane pane = new BorderPane();
		VBox controlBox = new VBox();
		controlBox.getChildren().add(createSoundSettings());
		controlBox.getChildren().add(createOptionSelectors());
		pane.setTop(controlBox);
		pane.setBottom(createButtonPanel());
		root.getChildren().add(pane);
		return root;
	}
	
	private VBox createSoundSettings() {
		return new VBox();
	}
	
	private JFXTextField getTextField(String text) {
		JFXTextField field = new JFXTextField(text);
		field.setEditable(false);
		field.setId("static-text");
		return field;
	}
	
	/**
	 * Creates the settings selectors with their description.
	 * @return
	 */
	private HBox createOptionSelectors() {
		HBox options = new HBox();
		VBox textBox = new VBox();
		textBox.getChildren().add(getTextField("Resolution:"));
		textBox.getChildren().add(getTextField("FPS:"));
		options.getChildren().add(textBox);
		VBox comboBoxes = createComboBoxes();
		comboBoxes.prefWidthProperty().bind(options.widthProperty());
		options.getChildren().add(comboBoxes);
		return options;
	}
	
	private VBox createComboBoxes() {
		VBox box = new VBox();
		JFXComboBox<Resolution> resolutions = new JFXComboBox<Resolution>();		
		resolutions.setId("combo-box");
		resolutions.setItems(FXCollections.observableArrayList(Resolution.values()));
		resolutions.setConverter(new ResolutionConverter());
		resolutions.maxWidth(Double.MAX_VALUE);
		resolutions.prefWidthProperty().bind(box.widthProperty());
		resolutions.valueProperty().bindBidirectional(selectedResolution);
		
		JFXComboBox<String> frameRates = new JFXComboBox<String>();
		frameRates.setId("combo-box");
		String selectedValue = null;
		if(settings.getFrameRate() == 0) {
			selectedValue = "Unlimited";
		}else {
			selectedValue = String.valueOf(settings.getFrameRate());
		}
		frameRates.valueProperty().bindBidirectional(frameRate);
		frameRates.getSelectionModel().select(selectedValue); //Set selected value
		frameRates.getItems().add(String.valueOf(30));
		frameRates.getItems().add(String.valueOf(60));
		frameRates.getItems().add("Unlimited");
		frameRates.prefWidthProperty().bind(box.widthProperty());	
		
		box.setFillWidth(true);
		box.getChildren().add(resolutions);
		box.getChildren().add(frameRates);
		return box;
	}
	
	private Pane createButtonPanel() {
		
		HBox buttons = new HBox();
		buttons.setSpacing(6);
		buttons.setPadding(new Insets(2));
		JFXButton applyButton = new JFXButton("Apply");
		applyButton.setId("button");
		applyButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				int actualFrameRate = 0;
				if(!frameRate.getValue().equals("Unlimited")) {
					actualFrameRate = Integer.parseInt(frameRate.getValue());
				}
				controller.applySettings(selectedResolution.getValue(), actualFrameRate);			
			}			
		});
		
		JFXButton cancelButton = new JFXButton("Cancel");
		cancelButton.setId("button");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
					close();			
			}			
		});
		
		buttons.setAlignment(Pos.CENTER);
		buttons.getChildren().add(applyButton);
		buttons.getChildren().add(cancelButton);
		
		BorderPane pane = new BorderPane();
		pane.setRight(buttons);
		return pane;
	}
}
