package game.launcher.view;

import java.util.Observable;
import java.util.Observer;

import com.jfoenix.animation.alert.JFXAlertAnimation;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;

import engine.utils.Tuple;
import game.client.GameClient;
import game.launcher.controller.LauncherMainController;
import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import server.transferdata.SystemMessage;

/**
 * @author BpZ
 * 
 *         Main window of the launcher.
 *
 */
public class LauncherWindow extends Stage implements Observer {

	protected LauncherMainController controller;
	protected JFXDecorator decorator;
	protected StringProperty loginButtonText = new SimpleStringProperty("Login");
	protected StringProperty username = new SimpleStringProperty();
	protected StringProperty password = new SimpleStringProperty();
	// Progress bar properties
	protected DoubleProperty progressBarProgress = new SimpleDoubleProperty();
	protected StringProperty progressText = new SimpleStringProperty();
	protected BooleanProperty progressBarVisible = new SimpleBooleanProperty(false);
	// Login field properties
	protected JFXTextField usernameField;
	protected JFXPasswordField passwordField;
	protected JFXButton logoutButton;
	protected Pane rootPane;
	/**
	 * True if a connection to the game server is established.
	 */
	protected BooleanProperty loggedIn = new SimpleBooleanProperty(false);
	/**
	 * If activated, user data is saved locally.
	 */
	private BooleanProperty saveLoginData = new SimpleBooleanProperty();

	public LauncherWindow(LauncherMainController controller, int width, int height, String title) {
		this.controller = controller;
		this.setTitle(title);
		rootPane = createRootPane();
		decorator = new JFXDecorator(this, rootPane);
		decorator.setCustomMaximize(true);
		Scene scene = new Scene(decorator, width, height);
		scene.getStylesheets().add(LauncherWindow.class.getResource("MainWindow.css").toExternalForm());
		createEvents();
		this.setScene(scene);
		this.setMinHeight(height);
		this.setMinWidth(width);
	}

	private void createEvents() {
		this.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				controller.disconnect();
			}
		});
		;
	}

	public void setLoginDataSaved(boolean state) {
		this.saveLoginData.setValue(state);
	}

	/**
	 * Returns the value of the checkbox for saving user data.
	 * 
	 * @return
	 */
	public boolean getLoginDataSaved() {
		return saveLoginData.getValue();
	}

	public void showProgressBar(String text) {
		if (text == null) {
			progressText.setValue("");
		} else {
			progressText.setValue(text);
		}
		progressBarVisible.setValue(true);
	}

	public void hideProgressBar() {
		progressBarVisible.setValue(false);
	}

	/**
	 * Sets the user data in the input fields.
	 * 
	 * @param username
	 * @param password
	 */
	public void setUserData(String username, String password) {
		this.username.setValue(username);
		this.password.setValue(password);
	}

	private VBox createProgessBar() {
		VBox box = new VBox();
		box.visibleProperty().bind(progressBarVisible);
		box.setAlignment(Pos.CENTER);

		JFXTextField progressTextField = new JFXTextField();
		progressTextField.textProperty().bind(new StringBinding() {
			{
				bind(progressText, progressBarProgress);
			}

			@Override
			protected String computeValue() {
				int progress = (int) (progressBarProgress.getValue() * 100);
				String text = progressText.getValue();
				String result = null;
				if (text == null) {
					text = "";
				}
				if (text.equals("")) {
					result = progress + "%";
				} else {
					result = progressText.getValue() + " - " + progress + "%";
				}
				return result;
			}
		});
		progressTextField.setAlignment(Pos.CENTER);
		progressTextField.setEditable(false);
		progressTextField.setId("loading-text");

		JFXProgressBar progressBar = new JFXProgressBar();
		progressBar.progressProperty().bind(progressBarProgress);

		box.getChildren().add(progressTextField);
		box.getChildren().add(progressBar);

		return box;
	}

	public void setProgressBarVisible(boolean value) {
		this.progressBarVisible.setValue(value);
	}

	/**
	 * Creates the root pane for the window.
	 * 
	 * @return
	 */
	private Pane createRootPane() {
		StackPane pane = new StackPane();

		BorderPane box = new BorderPane();
		box.setTop(createTopMenu());
		box.setCenter(createCenterPane());
		box.setBottom(createBottomPane());

		pane.getChildren().add(box);
		return pane;
	}

	protected Pane createCenterPane() {
		return new Pane();
	}

	private BorderPane createTopMenu() {
		BorderPane pane = new BorderPane();
		pane.setId("borderpane");
		pane.setPadding(new Insets(4, 4, 4, 4));
		JFXButton settingsButton = new JFXButton();
		settingsButton.setText("Settings");
		settingsButton.setId("button");
		settingsButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				controller.openSettingsWindow();
			}
		});
		settingsButton.setPadding(new Insets(2, 2, 2, 2));
		pane.setRight(settingsButton);
		return pane;
	}

	/**
	 * Creates the login bar with login and password field, as well as a loading
	 * bar.
	 * 
	 * @return
	 */
	private BorderPane createBottomPane() {
		BorderPane pane = new BorderPane();
		pane.setId("borderpane");
		pane.setPadding(new Insets(4, 4, 4, 4));
		HBox field = new HBox();
		field.setAlignment(Pos.CENTER);
		field.setSpacing(4);
		pane.setLeft(field);
		// Username field
		usernameField = createInputField("Username");
		usernameField.textProperty().bindBidirectional(username);
		usernameField.setId("input-field");

		// Password field
		passwordField = createPasswordField("Password");
		passwordField.textProperty().bindBidirectional(password);
		passwordField.setId("input-field");
		// Login checkbox
		JFXCheckBox checkBox = new JFXCheckBox("Save");
		checkBox.selectedProperty().bindBidirectional(saveLoginData);
		checkBox.setId("checkbox");

		HBox buttons = new HBox();
		buttons.setAlignment(Pos.CENTER);
		buttons.setSpacing(5);
		buttons.setPadding(new Insets(4, 4, 0, 0));

		// Login button
		JFXButton loginButton = new JFXButton();
		loginButton.setId("button");
		loginButton.setMinHeight(30);
		loginButton.setMinWidth(50);
		loginButton.textProperty().bind(loginButtonText);
		loginButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (!loggedIn.getValue()) {
					controller.login(usernameField.getText(), passwordField.getText(), saveLoginData.getValue());
				} else {
					controller.startGame();
				}
			}
		});

		logoutButton = new JFXButton();
		logoutButton.setId("button");
		logoutButton.setText("Logout");
		logoutButton.setMinHeight(30);
		logoutButton.setVisible(false);
		logoutButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				controller.disconnect();
				setLoggedIn(false);
			}
		});

		field.setPadding(new Insets(6));

		buttons.getChildren().add(logoutButton);
		buttons.getChildren().add(loginButton);
		pane.setRight(buttons);
		field.getChildren().add(usernameField);
		field.getChildren().add(passwordField);
		field.getChildren().add(checkBox);
		field.getChildren().add(createProgessBar());
		return pane;
	}

	/**
	 * Creates the login field.
	 * 
	 * @param prompt
	 *            shown in or above the field.
	 * @return
	 */
	private JFXTextField createInputField(String prompt) {
		JFXTextField field = new JFXTextField();
		field.setLabelFloat(true);
		field.setPromptText(prompt);
		return field;
	}

	/**
	 * Creates the password field.
	 * 
	 * @param prompt
	 *            shown in or above the field.
	 * @return
	 */
	private JFXPasswordField createPasswordField(String prompt) {
		JFXPasswordField field = new JFXPasswordField();
		field.setLabelFloat(true);
		field.setPromptText(prompt);
		return field;
	}

	/**
	 * Displays an alert with the given message.
	 * 
	 * @param message
	 */
	public void showAlert(String message) {
		JFXDialogLayout layout = new JFXDialogLayout();
		layout.setBody(new Label(message));
		JFXAlert<Void> alert = new JFXAlert<>(this);
		alert.setOverlayClose(true);
		alert.setContent(layout);
		alert.setAnimation(JFXAlertAnimation.CENTER_ANIMATION);
		alert.initModality(Modality.NONE);
		alert.showAndWait();
	}

	public void showPopup() {
		JFXDialog dialog = new JFXDialog();
		dialog.setContent(new Label("Content"));
		dialog.show((StackPane) rootPane);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 != null) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					// Print out system messages
					if (arg1 instanceof SystemMessage) {
						SystemMessage message = (SystemMessage) arg1;
						String content = null;
						//Failed to connect
						if (message.getType() == SystemMessage.FAIL) {
							progressBarVisible.setValue(false);
							content = message.getMessage();
						}else if(message.getType() == SystemMessage.KICK) {//Kicked 				
							setLoggedIn(false);
							content = "You have been kicked for the following reason: " 
							+ message.getMessage();
						}else if(message.getType() == SystemMessage.DISCONNECT) {
							setLoggedIn(false);
							content = message.getMessage();
						}else if(message.getType() == SystemMessage.BAN) {
							content = message.getMessage();
						}						
						showAlert(content);
					} else if (arg1 instanceof String) {
						String message = (String) arg1;
						showAlert(message);
						// Update loading bar.
					} else if (arg1 instanceof Tuple<?, ?>) {
						Tuple<?, ?> tuple = (Tuple<?, ?>) arg1;
						if (tuple.x instanceof String) {
							progressBarProgress.setValue((double) tuple.y);
							progressText.setValue((String) tuple.x);
							progressBarVisible.setValue(true);
						}
					}
				};
			});
		} else {
			// On login change login button
			GameClient client = (GameClient) arg0;
			if (client.isLoggedIn()) {
				loggedIn.set(true);
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						setLoggedIn(true);
					}
				});
			}
		}
	}

	public boolean isLoggedIn() {
		return loggedIn.getValue();
	}

	public void setLoggedIn(boolean value) {
		this.loggedIn.setValue(value);
		if (loggedIn.getValue()) {
			progressBarVisible.setValue(false);
			loginButtonText.set("Start");
			passwordField.setDisable(true);
			usernameField.setDisable(true);
			logoutButton.setVisible(true);
		} else {
			loginButtonText.set("Login");
			logoutButton.setVisible(false);
			passwordField.setDisable(false);
			usernameField.setDisable(false);
		}
	}
}
