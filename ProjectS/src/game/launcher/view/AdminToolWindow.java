package game.launcher.view;

import java.util.List;
import java.util.Observable;
import java.util.TimeZone;

import org.joda.time.DateTimeZone;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXDialog.DialogTransition;

import game.launcher.controller.AdminToolController;
import game.launcher.controller.LauncherMainController;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import server.model.Player;
import server.transferdata.ChatMessage;
import server.transferdata.PlayerConnection;
import server.transferdata.PlayerRestriction;

public class AdminToolWindow extends LauncherWindow {

	public AdminToolWindow(LauncherMainController controller) {
		super(controller, 800, 500, "Admin Launcher");
	}

	private TableView<Player> playerTable;
	private ListView<ChatMessage> chat;
	private ListView<String> commandWindow;
	private Player selectedPlayer;

	public Player getSelectedPlayer() {
		return selectedPlayer;
	}

	public void addPlayerToTable(Player player) {
		playerTable.getItems().add(player);
	}

	public void removePlayerFromTable(Player player) {
		playerTable.getItems().remove(player);
	}

	public void addChatMessage(ChatMessage message) {
		chat.getItems().add(message);
	}

	public void addCommandOutput(String message) {
		commandWindow.getItems().add(message);
	}

	@Override
	protected Pane createCenterPane() {
		StackPane pane = new StackPane();
		// Creating the tab pane
		JFXTabPane tabPane = new JFXTabPane();
		tabPane.visibleProperty().bind(loggedIn);
		tabPane.setId("tab-Pane");
		// Player tab
		Tab playerTab = new Tab("Player list");
		playerTab.setContent(createPlayerList());
		tabPane.getTabs().add(playerTab);
		// Command tab
		Tab commandTab = new Tab("Command");
		commandTab.setContent(createCommandView());
		tabPane.getTabs().add(commandTab);
		// Chat tab
		Tab chatTab = new Tab("Chat");
		chatTab.setContent(createChatView());
		tabPane.getTabs().add(chatTab);

		pane.getChildren().add(tabPane);
		return pane;
	}

	private Node createPlayerList() {		
		playerTable = new TableView<Player>();
		playerTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		// Change selected player on selection change.
		playerTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				selectedPlayer = newSelection;
			}
		});
		playerTable.setEditable(false);
		// Username column
		TableColumn<Player, String> usernameColumn = new TableColumn<>("Username");
		usernameColumn.setPrefWidth(150);
		usernameColumn.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> arg0) {
						ObservableValue<String> value = new ReadOnlyObjectWrapper<String>(
								arg0.getValue().getUsername());
						return value;
					}
				});
		// Current character column
		TableColumn<Player, String> characterColumn = new TableColumn<>("Character");
		characterColumn.setPrefWidth(150);
		characterColumn.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> arg0) {
						String characterName = null;
						if (arg0.getValue().getCharacter() == null) {
							characterName = "-";
						} else {
							characterName = arg0.getValue().getCharacter().getName();
						}
						ObservableValue<String> value = new ReadOnlyObjectWrapper<String>(characterName);
						return value;
					}
				});
		playerTable.setContextMenu(createContextMenu());
		playerTable.getColumns().add(usernameColumn);
		playerTable.getColumns().add(characterColumn);
		return playerTable;
	}

	private Node createCommandView() {
		SplitPane pane = new SplitPane();
		pane.setOrientation(Orientation.VERTICAL);
		commandWindow = new ListView<String>();
		JFXTextField inputField = new JFXTextField();
		inputField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent arg0) {
				if (arg0.getCode() == KeyCode.ENTER) {
					((AdminToolController) controller).sendCommand(inputField.getText());
					inputField.setText("");
				}
			}
		});
		pane.getItems().add(commandWindow);
		pane.getItems().add(inputField);
		return pane;
	}

	/**
	 * Creates the chat window and its input controls.
	 * 
	 * @return
	 */
	private Node createChatView() {
		VBox pane = new VBox();
		pane.maxHeight(Double.MAX_VALUE);
		chat = new ListView<ChatMessage>();
		chat.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		// Change selected player on selection change.
		chat.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				selectedPlayer = newSelection.getSender();
			}
		});
		chat.setCellFactory(new Callback<ListView<ChatMessage>, ListCell<ChatMessage>>() {
			@Override
			public ListCell<ChatMessage> call(ListView<ChatMessage> param) {
				return new ListCell<ChatMessage>() {
					@Override
					protected void updateItem(ChatMessage item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null && !empty) {
							//Set time zone
							String name = "(" +item.getDate().withZone(
									DateTimeZone.forTimeZone(TimeZone.getDefault())).toString("HH:mm") + ") ";
							
							if (item.getSender().getCharacter() == null) {
								name += item.getSender().getUsername();
							} else {
								name +=  item.getSender().getCharacter().getName() + "(" + item.getSender().getUsername()
										+ ")" + ": " + item.getContent();
							}
							String content = null;
							if (item.getContent() == null) {
								content = "No message";
								setStyle("-fx-text-fill: rgb(255,0,0);");
							} else {
								content = item.getContent();
							}
							setText(name + ": " + content);
						} else {
							setText("");
						}
					}
				};
			}
		});
		chat.setContextMenu(createContextMenu());
		VBox.setVgrow(chat, Priority.ALWAYS);
		chat.setMinHeight(20);
		chat.setMaxHeight(Double.MAX_VALUE);
		BorderPane inputField = new BorderPane();
		inputField.prefHeight(70);
		JFXTextArea inputText = new JFXTextArea();
		inputText.setId("chat-input");
		inputText.setPrefHeight(60);

		JFXButton inputButton = new JFXButton("Send");
		HBox.setHgrow(inputButton, Priority.ALWAYS);
		inputButton.setMaxWidth(Double.MAX_VALUE);
		inputButton.setId("button");
		inputButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				((AdminToolController) controller).sendChatMessage(inputText.getText());
				inputText.setText("");
			}
		});

		JFXButton clearButton = new JFXButton("Clear");
		HBox.setHgrow(clearButton, Priority.ALWAYS);
		clearButton.setMaxWidth(Double.MAX_VALUE);
		clearButton.setId("button");
		clearButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				inputText.setText("");
			}
		});

		HBox buttons = new HBox();
		inputField.setPadding(new Insets(2, 2, 2, 2));
		buttons.getChildren().add(inputButton);
		buttons.getChildren().add(clearButton);

		buttons.setSpacing(6);
		inputField.setTop(inputText);
		inputField.setBottom(buttons);

		pane.getChildren().add(chat);
		pane.getChildren().add(inputField);
		return pane;
	}

	/**
	 * Creates the context menu.
	 * 
	 * @return
	 */
	private ContextMenu createContextMenu() {
		ContextMenu contextMenu = new ContextMenu();
		MenuItem sendMessage = new MenuItem("Send Message");
		MenuItem kick = new MenuItem("Kick");
		MenuItem ban = new MenuItem("Ban");

		sendMessage.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (selectedPlayer != null) {
					((AdminToolController) controller).messagePlayer(selectedPlayer);
				}
			}
		});

		kick.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (selectedPlayer != null) {
					showKickDialog(selectedPlayer);
				}
			}
		});

		ban.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (selectedPlayer != null) {
					showBanDialog(selectedPlayer);
				}
			}
		});

		contextMenu.getItems().add(sendMessage);
		contextMenu.getItems().add(kick);
		contextMenu.getItems().add(ban);
		return contextMenu;
	}

	/**
	 * Opens a dialog that asks for additional information.
	 * 
	 * @param player
	 */
	public void showBanDialog(Player player) {
		JFXDialogLayout layout = new JFXDialogLayout();
		JFXDialog dialog = new JFXDialog((StackPane) rootPane, layout, DialogTransition.CENTER, false);

		HBox content = new HBox();
		content.setSpacing(4);
		VBox reasonField = new VBox();
		JFXTextField reason = new JFXTextField();
		reasonField.getChildren().add(new Text("Reason:"));
		reasonField.getChildren().add(reason);
		HBox.setHgrow(reasonField, Priority.ALWAYS);

		VBox durationField = new VBox();
		JFXTextField duration = new JFXTextField();
		durationField.getChildren().add(new Text("Duration:"));
		durationField.getChildren().add(duration);
		HBox.setHgrow(durationField, Priority.ALWAYS);

		content.getChildren().add(reasonField);
		content.getChildren().add(durationField);

		JFXButton confirmButton = new JFXButton("Confirm");
		confirmButton.setId("button");
		confirmButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				PlayerRestriction ban = new PlayerRestriction(player.getUsername(), reason.getText(), PlayerRestriction.BAN);
				int banDuration = 0;
				try {
					banDuration = Integer.parseInt(duration.getText());
				} catch (NumberFormatException e) {
					showAlert("Invalid duration");
					return;
				}
				if (banDuration == 0) {
					banDuration = 1000000;
				}
				ban.setDuration(banDuration);
				((AdminToolController) controller).banPlayer(ban);
				dialog.close();
			}
		});
		JFXButton cancelButton = new JFXButton("Cancel");
		cancelButton.setId("button");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				dialog.close();
			}
		});

		layout.setActions(confirmButton, cancelButton);
		layout.setHeading(new Text("Ban player " + player.getUsername()));
		layout.setBody(content);

		dialog.show();
	}

	/**
	 * Opens a dialog that asks for additional information.
	 * 
	 * @param player
	 */
	public void showKickDialog(Player player) {
		JFXDialogLayout layout = new JFXDialogLayout();
		JFXDialog dialog = new JFXDialog((StackPane) rootPane, layout, DialogTransition.CENTER, false);

		VBox content = new VBox();
		content.setSpacing(4);
		JFXTextField reason = new JFXTextField();
		content.getChildren().add(new Text("Reason:"));
		content.getChildren().add(reason);

		JFXButton confirmButton = new JFXButton("Confirm");
		confirmButton.setId("button");
		confirmButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				PlayerRestriction kick = new PlayerRestriction(player.getUsername(), reason.getText(), PlayerRestriction.KICK);
				((AdminToolController) controller).kickPlayer(kick);
				dialog.close();
			}
		});
		JFXButton cancelButton = new JFXButton("Cancel");
		cancelButton.setId("button");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				dialog.close();
			}
		});

		layout.setActions(confirmButton, cancelButton);
		layout.setHeading(new Text("Kick player " + player.getUsername()));
		layout.setBody(content);

		dialog.show();
	}

	@Override
	public void update(Observable sender, Object object) {
		super.update(sender, object);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (object instanceof PlayerConnection) {
					PlayerConnection connection = (PlayerConnection) object;
					if(connection.getAction() == PlayerConnection.LOGIN) {
						playerTable.getItems().add(connection.getPlayer());
					}else {
						playerTable.getItems().remove(connection.getPlayer());
					}
				}else if(object instanceof ChatMessage) {	
					ChatMessage message = (ChatMessage) object;
					chat.getItems().add(message);
				}else if (object instanceof List<?>) {
					@SuppressWarnings("unchecked")
					List<Player> players = (List<Player>) object;
					playerTable.getItems().clear();
					playerTable.getItems().addAll(players);
				}
			}
		});
	}
}
