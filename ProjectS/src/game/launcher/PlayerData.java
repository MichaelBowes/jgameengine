package game.launcher;

import javax.crypto.SecretKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import game.util.Encrypt;

public class PlayerData {

	private static final String PATH = "res/keystore/";
	private String username;
	private String password;

	public PlayerData() {};
	
	private PlayerData(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Reads the encrypted user data files and if they exist,
	 * returns the {@link PlayerData} containing the username and password.
	 * @return {@link PlayerData} containing username and password, or of the files
	 * don't exist, returns null.
	 */
	public static PlayerData readPlayerData() {
		File file1 = new File(PATH+ "dp1");
		File file2 = new File(PATH+ "dp2");
		PlayerData data = null;
		if(file1.exists() && file2.exists()) {
			try {
				byte[] usernameData = Encrypt.decryptRead(null, file1);
				byte[] passwordData = Encrypt.decryptRead(null, file2);
				if(usernameData != null && passwordData != null) {
					data = new PlayerData(
							new String(usernameData,"UTF-8"),
							new String(passwordData,"UTF-8"));				
				}
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		return data;
	}

	public boolean writePlayerData() {
		File file1 = new File(PATH+ "dp1");
		byte[] data1 = username.getBytes();
		try {
			Encrypt.encryptWrite(null, data1, file1);
		} catch (InvalidKeyException | IOException e) {
			e.printStackTrace();
			return false;
		}
		File file2 = new File(PATH+ "dp2");
		byte[] data2 = password.getBytes();
		try {
			Encrypt.encryptWrite(null, data2, file2);
		} catch (InvalidKeyException | IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static SecretKey getKey() {
		byte[] encoded = new byte[] { 
				12, 22, -2, 24, 41, -42, -2 
				, -1, 10, -2, 91, -63 
				, 82, -36, -12, 3};
		SecretKey secretKey = new SecretKeySpec(encoded, 0, encoded.length, "AES");
		return secretKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
