package game.launcher;

import game.launcher.controller.AdminToolController;
import javafx.application.Application;
import javafx.stage.Stage;

public class AdminToolMain extends Application {

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage arg0) throws Exception {
		LaunchConfig config = LaunchConfig.readConfig();
		AdminToolController controller = new AdminToolController(config, getParameters().getRaw());		
		controller.startup();
	}
}
