package game.launcher;

import game.launcher.controller.LauncherMainController;
import game.launcher.view.LauncherWindow;
import game.main.Game;
import javafx.application.Application;
import javafx.stage.Stage;

public class LauncherMain extends Application {
	
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage arg0) throws Exception {
		LaunchConfig config = LaunchConfig.readConfig();
		LauncherMainController controller = new LauncherMainController(config);
		
		controller.startup();
	}

}
