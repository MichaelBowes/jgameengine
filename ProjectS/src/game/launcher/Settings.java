package game.launcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import engine.display.Resolution;
import javolution.util.FastTable;

public class Settings {

	public static final String FILE_NAME = "config.ini";
	public static final String STANDARD_IP = "127.0.0.1";
	public static final int STANDARD_PORT = 7778;
	private boolean savedLoginData;
	private String loginServerIp;
	private int loginServerPort;
	private Resolution resolution;
	private int frameRate;

	private Settings() {
	};

	public Settings(boolean saveLoginData, String serverIp, int serverPort, Resolution resolution, int frameRate) {

		this.savedLoginData = saveLoginData;
		this.loginServerIp = serverIp;
		this.loginServerPort = serverPort;
		this.resolution = resolution;
		this.frameRate = frameRate;
	}

	public static Settings readSettings() {
		File file = new File(FILE_NAME);
		List<String> lines = new FastTable<String>();
		if (file.exists()) {
			try (FileReader fileReader = new FileReader(file)) {
				try (BufferedReader reader = new BufferedReader(fileReader)) {
					String currentLine;
					while ((currentLine = reader.readLine()) != null) {
						if (!currentLine.startsWith("//")) {
							lines.add(currentLine);
						}
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			return null;
		}
		Settings settings = new Settings();
		for (String line : lines) {
			boolean skipLine = false;
			String[] lineArguments = line.split(":");
			if (lineArguments.length != 2) {
				skipLine = true;
			}
			if (!skipLine) {
				switch (lineArguments[0].toLowerCase()) {
				case "resolution":
					Resolution resolution;
					try {
						resolution = Resolution.valueOf(lineArguments[1]);
					} catch (IllegalArgumentException e) {
						break;
					}
					;
					settings.setResolution(resolution);
					break;

				case "framerate":
					int frameRate;
					try {
						frameRate = Integer.parseInt(lineArguments[1]);
					} catch (NumberFormatException e) {
						break;
					}
					settings.setFrameRate(frameRate);
					break;

				case "savelogin":
					boolean saveLoginData = Boolean.parseBoolean(lineArguments[1]);
					settings.setSaveLoginData(saveLoginData);
					break;

				case "loginserverip":
					settings.loginServerIp = lineArguments[1];
					break;

				case "loginserverport":
					int port;
					try {
						port = Integer.parseInt(lineArguments[1]);
					} catch (NumberFormatException e) {
						break;
					}
					settings.setLoginServerPort(port);
					break;
				}
			}
		}
		return settings;
	}

	public void writeSettings() {
		try(PrintWriter writer = new PrintWriter(FILE_NAME, "UTF-8")){
				writer.println("resolution"+ ":" + resolution.toString());				
				writer.println("framerate"+ ":" + frameRate);		
				writer.println("savelogin"+ ":" + savedLoginData);			
				writer.println("loginserverip"+ ":" + loginServerIp);			
				writer.println("loginserverport"+ ":" + loginServerPort);
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Settings getDefaultSettings() {
		Settings settings = new Settings(false, STANDARD_IP, STANDARD_PORT, Resolution.HD, 60);
		return settings;
	}

	public String getLoginServerIp() {
		return loginServerIp;
	}

	public void setLoginServerIp(String loginServerIp) {
		this.loginServerIp = loginServerIp;
	}

	public int getLoginServerPort() {
		return loginServerPort;
	}

	public void setLoginServerPort(int loginServerPort) {
		this.loginServerPort = loginServerPort;
	}

	public Resolution getResolution() {
		return resolution;
	}

	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}

	public int getFrameRate() {
		return frameRate;
	}

	public void setFrameRate(int frameRate) {
		this.frameRate = frameRate;
	}

	public void setSaveLoginData(boolean saveLoginData) {
		this.savedLoginData = saveLoginData;
	}

	public boolean getSaveLoginData() {
		return savedLoginData;
	}
}
