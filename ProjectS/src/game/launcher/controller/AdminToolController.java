package game.launcher.controller;

import java.util.List;

import game.client.AdminClient;
import game.launcher.LaunchConfig;
import game.launcher.view.AdminToolWindow;
import server.model.Player;
import server.transferdata.PlayerRestriction;

public class AdminToolController extends LauncherMainController {

	private CommandParser parser;
	private String ip;
	private int port;
	
	public AdminToolController(LaunchConfig config, List<String> args) {
		super(config);
		if(args.size() > 0) {
			ip = args.get(0);
			if(args.size() > 1) {
				port = Integer.parseInt(args.get(1));
			}			
		}
	}
	
	@Override
	public void startup() {
		client = new AdminClient();
		if(ip != null) {
			((AdminClient)client).setServerIp(ip);
		}
		if(port != 0) {
			((AdminClient)client).setServerPort(port);
		}
		window = new AdminToolWindow(this);
		client.addObserver(window);
		parser = new CommandParser(this);
		client.addObserver(window);
		window.setLoginDataSaved(config.getSettings().getSaveLoginData());
		if(config.getPlayerData() != null) {
			window.setUserData(
					config.getPlayerData().getUsername(),
					config.getPlayerData().getPassword());
		}
		window.showAndWait();
	}
	
	public void sendCommand(String command) {
		String[] response = parser.parseCommand(command);
		for(String line : response) {
			((AdminToolWindow)window).addCommandOutput(line);
		}	
	}
	
	public void sendChatMessage(String message) {
		if(message == null) {
			throw new IllegalArgumentException("Cant process null as message");
		}
		if(message.equals("")) {
			window.showAlert("Please enter a message");
			return;
		}
		client.sendObject(message);
	}
	
	public void kickPlayer(PlayerRestriction kick) {
		client.sendObject(kick);
	}
	
	public void banPlayer(PlayerRestriction ban) {
		client.sendObject(ban);
	}

	public void messagePlayer(Player player) {
		//TODO Ask with dialog for message
		System.out.println("PM " + player.getUsername());
	}
}
