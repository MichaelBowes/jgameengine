package game.launcher.controller;

import server.transferdata.PlayerRestriction;

public class CommandParser {

	private AdminToolController controller;
	
	public CommandParser(AdminToolController controller) {
		this.controller = controller;
	}
	
	/**
	 * Parses the given command, sends
	 * @param command
	 * @return response
	 */
	public String[] parseCommand(String command) {
		if(command == null) {
			throw new IllegalArgumentException("Given command mustn't be null");
		}
		
		String[] commandArguments = command.split(" ");
		switch(commandArguments[0].toLowerCase()) {
		case "help":
			return getAllCommandsText();
			
		case "ban":
			return createBan(commandArguments);
			
		case "kick":
			return createKick(commandArguments);
			
		default:
			return new String[] {""};
		}
	}
	
	private String[] getAllCommandsText() {
		return new String[] {
				"help - Gives information about all commands",
				"ban <Username> <Reason> <Duration> - Bans the user with the given username",
				"ban <Username> <Reason> - Bans the user with the given username permanent",
				"kick <Username> <Reason> - Kicks the user with the given username",
				"pm <Username> <Message> - Sends a message to the user with the given username"
		};
	}
	
	private String[] createBan(String[] arguments) {
		if(arguments.length < 3) {
			return new String[] {"Invalid number of arguments."};
		}
		String reason = "";
		for(int i = 2; i < arguments.length-1; i++) {
			reason += arguments[i] + " ";
		}
		PlayerRestriction ban = new PlayerRestriction(arguments[1] , reason, PlayerRestriction.BAN);
		int duration = 0;
		if(arguments.length > 3) {
			try {
				int durationPosition = arguments.length-1;
				duration = Integer.parseInt(arguments[durationPosition]);				
			}catch(NumberFormatException e) {
				return new String[] {"Invalid duration"};
			}
		}
		if(duration == 0) {
			duration = 1000000;
		}
		ban.setDuration(duration);
		controller.banPlayer(ban);
		return new String[] {"Ban for player " + arguments[1] + " sent to server"};
	}
	
	private String[] createKick(String[] arguments) {
		if(arguments.length < 3) {
			return new String[] {"Invalid number of arguments."};
		}
		String reason = "";
		for(int i = 2; i < arguments.length; i++) {
			reason += arguments[i] + " ";
		}
		PlayerRestriction kick = new PlayerRestriction(arguments[1], reason, PlayerRestriction.KICK);

		controller.kickPlayer(kick);
		return new String[] {"Kick for player " + arguments[1] + " sent to server"};
	}
	
}
