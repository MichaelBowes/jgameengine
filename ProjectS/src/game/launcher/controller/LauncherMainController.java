package game.launcher.controller;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import engine.display.Resolution;
import game.client.GameClient;
import game.launcher.LaunchConfig;
import game.launcher.view.LauncherWindow;
import game.launcher.view.SettingsWindow;
import game.main.Game;
import javafx.stage.Modality;

public class LauncherMainController {

	private final static int LOGIN_DELAY = 15; 
	protected LauncherWindow window;
	protected GameClient client;
	protected Game game;
	protected LaunchConfig config;
	private DateTime time;
	
	public LauncherMainController(LaunchConfig config) {
		this.config = config;
	}
	
	/**
	 * Checks if the time that elapsed since the method was last
	 * called is greater than the set delay.
	 * @return
	 */
	private boolean compareTime() {
		if(time == null) {
			time = new DateTime();
			return true;
		}else {
			DateTime current = new DateTime();
			Interval elapsed = new Interval(time, current);
			long seconds = elapsed.toDuration().getStandardSeconds();
			if(seconds < LOGIN_DELAY) {
				long waitTime = LOGIN_DELAY - seconds;
				window.showAlert("You have to wait for " + waitTime + " seconds for the next login");
				return false;
			}else {
				time = current;
				return true;
			}
		}
	}
	
	public void startup() {
		client = new GameClient();
		window = new LauncherWindow(this, 800, 500, "Launcher");
		client.addObserver(window);
		window.setLoginDataSaved(config.getSettings().getSaveLoginData());
		if(config.getPlayerData() != null) {
			window.setUserData(
					config.getPlayerData().getUsername(),
					config.getPlayerData().getPassword());
		}
		window.showAndWait();
	}
	
	public void saveSettingsData() {
		config.getSettings().writeSettings();
	}
	
	private void savePlayerData(String username, String password) {
		config.getPlayerData().setUsername(username);
		config.getPlayerData().setPassword(password);
		config.getPlayerData().writePlayerData();
	}
	
	public void login(String username, String password, boolean saveUserdata) {
		//Check if user data was entered
		if(username.isEmpty() && password.isEmpty()) {
			window.showAlert("Please enter a username and password");
			return;
		}if(username.isEmpty()) {
			window.showAlert("Please enter a username");
			return;
		}if(password.isEmpty()) {
			window.showAlert("Please enter a password");
			return;
		}
		if(saveUserdata) {
			savePlayerData(username, password);			
		}
		//Save config data.
		config.getSettings().setSaveLoginData(window.getLoginDataSaved());
		config.saveSettings();
		
		//Connect to login server.
		if(compareTime()) {
			window.showProgressBar("Connecting to login server");
			client.connect(username, password, config.getSettings().getLoginServerIp(),
					config.getSettings().getLoginServerPort(), client);			
		}	
	}
	
	/**
	 * Saves the settings and writes them to the config file.
	 * @param resolution
	 * @param frameRate
	 */
	public void applySettings(Resolution resolution, int frameRate) {
		config.getSettings().setResolution(resolution);
		config.getSettings().setFrameRate(frameRate);
		config.getSettings().writeSettings();
	}
	
	public void logOut() {
		client.disconnect();
	}
	
	/**
	 * Starts the game and closes the launcher.
	 */
	public void startGame() {
		client.deleteObserver(window);
		window.close();
		game = new Game(
				config.getSettings().getResolution(),
				config.getSettings().getFrameRate(),
				client);
		game.start();
	}
	
	public void disconnect() {
		client.disconnect();
	}
	
	/**
	 * Opens the settings window.
	 */
	public void openSettingsWindow() {
		SettingsWindow settingsWindow = new SettingsWindow(this, config.getSettings());
		settingsWindow.initOwner(window);
		settingsWindow.initModality(Modality.WINDOW_MODAL);
		settingsWindow.showAndWait();
	}
	
}
