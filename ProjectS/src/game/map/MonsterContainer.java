package game.map;

/**
 * Contains all information necessary to spawn a monster.
 * Used for the spawn table of each map to determine which monster to spawn
 * with which parameters.
 *
 */
public class MonsterContainer {

	private int monsterId;
	private int spawnChance;
	private int level;
	
	public MonsterContainer(int monsterId, int level, int spawnChance){
		this.monsterId = monsterId;
		this.level = level;
		this.spawnChance = spawnChance;
	}
	
	public int getMonsterId() {
		return monsterId;
	}
	public void setMonsterId(int monsterId) {
		this.monsterId = monsterId;
	}
	public int getSpawnChance() {
		return spawnChance;
	}
	public void setSpawnChance(int spawnChance) {
		this.spawnChance = spawnChance;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	
	
}
