package game.map;

/**
 * maps floats to Strings of image Paths
 */
public class floatToPath {

	public float getFloat(String s){
		float f=0;
		
		switch(s){
		case "Fir.png":
			f=1;
		}
		
		return f;
	}
	
	public String getString(float f){
		String s=null;
		int i = (int)f;
		
		switch(i){
		case 1:
			s = "Fir.png";
		}
		return s;
	}
}
