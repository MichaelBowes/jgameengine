package game.map;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

public class TileLayer extends GameObject {
	
	/**
	 * id representing the sprite to display.
	 */
	int id;
	
	public TileLayer(int id) {
		super();
		this.id=id;
	}
	
	public TileLayer(Sprite sprite) {
		super();
		setSprite(sprite);
	}
	
	public TileLayer(int id, Sprite sprite, Vector3f position) {
		super(position);
		setSprite(sprite);
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	public void setID(int id){
		this.id = id;
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
}
