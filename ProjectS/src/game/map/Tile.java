package game.map;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * Tiles used for the MapEditor
 * 
 */
public class Tile extends GameObject {

	/**
	 * Contains up to 8 tile IDs. One for each visual layer of the tile.
	 * The id determines the sprite from the tileset.
	 * The position in the array determines the Z-Coord in the scene.
	 * The position of its Tile class decides where in the map it appears.
	 */
	TileLayer[] tileLayers = new TileLayer[9];
	/**
	 * x-th tile in the map array representing the current map.
	 */
	int x;
	/**
	 * y-th tile in the map array representing the current map.
	 */
	int y;
	
	public Tile(){
	}
	
	public Tile(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	public Tile(Sprite sprite, Vector3f position){
		super(position);
		setSprite(sprite);
	}
	
	public int getTileID(int layer) {
		if(tileLayers[layer]!=null){
			return tileLayers[layer].getID();
		}
		return 0;
	}
	
	/**
	 * Sets tileLayer to given ID from 1 to X of the tileset.
	 */
	public void setTileID(int id, int layer) {
		if(tileLayers[layer]==null){
			tileLayers[layer] = new TileLayer(id);
		} else {
			tileLayers[layer].setID(id);
		}
	}
	
	public TileLayer getLayer(int index){
		return tileLayers[index];
	}
	
	public void addLayer(int index, TileLayer layerObject){
		tileLayers[index] = layerObject;
	}
	
	/**
	 * Sets tileLayer to 0, indicating no visual.
	 * @param index
	 */
	public void removeLayer(int index){
		tileLayers[index]=null;
	}

	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
