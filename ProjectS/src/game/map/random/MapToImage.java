package game.map.random;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 
 * Class that runs to create image from randomly generated map with {@link DiamondSquareAlgorithm3}
 * and with functions from {@link DiamondSquareRaw}.
 * Width & Height: optimum: 10000 x 10000 maximum: 18798 x 18798
 *
 * soliciting map sizes: Always 2^x+1, e.g. 5, 9, 17, 33, 65, 129, 257, 513,
 * 1025, 2049, 4097, 8193
 *
 */
public class MapToImage {

	static public void main(String args[]) throws IOException {
		
		
		//DiamondSquareRaw raw = new DiamondSquareRaw();
		//raw.generateMap(257, "map1");
		//int[][] map = raw.loadMap(System.getProperty("user.dir") + "/res/Maps", "map2.csv", 257);
		//raw.createPng(map, 8400, 8400, "map1.png");
		
		//int[][] mapEast = raw.generateMapExtensionEast(map, "map6");
		//int[][] mapEast = raw.loadMap(System.getProperty("user.dir") + "/res/Maps", "map2.csv", 65);
		//raw.createPng(mapEast, 8400, 8400, "map6.png");
		
		//int[][] mapSouth = raw.generateMapExtensionSouth(map, "map3");
		//int[][] mapSouth = raw.loadMap(System.getProperty("user.dir") + "/res/Maps", "map3.csv", 65);
		//raw.createPng(mapSouth, 3100, 3100, "map3.png");
		//int[][] mapSouth = raw.generateMapExtensionSouthEast(map, mapEast, "map3");
		//raw.createPng(mapSouth, 3100, 3100, "map3.png");
		
		//int[][] fused = raw.fuseVertical(map, mapEast, "map4");
		//raw.saveMap(fused, Paths.get("res/Maps/"+"fused"+".csv"));
		//raw.createPng(fused, 6000, 6000, "fused.png");
		
		//int[][] fused2 = raw.fuseHorizontal(map, mapEast, "map4");
		//raw.saveMap(fused2, Paths.get("res/Maps/"+"fused2"+".csv"));
		//raw.createPng(fused2, 6000, 6000, "fused2.png");
	}

	private static BufferedImage scaleImage(BufferedImage Img, int width, int height) {
		BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = resizedImg.createGraphics();

		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(Img, 0, 0, width, height, null);
		g2.dispose();

		return resizedImg;
	}

	/**
	 * Debugging method that prints the generated map to the console.
	 */
	private static void printMap(int[][] mapArray, int arraySize) // this prints the 2D array.
	{
		for (int i = 0; i < arraySize; i++) {
			for (int j = 0; j < arraySize; j++) {
				System.out.print(mapArray[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
