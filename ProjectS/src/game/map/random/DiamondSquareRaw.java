package game.map.random;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.imageio.ImageIO;

/**
 * 
 * Generates, loads, saves, fuses maps and 
 * creates images from maps created by the {@link DiamondSquareAlgorithm3}
 * 
 */

public class DiamondSquareRaw {

	int mapSize; // size of the map
	int mapCount = 0;
	int[][] map;

	/**
	 * Default constructor. Does not generate anything.
	 */
	public DiamondSquareRaw() {
	}

	/**
	 * Constructor lets you choose the size of the map to be generated.
	 * 
	 * @param size
	 *            The square dimension for the map, e.g. 4 generates a map of size
	 *            (2^4+1)*(2^4+1)<br>
	 *            size needs to always take a value of 2^x + 1.
	 */
	public DiamondSquareRaw(int size) {
		this.map = generateMap(size, "map");
	}

	/**
	 * 
	 * Generates a single square map with the given size and name.
	 * 
	 * @param size
	 *            The square dimension for the map, e.g. 4 generates a map of size
	 *            4*4<br>
	 *            size needs to always take a value of 2^x + 1.
	 * @return
	 */
	public int[][] generateMap(int size, String name) {
		// TODO: Durchlaufe das 2D Array und ändere alle Zahlen
		// im Bereich des Grass-Gebiets(von X bis Y) ab.

		// Creates a raw map without drawing it.
		DiamondSquareAlgorithm3 algo = new DiamondSquareAlgorithm3(size);
        Thread t = new Thread(algo);
        t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		map = algo.getMap();
		saveMap(map, Paths.get("res/Maps/"+name+".csv"));
		mapCount+=1;
		/*
		 * System.out.println("generatedMap: "); // Prints the map in the console for
		 * (int i = 0; i < map.length; i++) { for (int j = 0; j < map[i].length; j++) {
		 * if (j == map[i].length - 1) { System.out.println(" [" + map[i][j] + "] "); }
		 * else { System.out.print(" [" + map[i][j] + "] "); } } }
		 */
		return map;
	}
	
	/**
	 * Generates the extension of a map towards the East
	 * and saves the map.
	 * @return
	 */
	public int[][] generateMapExtensionEast(int[][] leftMap, String name){
		DiamondSquareAlgorithm3 algo = new DiamondSquareAlgorithm3(leftMap, 0);
		saveMap(algo.getMap(), Paths.get("res/Maps/"+name+".csv"));
		mapCount+=1;
		return algo.getMap();
	}
	/**
	 * Generates the extension of a map towards the South
	 * and saves the map.
	 * @return
	 */
	public int[][] generateMapExtensionSouth(int[][] northMap, String name){
		DiamondSquareAlgorithm3 algo = new DiamondSquareAlgorithm3(northMap, 1);
		saveMap(algo.getMap(), Paths.get("res/Maps/"+name+".csv"));
		mapCount+=1;
		return algo.getMap();
	}
	/**
	 * Generates the extension of a map towards the South and East
	 * and saves the map.
	 * @return
	 */
	public int[][] generateMapExtensionSouthEast(int[][] northMap, int[][] westMap, String name){
		DiamondSquareAlgorithm3 algo = new DiamondSquareAlgorithm3(northMap, westMap);
		saveMap(algo.getMap(), Paths.get("res/Maps/"+name+".csv"));
		mapCount+=1;
		return algo.getMap();
	}
	
	/**
	 * 
	 * @param northMap
	 * @param southMap
	 * @param name
	 * @return
	 */
	public int[][] fuseVertical(int[][] northMap, int[][] southMap, String name){
		int fuseLength = northMap.length + southMap.length;
		int[][] fuseMap = new int[fuseLength][northMap.length];
		
		for(int i=0;i<fuseMap[0].length;i++) {
			for(int j=0;j<fuseMap.length/2;j++) {
				fuseMap[j][i] = northMap[j][i];
			}

		}
		
		int southWidth=0;
		for(int i=0;i<fuseMap[0].length;i++) {
			int southLength=0;
			for(int j=fuseMap.length/2;j<fuseMap.length;j++) {
				fuseMap[j][i] = southMap[southWidth][southLength];
				southLength++;
			}
			southWidth++;
		}
		
		System.out.println(fuseMap[0].length);
		System.out.println(fuseMap.length);
		for (int i = 0; i < fuseMap[0].length; i++) {
			for (int j = 0; j < fuseMap.length; j++) {
				System.out.print(fuseMap[j][i] + "\t");
			}
			System.out.println("");
		}

		return fuseMap;
	}
	
	/**
	 * Fuses 2 maps horizontally.
	 *
	 * @param eastMap
	 * @param westMap
	 * @param name
	 * @return
	 */
	public int[][] fuseHorizontal(int[][] eastMap, int[][] westMap, String name){
		int fuseLength = eastMap.length + westMap.length;
		int[][] fuseMap = new int[eastMap.length][fuseLength];
		
		System.out.println(fuseMap[0].length);
		System.out.println(fuseMap.length);
		for(int i=0;i<fuseMap[0].length/2;i++) {
			for(int j=0;j<fuseMap.length;j++) {
				fuseMap[j][i] = eastMap[j][i];
			}

		}
		
		int southWidth=0;
		for(int i=fuseMap[0].length/2;i<fuseMap[0].length;i++) {
			int southLength=0;
			for(int j=0;j<fuseMap.length;j++) {
				fuseMap[j][i] = westMap[southWidth][southLength];
				southLength++;
			}
			southWidth++;
		}
		
		System.out.println(fuseMap[0].length);
		System.out.println(fuseMap.length);
		for (int i = 0; i < fuseMap[0].length; i++) {
			for (int j = 0; j < fuseMap.length; j++) {
				System.out.print(fuseMap[j][i] + "\t");
			}
			System.out.println("");
		}
		
		return fuseMap;
	}

	/**
	 * Creates a .png file of the generated map.
	 * Requires a map to be generated with generateMap() beforehand. 
	 * Each map tile is 20*20 in size in the picture.
	 * If the generated map is bigger than the picture, the edges will be cut off in the image.
	 * 
	 * @param map the 2D integer array representing the map.
	 * @param width Width of the picture. Optimum value: 10.000; Maximum value: 18798
	 * @param height Height of the picture. Optimum value: 10.000; Maximum value: 18798
	 */
	public void createPng(int[][] map, int width, int height, String name) {

		try {
			// TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
			// into integer pixels
			BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			BufferedImage snow = null;
			BufferedImage green = null;
			BufferedImage darkgreen = null;
			BufferedImage w = null;
			BufferedImage dw = null;
			BufferedImage mountain = null;
			BufferedImage beach = null;

			snow = ImageIO.read(new File("res/pictures/snow.jpg"));
			green = ImageIO.read(new File("res/pictures/green.jpg"));
			darkgreen = ImageIO.read(new File("res/pictures/darkgreen.jpg"));
			w = ImageIO.read(new File("res/pictures/water.jpg"));
			dw = ImageIO.read(new File("res/pictures/deepWater.jpg"));
			mountain = ImageIO.read(new File("res/pictures/mountain.jpg"));
			beach = ImageIO.read(new File("res/pictures/beach.jpg"));

			Graphics2D ig2 = bi.createGraphics();

			//map[0].length necessary for non-square maps.
			int mapHeight=0;
			int mapWidth=0;
			if(map[0].length > map.length) {
				System.out.println("l1");
				mapHeight = map.length;
				mapWidth = map[0].length;
			} else if (map[0].length < map.length) {
				System.out.println("l2");
				mapHeight = map.length;
				mapWidth = map[0].length;
			} else {
				System.out.println("l3");
				mapHeight = map.length;
				mapWidth = map.length;
			}
			System.out.println("picL: "+mapHeight);
			System.out.println("picW: "+mapWidth);
			for (int i = 0; i < mapWidth; i++) {
				for (int j = 0; j < mapHeight; j++) {
					if (map[j][i] < -5) // 15% chance to turn BLUE = deep water
					{
						ig2.drawImage(dw, i * 32, j * 32, null);
					}

					else if (map[j][i] < 10) // 10% chance for shallow water
					{
						ig2.drawImage(w, i * 32, j * 32, null);
					}

					else if (map[j][i] < 15) // 15% chance for beach
					{
						ig2.drawImage(beach, i * 32, j * 32, null);
					}

					else if (map[j][i] < 25) // 15% chance for grass
					{
						ig2.drawImage(green, i * 32, j * 32, null);
					}

					else if (map[j][i] < 40) // 20% chance for forest
					{
						ig2.drawImage(darkgreen, i * 32, j * 32, null);
					}

					else if (map[j][i] < 60) // 20% chance for mountain
					{
						ig2.drawImage(mountain, i * 32, j * 32, null);
					}

					else // 20% chance for snow
					{
						ig2.drawImage(snow, i * 32, j * 32, null);
					}
				}
			}
			
			ig2.dispose();

			ImageIO.write(bi, "PNG", new File(System.getProperty("user.home")+"/Desktop" +File.separator+name));
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	/**
	 * 
	 * Takes a generated map and saves it in csv format.
	 * 
	 * @param map
	 *            The 2D map to save.
	 */
	public void saveMap(int[][] map, Path mapPath) {

		try (BufferedWriter writer = Files.newBufferedWriter(mapPath, Charset.forName("UTF-8"))) {
			for (int i = 0; i < map.length; i++) {
				for (int j = 0; j < map[i].length; j++) {
					if (j == map[i].length - 1) {
						writer.write(map[i][j] + "," + System.getProperty("line.separator"));
					} else {
						writer.write(map[i][j] + ",");
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Could not save map file");
		}

	}

	/**
	 * 
	 * Loads the world map from within a txt file into a 2d int Array.
	 * 
	 * @param loadPath
	 * @return
	 * @throws IOException
	 */
	public int[][] loadMap(String loadPath, String filename, int mapSize) throws IOException {

		int[][] map = new int[mapSize][mapSize];
		Path folder = Paths.get(loadPath);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {
			for (Path path : stream) {
				if (path.getFileName().toString().endsWith(filename)) {
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
					int i=0;	
					for (String line : lines) {
							String[] line2 = line.split(",");
							for (int j = 0; j < line2.length; j++) {
								map[i][j] = Integer.parseInt(line2[j]);
							}
							i++;
						}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	private void printMap(int[][] map) // this prints the 2D array.
	{
		for (int i = 0; i < map.length + 1; i++) {
			for (int j = 0; j < map.length + 1; j++) {
				System.out.print(map[j][i] + "\t");
			}
			System.out.println("");
		}
	}
	
}