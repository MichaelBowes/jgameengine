package game.map;

import engine.math.Vector3f;
import engine.textures.Sprite;
import engine.textures.manager.TextureManager;

public class ObjectFactory {

	private Sprite sprite;

	public ObjectFactory() {
	}

	public WorldObject createItem(int id, Vector3f position) {
		WorldObject wo = null;
		switch (id) {
		case 0:
			sprite = TextureManager.getSprite("res/pictures/Fir.png", 0, 0);
			wo = new WorldObject(sprite, position);
			wo.setYScaling(true);
			wo.changeGroundOffset(20f);
			wo.addHitbox();
			return wo;
		case 1:
			sprite = TextureManager.getSprite("res/pictures/block.png", 0, 0);
			wo = new WorldObject(sprite, position);
			wo.changeGroundOffset(0f);
			wo.addHitbox();
			return wo;
		case 2:
		case 3:
		}

		return wo;
	}
}
