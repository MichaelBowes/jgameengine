package game.map;


import engine.Engine;
import engine.GameLogic;
import engine.display.Resolution;
import engine.input.Input;
import engine.input.KeyInput;
import engine.input.KeyInputListener;
import engine.input.MouseInput;
import engine.input.MouseInputListener;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;
import engine.textures.manager.TextureManager;
import game.logic.GOManager;
import game.graphics.ui.MapEditorButton;
import game.logic.MapManager;
import game.logic.UIManager;

/**
 * Allows for creation and saving of tile-based maps with a visual editor.<br>
 * <strong>Controls:</strong><br>
 * - Numbers 1 to 9 for layer selection.<br>
 * - Arrow keys for movement of pointer to place tiles.<br>
 * - LMB clicks on the tileset to the left places the tile at the pointer on the
 * map.<br>
 * - Space key for hiding of the interface. - Enter saves the map as csv with
 * {@link MapManager}
 */
public class MapEditor implements GameLogic, MouseInputListener, KeyInputListener {

	Tile[][] map = new Tile[50][50];
	/**
	 * Position of the pivot element of the map editor. Tiles are placed the
	 * position of the pivot.
	 */
	private int pivotX = 0;
	private int pivotY = 0;
	private int selectedLayer = 0;
	private Engine engine;

	public MapEditor() {
		engine = new Engine(Resolution.HD, this, 60);
		engine.start();
	}

	public static void main(String[] args) {
		new MapEditor();
	}

	@Override
	public void update() {
		moveCamera();
	}

	@Override
	public void init() {
		new UIManager();
		new GOManager();
		MouseInput.addListener(this);
		KeyInput.addListener(this);
		engine.setFrameSyncRate(30);
		// specify name of tileset here
		UIManager.loadMapEditor("t1.png");
		GOManager.loadPlayer();
		GOManager.renderGameElements();
		Sprite s0 = TextureManager.getSprite("res/pictures/water.jpg", 0, 0);
		for (int i = 0; i < 50; i++) {
			Tile block = new Tile(s0, new Vector3f(0 + 16f * i, 32f, 0));
			block.addHitbox();
			Engine.getSceneManager().addGameObject(block);
			Tile block2 = new Tile(s0, new Vector3f(-32f, 0 - 16f * i, 0));
			block2.addHitbox();
			Engine.getSceneManager().addGameObject(block2);
			Tile block3 = new Tile(s0, new Vector3f(0 + 16f * i, -816f, 0));
			block3.addHitbox();
			Engine.getSceneManager().addGameObject(block3);
			Tile block4 = new Tile(s0, new Vector3f(816f, 0 - 16f * i, 0));
			block4.addHitbox();
			Engine.getSceneManager().addGameObject(block4);
		}

	}

	private void moveCamera() {
		/*
		 * Change the camera with WASD
		 */
		if (Input.keys[Input.KEY_W]) {
			Engine.getCamera().changePosition(0.0f, 0.1f, 0f);
		}
		if (Input.keys[Input.KEY_S]) {
			Engine.getCamera().changePosition(0.0f, -0.1f, 0f);
		}
		if (Input.keys[Input.KEY_A]) {
			Engine.getCamera().changePosition(-0.1f, 0.0f, 0f);
		}
		if (Input.keys[Input.KEY_D]) {
			Engine.getCamera().changePosition(0.1f, 0.0f, 0f);
		}
		if (Input.keys[Input.KEY_KP_ADD]) {
			Engine.getCamera().changePosition(0.0f, 0.0f, -0.01f);
		}
		if (Input.keys[Input.KEY_KP_SUBTRACT]) {
			Engine.getCamera().changePosition(0.0f, 0.0f, 0.01f);
		}
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMouseDown(int button) {
		for (GameObject go : Engine.getSceneManager().getUIScene().getObjects()) {
			if (go.hasHitbox()) {
				if (Input.keys[340] == true) {
					if (go.getHitbox().isInside(MouseInput.getMousePosition())) {
						int id = ((MapEditorButton) go).getID();
						fillAllMapTiles(id, ((MapEditorButton) go).getSprite());
					}
				} else if (go.getHitbox().isInside(MouseInput.getMousePosition())) {
					int id = ((MapEditorButton) go).getID();
					createMapTile(id, ((MapEditorButton) go).getSprite());
					System.out.println(id);
				}
			}
		}
		if (button == MouseInput.RIGHT_MOUSE_BUTTON) {
			if (map[pivotX][pivotY] != null && (map[pivotX][pivotY].getTileID(selectedLayer) != 0)) {
				Engine.getSceneManager().removeGameObject(map[pivotX][pivotY].getLayer(selectedLayer));
				map[pivotX][pivotY].removeLayer(selectedLayer);
			}
		}
	}

	private void fillAllMapTiles(int id, Sprite sprite) {
		float height = -20;
		switch (selectedLayer) {
		case 0:
			height = -20;
			break;
		case 1:
			height = -19.9999f;
			break;
		case 2:
			height = -19.9998f;
			break;
		case 3:
			height = -19.9997f;
			break;
		case 4:
			height = 0.0001f;
			break;
		case 5:
			height = 0.0002f;
			break;
		case 6:
			height = 0.0003f;
			break;
		case 7:
			height = 0.0004f;
			break;
		case 8:
			height = 0.0005f;
			break;
		}

		for (int i = 0; i < 50; i++) {
			for (int j = 0; j < 50; j++) {
				// remove visual of tiles on same layer and location
				if (map[i][j] != null && (map[i][j].getTileID(selectedLayer) != 0)) {
					Engine.getSceneManager().removeGameObject(map[i][j].getLayer(selectedLayer));
					map[i][j].removeLayer(selectedLayer);
				}
				System.out.println(height);
				// create tile only if not already one exists.
				if (map[i][j] == null) {
					map[i][j] = new Tile();
				}
				// create visual in the map
				TileLayer tileLayer = new TileLayer(id, sprite,
						new Vector3f(0+i*16, 0-j*16, height));
				Engine.getSceneManager().addGameObject(tileLayer);
				// add Tile to array
				map[i][j].addLayer(selectedLayer, tileLayer);
			}
		}
	}

	private void createMapTile(int id, Sprite sprite) {
		float height = -20;
		switch (selectedLayer) {
		case 0:
			height = -20;
			break;
		case 1:
			height = -19.9999f;
			break;
		case 2:
			height = -19.9998f;
			break;
		case 3:
			height = -19.9997f;
			break;
		case 4:
			height = 0.0001f;
			break;
		case 5:
			height = 0.0002f;
			break;
		case 6:
			height = 0.0003f;
			break;
		case 7:
			height = 0.0004f;
			break;
		case 8:
			height = 0.0005f;
			break;
		}

		// remove visual of tiles on same layer and location
		if (map[pivotX][pivotY] != null && (map[pivotX][pivotY].getTileID(selectedLayer) != 0)) {
			Engine.getSceneManager().removeGameObject(map[pivotX][pivotY].getLayer(selectedLayer));
			map[pivotX][pivotY].removeLayer(selectedLayer);
		}
		System.out.println(height);
		// create tile only if not already one exists.
		if (map[pivotX][pivotY] == null) {
			map[pivotX][pivotY] = new Tile();
		}
		// create visual in the map
		TileLayer tileLayer = new TileLayer(id, sprite,
				new Vector3f(GOManager.player.getPosition().x, GOManager.player.getPosition().y, height));
		Engine.getSceneManager().addGameObject(tileLayer);
		// add Tile to array
		map[pivotX][pivotY].addLayer(selectedLayer, tileLayer);
	}

	@Override
	public void onMouseDoubleClick(int button) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMouseReleased(int button) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onKeyDown(int key) {
		GOManager.player
				.setPrevPos(new Vector3f(GOManager.player.getPosition().x, GOManager.player.getPosition().y, 0));

		if (Input.keys[Input.KEY_ENTER]) {
			MapManager.saveGroundMap("map1", map);
		}
		if (Input.keys[Input.KEY_SPACE]) {
			if (UIManager.UIvisible == true) {
				UIManager.hideUI();
			} else {
				UIManager.showUI();
			}
		}

		if (key == Input.KEY_1) {
			selectedLayer = 0;
		}
		if (key == Input.KEY_2) {
			selectedLayer = 1;
		}
		if (key == Input.KEY_3) {
			selectedLayer = 2;
		}
		if (key == Input.KEY_4) {
			selectedLayer = 3;
		}
		if (key == Input.KEY_5) {
			selectedLayer = 4;
		}
		if (key == Input.KEY_LEFT) {
			GOManager.player.changePositionIgnoreDelta(-16f, 0f);
			Engine.getCamera().changePosition(-0.4f, 0);
			pivotX -= 1;
		}
		if (key == Input.KEY_RIGHT) {
			GOManager.player.changePositionIgnoreDelta(16f, 0f);
			Engine.getCamera().changePosition(0.4f, 0f, 0f);
			pivotX += 1;
		}
		if (key == Input.KEY_UP) {
			GOManager.player.changePositionIgnoreDelta(0f, 16f);
			Engine.getCamera().changePosition(0.0f, 0.4f, 0f);
			pivotY -= 1;
		}
		if (key == Input.KEY_DOWN) {
			GOManager.player.changePositionIgnoreDelta(0f, -16f);
			Engine.getCamera().changePosition(0.0f, -0.4f, 0f);
			pivotY += 1;
		}
		System.out.println(GOManager.player.getPosition().x + " , " + GOManager.player.getPosition().y);
	}

	@Override
	public void onKeyReleased(int key) {
		// TODO Auto-generated method stub

	}

}
