package game.map;

/**
* Decides spawn behavior of the map.
* NORMAL, DISABLE_SPAWN, DOUBLE_RARE_SPAWNS
*/
public enum spawnMode {
	/**
	* Default. Has standard spawns enabled.
	*/
	NORMAL,
	/**
	* Disables spawntiles to trigger for this map.
	*/
	DISABLE_SPAWN,
	/**
	* All rare monsters spawnrates are doubled.
	*/
	DOUBLE_RARE_SPAWNS
}
