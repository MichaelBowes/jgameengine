package game.map;

/**
 * Determines behavior of pvp requests on the map.
 * ENABLED, DISABLED, WAR
 */

public enum pvpMode {
	/**
	* Sending & accepting duel requests is enabled.
	*/
	ENABLED,
	/**
	* Sending & accepting duel requests is disabled.
	*/
	DISABLED,
	/**
	* Sending duel requests will start combat automatically.
	*/
	WAR
}
