package game.map;

import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * Object that is on the surface of a map, such as a tree, a house etc.
 */
public class WorldObject extends GameObject {

	
	public WorldObject(Sprite sprite, Vector3f position){
		super(position);
		setSprite(sprite);
	}
	
	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}

}
