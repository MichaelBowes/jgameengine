package game.map;

import java.util.LinkedList;
import java.util.List;
import engine.math.Vector3f;
import engine.objects.GameObject;
import engine.textures.Sprite;

/**
 * Representation of an in-game map.
 */
public class Map extends GameObject {

	public Map(Sprite sprite, Vector3f position, MonsterContainer[] spawnTable, spawnMode mode1) {
		super(position);
		setSprite(sprite);
		this.spawnTable = spawnTable;
		this.mode1 = mode1;
	}
	
	private String name;
	private int width, height;
	private Tile[][] map = new Tile[width][height];
	private MonsterContainer[] spawnTable;
	/**
	* Map changes spawning behavior based on the mode it is in.
	* Meant to be changeable by admin & mods for events etc.
	*/
	private spawnMode mode1;
	/**
	 * Determines how pvp requests are handled on the map.
	 */
	private pvpMode mode2;
	
	public Map() {}
	
	public Map(String name, Tile[][] map, MonsterContainer[] spawnTable, 
			spawnMode mode1, pvpMode mode2) {
		this.name = name;
		this.map = map;
		this.spawnTable = spawnTable;
		this.mode1 = mode1;
		this.mode2 = mode2;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Tile[][] getMap() {
		return map;
	}
	public void setMap(Tile[][] map) {
		this.map = map;
	}
	
	public spawnMode getMode1() {
		return mode1;
	}
	public void setMode1(spawnMode mode1) {
		this.mode1 = mode1;
	}
	public pvpMode getMode2() {
		return mode2;
	}
	public void setMode2(pvpMode mode2) {
		this.mode2 = mode2;
	}
	
	/*
	Terrain GetAt(int x, int y, int type){
        return map[y][x].layers.get(type);
    }
    */
    
    Tile GetAt(int x, int y){
        return map[y][x];
    }
    
    private List<Tile> GetNeighbors(int x, int y){
        List<Tile> neighbors = new LinkedList<Tile>();
        if(isValid(x + 1, y))
            neighbors.add(GetAt(x + 1, y));
        if(isValid(x, y - 1))
            neighbors.add(GetAt(x, y - 1));
        if(isValid(x - 1, y))
            neighbors.add(GetAt(x - 1, y));
        if(isValid(x, y + 1))
            neighbors.add(GetAt(x, y + 1));
        
        return neighbors;
        
    }
    
    private boolean isValid(int x, int y){
        if(x < 0 || x > width - 1 || y < 0 || y > height -1)
            return false;
        return true;
    }
	@Override
	public void onCollision(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onCollisionExit(GameObject collider) {
		// TODO Auto-generated method stub
		
	}
	public MonsterContainer[] getSpawnTable() {
		return spawnTable;
	}
	public void setSpawnTable(MonsterContainer[] spawnTable) {
		this.spawnTable = spawnTable;
	}

}
