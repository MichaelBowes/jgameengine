package game.map;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import game.logic.MapManager;

/**
 * Converts Integer Arrays into a picture form.
 * Mainly 3 layers of the ground of a map are to be converted to pictures.
 * 
 */
public class MapDrawer {

	private static HashMap<Integer, BufferedImage> Tileset = new HashMap<Integer, BufferedImage>();

	/**
	 * Selects a tileset to use for drawing the map.
	 * 
	 * @param picture
	 *            e.g. "res/pictures/tileset.png"
	 */
	private static void selectTileset(String picture) {
		BufferedImage bigImg;
		try {
			bigImg = ImageIO.read(new File(picture));

		final int tileWidth = 16;
		final int tileHeight = 16;
		final int rows = 11;
		final int cols = 44;

		int count=0;
		for (int i = 0; i < rows; i++)
		{
		    for (int j = 0; j < cols; j++)
		    {
		    	Tileset.put(count, 
		    			bigImg.getSubimage(
		    		            j * tileWidth,
		    		            i * tileHeight,
		    		            tileWidth,
		    		            tileHeight)
		        );
		    	count++;
		    }
		}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static public void main(String args[]) throws IOException {
		selectTileset("res/pictures/t1.png");
		MapManager.loadMapForPrint(System.getProperty("user.dir") + "/res/Maps", "map1.csv", 50);
		int[][] l1 = MapManager.layer1;
		int[][] l2 = MapManager.layer2;
		int[][] l3 = MapManager.layer3;
		int[][] l4 = MapManager.layer4;
		int[][] l5 = MapManager.layer5;
		printMap(l1, 50);
		drawMap("map1.png", l1, l2, l3, l4, l5);
	}

	/**
	 * Draws a picture out of (several) int array(s) and the currently set tileset.
	 * 
	 * @param name
	 * @param l1 layer 1 to be drawn
	 */
	public static void drawMap(String name,
			int[][] l1, int[][] l2, int[][] l3, int[][] l4, int[][] l5) {
		try {
			BufferedImage mapPicture = new BufferedImage(16*l1.length, 16*l1.length, BufferedImage.TYPE_INT_ARGB);
			Graphics2D ig2 = mapPicture.createGraphics();

			for (int i = 0; i < l1.length; i++) {
				for (int j = 0; j < l1.length; j++) {
					int zahl = l1[i][j];
						ig2.drawImage(Tileset.get(zahl), i * 16, j * 16, null);
					}
			}
			for (int i = 0; i < l2.length; i++) {
				for (int j = 0; j < l2.length; j++) {
					int zahl = l2[i][j];
						ig2.drawImage(Tileset.get(zahl), i * 16, j * 16, null);
					}
			}
			for (int i = 0; i < l3.length; i++) {
				for (int j = 0; j < l3.length; j++) {
					int zahl = l3[i][j];
						ig2.drawImage(Tileset.get(zahl), i * 16, j * 16, null);
					}
			}
			for (int i = 0; i < l4.length; i++) {
				for (int j = 0; j < l4.length; j++) {
					int zahl = l4[i][j];
						ig2.drawImage(Tileset.get(zahl), i * 16, j * 16, null);
					}
			}
			for (int i = 0; i < l5.length; i++) {
				for (int j = 0; j < l5.length; j++) {
					int zahl = l5[i][j];
						ig2.drawImage(Tileset.get(zahl), i * 16, j * 16, null);
					}
			}
			
			ig2.dispose();

			ImageIO.write(mapPicture, "PNG",
					new File("res/Maps" + File.separator + name));
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	public static void printMap(int[][] mapArray, int arraySize)
	{
		for (int i = 0; i < arraySize; i++) {
			for (int j = 0; j < arraySize; j++) {
				System.out.print(mapArray[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
