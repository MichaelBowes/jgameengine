package game.client;

import java.io.IOException;

public class AdminClient extends GameClient {

	private String serverIp;
	private int serverPort = 0;
	
	public void setServerIp(String ip) {
		this.serverIp = ip;		
	}
	
	public void setServerPort(int port) {
		this.serverPort = port;
	}
	
	@Override
	protected boolean connectToGameServer(String ip, int port) throws IOException {
		String usedIp = null;
		int usedPort = port;
		
		if(serverPort != 0) {
			usedPort = serverPort;
		}
		if(serverIp != null) {
			usedIp = serverIp;
		}else {
			usedIp = ip;
		}
		return super.connectToGameServer(usedIp, usedPort);
	}
	
	@Override
	public void processData(Object object) {
		super.processData(object);
		this.setChanged();
		this.notifyObservers(object);			
	}
}
