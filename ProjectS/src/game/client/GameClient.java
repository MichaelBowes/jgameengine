package game.client;

import client.Client;
import engine.utils.Tuple;
import server.transferdata.SystemMessage;

public class GameClient extends Client {
	
	private boolean isLoggedIn;
	
	@Override
	public void reportError(SystemMessage message, Throwable cause) {
		this.setChanged();
		this.notifyObservers(message);
	}

	@Override
	public void loginErrorResult(SystemMessage message) {
		this.setChanged();
		this.notifyObservers(message);
	}

	@Override
	public void onInvalidLoginToken(SystemMessage message) {
		this.setChanged();
		this.notifyObservers(message);
	}

	@Override
	public void onGameServerLogin() {
		isLoggedIn = true;
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void processData(Object object) {
		// TODO Auto-generated method stub
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	@Override
	public void onLoninServerLogin() {
		this.setChanged();
		this.notifyObservers(new Tuple<String,Double>("Connecting to game server", 0.5d));		
	}

	@Override
	public void onDisconnect() {
			
	}

}
